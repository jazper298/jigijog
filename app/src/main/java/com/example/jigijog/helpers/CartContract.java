package com.example.jigijog.helpers;

import android.provider.BaseColumns;

import com.example.jigijog.models.Products;

import java.util.ArrayList;

public final class CartContract {

    private CartContract() {
    }
    public static class UserTable implements BaseColumns {
        public static final String TABLE_NAME = "cart_user";
        public static final String COLUMN_ID = "user_id";
        public static final String COLUMN_NAME = "name";
    }

    public static class CartTable implements BaseColumns {
        public static final String TABLE_NAME = "cart_table";
        public static final String COLUMN_PRODCUTID = "product_id";    //FK
        public static final String COLUMN_PRODUCTCOUNT = "product_count";
        public static final String COLUMN_USERID = "user_id";
        public static final String COLUMN_NAMES = "name";
        public static final String COLUMN_ADDEDBY = "added_by";
        public static final String COLUMN_USER_IDS = "user_ids";
        public static final String COLUMN_CATEGORY_ID = "category_id";
        public static final String COLUMN_SUBCATEGORY_ID = "subcategory_id";
        public static final String COLUMN_SUBSUBCATEGORY_ID = "subsubcategory_id";
        public static final String COLUMN_BRAND_ID = "brand_id";
        //public static final String COLUMN_PHOTOS = "photos";
        public static final String COLUMN_THUMBNAIL_IMAGE = "thumbnail_img";
        public static final String COLUMN_FEATURED_IMAGE = "featured_img";
        public static final String COLUMN_FLASH_DEAL_IMAGE = "flash_deal_img";
        public static final String COLUMN_VIDEO_PROVIDER = "video_provider";
        public static final String COLUMN_VIDEO_LINK = "video_link";
        public static final String COLUMN_TAGS = "tags";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_UNIT_PRICE = "unit_price";
        public static final String COLUMN_PURCHASE_PRICE = "purchase_price";
        //public static final String COLUMN_CHOICE_OPTION = "choice_options";
        //public static final String COLUMN_COLORS = "colors";
        //public static final String COLUMN_VARIATIONS = "variations";
        public static final String COLUMN_TODAYS_DEAL = "todays_deal";
        public static final String COLUMN_PUBLISHED= "published";
        public static final String COLUMN_FEATURED = "featured";
        public static final String COLUMN_CURRENT_STOCK = "current_stock";
        public static final String COLUMN_UNIT = "unit";
        public static final String COLUMN_DISCOUNT = "discount";
        public static final String COLUMN_DISCOUNT_TYPE = "discount_type";
        public static final String COLUMN_TAX = "tax";
        public static final String COLUMN_TAX_TYPE = "tax_type";
        public static final String COLUMN_SHIPPING_TYPE = "shipping_type";
        public static final String COLUMN_SHIPPING_COST = "shipping_cost";
        public static final String COLUMN_WEIGHT = "weight";
        public static final String COLUMN_PARCEL_SIZE = "parcel_size";
        public static final String COLUMN_NUM_OF_SALE = "num_of_sale";
        public static final String COLUMN_META_TITLE = "meta_title";
        public static final String COLUMN_META_DESCRIPTION = "meta_description";
        public static final String COLUMN_META_IMG = "meta_img";
        public static final String COLUMN_PDF = "pdf";
        public static final String COLUMN_SLUG = "slug";
        public static final String COLUMN_RATING = "rating";
        public static final String COLUMN_CREATED_AT= "created_at";
        public static final String COLUMN_UPDATE_AT = "updated_at";

    }
    public static class OrderDetailsTable implements BaseColumns{
        public static final String TABLE_NAME = "order_details_table";
        public static final String COLUMN_USERID = "user_id"; //FK
        public static final String COLUMN_SELLER_ID = "seller_id";
        public static final String COLUMN_PRODUCT_ID = "product_id"; //FK
        public static final String COLUMN_VARIATION = "variation";
        public static final String COLUMN_BASE_PRICE = "base_price";
        public static final String COLUMN_SELLING_PRICE = "selling_price";
        public static final String COLUMN_PRICE = "price";
        public static final String COLUMN_TAX = "tax";
        public static final String COLUMN_SHIPPING_COST = "shipping_cost";
        public static final String COLUMN_COUPON_DISCOUNT = "coupon_discount";
        public static final String COLUMN_QUANTITY = "quantity";
        public static final String COLUMN_PAYMENT_STATUS = "payment_status";
        public static final String COLUMN_DELIVERY_STATUS = "delivery_status";
        public static final String COLUMN_PICK_FROM = "pick_from";
        public static final String COLUMN_PICK_TO = "pick_to";
        public static final String COLUMN_CREATED_AT = "created_at";
        public static final String COLUMN_UPDATE_AT = "updated_at";
    }
    public static class WishListTable implements BaseColumns {
        public static final String TABLE_NAME = "wish_list_table";
        public static final String COLUMN_PRODCUTID = "product_id";    //FK
        public static final String COLUMN_PRODUCTCOUNT = "product_count";
        public static final String COLUMN_USERID = "user_id";
        public static final String COLUMN_NAMES = "name";
        public static final String COLUMN_ADDEDBY = "added_by";
        public static final String COLUMN_USER_IDS = "user_ids";
        public static final String COLUMN_CATEGORY_ID = "category_id";
        public static final String COLUMN_SUBCATEGORY_ID = "subcategory_id";
        public static final String COLUMN_SUBSUBCATEGORY_ID = "subsubcategory_id";
        public static final String COLUMN_BRAND_ID = "brand_id";
        //public static final String COLUMN_PHOTOS = "photos";
        public static final String COLUMN_THUMBNAIL_IMAGE = "thumbnail_img";
        public static final String COLUMN_FEATURED_IMAGE = "featured_img";
        public static final String COLUMN_FLASH_DEAL_IMAGE = "flash_deal_img";
        public static final String COLUMN_VIDEO_PROVIDER = "video_provider";
        public static final String COLUMN_VIDEO_LINK = "video_link";
        public static final String COLUMN_TAGS = "tags";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_UNIT_PRICE = "unit_price";
        public static final String COLUMN_PURCHASE_PRICE = "purchase_price";
        //public static final String COLUMN_CHOICE_OPTION = "choice_options";
        //public static final String COLUMN_COLORS = "colors";
        //public static final String COLUMN_VARIATIONS = "variations";
        public static final String COLUMN_TODAYS_DEAL = "todays_deal";
        public static final String COLUMN_PUBLISHED= "published";
        public static final String COLUMN_FEATURED = "featured";
        public static final String COLUMN_CURRENT_STOCK = "current_stock";
        public static final String COLUMN_UNIT = "unit";
        public static final String COLUMN_DISCOUNT = "discount";
        public static final String COLUMN_DISCOUNT_TYPE = "discount_type";
        public static final String COLUMN_TAX = "tax";
        public static final String COLUMN_TAX_TYPE = "tax_type";
        public static final String COLUMN_SHIPPING_TYPE = "shipping_type";
        public static final String COLUMN_SHIPPING_COST = "shipping_cost";
        public static final String COLUMN_WEIGHT = "weight";
        public static final String COLUMN_PARCEL_SIZE = "parcel_size";
        public static final String COLUMN_NUM_OF_SALE = "num_of_sale";
        public static final String COLUMN_META_TITLE = "meta_title";
        public static final String COLUMN_META_DESCRIPTION = "meta_description";
        public static final String COLUMN_META_IMG = "meta_img";
        public static final String COLUMN_PDF = "pdf";
        public static final String COLUMN_SLUG = "slug";
        public static final String COLUMN_RATING = "rating";
        public static final String COLUMN_CREATED_AT= "created_at";
        public static final String COLUMN_UPDATE_AT = "updated_at";
    }
    public static class RecentViewTable implements BaseColumns {
        public static final String TABLE_NAME = "recent_view_table";
        public static final String COLUMN_PRODCUTID = "product_id";    //FK
        public static final String COLUMN_PRODUCTCOUNT = "product_count";
        public static final String COLUMN_USERID = "user_id";
        public static final String COLUMN_NAMES = "name";
        public static final String COLUMN_ADDEDBY = "added_by";
        public static final String COLUMN_USER_IDS = "user_ids";
        public static final String COLUMN_CATEGORY_ID = "category_id";
        public static final String COLUMN_SUBCATEGORY_ID = "subcategory_id";
        public static final String COLUMN_SUBSUBCATEGORY_ID = "subsubcategory_id";
        public static final String COLUMN_BRAND_ID = "brand_id";
        //public static final String COLUMN_PHOTOS = "photos";
        public static final String COLUMN_THUMBNAIL_IMAGE = "thumbnail_img";
        public static final String COLUMN_FEATURED_IMAGE = "featured_img";
        public static final String COLUMN_FLASH_DEAL_IMAGE = "flash_deal_img";
        public static final String COLUMN_VIDEO_PROVIDER = "video_provider";
        public static final String COLUMN_VIDEO_LINK = "video_link";
        public static final String COLUMN_TAGS = "tags";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_UNIT_PRICE = "unit_price";
        public static final String COLUMN_PURCHASE_PRICE = "purchase_price";
        //public static final String COLUMN_CHOICE_OPTION = "choice_options";
        //public static final String COLUMN_COLORS = "colors";
        //public static final String COLUMN_VARIATIONS = "variations";
        public static final String COLUMN_TODAYS_DEAL = "todays_deal";
        public static final String COLUMN_PUBLISHED= "published";
        public static final String COLUMN_FEATURED = "featured";
        public static final String COLUMN_CURRENT_STOCK = "current_stock";
        public static final String COLUMN_UNIT = "unit";
        public static final String COLUMN_DISCOUNT = "discount";
        public static final String COLUMN_DISCOUNT_TYPE = "discount_type";
        public static final String COLUMN_TAX = "tax";
        public static final String COLUMN_TAX_TYPE = "tax_type";
        public static final String COLUMN_SHIPPING_TYPE = "shipping_type";
        public static final String COLUMN_SHIPPING_COST = "shipping_cost";
        public static final String COLUMN_WEIGHT = "weight";
        public static final String COLUMN_PARCEL_SIZE = "parcel_size";
        public static final String COLUMN_NUM_OF_SALE = "num_of_sale";
        public static final String COLUMN_META_TITLE = "meta_title";
        public static final String COLUMN_META_DESCRIPTION = "meta_description";
        public static final String COLUMN_META_IMG = "meta_img";
        public static final String COLUMN_PDF = "pdf";
        public static final String COLUMN_SLUG = "slug";
        public static final String COLUMN_RATING = "rating";
        public static final String COLUMN_CREATED_AT= "created_at";
        public static final String COLUMN_UPDATE_AT = "updated_at";
    }



}
