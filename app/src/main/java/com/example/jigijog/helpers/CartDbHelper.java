package com.example.jigijog.helpers;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.jigijog.models.CartItem;
import com.example.jigijog.models.OrderDetails;
import com.example.jigijog.models.RecentlyView;
import com.example.jigijog.models.WishList;
import com.example.jigijog.utils.Debugger;

import java.util.ArrayList;

public class CartDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "MyCart.db";
    private static final int DATABASE_VERSION = 1;

    private static CartDbHelper instance;

    private SQLiteDatabase db;

    private CartDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized CartDbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new CartDbHelper(context.getApplicationContext());
        }
        return instance;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        this.db = db;
        final String SQL_CREATE_USER_TABLE = "CREATE TABLE " +
                CartContract.UserTable.TABLE_NAME + "( " +
                CartContract.UserTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CartContract.UserTable.COLUMN_ID + " INTEGER, " +
                CartContract.UserTable.COLUMN_NAME + " TEXT " +
                ")";

        final String SQL_CREATE_CART_TABLE = "CREATE TABLE " +
                CartContract.CartTable.TABLE_NAME + " ( " +
                CartContract.CartTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CartContract.CartTable.COLUMN_PRODCUTID + " TEXT, " +
                CartContract.CartTable.COLUMN_PRODUCTCOUNT + " TEXT, " +
                CartContract.CartTable.COLUMN_NAMES + " TEXT, " +
                CartContract.CartTable.COLUMN_ADDEDBY + " TEXT, " +
                CartContract.CartTable.COLUMN_USER_IDS + " TEXT, " +
                CartContract.CartTable.COLUMN_CATEGORY_ID + " TEXT, " +
                CartContract.CartTable.COLUMN_SUBCATEGORY_ID + " TEXT, " +
                CartContract.CartTable.COLUMN_SUBSUBCATEGORY_ID + " TEXT, " +
                CartContract.CartTable.COLUMN_BRAND_ID + " TEXT, " +
                CartContract.CartTable.COLUMN_THUMBNAIL_IMAGE + " TEXT, " +
                CartContract.CartTable.COLUMN_FEATURED_IMAGE + " TEXT, " +
                CartContract.CartTable.COLUMN_FLASH_DEAL_IMAGE + " TEXT, " +
                CartContract.CartTable.COLUMN_VIDEO_PROVIDER + " TEXT, " +
                CartContract.CartTable.COLUMN_VIDEO_LINK + " TEXT, " +
                CartContract.CartTable.COLUMN_TAGS + " TEXT, " +
                CartContract.CartTable.COLUMN_DESCRIPTION + " TEXT, " +
                CartContract.CartTable.COLUMN_UNIT_PRICE + " TEXT, " +
                CartContract.CartTable.COLUMN_PURCHASE_PRICE + " TEXT, " +
                CartContract.CartTable.COLUMN_TODAYS_DEAL + " TEXT, " +
                CartContract.CartTable.COLUMN_PUBLISHED + " TEXT, " +
                CartContract.CartTable.COLUMN_FEATURED + " TEXT, " +
                CartContract.CartTable.COLUMN_CURRENT_STOCK + " TEXT, " +
                CartContract.CartTable.COLUMN_UNIT + " TEXT, " +
                CartContract.CartTable.COLUMN_DISCOUNT + " TEXT, " +
                CartContract.CartTable.COLUMN_DISCOUNT_TYPE + " TEXT, " +
                CartContract.CartTable.COLUMN_TAX + " TEXT, " +
                CartContract.CartTable.COLUMN_TAX_TYPE + " TEXT, " +
                CartContract.CartTable.COLUMN_SHIPPING_TYPE + " TEXT, " +
                CartContract.CartTable.COLUMN_SHIPPING_COST + " TEXT, " +
                CartContract.CartTable.COLUMN_WEIGHT + " TEXT, " +
                CartContract.CartTable.COLUMN_PARCEL_SIZE + " TEXT, " +
                CartContract.CartTable.COLUMN_NUM_OF_SALE + " TEXT, " +
                CartContract.CartTable.COLUMN_META_TITLE + " TEXT, " +
                CartContract.CartTable.COLUMN_META_DESCRIPTION + " TEXT, " +
                CartContract.CartTable.COLUMN_META_IMG + " TEXT, " +
                CartContract.CartTable.COLUMN_PDF + " TEXT, " +
                CartContract.CartTable.COLUMN_SLUG + " TEXT, " +
                CartContract.CartTable.COLUMN_RATING + " TEXT, " +
                CartContract.CartTable.COLUMN_CREATED_AT + " TEXT, " +
                CartContract.CartTable.COLUMN_UPDATE_AT + " TEXT, " +

                CartContract.CartTable.COLUMN_USERID + " INTEGER, " +
                "FOREIGN KEY(" + CartContract.CartTable.COLUMN_USERID + ") REFERENCES " +
                CartContract.CartTable.TABLE_NAME + "(" + CartContract.UserTable._ID + ")" + "ON DELETE CASCADE" +
                ")";

        final String SQL_CREATE_ORDER_DETAIL_TABLE = "CREATE TABLE " +
                CartContract.OrderDetailsTable.TABLE_NAME + " ( " +
                CartContract.OrderDetailsTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CartContract.OrderDetailsTable.COLUMN_SELLER_ID + " TEXT, " +
                CartContract.OrderDetailsTable.COLUMN_VARIATION + " TEXT, " +
                CartContract.OrderDetailsTable.COLUMN_BASE_PRICE + " TEXT, " +
                CartContract.OrderDetailsTable.COLUMN_SELLING_PRICE + " TEXT, " +
                CartContract.OrderDetailsTable.COLUMN_PRICE + " TEXT, " +
                CartContract.OrderDetailsTable.COLUMN_TAX + " TEXT, " +
                CartContract.OrderDetailsTable.COLUMN_SHIPPING_COST + " TEXT, " +
                CartContract.OrderDetailsTable.COLUMN_COUPON_DISCOUNT + " TEXT, " +
                CartContract.OrderDetailsTable.COLUMN_QUANTITY + " TEXT, " +
                CartContract.OrderDetailsTable.COLUMN_PAYMENT_STATUS + " TEXT, " +
                CartContract.OrderDetailsTable.COLUMN_DELIVERY_STATUS + " TEXT, " +
                CartContract.OrderDetailsTable.COLUMN_PICK_FROM + " TEXT, " +
                CartContract.OrderDetailsTable.COLUMN_PICK_TO + " TEXT, " +
                CartContract.OrderDetailsTable.COLUMN_CREATED_AT + " TEXT, " +
                CartContract.OrderDetailsTable.COLUMN_UPDATE_AT + " TEXT, " +
                CartContract.OrderDetailsTable.COLUMN_PRODUCT_ID + " TEXT, " +

                CartContract.OrderDetailsTable.COLUMN_USERID + " INTEGER, " +
                "FOREIGN KEY(" + CartContract.OrderDetailsTable.COLUMN_USERID + ") REFERENCES " +
                CartContract.OrderDetailsTable.TABLE_NAME + "(" + CartContract.UserTable._ID + ")" + "ON DELETE CASCADE" +
                ")";

        final String SQL_CREATE_WISH_LIST_TABLE = "CREATE TABLE " +
                CartContract.WishListTable.TABLE_NAME + " ( " +
                CartContract.WishListTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CartContract.WishListTable.COLUMN_PRODCUTID + " TEXT, " +
                CartContract.WishListTable.COLUMN_PRODUCTCOUNT + " TEXT, " +
                CartContract.WishListTable.COLUMN_NAMES + " TEXT, " +
                CartContract.WishListTable.COLUMN_ADDEDBY + " TEXT, " +
                CartContract.WishListTable.COLUMN_USER_IDS + " TEXT, " +
                CartContract.WishListTable.COLUMN_CATEGORY_ID + " TEXT, " +
                CartContract.WishListTable.COLUMN_SUBCATEGORY_ID + " TEXT, " +
                CartContract.WishListTable.COLUMN_SUBSUBCATEGORY_ID + " TEXT, " +
                CartContract.WishListTable.COLUMN_BRAND_ID + " TEXT, " +
                CartContract.WishListTable.COLUMN_THUMBNAIL_IMAGE + " TEXT, " +
                CartContract.WishListTable.COLUMN_FEATURED_IMAGE + " TEXT, " +
                CartContract.WishListTable.COLUMN_FLASH_DEAL_IMAGE + " TEXT, " +
                CartContract.WishListTable.COLUMN_VIDEO_PROVIDER + " TEXT, " +
                CartContract.WishListTable.COLUMN_VIDEO_LINK + " TEXT, " +
                CartContract.WishListTable.COLUMN_TAGS + " TEXT, " +
                CartContract.WishListTable.COLUMN_DESCRIPTION + " TEXT, " +
                CartContract.WishListTable.COLUMN_UNIT_PRICE + " TEXT, " +
                CartContract.WishListTable.COLUMN_PURCHASE_PRICE + " TEXT, " +
                CartContract.WishListTable.COLUMN_TODAYS_DEAL + " TEXT, " +
                CartContract.WishListTable.COLUMN_PUBLISHED + " TEXT, " +
                CartContract.WishListTable.COLUMN_FEATURED + " TEXT, " +
                CartContract.WishListTable.COLUMN_CURRENT_STOCK + " TEXT, " +
                CartContract.WishListTable.COLUMN_UNIT + " TEXT, " +
                CartContract.WishListTable.COLUMN_DISCOUNT + " TEXT, " +
                CartContract.WishListTable.COLUMN_DISCOUNT_TYPE + " TEXT, " +
                CartContract.WishListTable.COLUMN_TAX + " TEXT, " +
                CartContract.WishListTable.COLUMN_TAX_TYPE + " TEXT, " +
                CartContract.WishListTable.COLUMN_SHIPPING_TYPE + " TEXT, " +
                CartContract.WishListTable.COLUMN_SHIPPING_COST + " TEXT, " +
                CartContract.WishListTable.COLUMN_WEIGHT + " TEXT, " +
                CartContract.WishListTable.COLUMN_PARCEL_SIZE + " TEXT, " +
                CartContract.WishListTable.COLUMN_NUM_OF_SALE + " TEXT, " +
                CartContract.WishListTable.COLUMN_META_TITLE + " TEXT, " +
                CartContract.WishListTable.COLUMN_META_DESCRIPTION + " TEXT, " +
                CartContract.WishListTable.COLUMN_META_IMG + " TEXT, " +
                CartContract.WishListTable.COLUMN_PDF + " TEXT, " +
                CartContract.WishListTable.COLUMN_SLUG + " TEXT, " +
                CartContract.WishListTable.COLUMN_RATING + " TEXT, " +
                CartContract.WishListTable.COLUMN_CREATED_AT + " TEXT, " +
                CartContract.WishListTable.COLUMN_UPDATE_AT + " TEXT, " +

                CartContract.WishListTable.COLUMN_USERID + " INTEGER, " +
                "FOREIGN KEY(" + CartContract.WishListTable.COLUMN_USERID + ") REFERENCES " +
                CartContract.WishListTable.TABLE_NAME + "(" + CartContract.UserTable._ID + ")" + "ON DELETE CASCADE" +
                ")";


        final String SQL_CREATE_RECENT_VIEW_TABLE = "CREATE TABLE " +
                CartContract.RecentViewTable.TABLE_NAME + " ( " +
                CartContract.RecentViewTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CartContract.RecentViewTable.COLUMN_PRODCUTID + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_PRODUCTCOUNT + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_NAMES + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_ADDEDBY + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_USER_IDS + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_CATEGORY_ID + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_SUBCATEGORY_ID + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_SUBSUBCATEGORY_ID + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_BRAND_ID + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_THUMBNAIL_IMAGE + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_FEATURED_IMAGE + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_FLASH_DEAL_IMAGE + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_VIDEO_PROVIDER + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_VIDEO_LINK + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_TAGS + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_DESCRIPTION + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_UNIT_PRICE + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_PURCHASE_PRICE + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_TODAYS_DEAL + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_PUBLISHED + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_FEATURED + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_CURRENT_STOCK + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_UNIT + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_DISCOUNT + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_DISCOUNT_TYPE + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_TAX + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_TAX_TYPE + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_SHIPPING_TYPE + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_SHIPPING_COST + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_WEIGHT + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_PARCEL_SIZE + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_NUM_OF_SALE + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_META_TITLE + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_META_DESCRIPTION + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_META_IMG + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_PDF + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_SLUG + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_RATING + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_CREATED_AT + " TEXT, " +
                CartContract.RecentViewTable.COLUMN_UPDATE_AT + " TEXT, " +

                CartContract.RecentViewTable.COLUMN_USERID + " INTEGER, " +
                "FOREIGN KEY(" + CartContract.RecentViewTable.COLUMN_USERID + ") REFERENCES " +
                CartContract.RecentViewTable.TABLE_NAME + "(" + CartContract.UserTable._ID + ")" + "ON DELETE CASCADE" +
                ")";

        db.execSQL(SQL_CREATE_CART_TABLE);
        db.execSQL(SQL_CREATE_USER_TABLE);
        db.execSQL(SQL_CREATE_ORDER_DETAIL_TABLE);
        db.execSQL(SQL_CREATE_WISH_LIST_TABLE);
        db.execSQL(SQL_CREATE_RECENT_VIEW_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CartContract.CartTable.TABLE_NAME);
        onCreate(db);
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }
    public ArrayList<CartItem> getCartItems(int userID) {
        ArrayList<CartItem> cartItemArrayList = new ArrayList<>();
        db = getReadableDatabase();

        String selection = CartContract.CartTable.COLUMN_USERID + " = ? ";
        String[] selectionArgs = new String[]{String.valueOf(userID)};

        Cursor c = db.query(
                CartContract.CartTable.TABLE_NAME,
                null,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        if (c.moveToFirst()) {
            do {
                CartItem cartItem = new CartItem();
                cartItem.setId(c.getInt(c.getColumnIndex("_id")));
                cartItem.setUser_id(c.getString(c.getColumnIndex("user_id")));
                cartItem.setProduct_id(c.getString(c.getColumnIndex("product_id")));
                cartItem.setProduct_count(c.getString(c.getColumnIndex("product_count")));

                cartItem.setName(c.getString(c.getColumnIndex("name")));
                cartItem.setAdded_by(c.getString(c.getColumnIndex("added_by")));
                cartItem.setUser_ids(c.getString(c.getColumnIndex("user_ids")));
                cartItem.setCategory_id(c.getString(c.getColumnIndex("category_id")));
                cartItem.setSubcategory_id(c.getString(c.getColumnIndex("subcategory_id")));
                cartItem.setSubsubcategory_id(c.getString(c.getColumnIndex("subsubcategory_id")));
                cartItem.setBrand_id(c.getString(c.getColumnIndex("brand_id")));
                //cartItem.setPhotos(c.getString(c.getColumnIndex(CartContract.CartTable.COLUMN_PHOTOS)));
                cartItem.setThumbnail_img(c.getString(c.getColumnIndex("thumbnail_img")));
                cartItem.setFeatured_img(c.getString(c.getColumnIndex("featured_img")));
                cartItem.setFlash_deal_img(c.getString(c.getColumnIndex("flash_deal_img")));
                cartItem.setVideo_provider(c.getString(c.getColumnIndex("video_provider")));
                cartItem.setVideo_link(c.getString(c.getColumnIndex("video_link")));
                cartItem.setTags(c.getString(c.getColumnIndex("tags")));
                cartItem.setDescription(c.getString(c.getColumnIndex("description")));
                cartItem.setUnit_price(c.getString(c.getColumnIndex("unit_price")));
                cartItem.setPurchase_price(c.getString(c.getColumnIndex("purchase_price")));
                //cartItem.setProduct_id(c.getString(c.getColumnIndex(CartContract.CartTable.COLUMN_CHOICE_OPTION)));
                //cartItem.setColors(c.getString(c.getColumnIndex(CartContract.CartTable.COLUMN_COLORS)));
                //cartItem.setTodays_deal(c.getString(c.getColumnIndex(CartContract.CartTable.COLUMN_VARIATIONS)));
                cartItem.setTodays_deal(c.getString(c.getColumnIndex("todays_deal")));
                cartItem.setPublished(c.getString(c.getColumnIndex("published")));
                cartItem.setFeatured(c.getString(c.getColumnIndex("featured")));
                cartItem.setCurrent_stock(c.getString(c.getColumnIndex("current_stock")));
                cartItem.setUnit(c.getString(c.getColumnIndex("unit")));
                cartItem.setDiscount(c.getString(c.getColumnIndex("discount")));
                cartItem.setDiscount_type(c.getString(c.getColumnIndex("discount_type")));
                cartItem.setTax(c.getString(c.getColumnIndex("tax")));
                cartItem.setTax_type(c.getString(c.getColumnIndex("tax_type")));
                cartItem.setShipping_type(c.getString(c.getColumnIndex("shipping_type")));
                cartItem.setShipping_cost(c.getString(c.getColumnIndex("shipping_cost")));
                cartItem.setWeight(c.getString(c.getColumnIndex("weight")));
                cartItem.setParcel_size(c.getString(c.getColumnIndex("parcel_size")));
                cartItem.setProduct_id(c.getString(c.getColumnIndex("num_of_sale")));
                cartItem.setMeta_title(c.getString(c.getColumnIndex("meta_title")));
                cartItem.setMeta_description(c.getString(c.getColumnIndex("meta_description")));
                cartItem.setMeta_img(c.getString(c.getColumnIndex("meta_img")));
                cartItem.setPdf(c.getString(c.getColumnIndex("pdf")));
                cartItem.setSlug(c.getString(c.getColumnIndex("slug")));
                cartItem.setRating(c.getString(c.getColumnIndex("rating")));
                cartItem.setCreated_at(c.getString(c.getColumnIndex("created_at")));
                cartItem.setUpdated_at(c.getString(c.getColumnIndex("updated_at")));
                cartItemArrayList.add(cartItem);

            } while (c.moveToNext());
        }
        c.close();
        return cartItemArrayList;
    }

    public void removeSingleContact(String title) {
        //Open the database
        SQLiteDatabase database = this.getWritableDatabase();

        //Execute sql query to remove from database
        //NOTE: When removing by String in SQL, value must be enclosed with ''
        database.execSQL("DELETE FROM " + CartContract.CartTable.TABLE_NAME + " WHERE " + CartContract.CartTable.COLUMN_NAMES + "= '" + title + "'");

        //Close the database
        database.close();
    }

    public ArrayList<OrderDetails> getAllOrderDetails(int userID) {
        ArrayList<OrderDetails> orderDetailsArrayList = new ArrayList<>();
        db = getReadableDatabase();

        String selection = CartContract.OrderDetailsTable.COLUMN_USERID + " = ? ";
        String[] selectionArgs = new String[]{String.valueOf(userID)};

        Cursor c = db.query(
                CartContract.OrderDetailsTable.TABLE_NAME,
                null,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        if (c.moveToFirst()) {
            do {
                OrderDetails orderDetails = new OrderDetails();
                orderDetails.setId(c.getInt(c.getColumnIndex("_id")));
                orderDetails.setUser_id(c.getString(c.getColumnIndex("user_id")));
                orderDetails.setSeller_id(c.getString(c.getColumnIndex("seller_id")));
                orderDetails.setProduct_id(c.getString(c.getColumnIndex("product_id")));
                orderDetails.setVariation(c.getString(c.getColumnIndex("variation")));

                orderDetails.setBase_price(c.getString(c.getColumnIndex("base_price")));
                orderDetails.setSelling_price(c.getString(c.getColumnIndex("selling_price")));
                orderDetails.setPrice(c.getString(c.getColumnIndex("price")));
                orderDetails.setTax(c.getString(c.getColumnIndex("tax")));
                orderDetails.setShipping_cost(c.getString(c.getColumnIndex("shipping_cost")));
                orderDetails.setCoupon_discount(c.getString(c.getColumnIndex("coupon_discount")));
                orderDetails.setQuantity(c.getString(c.getColumnIndex("quantity")));
                orderDetails.setPayment_status(c.getString(c.getColumnIndex("payment_status")));
                orderDetails.setDelivery_status(c.getString(c.getColumnIndex("delivery_status")));
                orderDetails.setPick_from(c.getString(c.getColumnIndex("pick_from")));
                orderDetails.setPick_to(c.getString(c.getColumnIndex("pick_to")));
                orderDetails.setCreated_at(c.getString(c.getColumnIndex("created_at")));
                orderDetails.setUpdated_at(c.getString(c.getColumnIndex("updated_at")));
                orderDetailsArrayList.add(orderDetails);

            } while (c.moveToNext());
        }
        c.close();
        return orderDetailsArrayList;
    }

    public ArrayList<RecentlyView> getAllRecentlyView(int productID) {
        ArrayList<RecentlyView> recentlyViewArrayList = new ArrayList<>();
        db = getReadableDatabase();

        String selection = CartContract.RecentViewTable.COLUMN_USERID + " = ? ";
        String[] selectionArgs = new String[]{String.valueOf(productID)};

        Cursor c = db.query(
                CartContract.RecentViewTable.TABLE_NAME,
                null,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        if (c.moveToFirst()) {
            do {
                RecentlyView recentlyView = new RecentlyView();
                recentlyView.setId(c.getInt(c.getColumnIndex("_id")));
                recentlyView.setUser_id(c.getString(c.getColumnIndex("user_id")));
                recentlyView.setProduct_id(c.getString(c.getColumnIndex("product_id")));
                recentlyView.setProduct_count(c.getString(c.getColumnIndex("product_count")));

                recentlyView.setName(c.getString(c.getColumnIndex("name")));
                recentlyView.setAdded_by(c.getString(c.getColumnIndex("added_by")));
                recentlyView.setUser_ids(c.getString(c.getColumnIndex("user_ids")));
                recentlyView.setCategory_id(c.getString(c.getColumnIndex("category_id")));
                recentlyView.setSubcategory_id(c.getString(c.getColumnIndex("subcategory_id")));
                recentlyView.setSubsubcategory_id(c.getString(c.getColumnIndex("subsubcategory_id")));
                recentlyView.setBrand_id(c.getString(c.getColumnIndex("brand_id")));
                //recentlyView.setPhotos(c.getString(c.getColumnIndex(CartContract.CartTable.COLUMN_PHOTOS)));
                recentlyView.setThumbnail_img(c.getString(c.getColumnIndex("thumbnail_img")));
                recentlyView.setFeatured_img(c.getString(c.getColumnIndex("featured_img")));
                recentlyView.setFlash_deal_img(c.getString(c.getColumnIndex("flash_deal_img")));
                recentlyView.setVideo_provider(c.getString(c.getColumnIndex("video_provider")));
                recentlyView.setVideo_link(c.getString(c.getColumnIndex("video_link")));
                recentlyView.setTags(c.getString(c.getColumnIndex("tags")));
                recentlyView.setDescription(c.getString(c.getColumnIndex("description")));
                recentlyView.setUnit_price(c.getString(c.getColumnIndex("unit_price")));
                recentlyView.setPurchase_price(c.getString(c.getColumnIndex("purchase_price")));
                //recentlyView.setProduct_id(c.getString(c.getColumnIndex(CartContract.CartTable.COLUMN_CHOICE_OPTION)));
                //recentlyView.setColors(c.getString(c.getColumnIndex(CartContract.CartTable.COLUMN_COLORS)));
                //recentlyView.setTodays_deal(c.getString(c.getColumnIndex(CartContract.CartTable.COLUMN_VARIATIONS)));
                recentlyView.setTodays_deal(c.getString(c.getColumnIndex("todays_deal")));
                recentlyView.setPublished(c.getString(c.getColumnIndex("published")));
                recentlyView.setFeatured(c.getString(c.getColumnIndex("featured")));
                recentlyView.setCurrent_stock(c.getString(c.getColumnIndex("current_stock")));
                recentlyView.setUnit(c.getString(c.getColumnIndex("unit")));
                recentlyView.setDiscount(c.getString(c.getColumnIndex("discount")));
                recentlyView.setDiscount_type(c.getString(c.getColumnIndex("discount_type")));
                recentlyView.setTax(c.getString(c.getColumnIndex("tax")));
                recentlyView.setTax_type(c.getString(c.getColumnIndex("tax_type")));
                recentlyView.setShipping_type(c.getString(c.getColumnIndex("shipping_type")));
                recentlyView.setShipping_cost(c.getString(c.getColumnIndex("shipping_cost")));
                recentlyView.setWeight(c.getString(c.getColumnIndex("weight")));
                recentlyView.setParcel_size(c.getString(c.getColumnIndex("parcel_size")));
                recentlyView.setProduct_id(c.getString(c.getColumnIndex("num_of_sale")));
                recentlyView.setMeta_title(c.getString(c.getColumnIndex("meta_title")));
                recentlyView.setMeta_description(c.getString(c.getColumnIndex("meta_description")));
                recentlyView.setMeta_img(c.getString(c.getColumnIndex("meta_img")));
                recentlyView.setPdf(c.getString(c.getColumnIndex("pdf")));
                recentlyView.setSlug(c.getString(c.getColumnIndex("slug")));
                recentlyView.setRating(c.getString(c.getColumnIndex("rating")));
                recentlyView.setCreated_at(c.getString(c.getColumnIndex("created_at")));
                recentlyView.setUpdated_at(c.getString(c.getColumnIndex("updated_at")));

                recentlyViewArrayList.add(recentlyView);

            } while (c.moveToNext());
        }
        c.close();
        return recentlyViewArrayList;
    }

    public ArrayList<WishList> getAllWishList(int productID) {
        ArrayList<WishList> wishListArrayList = new ArrayList<>();
        db = getReadableDatabase();

        String selection = CartContract.WishListTable.COLUMN_USERID + " = ? ";
        String[] selectionArgs = new String[]{String.valueOf(productID)};

        Cursor c = db.query(
                CartContract.WishListTable.TABLE_NAME,
                null,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        if (c.moveToFirst()) {
            do {
                WishList wishList = new WishList();
                wishList.setId(c.getInt(c.getColumnIndex("_id")));
                wishList.setUser_id(c.getString(c.getColumnIndex("user_id")));
                wishList.setProduct_id(c.getString(c.getColumnIndex("product_id")));
                wishList.setProduct_count(c.getString(c.getColumnIndex("product_count")));

                wishList.setName(c.getString(c.getColumnIndex("name")));
                wishList.setAdded_by(c.getString(c.getColumnIndex("added_by")));
                wishList.setUser_ids(c.getString(c.getColumnIndex("user_ids")));
                wishList.setCategory_id(c.getString(c.getColumnIndex("category_id")));
                wishList.setSubcategory_id(c.getString(c.getColumnIndex("subcategory_id")));
                wishList.setSubsubcategory_id(c.getString(c.getColumnIndex("subsubcategory_id")));
                wishList.setBrand_id(c.getString(c.getColumnIndex("brand_id")));
                //wishList.setPhotos(c.getString(c.getColumnIndex(CartContract.CartTable.COLUMN_PHOTOS)));
                wishList.setThumbnail_img(c.getString(c.getColumnIndex("thumbnail_img")));
                wishList.setFeatured_img(c.getString(c.getColumnIndex("featured_img")));
                wishList.setFlash_deal_img(c.getString(c.getColumnIndex("flash_deal_img")));
                wishList.setVideo_provider(c.getString(c.getColumnIndex("video_provider")));
                wishList.setVideo_link(c.getString(c.getColumnIndex("video_link")));
                wishList.setTags(c.getString(c.getColumnIndex("tags")));
                wishList.setDescription(c.getString(c.getColumnIndex("description")));
                wishList.setUnit_price(c.getString(c.getColumnIndex("unit_price")));
                wishList.setPurchase_price(c.getString(c.getColumnIndex("purchase_price")));
                //wishList.setProduct_id(c.getString(c.getColumnIndex(CartContract.CartTable.COLUMN_CHOICE_OPTION)));
                //wishList.setColors(c.getString(c.getColumnIndex(CartContract.CartTable.COLUMN_COLORS)));
                //wishList.setTodays_deal(c.getString(c.getColumnIndex(CartContract.CartTable.COLUMN_VARIATIONS)));
                wishList.setTodays_deal(c.getString(c.getColumnIndex("todays_deal")));
                wishList.setPublished(c.getString(c.getColumnIndex("published")));
                wishList.setFeatured(c.getString(c.getColumnIndex("featured")));
                wishList.setCurrent_stock(c.getString(c.getColumnIndex("current_stock")));
                wishList.setUnit(c.getString(c.getColumnIndex("unit")));
                wishList.setDiscount(c.getString(c.getColumnIndex("discount")));
                wishList.setDiscount_type(c.getString(c.getColumnIndex("discount_type")));
                wishList.setTax(c.getString(c.getColumnIndex("tax")));
                wishList.setTax_type(c.getString(c.getColumnIndex("tax_type")));
                wishList.setShipping_type(c.getString(c.getColumnIndex("shipping_type")));
                wishList.setShipping_cost(c.getString(c.getColumnIndex("shipping_cost")));
                wishList.setWeight(c.getString(c.getColumnIndex("weight")));
                wishList.setParcel_size(c.getString(c.getColumnIndex("parcel_size")));
                wishList.setProduct_id(c.getString(c.getColumnIndex("num_of_sale")));
                wishList.setMeta_title(c.getString(c.getColumnIndex("meta_title")));
                wishList.setMeta_description(c.getString(c.getColumnIndex("meta_description")));
                wishList.setMeta_img(c.getString(c.getColumnIndex("meta_img")));
                wishList.setPdf(c.getString(c.getColumnIndex("pdf")));
                wishList.setSlug(c.getString(c.getColumnIndex("slug")));
                wishList.setRating(c.getString(c.getColumnIndex("rating")));
                wishList.setCreated_at(c.getString(c.getColumnIndex("created_at")));
                wishList.setUpdated_at(c.getString(c.getColumnIndex("updated_at")));
                wishListArrayList.add(wishList);

            } while (c.moveToNext());
        }
        c.close();
        return wishListArrayList;
    }

}
