package com.example.jigijog.interfaces;

import android.view.View;

public interface OnClickRecyclerView {

        void onItemClick(View view, int position);
}
