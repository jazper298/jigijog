package com.example.jigijog.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Chats;

import java.util.ArrayList;

public class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Chats> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public ChatsAdapter(Context context, ArrayList<Chats> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_messages, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Chats chats = mList.get(position);
        ImageView imageView = holder.messageIcon;
        ImageView imageView1 = holder.messageImage;
        TextView textView1, textView2, textView3;

        textView1 = holder.messageTitle;
        textView2 = holder.messageDate;
        textView3 = holder.messageDescription;

        imageView.setImageResource(chats.getChatIcon());
        imageView1.setImageResource(chats.getChatImage());

        textView1.setText(chats.getChatTitle());
        textView2.setText(chats.getChatDate()    );
        textView3.setText(chats.getChatDescription());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView messageIcon, messageImage;
        private TextView messageTitle, messageDate, messageDescription;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            messageIcon = (ImageView) itemView.findViewById(R.id.iv_messageIcon);
            messageTitle = (TextView) itemView.findViewById(R.id.tv_messageTitle);
            messageDate = (TextView) itemView.findViewById(R.id.tv_messageDate);
            messageDescription = (TextView) itemView.findViewById(R.id.tv_messageDescription);
            messageImage = (ImageView) itemView.findViewById(R.id.iv_messageImage);

            messageImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
