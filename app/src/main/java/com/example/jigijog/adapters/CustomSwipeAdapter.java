package com.example.jigijog.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.example.jigijog.R;
import com.example.jigijog.models.Slide;

import java.util.ArrayList;

public class CustomSwipeAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<Slide> mList;
    private LayoutInflater layoutInflater;
    private int current_pos = 0;

    public CustomSwipeAdapter(Context context, ArrayList<Slide> list){
        this.context = context;
        this.mList = list;
    }
    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == (LinearLayout)object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        if(current_pos>mList.size()-1){
            current_pos = 0;
        }
        Slide slide = mList.get(current_pos);
        current_pos++;
        //Slide slide = mList.get(position);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.swipe_layout, container, false);
        ImageView imageView = view.findViewById(R.id.imageView);

        Glide.with(context)
                .load( "https://www.jigijog.com/public/" + slide.getPhotos())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(imageView);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout)object);
    }
}
