package com.example.jigijog.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Notifications;

import java.util.ArrayList;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Notifications> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public NotificationsAdapter(Context context, ArrayList<Notifications> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_messages, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Notifications notificationsModel = mList.get(position);
        ImageView imageView = holder.messageIcon;
        ImageView imageView1 = holder.messageImage;
        TextView textView1, textView2, textView3;

        textView1 = holder.messageTitle;
        textView2 = holder.messageDate;
        textView3 = holder.messageDescription;

        imageView.setImageResource(notificationsModel.getNotifIcon());
        imageView1.setImageResource(notificationsModel.getNotifImage());

        textView1.setText(notificationsModel.getNotifTitle());
        textView2.setText(notificationsModel.getNotiftDate()    );
        textView3.setText(notificationsModel.getNotifDescription());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView messageIcon, messageImage;
        private TextView messageTitle, messageDate, messageDescription;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            messageIcon = (ImageView) itemView.findViewById(R.id.iv_messageIcon);
            messageTitle = (TextView) itemView.findViewById(R.id.tv_messageTitle);
            messageDate = (TextView) itemView.findViewById(R.id.tv_messageDate);
            messageDescription = (TextView) itemView.findViewById(R.id.tv_messageDescription);
            messageImage = (ImageView) itemView.findViewById(R.id.iv_messageImage);

            messageImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
