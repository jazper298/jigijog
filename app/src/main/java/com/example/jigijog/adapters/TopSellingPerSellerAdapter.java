package com.example.jigijog.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.TopSelling;

import java.util.ArrayList;

public class TopSellingPerSellerAdapter extends RecyclerView.Adapter<TopSellingPerSellerAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<TopSelling> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public TopSellingPerSellerAdapter(Context context, ArrayList<TopSelling> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public TopSellingPerSellerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_new_arrival, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TopSellingPerSellerAdapter.ViewHolder holder, int position) {
        final TopSelling topSellingModel = mList.get(position);
        ImageView imageView = holder.iv_newArrivalImage;

        TextView textView1, textView2,textView3;

        textView1 = holder.tv_newArrivalPrice;
        textView2 = holder.tv_newArrivalDisPrice;
        textView3 = holder.tv_newArrivalItemName;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + topSellingModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_newArrivalImage);
        double unit_price =  Double.parseDouble(topSellingModel.getUnit_price());
        double discount;
        if (topSellingModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(topSellingModel.getDiscount());
        }
        double total = unit_price - discount;

        textView1.setText("\u20B1" + " " + String.valueOf(total) + "0");
        textView2.setText("\u20B1" + " " + topSellingModel.getDiscount());
        textView2.setPaintFlags(textView2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        if (topSellingModel.getName().length() <= 14){
            textView3.setText(topSellingModel.getName());
        }else{
            textView3.setText(topSellingModel.getName().substring(0, 14) + ".." );
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_newArrivalImage;
        TextView tv_newArrivalPrice, tv_newArrivalDisPrice, tv_newArrivalItemName;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_newArrivalImage = (ImageView) itemView.findViewById(R.id.iv_newArrivalImage);
            tv_newArrivalPrice = (TextView) itemView.findViewById(R.id.tv_newArrivalPrice);
            tv_newArrivalDisPrice = (TextView) itemView.findViewById(R.id.tv_newArrivalDisPrice);
            tv_newArrivalItemName = (TextView) itemView.findViewById(R.id.tv_newArrivalItemName);

            cardView = (CardView) itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
