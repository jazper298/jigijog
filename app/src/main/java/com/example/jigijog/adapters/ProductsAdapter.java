package com.example.jigijog.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Products;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> implements Filterable {
    private Context mContext;
    private ArrayList<Products> mList;
    private OnClickRecyclerView onClickRecyclerView;
    private ArrayList<Products> mListAll;
    public ProductsAdapter(Context context, ArrayList<Products> list) {
        mContext = context;
        mList = list;
        mListAll = new ArrayList<>(mList);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_products, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Products productsModel = mList.get(position);
        ImageView imageView = holder.iv_sportsImage;

        TextView textView1, textView2,textView3,textView4;

        textView1 = holder.tv_sportsPrice;
        textView2 = holder.tv_sportsDisPrice;
        textView3 = holder.tv_sportsItemName;
        textView4 = holder.tv_discounted;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + productsModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_sportsImage);
        double unit_price =  Double.parseDouble(productsModel.getUnit_price());
        double discount;
        if (productsModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(productsModel.getDiscount());
        }
        double total = unit_price - discount;

        textView1.setText("\u20B1" + " " + String.valueOf(total) + "0");
        if (productsModel.getName().length() <= 24){
            textView3.setText(productsModel.getName());
        }else{
            textView3.setText(productsModel.getName().substring(0, 24) + ".." );
        }
        if (!productsModel.getDiscount().equals("0.00")){
            textView4.setVisibility(View.VISIBLE);
            textView2.setText("\u20B1" + " " + productsModel.getDiscount());
            textView2.setPaintFlags(textView2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else{
            textView2.setText("");
            textView4.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }
    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<Products> filteredList = new ArrayList<>();
            if(constraint.toString().isEmpty()){
                filteredList.addAll(mListAll);
            }else{
                for (Products list : mListAll){
                    if(list.getName().toLowerCase().contains(constraint.toString().toLowerCase())){
                        filteredList.add(list);
                    }
                }
            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mList.clear();
            mList.addAll((Collection<? extends Products>) results.values);
            notifyDataSetChanged();
        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_sportsImage;
        TextView tv_sportsPrice, tv_sportsDisPrice, tv_sportsItemName, tv_discounted;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_sportsImage = (ImageView) itemView.findViewById(R.id.iv_sportsImage);
            tv_sportsPrice = (TextView) itemView.findViewById(R.id.tv_sportsPrice);
            tv_sportsDisPrice = (TextView) itemView.findViewById(R.id.tv_sportsDisPrice);
            tv_sportsItemName = (TextView) itemView.findViewById(R.id.tv_sportsItemName);
            tv_discounted = (TextView) itemView.findViewById(R.id.tv_discounted);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
