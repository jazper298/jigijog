package com.example.jigijog.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.HotPicks;

import java.util.ArrayList;

public class HotPicksAdapter extends RecyclerView.Adapter<HotPicksAdapter.ViewHolder>  {
    private Context mContext;
    private ArrayList<HotPicks> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public HotPicksAdapter(Context context, ArrayList<HotPicks> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_hot_picks, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final HotPicks hotPicksModel = mList.get(position);
        ImageView imageView = holder.iv_mensHotImage;

        TextView textView1, textView2,textView3,textView4;

        textView1 = holder.tv_mensHotPrice;
        textView2 = holder.tv_mensHotDisPrice;
        textView3 = holder.tv_mensHotItemName;
        textView4 = holder.tv_discounted;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + hotPicksModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_mensHotImage);
        double unit_price =  Double.parseDouble(hotPicksModel.getUnit_price());
        double discount;
        if (hotPicksModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(hotPicksModel.getDiscount());
        }
        double total = unit_price - discount;

        textView1.setText("\u20B1" + " " + String.valueOf(total) + "0");
        if (hotPicksModel.getName().length() <= 14){
            textView3.setText(hotPicksModel.getName());
        }else{
            textView3.setText(hotPicksModel.getName().substring(0, 14) + ".." );
        }
        if (!hotPicksModel.getDiscount().equals("0.00")){
            textView4.setVisibility(View.VISIBLE);
            textView2.setText("\u20B1" + " " + hotPicksModel.getDiscount());
            textView2.setPaintFlags(textView2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else{
            textView2.setText("");
            textView4.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_mensHotImage;
        TextView tv_mensHotPrice, tv_mensHotDisPrice, tv_mensHotItemName, tv_discounted;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_mensHotImage = (ImageView) itemView.findViewById(R.id.iv_mensHotImage);
            tv_mensHotPrice = (TextView) itemView.findViewById(R.id.tv_mensHotPrice);
            tv_mensHotDisPrice = (TextView) itemView.findViewById(R.id.tv_mensHotDisPrice);
            tv_mensHotItemName = (TextView) itemView.findViewById(R.id.tv_mensHotItemName);
            tv_discounted = (TextView) itemView.findViewById(R.id.tv_discounted);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
