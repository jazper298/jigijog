package com.example.jigijog.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Assorted;

import java.util.List;

public class AssortedAdapter extends RecyclerView.Adapter<AssortedAdapter.ViewHolder> {


    private View view;
    private Context mContext;
    private List<Assorted> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public AssortedAdapter(Context context, List<Assorted> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

         view = layoutInflater.inflate(R.layout.listrow_products,parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Assorted assortedModel = mList.get(position);
        ImageView imageView = holder.iv_sportsImage;
        TextView textView1, textView2,textView3,textView4;

        textView1 = holder.tv_sportsPrice;
        textView2 = holder.tv_sportsDisPrice;
        textView3 = holder.tv_sportsItemName;
        textView4 = holder.tv_discounted;
        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + assortedModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_sportsImage);
        double unit_price =  Double.parseDouble(assortedModel.getUnit_price());
        double discount;
        if (assortedModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(assortedModel.getDiscount());
        }
        double total = unit_price - discount;

        textView1.setText("\u20B1" + " " + String.valueOf(total) + "0");

        if (assortedModel.getName().length() <= 24){
            textView3.setText(assortedModel.getName());
        }else{
            textView3.setText(assortedModel.getName().substring(0, 24) + ".." );
        }
        if (!assortedModel.getDiscount().equals("0.00")){
            textView4.setVisibility(View.VISIBLE);
            textView2.setText("\u20B1" + " " + assortedModel.getDiscount());
            textView2.setPaintFlags(textView2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else{
            textView2.setText("");
            textView4.setVisibility(View.GONE);
        }
    }



    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_sportsImage;
        TextView tv_sportsPrice, tv_sportsDisPrice, tv_sportsItemName, tv_discounted;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_sportsImage = (ImageView) itemView.findViewById(R.id.iv_sportsImage);
            tv_sportsPrice = (TextView) itemView.findViewById(R.id.tv_sportsPrice);
            tv_sportsDisPrice = (TextView) itemView.findViewById(R.id.tv_sportsDisPrice);
            tv_sportsItemName = (TextView) itemView.findViewById(R.id.tv_sportsItemName);
            tv_discounted = (TextView) itemView.findViewById(R.id.tv_discounted);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }



}
