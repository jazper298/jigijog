package com.example.jigijog.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.example.jigijog.R;
import com.example.jigijog.activities.CartActivity;
import com.example.jigijog.activities.ProductImagesActivity;
import com.example.jigijog.models.Products;
import com.example.jigijog.utils.Debugger;

import java.util.ArrayList;

public class CustomSwipeAdapter2 extends PagerAdapter {
    private Context context;
    private ArrayList<Products.Photos> mList;
    private LayoutInflater layoutInflater;
    private int current_pos = 0;

    public CustomSwipeAdapter2(Context context, ArrayList<Products.Photos> list){
        this.context = context;
        this.mList = list;
    }
    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == (ConstraintLayout)object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        if(current_pos>mList.size()-1){
            current_pos = 0;
        }
        final Products.Photos photos = mList.get(current_pos);
        current_pos++;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.product_swipe_layout, container, false);
        ImageView imageView = view.findViewById(R.id.imageView1);

        Glide.with(context)
                .load( "https://www.jigijog.com/public/" + photos.getPhoto())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(imageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductImagesActivity.class);
                intent.putExtra("PRODUCTID",photos.getId());
                context.startActivity(intent);
            }
        });
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ConstraintLayout)object);
    }
}
