package com.example.jigijog.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Cart;
import com.example.jigijog.models.CartItem;
import com.example.jigijog.utils.Debugger;

import java.util.ArrayList;

public class CartItemAdapter extends RecyclerView.Adapter<CartItemAdapter.ViewHolder>  {
    private Context mContext;
    private ArrayList<CartItem> mList;
    private OnClickRecyclerView onClickRecyclerView;

    private int itemCount = 0;
    private int test = 0;
    private Cursor mCursor;
    public CartItemAdapter(Context context, ArrayList<CartItem> list) {
        mContext = context;
        mList = list;
        //mCursor = cursor;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_cart,parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final CartItem cartItemModel = mList.get(position);
        ImageView imageView = holder.iv_itemImage;
        final TextView textView1, textView2,textView3,textView4;

        textView1 = holder.tv_itemPrice;
        textView2 = holder.tv_itemDisPrice;
        textView3 = holder.tv_itemName;
        textView4 = holder.tv_yourStore;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + cartItemModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_itemImage);
        double unit_price =  Double.parseDouble(cartItemModel.getUnit_price());
        double discount;
        if (cartItemModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(cartItemModel.getDiscount());
        }
        double total = unit_price - discount;

        textView1.setText("\u20B1" + " " + String.valueOf(total) + "0");
        textView2.setText("QTY :   " + cartItemModel.getProduct_count());
        textView4.setText(cartItemModel.getAdded_by());

        if (cartItemModel.getName().length() <= 24){
            textView3.setText(cartItemModel.getName());
        }else{
            textView3.setText(cartItemModel.getName().substring(0, 24) + ".." );
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_yourStore,tv_itemName,tv_itemPrice,tv_itemDisPrice,tv_itemCount;
        ImageView iv_storeArrow,iv_itemImage;
        Spinner spinner;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_storeArrow =  itemView.findViewById(R.id.iv_storeArrow);
            iv_itemImage =  itemView.findViewById(R.id.iv_itemImage);
            tv_yourStore =  itemView.findViewById(R.id.tv_yourStore);
            tv_itemName =  itemView.findViewById(R.id.tv_itemName);
            tv_itemPrice =  itemView.findViewById(R.id.tv_itemPrice);
            tv_itemDisPrice =  itemView.findViewById(R.id.tv_itemDisPrice);


        }
    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
