package com.example.jigijog.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Products;

import java.util.ArrayList;

public class ItemColorAdapter extends RecyclerView.Adapter<ItemColorAdapter.ViewHolder>  {
    private Context mContext;
    private ArrayList<Products.Colors> mList;
    private OnClickRecyclerView onClickRecyclerView;

    private int itemCount = 0;
    private int test = 0;
    private Cursor mCursor;
    public ItemColorAdapter(Context context, ArrayList<Products.Colors> list) {
        mContext = context;
        mList = list;
        //mCursor = cursor;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_item_color,parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Products.Colors colorsModel = mList.get(position);
        ImageView imageView = holder.iv_color;

        imageView.setBackgroundColor(Integer.parseInt(String.valueOf(Color.parseColor(colorsModel.getColor()))));

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_color;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_color = itemView.findViewById(R.id.iv_color);

            cardView = (CardView) itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
