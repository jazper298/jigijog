package com.example.jigijog.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Categories;

import java.util.ArrayList;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder>{
    private Context mContext;
    private ArrayList<Categories> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public CategoriesAdapter(Context context, ArrayList<Categories> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_categories,parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Categories categoriesModel = mList.get(position);
        ImageView imageView = holder.iv_categoryImage;
        TextView tv_categoryName;

        tv_categoryName = holder.tv_categoryName;
        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + categoriesModel.getBanner())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_categoryImage);
        tv_categoryName.setText(categoriesModel.getName());
    }

    @Override
    public int getItemCount() {
            return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_categoryImage;
        TextView tv_categoryName;
        CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_categoryImage = (ImageView) itemView.findViewById(R.id.iv_categoryImage);
            tv_categoryName = (TextView) itemView.findViewById(R.id.tv_categoryName);

            cardView = (CardView) itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }

    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
