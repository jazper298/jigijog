package com.example.jigijog.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.JustForYou;

import java.util.ArrayList;

public class JustForYouAdapter extends RecyclerView.Adapter<JustForYouAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<JustForYou> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public JustForYouAdapter(Context context, ArrayList<JustForYou> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_just_for_you, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final JustForYou justForYouModel = mList.get(position);
        ImageView imageView1 = holder.iv_justForYouImage1;
        TextView textView1, textView2, textView3,textView4;

        textView1 = holder.tv_justForYouPrice1;
        textView2 = holder.tv_justForYouDisPrice1;
        textView3 = holder.tv_justForYouItemName1;
        textView4 = holder.tv_discounted;
        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + justForYouModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_justForYouImage1);
        double unit_price =  Double.parseDouble(justForYouModel.getUnit_price());
        double discount;
        if (justForYouModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(justForYouModel.getDiscount());
        }

        double total = unit_price - discount;

        textView1.setText("\u20B1" + " " + String.valueOf(total) + "0");
        if (justForYouModel.getName().length() <= 24){
            textView3.setText(justForYouModel.getName());
        }else{
            textView3.setText(justForYouModel.getName().substring(0, 24) + ".." );
        }
        if (!justForYouModel.getDiscount().equals("0.00")){
            textView4.setVisibility(View.VISIBLE);
            textView2.setText("\u20B1" + " " + justForYouModel.getDiscount());
            textView2.setPaintFlags(textView2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else{
            textView2.setText("");
            textView4.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_justForYouImage1;
        TextView tv_justForYouItemName1,tv_justForYouPrice1, tv_justForYouDisPrice1,tv_discounted;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_justForYouImage1 = (ImageView) itemView.findViewById(R.id.iv_justForYouImage1);
            tv_justForYouItemName1 = (TextView) itemView.findViewById(R.id.tv_justForYouItemName1);
            tv_justForYouPrice1 = (TextView) itemView.findViewById(R.id.tv_justForYouPrice1);
            tv_justForYouDisPrice1 = (TextView) itemView.findViewById(R.id.tv_justForYouDisPrice1);
            tv_discounted = (TextView) itemView.findViewById(R.id.tv_discounted);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
