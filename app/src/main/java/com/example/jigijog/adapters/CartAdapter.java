package com.example.jigijog.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Cart;
import com.example.jigijog.utils.Debugger;

import java.util.ArrayList;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder>  {
    private Context mContext;
    private ArrayList<Cart> mList;
    private OnClickRecyclerView onClickRecyclerView;

    private int itemCount = 0;
    private int test = 0;

    public CartAdapter(Context context, ArrayList<Cart> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_cart,parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Cart productsModel = mList.get(position);
        ImageView imageView = holder.iv_itemImage;
        ImageView imageView1 = holder.tv_itemMinus;
        ImageView imageView2 = holder.tv_itemPlus;
        final TextView textView1, textView2,textView3,textView4,textView5;
        RadioGroup radioGroup, radioGroup1;
        RadioButton radioButton,radioButton1;

        textView1 = holder.tv_itemPrice;
        textView2 = holder.tv_itemDisPrice;
        textView3 = holder.tv_itemName;
        textView4 = holder.tv_yourStore;
        textView5 = holder.tv_itemCount;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + productsModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_itemImage);
        double unit_price =  Double.parseDouble(productsModel.getUnit_price());
        double discount;
        if (productsModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(productsModel.getDiscount());
        }
        double total = unit_price - discount;

        textView1.setText("\u20B1" + " " + String.valueOf(total) + "0");
        textView2.setText("\u20B1" + " " + productsModel.getDiscount());
        textView2.setPaintFlags(textView2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        textView4.setText(productsModel.getAdded_by());

        if (productsModel.getName().length() <= 24){
            textView3.setText(productsModel.getName());
        }else{
            textView3.setText(productsModel.getName().substring(0, 24) + ".." );
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RadioGroup radioGroup, radioGroup1;
        RadioButton radioButton,radioButton1;
        TextView tv_yourStore,tv_itemName,tv_itemPrice,tv_itemDisPrice,tv_itemCount;
        ImageView iv_storeArrow,iv_itemImage,tv_itemMinus,tv_itemPlus;
        Spinner spinner;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_storeArrow =  itemView.findViewById(R.id.iv_storeArrow);
            iv_itemImage =  itemView.findViewById(R.id.iv_itemImage);
            tv_yourStore =  itemView.findViewById(R.id.tv_yourStore);
            tv_itemName =  itemView.findViewById(R.id.tv_itemName);
            tv_itemPrice =  itemView.findViewById(R.id.tv_itemPrice);
            tv_itemDisPrice =  itemView.findViewById(R.id.tv_itemDisPrice);

            tv_itemMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            tv_itemPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

        }
    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }


}
