package com.example.jigijog.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.SubCategories;

import java.util.ArrayList;

public class SubCategoriesAdapter extends RecyclerView.Adapter<SubCategoriesAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<SubCategories> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public SubCategoriesAdapter(Context context, ArrayList<SubCategories> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_sub_categories_men, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final SubCategories subCategoriesModel = mList.get(position);
        ImageView imageView = holder.iv_categoryMenImage;
        TextView tv_categoryName;

        tv_categoryName = holder.tv_categoryMenName;
        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + subCategoriesModel.getSlug())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_categoryMenImage);

        tv_categoryName.setText(subCategoriesModel.getName());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_categoryMenImage;
        TextView tv_categoryMenName;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_categoryMenImage = (ImageView) itemView.findViewById(R.id.iv_categoryMenImage);
            tv_categoryMenName = (TextView) itemView.findViewById(R.id.tv_categoryMenName);

            cardView = (CardView) itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }

    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
