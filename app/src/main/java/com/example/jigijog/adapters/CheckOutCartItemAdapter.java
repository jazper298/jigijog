package com.example.jigijog.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.CartItem;
import com.example.jigijog.utils.Debugger;

import java.util.ArrayList;

public class CheckOutCartItemAdapter extends RecyclerView.Adapter<CheckOutCartItemAdapter.ViewHolder>  {
    private Context mContext;
    private ArrayList<CartItem> mList;
    private OnClickRecyclerView onClickRecyclerView;

    private int itemCount = 0;
    private int test = 0;
    private Cursor mCursor;
    public CheckOutCartItemAdapter(Context context, ArrayList<CartItem> list) {
        mContext = context;
        mList = list;
        //mCursor = cursor;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_checkoout_cart,parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final CartItem cartItemModel = mList.get(position);
        ImageView imageView = holder.iv_itemImage;
        final TextView textView1, textView2,textView3;

        textView1 = holder.tv_itemName;
        textView2 = holder.tv_quantity;
        textView3 = holder.tv_itemPrice;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + cartItemModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_itemImage);
        double unit_price =  Double.parseDouble(cartItemModel.getUnit_price());
        double discount;
        if (cartItemModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(cartItemModel.getDiscount());
        }
        double total = unit_price - discount;

        textView3.setText("\u20B1" + " " + String.valueOf(total) + "0");
        textView2.setText(cartItemModel.getProduct_count());
        if (cartItemModel.getName().length() <= 24){
            textView1.setText(cartItemModel.getName());
        }else{
            textView1.setText(cartItemModel.getName().substring(0, 24) + ".." );
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_itemImage;
        TextView tv_itemName,tv_quantity, tv_itemPrice;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_itemImage =  itemView.findViewById(R.id.iv_itemImage);
            tv_itemName =  itemView.findViewById(R.id.tv_itemName);
            tv_quantity =  itemView.findViewById(R.id.tv_quantity);
            tv_itemPrice =  itemView.findViewById(R.id.tv_itemPrice);
        }
    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
