package com.example.jigijog.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Products;
import com.example.jigijog.utils.Debugger;

import java.util.ArrayList;

public class ItemOptionAdapter extends RecyclerView.Adapter<ItemOptionAdapter.ViewHolder>  {
    private Context mContext;
    private ArrayList<Products.Choice_Options.Options> mList;
    private OnClickRecyclerView onClickRecyclerView;

    private int itemCount = 0;
    private int test = 0;
    private Cursor mCursor;
    public ItemOptionAdapter(Context context, ArrayList<Products.Choice_Options.Options> list) {
        mContext = context;
        mList = list;
        //mCursor = cursor;
    }

    @NonNull
    @Override
    public ItemOptionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_item_size,parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Products.Choice_Options.Options optionsModel= mList.get(position);
        final TextView textView1;

        textView1 = holder.tv_size;
        textView1.setText(optionsModel.getOption());
        if(optionsModel.getOption() == null){
            textView1.setText("Not Available ");
        }else{
            textView1.setText(optionsModel.getOption());
        }
        Debugger.logD("optionsModel.getOption() " + optionsModel.getOption() );
    }


    @Override
    public int getItemCount() {
        return mList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_size;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_size = itemView.findViewById(R.id.tv_size);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
