package com.example.jigijog.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog.R;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Assorted;

import java.util.ArrayList;

public class AssordtedGridAdapter extends RecyclerView.Adapter<AssordtedGridAdapter.ViewHolder> {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROGRESS = 0;
    private int item_per_display = 0;
    private Context mContext;
    private ArrayList<Assorted> mList;
    private OnClickRecyclerView onClickRecyclerView;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener = null;
    public interface OnItemClickListener {
        void onItemClick(View view, Assorted obj, int position);
    }
    public AssordtedGridAdapter(Context context, ArrayList<Assorted> list) {
        //this.item_per_display = item_per_display;
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_assorted_products_grid,parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Assorted assortedModel = mList.get(position);
        ImageView imageView = holder.iv_assortedImage;
        TextView textView1, textView2,textView3;

        textView1 = holder.tv_assortedPrice;
        textView2 = holder.tv_assortedDisPrice;
        textView3 = holder.tv_assortedItemName;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + assortedModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_assortedImage);
        double unit_price =  Double.parseDouble(assortedModel.getUnit_price());
        double discount =  Double.parseDouble(assortedModel.getDiscount());
        double total = unit_price - discount;

        textView1.setText("\u20B1" + " " + String.valueOf(total) + "0");
        textView2.setText("\u20B1" + " " + assortedModel.getDiscount());
        if (assortedModel.getName().length() <= 24){
            textView3.setText(assortedModel.getName());
        }else{
            textView3.setText(assortedModel.getName().substring(0, 24) + ".." );
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        ImageView iv_assortedImage;
        TextView tv_assortedPrice,tv_assortedDisPrice, tv_assortedItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_assortedImage = (ImageView) itemView.findViewById(R.id.iv_assortedImage);
            tv_assortedPrice = (TextView) itemView.findViewById(R.id.tv_assortedPrice);
            tv_assortedDisPrice = (TextView) itemView.findViewById(R.id.tv_assortedDisPrice);
            tv_assortedItemName = (TextView) itemView.findViewById(R.id.tv_assortedItemName);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }
    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int current_page);
    }

    private int getLastVisibleItem(int[] into) {
        int last_idx = into[0];
        for (int i : into) {
            if (last_idx < i) last_idx = i;
        }
        return last_idx;
    }
}
