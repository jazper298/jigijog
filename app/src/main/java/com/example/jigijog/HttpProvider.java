package com.example.jigijog;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class HttpProvider {

    //OFFICE IP
    private static final String BASE_URL = "https://jigijog.com/v1/mobile-api/api";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void post(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(context, getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

    public static String getBaseURL() {
        return BASE_URL.replace("api/", "");
    }

    public static String getNewsfeedDir() {
        return BASE_URL.replace("controllerClass/", "newsfeed-files/");
    }

    public static String getProfileDir() {
        return BASE_URL.replace("controllerClass/", "user_images/");
    }

    public static String getSubjectDir() {
        return BASE_URL.replace("controllerClass/", "subject-files/");
    }

    public static String getTopicDir() {
        return BASE_URL.replace("controllerClass/", "topic-files/");
    }

    public static String getQuizDir() {
        return BASE_URL.replace("controllerClass/", "");
    }

}
