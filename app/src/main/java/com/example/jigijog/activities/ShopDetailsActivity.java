package com.example.jigijog.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jigijog.HttpProvider;
import com.example.jigijog.MainActivity;
import com.example.jigijog.R;
import com.example.jigijog.adapters.ShopDetailsPagerAdapter;
import com.example.jigijog.fragments.ShopDetailsChatFragment;
import com.example.jigijog.fragments.ShopDetailsProductFragment;
import com.example.jigijog.fragments.ShopDetailsProfileFragment;
import com.example.jigijog.models.Seller;
import com.example.jigijog.models.ShopDetails;
import com.example.jigijog.utils.Debugger;
import com.google.android.material.tabs.TabLayout;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;

public class ShopDetailsActivity extends AppCompatActivity {
    private View view;

    private Context context;
    private ImageView iv_storeBack, iv_storeMore,iv_storeImage;
    private TextView tv_followStore, tv_followCountStore,tv_storeName, tv_storeRate;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ShopDetailsPagerAdapter shopDetailsPagerAdapter;
    private ShopDetails shopDetails;
    private ArrayList<ShopDetails> shopDetailsArrayList;
    private ArrayList<ShopDetails.Slider> sliderSellerArrayList;
    private String userID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_details);
        context = this;

    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        userID = intent.getStringExtra("USERID");
        initializeUI();
        getShopDetails();
    }

    private void initializeUI() {
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.view_pager);
        iv_storeBack = findViewById(R.id.iv_storeBack);
        iv_storeMore = findViewById(R.id.iv_storeMore);
        iv_storeImage = findViewById(R.id.iv_storeImage);

        tv_followStore = findViewById(R.id.tv_followStore);
        tv_followCountStore = findViewById(R.id.tv_followCountStore);
        tv_storeName = findViewById(R.id.tv_storeName);
        tv_storeRate = findViewById(R.id.tv_storeRate);
        Bundle bundle = new Bundle();
        bundle.putString("USERID", userID);
        shopDetailsPagerAdapter = new ShopDetailsPagerAdapter(getSupportFragmentManager());
        ShopDetailsProductFragment shopDetailsProductFragment = new ShopDetailsProductFragment() ;
        shopDetailsProductFragment.setArguments(bundle);
        shopDetailsPagerAdapter.addFragment(shopDetailsProductFragment, "Products");
        ShopDetailsProfileFragment shopDetailsProfileFragment = new ShopDetailsProfileFragment();
        shopDetailsProfileFragment.setArguments(bundle);
        shopDetailsPagerAdapter.addFragment(shopDetailsProfileFragment, "Profile");
        ShopDetailsChatFragment shopDetailsChatFragment = new ShopDetailsChatFragment();
        shopDetailsChatFragment.setArguments(bundle);
        shopDetailsPagerAdapter.addFragment(shopDetailsChatFragment, "Chat");
        viewPager.setAdapter(shopDetailsPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        createTabIcons();

        iv_storeBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_storeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(context,v);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                //finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                //finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, AccountActivity.class);
                                startActivity(intent2);
                                //finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                //finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }
    private void createTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("Products");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_storage_black_24dp, 0, 0);
        Objects.requireNonNull(tabLayout.getTabAt(0)).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("Profile");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_box_black_24dp, 0, 0);
        Objects.requireNonNull(tabLayout.getTabAt(1)).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText("Chat");
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_chat_black_24dp, 0, 0);
        Objects.requireNonNull(tabLayout.getTabAt(2)).setCustomView(tabThree);

    }

    private void getShopDetails(){
        shopDetailsArrayList = new ArrayList<>();
        RequestParams params = new RequestParams();
        params.put("user_id", userID);
        Debugger.logD("user_ids "+ userID);
        HttpProvider.post(context, "/mobile/shopDetails", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String provider_id = jsonObject.getString("provider_id");
                            String user_type = jsonObject.getString("user_type");
                            String name = jsonObject.getString("name");
                            String email = jsonObject.getString("email");
                            String referred_by = jsonObject.getString("referred_by");
                            String refer_points = jsonObject.getString("refer_points");
                            String ref_id_status = jsonObject.getString("ref_id_status");
                            String email_verified_at = jsonObject.getString("email_verified_at");
                            String password = jsonObject.getString("password");
                            String remember_token = jsonObject.getString("remember_token");
                            String avatar = jsonObject.getString("avatar");
                            String avatar_original = jsonObject.getString("avatar_original");
                            String address = jsonObject.getString("address");
                            String country = jsonObject.getString("country");
                            String province = jsonObject.getString("province");
                            String city = jsonObject.getString("city");
                            String barangay = jsonObject.getString("barangay");
                            String landmark = jsonObject.getString("landmark");
                            String postal_code = jsonObject.getString("postal_code");
                            String phone = jsonObject.getString("phone");
                            String bank_name = jsonObject.getString("bank_name");
                            String account_name = jsonObject.getString("account_name");
                            String account_number = jsonObject.getString("account_number");
                            String user_id = jsonObject.getString("user_id");
                            String logo = jsonObject.getString("logo");
                            String sliders = jsonObject.getString("sliders");
                            String facebook = jsonObject.getString("facebook");
                            String google = jsonObject.getString("google");
                            String twitter = jsonObject.getString("twitter");
                            String youtube = jsonObject.getString("youtube");
                            String instagram = jsonObject.getString("instagram");
                            String slug = jsonObject.getString("slug");

                            ShopDetails sellerModel = new ShopDetails();

                            sliderSellerArrayList = new ArrayList<>();
                            JSONArray jsonArrays = new JSONArray(Arrays.asList(sliders));

                            for(int z=0; z<jsonArrays.length(); z++) {
                                JSONObject jobject = jsonArray.getJSONObject(z);
                                String result = null;
                                String sliders1 = jobject.getString("sliders");
                                if(sliders1 == null || sliders1.isEmpty() || sliders1.contains("[]")){

                                }else{
                                    result = sliders1.substring(2, sliders1.length() -2).replace("\\","");
                                }


                                ShopDetails.Slider sliders2 = new ShopDetails.Slider();
                                sliders2.setSlider(result);

                                sliderSellerArrayList.add(sliders2);

                            }
                            sellerModel.setId(id);
                            sellerModel.setProvider_id(provider_id);
                            sellerModel.setUser_type(user_type);
                            sellerModel.setName(name);
                            sellerModel.setEmail(email);
                            sellerModel.setReferred_by(referred_by);
                            sellerModel.setRefer_points(refer_points);
                            sellerModel.setRef_id_status(ref_id_status);
                            sellerModel.setEmail_verified_at(email_verified_at);
                            sellerModel.setPassword(password);
                            sellerModel.setRemember_token(remember_token);
                            sellerModel.setAvatar(avatar);
                            sellerModel.setAvatar_original(avatar_original);
                            sellerModel.setAddress(address);
                            sellerModel.setCountry(country);
                            sellerModel.setProvince(province);
                            sellerModel.setCity(city);
                            sellerModel.setBarangay(barangay);
                            sellerModel.setLandmark(landmark);
                            sellerModel.setPostal_code(postal_code);
                            sellerModel.setPhone(phone);
                            sellerModel.setBank_name(bank_name);
                            sellerModel.setAccount_name(account_name);
                            sellerModel.setAccount_number(account_number);
                            sellerModel.setUser_id(user_id);
                            sellerModel.setLogo(logo);
                            sellerModel.setFacebook(facebook);
                            sellerModel.setGoogle(google);
                            sellerModel.setTwitter(twitter);
                            sellerModel.setYoutube(youtube);
                            sellerModel.setInstagram(instagram);
                            sellerModel.setSlug(slug);
                            shopDetailsArrayList.add(sellerModel);

                            tv_storeName.setText(name);
                            Glide.with(context)
                                    .load( "https://www.jigijog.com/public/" + logo)
                                    .placeholder(R.drawable.ic_account_box_black_24dp)
                                    .into(iv_storeImage);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("getSellersInfo " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

}
