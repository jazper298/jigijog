package com.example.jigijog.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.jigijog.HttpProvider;
import com.example.jigijog.MainActivity;
import com.example.jigijog.R;
import com.example.jigijog.adapters.ProductsAdapter;
import com.example.jigijog.adapters.ProductsGridAdapter;
import com.example.jigijog.helpers.CartDbHelper;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.CartItem;
import com.example.jigijog.models.Products;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.UserRole;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class ProductCategoryActivity extends AppCompatActivity {
    private View view;
    private Context context;
    private String subcategoryName;
    private String subcategory;
    private int subcategoryID = 0;

    private ImageView iv_productcategoryBack, iv_productcategoySearch, iv_productcategoryCart, iv_productcategoryMore, iv_productcategoryGrid, iv_productcategoryNewest;
    private TextView tv_productcategoryName,tv_productcategoryCart;

    private SwipeRefreshLayout productswipe_Home;
    private RecyclerView productcategoryRecyclerView;

    private ArrayList<Products> productsArrayList;
    private ProductsAdapter productsAdapter;
    private ProductsGridAdapter productsGridAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Products selectedProducts = new Products();
    private ConstraintLayout emptyIndicator;

    private int cartCount = 0;
    private String role;
    private ArrayList<CartItem> cartItemArrayList = new ArrayList<>();
    private int cartCountTotal = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_category);
        context = this;
        role = UserRole.getRole(context);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        subcategoryName = intent.getStringExtra("SUBCATEGORYNAME");
        subcategoryID = Integer.parseInt(subcategory = intent.getStringExtra("SUBCATEGORYID"));

        initializeUI();
        tv_productcategoryName.setText(subcategoryName);
        loadSubcategories();

        checkUserRole();
    }

    private void initializeUI(){
        emptyIndicator = findViewById(R.id.view_Empty);
        tv_productcategoryCart = findViewById(R.id.tv_productcategoryCart);
        iv_productcategoryBack = findViewById(R.id.iv_productcategoryBack);
        iv_productcategoySearch = findViewById(R.id.iv_productcategoySearch);
        iv_productcategoySearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductSearchActivity.class);
                startActivity(intent);
            }
        });
        iv_productcategoryCart = findViewById(R.id.iv_productcategoryCart);
        iv_productcategoryMore = findViewById(R.id.iv_productcategoryMore);
        iv_productcategoryGrid = findViewById(R.id.iv_productcategoryGrid);
        iv_productcategoryNewest = findViewById(R.id.iv_productcategoryNewest);
        tv_productcategoryName = findViewById(R.id.tv_productcategoryName);
        iv_productcategoryBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_productcategoryCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CartActivity.class);
                startActivity(intent);
            }
        });
        iv_productcategoryMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, AccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });
        iv_productcategoryGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadGridSubcategories();
            }
        });
        swipeRefreshLayout = findViewById(R.id.productswipe_Home);
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadSubcategories();
                loadGridSubcategories();
            }
        });
        iv_productcategoryNewest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadSubcategories();
                loadGridSubcategories();
            }
        });

    }

    private void checkUserRole(){
        if (role.equals(UserRole.Customer())) {
            getCartItems();
        } else {
            //checkStudentSession();
        }
    }



    private void getCartItems(){
        CartDbHelper dbHelper = CartDbHelper.getInstance(context);
        cartItemArrayList = dbHelper.getCartItems(Integer.parseInt(UserCustomer.getID(context)));
        cartCountTotal = cartItemArrayList.size();
        if (cartCountTotal == 0){
            tv_productcategoryCart.setText("");
        }else{
            tv_productcategoryCart.setVisibility(View.VISIBLE);
            tv_productcategoryCart.setText(String.valueOf(cartCountTotal));
        }
        Debugger.logD("asdssss" + cartCountTotal);
    }

    private void loadSubcategories(){
        swipeRefreshLayout.setRefreshing(true);
        productcategoryRecyclerView = findViewById(R.id.productcategoryRecyclerView);
        productsArrayList = new ArrayList<>();

        RequestParams params = new RequestParams();
        params.put("subcategory_id", subcategoryID);
        Debugger.logD("subcategoryID " + subcategoryID);

        HttpProvider.post(context, "/mobile/subCategoryProduct", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                swipeRefreshLayout.setRefreshing(false);
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]") || str.equals("{\"message\": {\"text\": \"No data!\"} }")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Products sportsModel = new Products();
                            sportsModel.setId(id);
                            sportsModel.setName(name);
                            sportsModel.setAdded_by(added_by);
                            sportsModel.setCategory_id(category_id);
                            sportsModel.setSubcategory_id(subcategory_id);
                            sportsModel.setSubsubcategory_id(subsubcategory_id);
                            sportsModel.setBrand_id(brand_id);
                            //sportsModel.setPhotos(photos);
                            sportsModel.setThumbnail_img(thumbnail_img);
                            sportsModel.setFeatured_img(featured_img);
                            sportsModel.setFlash_deal_img(flash_deal_img);
                            sportsModel.setVideo_provider(video_provider);
                            sportsModel.setVideo_link(video_link);
                            sportsModel.setTags(tags);
                            sportsModel.setDescription(description);
                            sportsModel.setUnit_price(unit_price);
                            sportsModel.setPurchase_price(purchase_price);
                            //sportsModel.setChoice_options(choice_options);
                            //sportsModel.setColors(colors);
                            //sportsModel.setVariations(variations);
                            sportsModel.setTodays_deal(todays_deal);
                            sportsModel.setPublished(published);
                            sportsModel.setFeatured(featured);
                            sportsModel.setCurrent_stock(current_stock);
                            sportsModel.setUnit(unit);
                            sportsModel.setDiscount(discount);
                            sportsModel.setDiscount_type(discount_type);
                            sportsModel.setTax(tax);
                            sportsModel.setTax_type(tax_type);
                            sportsModel.setShipping_type(shipping_type);
                            sportsModel.setShipping_cost(shipping_cost);
                            sportsModel.setWeight(weight);
                            sportsModel.setParcel_size(parcel_size);
                            sportsModel.setNum_of_sale(num_of_sale);
                            sportsModel.setMeta_title(meta_title);
                            sportsModel.setMeta_description(meta_description);
                            sportsModel.setMeta_img(meta_img);
                            sportsModel.setPdf(pdf);
                            sportsModel.setSlug(slug);
                            sportsModel.setRating(rating);
                            sportsModel.setCreated_at(created_at);
                            sportsModel.setUpdated_at(updated_at);

                            productsArrayList.add(sportsModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        productcategoryRecyclerView.setLayoutManager(layoutManager);

                        productsAdapter = new ProductsAdapter(context, productsArrayList);
                        productsAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedProducts = productsArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMID", selectedProducts.getId());
                                intent.putExtra("ITEMNAME", selectedProducts.getName());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        productcategoryRecyclerView.setAdapter(productsAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    private void loadGridSubcategories(){
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        productcategoryRecyclerView.setLayoutManager(layoutManager);

        productsGridAdapter = new ProductsGridAdapter(context, productsArrayList);
        productsGridAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedProducts = productsArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedProducts.getId());
                intent.putExtra("ITEMNAME", selectedProducts.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        productcategoryRecyclerView.setAdapter(productsGridAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        resumeSubcategories();
        loadGridSubcategories();
    }

    private void resumeSubcategories(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
        productcategoryRecyclerView.setLayoutManager(layoutManager);

        productsAdapter = new ProductsAdapter(context, productsArrayList);
        productsAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedProducts = productsArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedProducts.getId());
                intent.putExtra("ITEMNAME", selectedProducts.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        productcategoryRecyclerView.setAdapter(productsAdapter);
    }

}
