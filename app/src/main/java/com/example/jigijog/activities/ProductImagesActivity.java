package com.example.jigijog.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.jigijog.HttpProvider;
import com.example.jigijog.R;
import com.example.jigijog.adapters.CustomSwipeAdapter2;
import com.example.jigijog.adapters.CustomSwipeAdapter3;
import com.example.jigijog.models.Products;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.utils.ColorUtils;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.Tools;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProductImagesActivity extends AppCompatActivity {
    private View view;
    private Context context;
    private String productID;
    private CircleImageView circleImageView;
    private ViewPager viewPager;
    private ArrayList<Products> productsArrayList;
    private CustomSwipeAdapter3 customSwipeAdapter3;
    private LinearLayout dotsLayout;
    private Products productsModel;
    private ArrayList<Products.Photos> photosArrayList;
    private int custom_pos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_images);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        productID = intent.getStringExtra("PRODUCTID");
        initializeUI();
        loadProductItemByID();
    }

    private void initializeUI() {
        circleImageView = findViewById(R.id.iv_imageClose);
        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void loadProductItemByID(){
        viewPager = findViewById(R.id.view_pager);
        dotsLayout = findViewById(R.id.dotsContainer);
        productsArrayList = new ArrayList<>();
        RequestParams params = new RequestParams();
        params.put("product_id", productID);
        Debugger.logD("asd "+ productID);
        HttpProvider.post(context, "/mobile/selectedProductById", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String user_id = jsonObject.getString("user_id");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");

                            productsModel = new Products();

                            photosArrayList = new ArrayList<>();
                            JSONArray jsonArrays = new JSONArray(Arrays.asList(photos));
                            for(int z=0; z<jsonArrays.length(); z++) {
                                JSONObject jobject = jsonArray.getJSONObject(z);
                                String photos2 = jobject.getString("photos");
                                JSONArray jsonArrayssss = new JSONArray(photos2);
                                if(jsonArrayssss.length() > 0){
                                    for (int h = 0; h < jsonArrayssss.length(); h++) {

                                        Products.Photos photos1 = new Products.Photos();
                                        photos1.setPhoto(jsonArrayssss.optString(h));

                                        photosArrayList.add(photos1);
                                        JSONArray childJsonArray=jsonArrayssss.optJSONArray(h);
                                        if(childJsonArray!=null && childJsonArray.length()>0){
                                            for (int w = 0; w < childJsonArray.length(); w++) {
                                                Debugger.logD( "fuck " + childJsonArray.optString(w));
                                                String sss = childJsonArray.optString(w);
                                                String results = sss.substring(2, photos2.length() -2).replace("\\","");
                                                Debugger.logD( "sss " + sss);
                                                Products.Photos photos1s = new Products.Photos();
                                                photos1s.setPhoto(childJsonArray.optString(w));
                                                photosArrayList.add(photos1s);
                                            }
                                        }
                                        customSwipeAdapter3 = new CustomSwipeAdapter3(context, photosArrayList);
                                        viewPager.setAdapter(customSwipeAdapter3);
                                        loadPreparedDots(custom_pos++);
                                        //loadCreateSlideShow();
                                        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                            @Override
                                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                            }
                                            @Override
                                            public void onPageSelected(int position) {
                                                if (custom_pos>photosArrayList.size()){
                                                    custom_pos = 0;
                                                }
                                                loadPreparedDots(custom_pos++);
                                            }
                                            @Override
                                            public void onPageScrollStateChanged(int state) {
                                            }

                                        });
                                    }
                                }
                            }
                            productsModel.setId(id);
                            productsModel.setName(name);
                            productsModel.setAdded_by(added_by);
                            productsModel.setUser_id(user_id);
                            productsModel.setCategory_id(category_id);
                            productsModel.setSubcategory_id(subcategory_id);
                            productsModel.setSubsubcategory_id(subsubcategory_id);
                            productsModel.setBrand_id(brand_id);
                            productsModel.setThumbnail_img(thumbnail_img);
                            productsModel.setFeatured_img(featured_img);
                            productsModel.setFlash_deal_img(flash_deal_img);
                            productsModel.setVideo_provider(video_provider);
                            productsModel.setVideo_link(video_link);
                            productsModel.setTags(tags);
                            productsModel.setDescription(description);
                            productsModel.setUnit_price(unit_price);
                            productsModel.setPurchase_price(purchase_price);
                            productsModel.setTodays_deal(todays_deal);
                            productsModel.setPublished(published);
                            productsModel.setFeatured(featured);
                            productsModel.setCurrent_stock(current_stock);
                            productsModel.setUnit(unit);
                            productsModel.setDiscount(discount);
                            productsModel.setDiscount_type(discount_type);
                            productsModel.setTax(tax);
                            productsModel.setTax_type(tax_type);
                            productsModel.setShipping_type(shipping_type);
                            productsModel.setShipping_cost(shipping_cost);
                            productsModel.setWeight(weight);
                            productsModel.setParcel_size(parcel_size);
                            productsModel.setNum_of_sale(num_of_sale);
                            productsModel.setMeta_title(meta_title);
                            productsModel.setMeta_description(meta_description);
                            productsModel.setMeta_img(meta_img);
                            productsModel.setPdf(pdf);
                            productsModel.setSlug(slug);
                            productsModel.setRating(rating);
                            productsModel.setCreated_at(created_at);
                            productsModel.setUpdated_at(updated_at);

                            productsArrayList.add(productsModel);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    private void loadPreparedDots(int currentSlidePos){
        if(dotsLayout.getChildCount()>0){
            dotsLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[photosArrayList.size()];
        for (int i =0; i<photosArrayList.size();i++){
            dots[i] = new ImageView(context);
            if(i==currentSlidePos){
                dots[i].setImageDrawable(ContextCompat.getDrawable(context,R.drawable.active_dot));
            }else{
                dots[i].setImageDrawable(ContextCompat.getDrawable(context,R.drawable.inactive_dots));
            }
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            layoutParams.setMargins(4, 0, 4,0);
            dotsLayout.addView(dots[i],layoutParams);
        }
    }
}
