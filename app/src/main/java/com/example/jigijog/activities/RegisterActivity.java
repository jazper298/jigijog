package com.example.jigijog.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.jigijog.HttpProvider;
import com.example.jigijog.R;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.ProgressPopup;
import com.example.jigijog.utils.Tools;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.nio.charset.StandardCharsets;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class RegisterActivity extends AppCompatActivity {
    private View view;
    private Context context;

    private EditText et_RegisterEmail, et_RegisterName, et_RegisterPhone,  et_RegisterPassword, et_RegisterConfirmPassword;
    private Button btn_signup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Tools.setSystemBarColor(this, R.color.deep_orange_300);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();

        initializeUI();
    }
    private void initializeUI(){
        et_RegisterEmail = findViewById(R.id.et_RegisterEmail);
        et_RegisterName = findViewById(R.id.et_RegisterName);
        et_RegisterPhone = findViewById(R.id.et_RegisterPhone);
        et_RegisterPassword = findViewById(R.id.et_RegisterPassword);
        et_RegisterConfirmPassword = findViewById(R.id.et_RegisterConfirmPassword);
        btn_signup = findViewById(R.id.btn_signup);
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fieldsAreEmpty())
                    Toasty.warning(context, "Complete the fields.").show();
                else {
                    if (!et_RegisterEmail.getText().toString().contains("@"))
                        Toasty.warning(context, "Invalid email.").show();
                    else if (!et_RegisterPassword.getText().toString().equals(et_RegisterConfirmPassword.getText().toString()))
                        Toasty.warning(context, "Password didn't match.").show();
                    else {
                        registerUser();
                    }
                }
            }
        });
    }
    private boolean fieldsAreEmpty() {
        if (et_RegisterEmail.getText().toString().isEmpty() &&
                et_RegisterName.getText().toString().isEmpty() &&
                et_RegisterPhone.getText().toString().isEmpty() &&
                et_RegisterPassword.getText().toString().isEmpty() &&
                et_RegisterConfirmPassword.getText().toString().isEmpty())
            return true;
        else
            return false;
    }

    private void registerUser(){
        ProgressPopup.showProgress(context);

        RequestParams params = new RequestParams();
        params.put("user_type", "customer");
        params.put("email", et_RegisterEmail.getText().toString());
        params.put("name", et_RegisterName.getText().toString());
        params.put("phone", et_RegisterPhone.getText().toString());
        params.put("password", et_RegisterPassword.getText().toString());
        params.put("confirm_password", et_RegisterConfirmPassword.getText().toString());

        HttpProvider.post(context, "/mobile/addCustomer", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ProgressPopup.hideProgress();

                String str = new String(responseBody, StandardCharsets.UTF_8);
                Debugger.logD(str);
                if (str.equals("{\"message\": {\"text\": \"Success\"} }")) {
                    Intent intent = new Intent(context, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    Toasty.success(context, "Saved.").show();
                } else
                    et_RegisterEmail.requestFocus();

                //Toasty.warning(context, str).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ProgressPopup.hideProgress();
                Toasty.error(context, "No internet connect. Please try again.").show();
            }
        });
    }

}
