package com.example.jigijog.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jigijog.HttpProvider;
import com.example.jigijog.MainActivity;
import com.example.jigijog.R;
import com.example.jigijog.adapters.CustomSwipeAdapter2;
import com.example.jigijog.adapters.ItemColorAdapter;
import com.example.jigijog.adapters.ItemOptionAdapter;
import com.example.jigijog.adapters.ItemSizeAdapter;
import com.example.jigijog.adapters.RelatedProductsAdapter;
import com.example.jigijog.adapters.TopSellingPerSellerAdapter;
import com.example.jigijog.helpers.CartContract;
import com.example.jigijog.helpers.CartDbHelper;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Cart;
import com.example.jigijog.models.CartItem;
import com.example.jigijog.models.OrderDetails;
import com.example.jigijog.models.Products;
import com.example.jigijog.models.RecentlyView;
import com.example.jigijog.models.RelatedProducts;
import com.example.jigijog.models.Seller;
import com.example.jigijog.models.TopSelling;
import com.example.jigijog.models.User;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.models.WishList;
import com.example.jigijog.utils.ColorUtils;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.Heart;
import com.example.jigijog.utils.ProgressPopup;
import com.example.jigijog.utils.Tools;
import com.example.jigijog.utils.UserRole;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonParser;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

import static com.google.android.material.snackbar.Snackbar.*;
import static java.lang.StrictMath.max;

public class ProductViewActivity extends AppCompatActivity {
    private View view;
    private Context context;
    private ImageView iv_productItemBack, iv_productItemCart, iv_productItemMore, tv_productItemImage, iv_productItemHeart,iv_productItemShare, ivHeartRed, ivHeartWhite,iv_itemStore;
    private TextView tv_productItemItemName, tv_productItemPrice, tv_productItemDisPrice,tv_description,tv_descriptionDis,tv_productcategoryCart,tv_deliveriesDis,tv_productItemStock,tv_installmentDis,tv_shippingRate;
    private Button btn_BuyNow, btn_AddToCart;

    private String itemName, itemPrice, itemDisPrice,itemImage, itemDes, itemID;
    private int productID = 0;
    private ViewPager viewPager;

    private ArrayList<Products> productsArrayList;
    private CustomSwipeAdapter2 customSwipeAdapter2;
    private LinearLayout dotsLayout;
    private int custom_pos = 0;

    private Timer timer;
    private int current_pos = 0;
    private ArrayList<Products.Photos> photosArrayList;
    private ArrayList<Products.Choice_Options> choice_optionsArrayList;
    private ArrayList<Products.Choice_Options.Options> optionsArrayList;
    private ArrayList<Products.Colors> colorsArrayList;
    private ArrayList<Products.Variations> variationsArrayList;
    private ArrayList<Products.Variations.Variation> variationArrayList;
    private Products.Colors productsColor;
    private Products.Choice_Options.Options productsOption;
    private Products.Variations.Variation productsVariation;

    private RecyclerView topSellingRecyclerView;
    private TopSellingPerSellerAdapter topSellingPerSellerAdapter;
    private ArrayList<TopSelling> topSellingArrayList;

    private ArrayList<TopSelling.Photos> topSeelingPhotosArrayList;
    private ArrayList<TopSelling.Choice_Options> topSellingChoice_optionsArrayList;
    private ArrayList<TopSelling.Colors> topSellingColorsArrayList;
    private ArrayList<TopSelling.Variations> topSellingVariationsArrayList;
    private Products productsModel;


    private RecyclerView relatedProdRecyclerView;
    private RelatedProductsAdapter relatedProductsAdapter;
    private ArrayList<RelatedProducts> relatedProductsArrayList;

    private ArrayList<RelatedProducts.Photos> relatedProdPhotosArrayList;
    private ArrayList<RelatedProducts.Choice_Options> relatedProdChoice_optionsArrayList;
    private ArrayList<RelatedProducts.Colors> relatedProdColorsArrayList;
    private ArrayList<RelatedProducts.Variations> relatedProdVariationsArrayList;
    private RelatedProducts relatedProducts;

    private ArrayList<Seller> sellerArrayList;
    private ArrayList<Seller.Slider> sliderSellerArrayList;

    private ConstraintLayout constraintLayout3;
    private String role;
    private ConstraintLayout constraintLayout5;
    private ArrayList<CartItem> cartItemArrayList = new ArrayList<>();
    private int cartCount = 0;
    private int cartCountTotal = 0;
    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private View bottom_sheet;

    private RecyclerView sizeRecyclerView;
    private ItemSizeAdapter itemSizeAdapter;
    private ItemOptionAdapter itemOptionAdapter;
    private RecyclerView colorRecyclerview;
    private ItemColorAdapter itemColorAdapter;
    int colorName;
    String variationDis = "";
    private Heart mHeart;
    private GestureDetector mGestureDetector;
    String selectedOption = "No Size available";
    int itemCount = 1;int countTotal=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_view);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        role = UserRole.getRole(context);
        Intent intent = getIntent();
        productID = Integer.parseInt(itemID = intent.getStringExtra("ITEMID"));
        itemName = intent.getStringExtra("ITEMNAME");
        Debugger.logD(""+ productID);
        initializeUI();
        loadProductItemByID();
        checkUserRole();

    }

    private void initializeUI() {
        bottom_sheet = findViewById(R.id.bottom_sheet);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);
        constraintLayout5 = findViewById(R.id.constraintLayout5);
        viewPager = findViewById(R.id.view_pager);
        dotsLayout = findViewById(R.id.dotsContainer);

        ivHeartRed = (ImageView) findViewById(R.id.ivHeartRed);
        ivHeartWhite = (ImageView) findViewById(R.id.ivHeartWhite);
        tv_productcategoryCart = findViewById(R.id.tv_productcategoryCart);
        iv_productItemBack = findViewById(R.id.iv_productItemBack);
        iv_productItemCart = findViewById(R.id.iv_productItemCart);
        iv_productItemMore = findViewById(R.id.iv_productItemMore);
//        tv_productItemImage = findViewById(R.id.tv_productItemImage);
        iv_productItemShare = findViewById(R.id.iv_productItemShare);
        tv_productItemItemName = findViewById(R.id.tv_productItemItemName);
        tv_productItemPrice = findViewById(R.id.tv_productItemPrice);
        tv_productItemDisPrice = findViewById(R.id.tv_productItemDisPrice);
        tv_description = findViewById(R.id.tv_description);
        btn_BuyNow = findViewById(R.id.btn_BuyNow);
        btn_AddToCart = findViewById(R.id.btn_AddToCart);
        tv_descriptionDis = findViewById(R.id.tv_descriptionDis);
        tv_deliveriesDis = findViewById(R.id.tv_deliveriesDis);
        tv_productItemStock = findViewById(R.id.tv_productItemStock);
        tv_installmentDis = findViewById(R.id.tv_installmentDis);
        tv_shippingRate = findViewById(R.id.tv_shippingRate);
        iv_itemStore = findViewById(R.id.iv_itemStore);
        iv_itemStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ShopDetailsActivity.class);
                intent.putExtra("USERID",productsModel.getUser_id());
                startActivity(intent);
            }
        });
        iv_productItemBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        constraintLayout3  = findViewById(R.id.constraintLayout3);
        constraintLayout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomSheetDialog();
            }
        });
        btn_BuyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (role.equals(UserRole.Customer())){
                    fillCartTable();
                    fillOrderDetails();
                    Intent intent = new Intent(context, CheckOutPageActivity.class);
                    startActivity(intent);
                }else{
                    showDialogLogin();
                }

            }
        });
        ivHeartRed.setVisibility(View.GONE);
        mHeart = new Heart(ivHeartWhite, ivHeartRed);
        mGestureDetector = new GestureDetector(context, new GestureListener());
        toggleWishList();

        btn_AddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (role.equals(UserRole.Customer())){
                    fillCartTable();
                    fillOrderDetails();
                    cartCount++;
                    int totalCartCount = cartCountTotal + cartCount;
                    tv_productcategoryCart.setVisibility(View.VISIBLE);
                    tv_productcategoryCart.setText(String.valueOf(totalCartCount));
                    productsModel.getId();
                }else{
                    showDialogLogin();
                }
            }
        });
        iv_productItemCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cartCountTotal != 0){
                    Intent intent = new Intent(context,CartActivity.class);
                    intent.putExtra("PRODUCTID",productsModel.getId());
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(context,CartActivity.class);
                    startActivity(intent);
                }
            }
        });
        iv_productItemMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, AccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    private void checkUserRole(){
        if (role.equals(UserRole.Customer())) {
            getCartItems();
        } else {
            //checkStudentSession();
        }
    }

    private void getCartItems(){
        CartDbHelper dbHelper = CartDbHelper.getInstance(context);
        cartItemArrayList = dbHelper.getCartItems(Integer.parseInt(UserCustomer.getID(context)));
        cartCountTotal = cartItemArrayList.size();
        if (cartCountTotal == 0){
            tv_productcategoryCart.setText("");
        }else{
            tv_productcategoryCart.setVisibility(View.VISIBLE);
            tv_productcategoryCart.setText(String.valueOf(cartCountTotal));
        }
        Debugger.logD("asdssss" + cartCountTotal);
    }


    String options3, stringColors3, variation;
    Products.Choice_Options.Options[] arrayOfOptions;
    Products.Colors[] arrayOfColors;
    ArrayList<Products.Colors> colorsArrayList2;
    Products.Variations.Variation[] arrayOfVariation;
    String[] array_color;
    String[] array_option;
    String[] array_variation;
    private void loadProductItemByID(){
        viewPager = findViewById(R.id.view_pager);
        dotsLayout = findViewById(R.id.dotsContainer);
        productsArrayList = new ArrayList<>();
        RequestParams params = new RequestParams();
        params.put("product_id", productID);
        Debugger.logD("asd "+ productID);
        HttpProvider.post(context, "/mobile/selectedProductById", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String user_id = jsonObject.getString("user_id");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");

                            productsModel = new Products();

                            photosArrayList = new ArrayList<>();
                            JSONArray jsonArrays = new JSONArray(Arrays.asList(photos));
                            for(int z=0; z<jsonArrays.length(); z++) {
                                JSONObject jobject = jsonArray.getJSONObject(z);
                                String photos2 = jobject.getString("photos");
                                JSONArray jsonArrayssss = new JSONArray(photos2);
                                Debugger.logD("jsonArrayssss  " + jsonArrayssss);
                                if(jsonArrayssss.length() > 0){
                                    for (int h = 0; h < jsonArrayssss.length(); h++) {
                                        Debugger.logD( "fucksss " + jsonArrayssss.optString(h));

                                        Products.Photos photos1 = new Products.Photos();
                                        photos1.setId(id);
                                        photos1.setPhoto(jsonArrayssss.optString(h));

                                        photosArrayList.add(photos1);
                                        JSONArray childJsonArray=jsonArrayssss.optJSONArray(h);
                                        if(childJsonArray!=null && childJsonArray.length()>0){
                                            for (int w = 0; w < childJsonArray.length(); w++) {
                                                Debugger.logD( "fuck " + childJsonArray.optString(w));
                                                String sss = childJsonArray.optString(w);
                                                String results = sss.substring(2, photos2.length() -2).replace("\\","");
                                                Debugger.logD( "sss " + sss);
                                                Products.Photos photos1s = new Products.Photos();
                                                photos1s.setId(id);
                                                photos1s.setPhoto(childJsonArray.optString(w));
                                                photosArrayList.add(photos1s);
                                            }
                                        }
                                        customSwipeAdapter2 = new CustomSwipeAdapter2(context, photosArrayList);
                                        viewPager.setAdapter(customSwipeAdapter2);
                                        loadPreparedDots(custom_pos++);
                                        //loadCreateSlideShow();
                                        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                            @Override
                                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                            }
                                            @Override
                                            public void onPageSelected(int position) {
                                                if (custom_pos>photosArrayList.size()){
                                                    custom_pos = 0;
                                                }
                                                loadPreparedDots(custom_pos++);
                                                Debugger.logD("productsModel " + productsModel.getId());
                                            }
                                            @Override
                                            public void onPageScrollStateChanged(int state) {
                                            }

                                        });
                                    }
                                }
                            }
                            choice_optionsArrayList = new ArrayList<>();
                            JSONArray jsonArray1 = new JSONArray(choice_options);
                            for (int j = 0; j <= jsonArray1.length() - 1; j++) {
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(j);
                                String names = jsonObject1.getString("name");
                                String title = jsonObject1.getString("title");
                                String options = jsonObject1.getString("options");
                                Debugger.logD("options " + options);
                                optionsArrayList = new ArrayList<>();
                                JSONArray jsonArray4 = new JSONArray(options);
                                if(jsonArray4.length() > 0){
                                    for (int h = 0; h < jsonArray4.length(); h++) {
                                        Products.Choice_Options.Options options2 = new Products.Choice_Options.Options();
                                        options2.setOption(jsonArray4.optString(h));
                                        optionsArrayList.add(options2);
                                    }
                                    array_option = new String[optionsArrayList.size()];
                                    for (int x = 0; x < optionsArrayList.size(); x++) {
                                        array_option[x] = optionsArrayList.get(x).getOption().trim().replaceAll(" ","");
                                        Debugger.logD("array_option  " + array_option[x].trim());
                                    }
                                }
                                Products.Choice_Options choice_options2 = new Products.Choice_Options();
                                choice_options2.setName(names);
                                choice_options2.setTitle(title);
                                choice_optionsArrayList.add(choice_options2);
                            }
                            colorsArrayList = new ArrayList<>();
                            JSONArray colorJSONArray = new JSONArray(colors);
                            if(colorJSONArray.length() > 0){
                                for (int n = 0; n <= colorJSONArray.length() - 1; n++) {
                                    Products.Colors colors1s = new Products.Colors();
                                    colors1s.setColor(colorJSONArray.optString(n));
                                    colorsArrayList.add(colors1s);
                                }
                                array_color = new String[colorsArrayList.size()];
                                for (int x = 0; x < colorsArrayList.size(); x++) {
                                    colorName = Color.parseColor(colorsArrayList.get(x).getColor());
                                    array_color[x] =ColorUtils.getColorNameFromHex(colorName);
                                    Debugger.logD("array_color  " + array_color[x]);
                                }
                            }
                            variationsArrayList = new ArrayList<>();
                            ArrayList<String> list = new ArrayList<String>();
                            if (array_color != null && array_option != null){
                                for (String x : array_color){
                                    for (String y : array_option){
                                        list.add(x.trim() + "-" + y.trim());
                                    }
                                }
                            }else if (array_color == null && array_option != null){
                                for (String y : array_option){
                                    list.add(y.trim());
                                }
                            }else if (array_color != null && array_option == null){
                                for (String x : array_color){
                                    list.add(x.trim());
                                }
                            }
                            JSONArray varJsonArray23 = new JSONArray(list);
                            Debugger.logD("varJsonArray23 " + varJsonArray23);

//                            variationsArrayList = new ArrayList<>();
//                            JSONArray varJsonArray2 = new JSONArray(variations);
//                            Debugger.logD("varJsonArray2 " + varJsonArray2);
//                            for (int j = 0; j <= varJsonArray2.length() - 1; j++) {
//                                JSONObject jsonObject1 = varJsonArray2.getJSONObject(0);
//                                String fucks = jsonObject1.getString(String.valueOf(varJsonArray23));
//                                Debugger.logD("s " + fucks);
//                                variationArrayList = new ArrayList<>();
//                                JSONArray jsonArray5 = new JSONArray(fucks);
//                                Debugger.logD("array_variation2 " + jsonArray5);
//                                for (int e = 0; e <= jsonArray5.length() - 1; e++) {
//                                    JSONObject jsonobject = jsonArray5.getJSONObject(e);
//                                    String price = jsonobject.getString("price");
//                                    String sku = jsonobject.getString("sku");
//                                    String qty = jsonobject.getString("qty");
//                                    Products.Variations.Variation vars = new Products.Variations.Variation();
//                                    vars.setPrice(price);
//                                    vars.setSku(sku);
//                                    vars.setQty(qty);
//                                    variationArrayList.add(vars);
//                                    Debugger.logD("FUCK " + productsVariation.getQty());
//                                }
//                                Debugger.logD("FUCK " + variationArrayList.size());
//                            }

                            productsModel.setId(id);
                            productsModel.setName(name);
                            productsModel.setAdded_by(added_by);
                            productsModel.setUser_id(user_id);
                            productsModel.setCategory_id(category_id);
                            productsModel.setSubcategory_id(subcategory_id);
                            productsModel.setSubsubcategory_id(subsubcategory_id);
                            productsModel.setBrand_id(brand_id);
                            productsModel.setThumbnail_img(thumbnail_img);
                            productsModel.setFeatured_img(featured_img);
                            productsModel.setFlash_deal_img(flash_deal_img);
                            productsModel.setVideo_provider(video_provider);
                            productsModel.setVideo_link(video_link);
                            productsModel.setTags(tags);
                            productsModel.setDescription(description);
                            productsModel.setUnit_price(unit_price);
                            productsModel.setPurchase_price(purchase_price);
                            productsModel.setTodays_deal(todays_deal);
                            productsModel.setPublished(published);
                            productsModel.setFeatured(featured);
                            productsModel.setCurrent_stock(current_stock);
                            productsModel.setUnit(unit);
                            productsModel.setDiscount(discount);
                            productsModel.setDiscount_type(discount_type);
                            productsModel.setTax(tax);
                            productsModel.setTax_type(tax_type);
                            productsModel.setShipping_type(shipping_type);
                            productsModel.setShipping_cost(shipping_cost);
                            productsModel.setWeight(weight);
                            productsModel.setParcel_size(parcel_size);
                            productsModel.setNum_of_sale(num_of_sale);
                            productsModel.setMeta_title(meta_title);
                            productsModel.setMeta_description(meta_description);
                            productsModel.setMeta_img(meta_img);
                            productsModel.setPdf(pdf);
                            productsModel.setSlug(slug);
                            productsModel.setRating(rating);
                            productsModel.setCreated_at(created_at);
                            productsModel.setUpdated_at(updated_at);

                            productsArrayList.add(productsModel);

                            tv_installmentDis.setText("Colors, Sizes, Qty, Total " + "\u20B1" + productsModel.getUnit_price() + ".00");

                            tv_productItemPrice.setText("\u20B1" + unit_price + ".00");
                            tv_productItemDisPrice.setText(discount);
                            tv_productItemDisPrice.setPaintFlags(tv_productItemDisPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            if (name.length() <= 24){
                                tv_productItemItemName.setText(name);
                            }else{
                                tv_productItemItemName.setText((name).substring(0, 24) + ".." );
                            }
                            if(description.contains("null")){
                                tv_descriptionDis.setText("No Description Available");
                            }else{
                                tv_descriptionDis.setText(Html.fromHtml(" " + description).toString());
                            }
                            tv_deliveriesDis.setText("Fulfilled by Jigijog available - " + shipping_type );
                            if (current_stock.equals("0")){
                                tv_productItemStock.setText("No Stock Available");
                            }else{
                                tv_productItemStock.setText("( " + current_stock + " available )" );
                            }
                            tv_shippingRate.setText("\u20B1" + shipping_cost);
                            tv_shippingRate.setTextColor(Color.BLACK);
//                            if (current_stock.equals("0")){
//                                btn_AddToCart.setEnabled(false);
//                                btn_AddToCart.setBackgroundColor(Color.LTGRAY);
//                                btn_AddToCart.setTextColor(Color.BLACK);
//                            }else{
//                                btn_AddToCart.setEnabled(true);
//                            }


                            loadTopSellingPerSeller(Integer.parseInt(productsModel.getUser_id()));
                            loadRelatedProducts(Integer.parseInt(productsModel.getCategory_id()));
                            getSellersInfo(Integer.parseInt(productsModel.getUser_id()));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }
    private void loadPreparedDots(int currentSlidePos){
        if(dotsLayout.getChildCount()>0){
            dotsLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[photosArrayList.size()];
        for (int i =0; i<photosArrayList.size();i++){
            dots[i] = new ImageView(context);
            if(i==currentSlidePos){
                dots[i].setImageDrawable(ContextCompat.getDrawable(context,R.drawable.active_dot));
            }else{
                dots[i].setImageDrawable(ContextCompat.getDrawable(context,R.drawable.inactive_dots));
            }
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            layoutParams.setMargins(4, 0, 4,0);
            dotsLayout.addView(dots[i],layoutParams);
        }
    }

    private void getSellersInfo(int userID){
        sellerArrayList = new ArrayList<>();
        RequestParams params = new RequestParams();
        params.put("user_id", userID);
        Debugger.logD("user_ids "+ userID);
        HttpProvider.post(context, "/mobile/shopDetails", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String provider_id = jsonObject.getString("provider_id");
                            String user_type = jsonObject.getString("user_type");
                            String name = jsonObject.getString("name");
                            String email = jsonObject.getString("email");
                            String referred_by = jsonObject.getString("referred_by");
                            String refer_points = jsonObject.getString("refer_points");
                            String ref_id_status = jsonObject.getString("ref_id_status");
                            String email_verified_at = jsonObject.getString("email_verified_at");
                            String password = jsonObject.getString("password");
                            String remember_token = jsonObject.getString("remember_token");
                            String avatar = jsonObject.getString("avatar");
                            String avatar_original = jsonObject.getString("avatar_original");
                            String address = jsonObject.getString("address");
                            String country = jsonObject.getString("country");
                            String province = jsonObject.getString("province");
                            String city = jsonObject.getString("city");
                            String barangay = jsonObject.getString("barangay");
                            String landmark = jsonObject.getString("landmark");
                            String postal_code = jsonObject.getString("postal_code");
                            String phone = jsonObject.getString("phone");
                            String bank_name = jsonObject.getString("bank_name");
                            String account_name = jsonObject.getString("account_name");
                            String account_number = jsonObject.getString("account_number");
                            String user_id = jsonObject.getString("user_id");
                            String logo = jsonObject.getString("logo");
                            String sliders = jsonObject.getString("sliders");
                            String facebook = jsonObject.getString("facebook");
                            String google = jsonObject.getString("google");
                            String twitter = jsonObject.getString("twitter");
                            String youtube = jsonObject.getString("youtube");
                            String instagram = jsonObject.getString("instagram");
                            String slug = jsonObject.getString("slug");
                            Seller sellerModel = new Seller();

                            sliderSellerArrayList = new ArrayList<>();
                            JSONArray jsonArrays = new JSONArray(Arrays.asList(sliders));

                            for(int z=0; z<jsonArrays.length(); z++) {
                                JSONObject jobject = jsonArray.getJSONObject(z);
                                String result = null;
                                String sliders1 = jobject.getString("sliders");
                                if(sliders1 == null || sliders1.isEmpty() || sliders1.contains("[]")){

                                }else{
                                    result = sliders1.substring(2, sliders1.length() -2).replace("\\","");
                                }


                                Seller.Slider sliders2 = new Seller.Slider();
                                sliders2.setSlider(result);

                                sliderSellerArrayList.add(sliders2);

                            }
                            sellerModel.setId(id);
                            sellerModel.setProvider_id(provider_id);
                            sellerModel.setUser_type(user_type);
                            sellerModel.setName(name);
                            sellerModel.setEmail(email);
                            sellerModel.setReferred_by(referred_by);
                            sellerModel.setRefer_points(refer_points);
                            sellerModel.setRef_id_status(ref_id_status);
                            sellerModel.setEmail_verified_at(email_verified_at);
                            sellerModel.setPassword(password);
                            sellerModel.setRemember_token(remember_token);
                            sellerModel.setAvatar(avatar);
                            sellerModel.setAvatar_original(avatar_original);
                            sellerModel.setAddress(address);
                            sellerModel.setCountry(country);
                            sellerModel.setProvince(province);
                            sellerModel.setCity(city);
                            sellerModel.setBarangay(barangay);
                            sellerModel.setLandmark(landmark);
                            sellerModel.setPostal_code(postal_code);
                            sellerModel.setPhone(phone);
                            sellerModel.setBank_name(bank_name);
                            sellerModel.setAccount_name(account_name);
                            sellerModel.setAccount_number(account_number);
                            sellerModel.setUser_id(user_id);
                            sellerModel.setLogo(logo);
                            sellerModel.setFacebook(facebook);
                            sellerModel.setGoogle(google);
                            sellerModel.setTwitter(twitter);
                            sellerModel.setYoutube(youtube);
                            sellerModel.setInstagram(instagram);
                            sellerModel.setSlug(slug);
                            sellerArrayList.add(sellerModel);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("getSellersInfo " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    private void loadTopSellingPerSeller(int user_ids){
        topSellingArrayList = new ArrayList<>();
        topSellingRecyclerView = findViewById(R.id.topSellingRecyclerView);

        RequestParams params = new RequestParams();
        params.put("user_id", user_ids);
        Debugger.logD("user_ids "+ user_ids);
        HttpProvider.post(context, "/mobile/topSellingPerSeller", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String user_id = jsonObject.getString("user_id");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");

                            TopSelling topSellingModel = new TopSelling();

                            topSeelingPhotosArrayList = new ArrayList<>();
                            JSONArray jsonArrays = new JSONArray(Arrays.asList(photos));

                            for(int z=0; z<jsonArrays.length(); z++) {
                                JSONObject jobject = jsonArray.getJSONObject(z);

                                String photos2 = jobject.getString("photos");

                                String result = photos2.substring(2, photos2.length() -2).replace("\\","");

                                TopSelling.Photos photos1 = new TopSelling.Photos();
                                photos1.setPhoto(result);

                                topSeelingPhotosArrayList.add(photos1);

                            }

                            topSellingModel.setId(id);
                            topSellingModel.setName(name);
                            topSellingModel.setAdded_by(added_by);
                            topSellingModel.setUser_id(user_id);
                            topSellingModel.setCategory_id(category_id);
                            topSellingModel.setSubcategory_id(subcategory_id);
                            topSellingModel.setSubsubcategory_id(subsubcategory_id);
                            topSellingModel.setBrand_id(brand_id);
                            topSellingModel.setThumbnail_img(thumbnail_img);
                            topSellingModel.setFeatured_img(featured_img);
                            topSellingModel.setFlash_deal_img(flash_deal_img);
                            topSellingModel.setVideo_provider(video_provider);
                            topSellingModel.setVideo_link(video_link);
                            topSellingModel.setTags(tags);
                            topSellingModel.setDescription(description);
                            topSellingModel.setUnit_price(unit_price);
                            topSellingModel.setPurchase_price(purchase_price);
                            topSellingModel.setTodays_deal(todays_deal);
                            topSellingModel.setPublished(published);
                            topSellingModel.setFeatured(featured);
                            topSellingModel.setCurrent_stock(current_stock);
                            topSellingModel.setUnit(unit);
                            topSellingModel.setDiscount(discount);
                            topSellingModel.setDiscount_type(discount_type);
                            topSellingModel.setTax(tax);
                            topSellingModel.setTax_type(tax_type);
                            topSellingModel.setShipping_type(shipping_type);
                            topSellingModel.setShipping_cost(shipping_cost);
                            topSellingModel.setWeight(weight);
                            topSellingModel.setParcel_size(parcel_size);
                            topSellingModel.setNum_of_sale(num_of_sale);
                            topSellingModel.setMeta_title(meta_title);
                            topSellingModel.setMeta_description(meta_description);
                            topSellingModel.setMeta_img(meta_img);
                            topSellingModel.setPdf(pdf);
                            topSellingModel.setSlug(slug);
                            topSellingModel.setRating(rating);
                            topSellingModel.setCreated_at(created_at);
                            topSellingModel.setUpdated_at(updated_at);

                            topSellingArrayList.add(topSellingModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false);
                        topSellingRecyclerView.setLayoutManager(layoutManager);

                        topSellingPerSellerAdapter = new TopSellingPerSellerAdapter(context, topSellingArrayList);
                        topSellingPerSellerAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                TopSelling topSelling = topSellingArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMID", topSelling.getId());
                                intent.putExtra("ITEMNAME", topSelling.getName());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        topSellingRecyclerView.setAdapter(topSellingPerSellerAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    private void loadRelatedProducts(int categories_ids){
        relatedProductsArrayList = new ArrayList<>();
        relatedProdRecyclerView = findViewById(R.id.relatedProdRecyclerView);

        RequestParams params = new RequestParams();
        params.put("category_id", categories_ids);
        Debugger.logD("category_id "+ categories_ids);
        HttpProvider.post(context, "/mobile/relatedProducts", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String user_id = jsonObject.getString("user_id");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");

                            RelatedProducts relatedProductsModel = new RelatedProducts();

                            relatedProdPhotosArrayList = new ArrayList<>();
                            JSONArray jsonArrays = new JSONArray(Arrays.asList(photos));

                            for(int z=0; z<jsonArrays.length(); z++) {
                                JSONObject jobject = jsonArray.getJSONObject(z);

                                String photos2 = jobject.getString("photos");

                                String result = photos2.substring(2, photos2.length() -2).replace("\\","");

                                RelatedProducts.Photos photos1 = new RelatedProducts.Photos();
                                photos1.setPhoto(result);

                                relatedProdPhotosArrayList.add(photos1);
                            }
                            relatedProductsModel.setId(id);
                            relatedProductsModel.setName(name);
                            relatedProductsModel.setAdded_by(added_by);
                            relatedProductsModel.setUser_id(user_id);
                            relatedProductsModel.setCategory_id(category_id);
                            relatedProductsModel.setSubcategory_id(subcategory_id);
                            relatedProductsModel.setSubsubcategory_id(subsubcategory_id);
                            relatedProductsModel.setBrand_id(brand_id);
                            relatedProductsModel.setThumbnail_img(thumbnail_img);
                            relatedProductsModel.setFeatured_img(featured_img);
                            relatedProductsModel.setFlash_deal_img(flash_deal_img);
                            relatedProductsModel.setVideo_provider(video_provider);
                            relatedProductsModel.setVideo_link(video_link);
                            relatedProductsModel.setTags(tags);
                            relatedProductsModel.setDescription(description);
                            relatedProductsModel.setUnit_price(unit_price);
                            relatedProductsModel.setPurchase_price(purchase_price);
                            relatedProductsModel.setTodays_deal(todays_deal);
                            relatedProductsModel.setPublished(published);
                            relatedProductsModel.setFeatured(featured);
                            relatedProductsModel.setCurrent_stock(current_stock);
                            relatedProductsModel.setUnit(unit);
                            relatedProductsModel.setDiscount(discount);
                            relatedProductsModel.setDiscount_type(discount_type);
                            relatedProductsModel.setTax(tax);
                            relatedProductsModel.setTax_type(tax_type);
                            relatedProductsModel.setShipping_type(shipping_type);
                            relatedProductsModel.setShipping_cost(shipping_cost);
                            relatedProductsModel.setWeight(weight);
                            relatedProductsModel.setParcel_size(parcel_size);
                            relatedProductsModel.setNum_of_sale(num_of_sale);
                            relatedProductsModel.setMeta_title(meta_title);
                            relatedProductsModel.setMeta_description(meta_description);
                            relatedProductsModel.setMeta_img(meta_img);
                            relatedProductsModel.setPdf(pdf);
                            relatedProductsModel.setSlug(slug);
                            relatedProductsModel.setRating(rating);
                            relatedProductsModel.setCreated_at(created_at);
                            relatedProductsModel.setUpdated_at(updated_at);

                            relatedProductsArrayList.add(relatedProductsModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        relatedProdRecyclerView.setLayoutManager(layoutManager);

                        relatedProductsAdapter = new RelatedProductsAdapter(context, relatedProductsArrayList);
                        relatedProductsAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Debugger.logD("shit " );
                                relatedProducts = relatedProductsArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMID", relatedProducts.getId());
                                intent.putExtra("ITEMNAME", relatedProducts.getName());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        relatedProdRecyclerView.setAdapter(relatedProductsAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    private void fillCartTable() {
        view = findViewById(android.R.id.content);
        CartItem cartitems = new CartItem(
                UserCustomer.getID(context),
                productsModel.getId(),
                String.valueOf(itemCount),
                productsModel.getName(),
                productsModel.getAdded_by(),
                productsModel.getUser_id(),
                productsModel.getCategory_id(),
                productsModel.getSubcategory_id(),
                productsModel.getSubsubcategory_id(),
                productsModel.getBrand_id(),
                productsModel.getThumbnail_img(),
                productsModel.getFeatured_img(),
                productsModel.getFlash_deal_img(),
                productsModel.getVideo_provider(),
                productsModel.getVideo_link(),
                productsModel.getTags(),
                productsModel.getDescription(),
                productsModel.getUnit_price(),
                productsModel.getPurchase_price(),
                productsModel.getTodays_deal(),
                productsModel.getPublished(),
                productsModel.getFeatured(),
                productsModel.getCurrent_stock(),
                productsModel.getUnit(),
                productsModel.getDiscount(),
                productsModel.getDiscount_type(),
                productsModel.getTax(),
                productsModel.getTax_type(),
                productsModel.getShipping_type(),
                productsModel.getShipping_cost(),
                productsModel.getWeight(),
                productsModel.getParcel_size(),
                productsModel.getNum_of_sale(),
                productsModel.getMeta_title(),
                productsModel.getMeta_description(),
                productsModel.getMeta_img(),
                productsModel.getPdf(),
                productsModel.getSlug(),
                productsModel.getRating(),
                productsModel.getCreated_at(),
                productsModel.getUpdated_at()
        );

        Debugger.logD("getName " + productsModel.getName());

        Snackbar snackbar = Snackbar.make(view, "Item Added To Cart", Snackbar.LENGTH_LONG)
                .setAction("Go To Cart", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (cartCount != 0){
                            Intent intent = new Intent(context,CartActivity.class);
                            intent.putExtra("USERID",UserCustomer.getID(context));
                            startActivity(intent);
                        }else{
                            Intent intent = new Intent(context,CartActivity.class);
                            startActivity(intent);
                        }
                    }
                });
        snackbar.show();
        insertCartTable(cartitems);
    }
    @SuppressLint("WrongConstant")
    private void insertCartTable(CartItem cartItem) {
        SQLiteDatabase db;
        db = openOrCreateDatabase("MyCart.db", SQLiteDatabase.CREATE_IF_NECESSARY
                , null);
        ContentValues cv = new ContentValues();
        cv.put(CartContract.CartTable.COLUMN_USERID, cartItem.getUser_id());
        cv.put(CartContract.CartTable.COLUMN_PRODCUTID, cartItem.getProduct_id());
        cv.put(CartContract.CartTable.COLUMN_PRODUCTCOUNT, cartItem.getProduct_count());

        cv.put(CartContract.CartTable.COLUMN_NAMES, cartItem.getName());
        cv.put(CartContract.CartTable.COLUMN_ADDEDBY, cartItem.getAdded_by());
        cv.put(CartContract.CartTable.COLUMN_USER_IDS, cartItem.getUser_ids());
        cv.put(CartContract.CartTable.COLUMN_CATEGORY_ID, cartItem.getCategory_id());
        cv.put(CartContract.CartTable.COLUMN_SUBCATEGORY_ID, cartItem.getSubcategory_id());
        cv.put(CartContract.CartTable.COLUMN_SUBSUBCATEGORY_ID, cartItem.getSubsubcategory_id());
        cv.put(CartContract.CartTable.COLUMN_BRAND_ID, cartItem.getBrand_id());
        cv.put(CartContract.CartTable.COLUMN_THUMBNAIL_IMAGE, cartItem.getThumbnail_img());
        cv.put(CartContract.CartTable.COLUMN_FEATURED_IMAGE, cartItem.getFeatured_img());
        cv.put(CartContract.CartTable.COLUMN_FLASH_DEAL_IMAGE, cartItem.getProduct_id());
        cv.put(CartContract.CartTable.COLUMN_VIDEO_PROVIDER, cartItem.getFlash_deal_img());
        cv.put(CartContract.CartTable.COLUMN_VIDEO_LINK, cartItem.getVideo_link());
        cv.put(CartContract.CartTable.COLUMN_TAGS, cartItem.getTags());
        cv.put(CartContract.CartTable.COLUMN_DESCRIPTION, cartItem.getDescription());
        cv.put(CartContract.CartTable.COLUMN_UNIT_PRICE, cartItem.getUnit_price());
        cv.put(CartContract.CartTable.COLUMN_PURCHASE_PRICE, cartItem.getPurchase_price());
        cv.put(CartContract.CartTable.COLUMN_TODAYS_DEAL, cartItem.getTodays_deal());
        cv.put(CartContract.CartTable.COLUMN_PUBLISHED, cartItem.getPublished());
        cv.put(CartContract.CartTable.COLUMN_FEATURED, cartItem.getFeatured());
        cv.put(CartContract.CartTable.COLUMN_CURRENT_STOCK, cartItem.getCurrent_stock());
        cv.put(CartContract.CartTable.COLUMN_UNIT, cartItem.getUnit_price());
        cv.put(CartContract.CartTable.COLUMN_DISCOUNT, cartItem.getDiscount());
        cv.put(CartContract.CartTable.COLUMN_DISCOUNT_TYPE, cartItem.getDiscount_type());
        cv.put(CartContract.CartTable.COLUMN_TAX, cartItem.getTax());
        cv.put(CartContract.CartTable.COLUMN_TAX_TYPE, cartItem.getTax_type());
        cv.put(CartContract.CartTable.COLUMN_SHIPPING_TYPE, cartItem.getShipping_type());
        cv.put(CartContract.CartTable.COLUMN_SHIPPING_COST, cartItem.getShipping_cost());
        cv.put(CartContract.CartTable.COLUMN_WEIGHT, cartItem.getWeight());
        cv.put(CartContract.CartTable.COLUMN_PARCEL_SIZE, cartItem.getParcel_size());
        cv.put(CartContract.CartTable.COLUMN_NUM_OF_SALE, cartItem.getNum_of_sale());
        cv.put(CartContract.CartTable.COLUMN_META_TITLE, cartItem.getMeta_title());
        cv.put(CartContract.CartTable.COLUMN_META_DESCRIPTION, cartItem.getMeta_description());
        cv.put(CartContract.CartTable.COLUMN_META_IMG, cartItem.getMeta_img());
        cv.put(CartContract.CartTable.COLUMN_PDF, cartItem.getPdf());
        cv.put(CartContract.CartTable.COLUMN_SLUG, cartItem.getSlug());
        cv.put(CartContract.CartTable.COLUMN_RATING, cartItem.getRating());
        cv.put(CartContract.CartTable.COLUMN_CREATED_AT, cartItem.getCreated_at());
        cv.put(CartContract.CartTable.COLUMN_UPDATE_AT, cartItem.getUpdated_at());
        db.insert(CartContract.CartTable.TABLE_NAME, null, cv);

    }
    String coupon = null;
    String pick_from = null;
    String pick_to = null;
    private void fillOrderDetails() {
        String vars;
        if (selectedOption.contains("No Size available")){
            vars = ColorUtils.getColorNameFromHex(colorName);
        }else if (ColorUtils.getColorNameFromHex(colorName) == null){
            vars = selectedOption;
        }else{
            vars = variationDis;
        }
        OrderDetails orderDetails = new OrderDetails(
                UserCustomer.getID(context),
                productsModel.getUser_id(),
                productsModel.getId(),
                vars,
                productsModel.getPurchase_price(),
                productsModel.getUnit_price(),
                String.valueOf(countTotal),
                productsModel.getTax(),
                productsModel.getShipping_cost(),
                coupon,
                String.valueOf(itemCount),
                "unpaid",
                "on_review",
                pick_from,
                pick_to,
                "",
                ""
        );
        insertOrderDetailsTable(orderDetails);

    }
    @SuppressLint("WrongConstant")
    private void insertOrderDetailsTable(OrderDetails orderDetails){
        SQLiteDatabase db;
        db = openOrCreateDatabase("MyCart.db", SQLiteDatabase.CREATE_IF_NECESSARY
                , null);
        ContentValues cv = new ContentValues();
        cv.put(CartContract.OrderDetailsTable.COLUMN_USERID, orderDetails.getUser_id());
        cv.put(CartContract.OrderDetailsTable.COLUMN_SELLER_ID, orderDetails.getSeller_id());
        cv.put(CartContract.OrderDetailsTable.COLUMN_PRODUCT_ID, orderDetails.getProduct_id());
        cv.put(CartContract.OrderDetailsTable.COLUMN_VARIATION, orderDetails.getVariation());
        cv.put(CartContract.OrderDetailsTable.COLUMN_BASE_PRICE, orderDetails.getBase_price());
        cv.put(CartContract.OrderDetailsTable.COLUMN_SELLING_PRICE, orderDetails.getSelling_price());
        cv.put(CartContract.OrderDetailsTable.COLUMN_PRICE, orderDetails.getPrice());
        cv.put(CartContract.OrderDetailsTable.COLUMN_TAX, orderDetails.getTax());
        cv.put(CartContract.OrderDetailsTable.COLUMN_SHIPPING_COST, orderDetails.getShipping_cost());
        cv.put(CartContract.OrderDetailsTable.COLUMN_COUPON_DISCOUNT, orderDetails.getCoupon_discount());
        cv.put(CartContract.OrderDetailsTable.COLUMN_QUANTITY, orderDetails.getQuantity());
        cv.put(CartContract.OrderDetailsTable.COLUMN_PAYMENT_STATUS, orderDetails.getPayment_status());
        cv.put(CartContract.OrderDetailsTable.COLUMN_DELIVERY_STATUS, orderDetails.getDelivery_status());
        cv.put(CartContract.OrderDetailsTable.COLUMN_PICK_FROM, orderDetails.getPick_from());
        cv.put(CartContract.OrderDetailsTable.COLUMN_PICK_TO, orderDetails.getPick_to());
        cv.put(CartContract.OrderDetailsTable.COLUMN_CREATED_AT, orderDetails.getCreated_at());
        cv.put(CartContract.OrderDetailsTable.COLUMN_UPDATE_AT, orderDetails.getUpdated_at());
        db.insert(CartContract.OrderDetailsTable.TABLE_NAME, null, cv);

    }

    private void toggleWishList(){
        ivHeartRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        ivHeartRed.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return mGestureDetector.onTouchEvent(event);
            }
        });
        ivHeartWhite.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return mGestureDetector.onTouchEvent(event);
            }
        });
    }
    public class GestureListener extends GestureDetector.SimpleOnGestureListener{
        @Override
        public boolean onDown(MotionEvent e) {
            view = findViewById(android.R.id.content);
            mHeart.toggleLike();
            if (role.equals(UserRole.Customer())){
                if (ivHeartRed.getVisibility() == View.VISIBLE) {
                    Snackbar snackbar = Snackbar.make(view, "Item Added to WishList", Snackbar.LENGTH_LONG)
                            .setAction("Go To WishList", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (cartCount != 0){
                                        Intent intent = new Intent(context,WishListActivity.class);
                                        intent.putExtra("USERID",UserCustomer.getID(context));
                                        startActivity(intent);
                                    }else{
                                        Intent intent = new Intent(context,WishListActivity.class);
                                        startActivity(intent);
                                    }
                                }
                            });
                    snackbar.show();
                }else {
                    Snackbar snackbar = Snackbar.make(view, "Item Remove from WishList", Snackbar.LENGTH_LONG)
                            .setAction("Go To WishList", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (cartCount != 0){
                                        Intent intent = new Intent(context,WishListActivity.class);
                                        intent.putExtra("USERID",UserCustomer.getID(context));
                                        startActivity(intent);
                                    }else{
                                        Intent intent = new Intent(context,WishListActivity.class);
                                        startActivity(intent);
                                    }
                                }
                            });
                    snackbar.show();
                }
            }else{
                showDialogLogin();
            }
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
           // mHeart.toggleLike();


            return true;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveWishList();
        saveRecentViews();
    }

    private void saveWishList(){
        if (ivHeartRed.getVisibility() == View.VISIBLE){
            Debugger.logD("Save to WishList");
            view = findViewById(android.R.id.content);
            WishList wishList = new WishList(
                    UserCustomer.getID(context),
                    productsModel.getId(),
                    String.valueOf(cartCountTotal),
                    productsModel.getName(),
                    productsModel.getAdded_by(),
                    productsModel.getUser_id(),
                    productsModel.getCategory_id(),
                    productsModel.getSubcategory_id(),
                    productsModel.getSubsubcategory_id(),
                    productsModel.getBrand_id(),
                    productsModel.getThumbnail_img(),
                    productsModel.getFeatured_img(),
                    productsModel.getFlash_deal_img(),
                    productsModel.getVideo_provider(),
                    productsModel.getVideo_link(),
                    productsModel.getTags(),
                    productsModel.getDescription(),
                    productsModel.getUnit_price(),
                    productsModel.getPurchase_price(),
                    productsModel.getTodays_deal(),
                    productsModel.getPublished(),
                    productsModel.getFeatured(),
                    productsModel.getCurrent_stock(),
                    productsModel.getUnit(),
                    productsModel.getDiscount(),
                    productsModel.getDiscount_type(),
                    productsModel.getTax(),
                    productsModel.getTax_type(),
                    productsModel.getShipping_type(),
                    productsModel.getShipping_cost(),
                    productsModel.getWeight(),
                    productsModel.getParcel_size(),
                    productsModel.getNum_of_sale(),
                    productsModel.getMeta_title(),
                    productsModel.getMeta_description(),
                    productsModel.getMeta_img(),
                    productsModel.getPdf(),
                    productsModel.getSlug(),
                    productsModel.getRating(),
                    productsModel.getCreated_at(),
                    productsModel.getUpdated_at()
            );
            insertWishListTable(wishList);
        }else{
            Debugger.logD("Not Save to WishList");
        }

    }
    @SuppressLint("WrongConstant")
    private void insertWishListTable(WishList wishList){
        SQLiteDatabase db;
        db = openOrCreateDatabase("MyCart.db", SQLiteDatabase.CREATE_IF_NECESSARY
                , null);
        ContentValues cv = new ContentValues();
        cv.put(CartContract.WishListTable.COLUMN_USERID, wishList.getUser_id());
        cv.put(CartContract.WishListTable.COLUMN_PRODCUTID, wishList.getProduct_id());
        cv.put(CartContract.WishListTable.COLUMN_PRODUCTCOUNT, wishList.getProduct_count());

        cv.put(CartContract.WishListTable.COLUMN_NAMES, wishList.getName());
        cv.put(CartContract.WishListTable.COLUMN_ADDEDBY, wishList.getAdded_by());
        cv.put(CartContract.WishListTable.COLUMN_USER_IDS, wishList.getUser_ids());
        cv.put(CartContract.WishListTable.COLUMN_CATEGORY_ID, wishList.getCategory_id());
        cv.put(CartContract.WishListTable.COLUMN_SUBCATEGORY_ID, wishList.getSubcategory_id());
        cv.put(CartContract.WishListTable.COLUMN_SUBSUBCATEGORY_ID, wishList.getSubsubcategory_id());
        cv.put(CartContract.WishListTable.COLUMN_BRAND_ID, wishList.getBrand_id());
        cv.put(CartContract.WishListTable.COLUMN_THUMBNAIL_IMAGE, wishList.getThumbnail_img());
        cv.put(CartContract.WishListTable.COLUMN_FEATURED_IMAGE, wishList.getFeatured_img());
        cv.put(CartContract.WishListTable.COLUMN_FLASH_DEAL_IMAGE, wishList.getProduct_id());
        cv.put(CartContract.WishListTable.COLUMN_VIDEO_PROVIDER, wishList.getFlash_deal_img());
        cv.put(CartContract.WishListTable.COLUMN_VIDEO_LINK, wishList.getVideo_link());
        cv.put(CartContract.WishListTable.COLUMN_TAGS, wishList.getTags());
        cv.put(CartContract.WishListTable.COLUMN_DESCRIPTION, wishList.getDescription());
        cv.put(CartContract.WishListTable.COLUMN_UNIT_PRICE, wishList.getUnit_price());
        cv.put(CartContract.WishListTable.COLUMN_PURCHASE_PRICE, wishList.getPurchase_price());
        cv.put(CartContract.WishListTable.COLUMN_TODAYS_DEAL, wishList.getTodays_deal());
        cv.put(CartContract.WishListTable.COLUMN_PUBLISHED, wishList.getPublished());
        cv.put(CartContract.WishListTable.COLUMN_FEATURED, wishList.getFeatured());
        cv.put(CartContract.WishListTable.COLUMN_CURRENT_STOCK, wishList.getCurrent_stock());
        cv.put(CartContract.WishListTable.COLUMN_UNIT, wishList.getUnit_price());
        cv.put(CartContract.WishListTable.COLUMN_DISCOUNT, wishList.getDiscount());
        cv.put(CartContract.WishListTable.COLUMN_DISCOUNT_TYPE, wishList.getDiscount_type());
        cv.put(CartContract.WishListTable.COLUMN_TAX, wishList.getTax());
        cv.put(CartContract.WishListTable.COLUMN_TAX_TYPE, wishList.getTax_type());
        cv.put(CartContract.WishListTable.COLUMN_SHIPPING_TYPE, wishList.getShipping_type());
        cv.put(CartContract.WishListTable.COLUMN_SHIPPING_COST, wishList.getShipping_cost());
        cv.put(CartContract.WishListTable.COLUMN_WEIGHT, wishList.getWeight());
        cv.put(CartContract.WishListTable.COLUMN_PARCEL_SIZE, wishList.getParcel_size());
        cv.put(CartContract.WishListTable.COLUMN_NUM_OF_SALE, wishList.getNum_of_sale());
        cv.put(CartContract.WishListTable.COLUMN_META_TITLE, wishList.getMeta_title());
        cv.put(CartContract.WishListTable.COLUMN_META_DESCRIPTION, wishList.getMeta_description());
        cv.put(CartContract.WishListTable.COLUMN_META_IMG, wishList.getMeta_img());
        cv.put(CartContract.WishListTable.COLUMN_PDF, wishList.getPdf());
        cv.put(CartContract.WishListTable.COLUMN_SLUG, wishList.getSlug());
        cv.put(CartContract.WishListTable.COLUMN_RATING, wishList.getRating());
        cv.put(CartContract.WishListTable.COLUMN_CREATED_AT, wishList.getCreated_at());
        cv.put(CartContract.WishListTable.COLUMN_UPDATE_AT, wishList.getUpdated_at());
        db.insert(CartContract.WishListTable.TABLE_NAME, null, cv);
    }

    private void saveRecentViews(){
        RecentlyView recentlyView = new RecentlyView(
                UserCustomer.getID(context),
                productsModel.getId(),
                String.valueOf(cartCountTotal),
                productsModel.getName(),
                productsModel.getAdded_by(),
                productsModel.getUser_id(),
                productsModel.getCategory_id(),
                productsModel.getSubcategory_id(),
                productsModel.getSubsubcategory_id(),
                productsModel.getBrand_id(),
                productsModel.getThumbnail_img(),
                productsModel.getFeatured_img(),
                productsModel.getFlash_deal_img(),
                productsModel.getVideo_provider(),
                productsModel.getVideo_link(),
                productsModel.getTags(),
                productsModel.getDescription(),
                productsModel.getUnit_price(),
                productsModel.getPurchase_price(),
                productsModel.getTodays_deal(),
                productsModel.getPublished(),
                productsModel.getFeatured(),
                productsModel.getCurrent_stock(),
                productsModel.getUnit(),
                productsModel.getDiscount(),
                productsModel.getDiscount_type(),
                productsModel.getTax(),
                productsModel.getTax_type(),
                productsModel.getShipping_type(),
                productsModel.getShipping_cost(),
                productsModel.getWeight(),
                productsModel.getParcel_size(),
                productsModel.getNum_of_sale(),
                productsModel.getMeta_title(),
                productsModel.getMeta_description(),
                productsModel.getMeta_img(),
                productsModel.getPdf(),
                productsModel.getSlug(),
                productsModel.getRating(),
                productsModel.getCreated_at(),
                productsModel.getUpdated_at()
        );
        insertRecentViewsTable(recentlyView);
    }
    @SuppressLint("WrongConstant")
    private void insertRecentViewsTable(RecentlyView recentlyView){
        SQLiteDatabase db;
        db = openOrCreateDatabase("MyCart.db", SQLiteDatabase.CREATE_IF_NECESSARY
                , null);
        ContentValues cv = new ContentValues();
        cv.put(CartContract.RecentViewTable.COLUMN_USERID, recentlyView.getUser_id());
        cv.put(CartContract.RecentViewTable.COLUMN_PRODCUTID, recentlyView.getProduct_id());
        cv.put(CartContract.RecentViewTable.COLUMN_PRODUCTCOUNT, recentlyView.getProduct_count());

        cv.put(CartContract.RecentViewTable.COLUMN_NAMES, recentlyView.getName());
        cv.put(CartContract.RecentViewTable.COLUMN_ADDEDBY, recentlyView.getAdded_by());
        cv.put(CartContract.RecentViewTable.COLUMN_USER_IDS, recentlyView.getUser_ids());
        cv.put(CartContract.RecentViewTable.COLUMN_CATEGORY_ID, recentlyView.getCategory_id());
        cv.put(CartContract.RecentViewTable.COLUMN_SUBCATEGORY_ID, recentlyView.getSubcategory_id());
        cv.put(CartContract.RecentViewTable.COLUMN_SUBSUBCATEGORY_ID, recentlyView.getSubsubcategory_id());
        cv.put(CartContract.RecentViewTable.COLUMN_BRAND_ID, recentlyView.getBrand_id());
        cv.put(CartContract.RecentViewTable.COLUMN_THUMBNAIL_IMAGE, recentlyView.getThumbnail_img());
        cv.put(CartContract.RecentViewTable.COLUMN_FEATURED_IMAGE, recentlyView.getFeatured_img());
        cv.put(CartContract.RecentViewTable.COLUMN_FLASH_DEAL_IMAGE, recentlyView.getProduct_id());
        cv.put(CartContract.RecentViewTable.COLUMN_VIDEO_PROVIDER, recentlyView.getFlash_deal_img());
        cv.put(CartContract.RecentViewTable.COLUMN_VIDEO_LINK, recentlyView.getVideo_link());
        cv.put(CartContract.RecentViewTable.COLUMN_TAGS, recentlyView.getTags());
        cv.put(CartContract.RecentViewTable.COLUMN_DESCRIPTION, recentlyView.getDescription());
        cv.put(CartContract.RecentViewTable.COLUMN_UNIT_PRICE, recentlyView.getUnit_price());
        cv.put(CartContract.RecentViewTable.COLUMN_PURCHASE_PRICE, recentlyView.getPurchase_price());
        cv.put(CartContract.RecentViewTable.COLUMN_TODAYS_DEAL, recentlyView.getTodays_deal());
        cv.put(CartContract.RecentViewTable.COLUMN_PUBLISHED, recentlyView.getPublished());
        cv.put(CartContract.RecentViewTable.COLUMN_FEATURED, recentlyView.getFeatured());
        cv.put(CartContract.RecentViewTable.COLUMN_CURRENT_STOCK, recentlyView.getCurrent_stock());
        cv.put(CartContract.RecentViewTable.COLUMN_UNIT, recentlyView.getUnit_price());
        cv.put(CartContract.RecentViewTable.COLUMN_DISCOUNT, recentlyView.getDiscount());
        cv.put(CartContract.RecentViewTable.COLUMN_DISCOUNT_TYPE, recentlyView.getDiscount_type());
        cv.put(CartContract.RecentViewTable.COLUMN_TAX, recentlyView.getTax());
        cv.put(CartContract.RecentViewTable.COLUMN_TAX_TYPE, recentlyView.getTax_type());
        cv.put(CartContract.RecentViewTable.COLUMN_SHIPPING_TYPE, recentlyView.getShipping_type());
        cv.put(CartContract.RecentViewTable.COLUMN_SHIPPING_COST, recentlyView.getShipping_cost());
        cv.put(CartContract.RecentViewTable.COLUMN_WEIGHT, recentlyView.getWeight());
        cv.put(CartContract.RecentViewTable.COLUMN_PARCEL_SIZE, recentlyView.getParcel_size());
        cv.put(CartContract.RecentViewTable.COLUMN_NUM_OF_SALE, recentlyView.getNum_of_sale());
        cv.put(CartContract.RecentViewTable.COLUMN_META_TITLE, recentlyView.getMeta_title());
        cv.put(CartContract.RecentViewTable.COLUMN_META_DESCRIPTION, recentlyView.getMeta_description());
        cv.put(CartContract.RecentViewTable.COLUMN_META_IMG, recentlyView.getMeta_img());
        cv.put(CartContract.RecentViewTable.COLUMN_PDF, recentlyView.getPdf());
        cv.put(CartContract.RecentViewTable.COLUMN_SLUG, recentlyView.getSlug());
        cv.put(CartContract.RecentViewTable.COLUMN_RATING, recentlyView.getRating());
        cv.put(CartContract.RecentViewTable.COLUMN_CREATED_AT, recentlyView.getCreated_at());
        cv.put(CartContract.RecentViewTable.COLUMN_UPDATE_AT, recentlyView.getUpdated_at());
        db.insert(CartContract.RecentViewTable.TABLE_NAME, null, cv);
    }


    String variationDis2;
    private void showBottomSheetDialog() {
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        final View view = getLayoutInflater().inflate(R.layout.bottom_sheet_item, null);
        sizeRecyclerView = view.findViewById(R.id.sizeRecyclerview);
        colorRecyclerview = view.findViewById(R.id.colorRecyclerview);
        ImageView iv_item = view.findViewById(R.id.iv_item);
        final TextView tv_unitPrice = view.findViewById(R.id.tv_unitPrice);
        TextView tv_discount = view.findViewById(R.id.tv_discount);
        TextView tv_quantity = view.findViewById(R.id.tv_quantity);
        final TextView tv_size = view.findViewById(R.id.tv_size);
        final TextView tv_color = view.findViewById(R.id.tv_color);
        ImageView iv_close = view.findViewById(R.id.iv_close);
        final ImageView tv_itemMinus = view.findViewById(R.id.tv_itemMinus);
        ImageView tv_itemPlus = view.findViewById(R.id.tv_itemPlus);
        final TextView tv_itemCount = view.findViewById(R.id.tv_itemCount);


        tv_itemMinus.setEnabled(false);
        tv_itemPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemCount++;
                countTotal = Integer.parseInt(productsModel.getUnit_price()) * itemCount;
                tv_itemCount.setText("" + itemCount);
                tv_unitPrice.setText("\u20B1" + countTotal+ ".00");
                Debugger.logD(": " + itemCount);
                if(itemCount <= 1){
                    tv_itemMinus.setEnabled(false);
                }else{
                    tv_itemMinus.setEnabled(true);
                }
            }
        });
        tv_itemMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int minusTotal = Integer.parseInt(productsModel.getUnit_price()) * itemCount;
                countTotal = minusTotal - Integer.parseInt(productsModel.getUnit_price());
                itemCount--;
                tv_itemCount.setText("" + itemCount);
                tv_unitPrice.setText("\u20B1" + countTotal + ".00");
                if(itemCount <= 1){
                    tv_itemMinus.setEnabled(false);
                }else{
                    tv_itemMinus.setEnabled(true);
                }

            }
        });

        tv_color.setText("Color");
        //tv_quantity.setText("Quantity " + productsVariation.getQty() + " " + "remaining");
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        tv_unitPrice.setText("\u20B1" + productsModel.getUnit_price()+ ".00");
        tv_discount.setText(productsModel.getDiscount());
        tv_discount.setPaintFlags(tv_discount.getPaintFlags()  | Paint.STRIKE_THRU_TEXT_FLAG);

        Glide.with(context)
                .load( "https://www.jigijog.com/public/" + productsModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(iv_item);
        if(colorsArrayList.isEmpty() || colorsArrayList.contains("[]")){
        }else{
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false);
            colorRecyclerview.setLayoutManager(layoutManager);

            itemColorAdapter = new ItemColorAdapter(context, colorsArrayList);
            itemColorAdapter.setClickListener(new OnClickRecyclerView() {
                @Override
                public void onItemClick(View view, int position) {
                    productsColor = colorsArrayList.get(position);
                    colorName = Color.parseColor(productsColor.getColor());
                    tv_color.setText(ColorUtils.getColorNameFromHex(colorName));
                }
            });
            colorRecyclerview.setAdapter(itemColorAdapter);
        }
        if( optionsArrayList == null){
        }else{
            GridLayoutManager layoutManager1 = new GridLayoutManager(context, 4);
            sizeRecyclerView.setLayoutManager(layoutManager1);

            itemOptionAdapter = new ItemOptionAdapter(context, optionsArrayList);
            itemOptionAdapter.setClickListener(new OnClickRecyclerView() {
                @Override
                public void onItemClick(View view, int position) {
                    if (optionsArrayList == null || optionsArrayList.isEmpty()){
                        selectedOption = "No Size available";
                    }else{
                        productsOption = optionsArrayList.get(position);
                        selectedOption = productsOption.getOption() + ", ";
                    }
                    tv_size.setText(selectedOption);
                }
            });
            sizeRecyclerView.setAdapter(itemOptionAdapter);
        }



        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        // set background transparent
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
                if (selectedOption == null ) {
                    variationDis = "Color " +  ColorUtils.getColorNameFromHex(colorName);
                }else if (ColorUtils.getColorNameFromHex(colorName) == null){
                    variationDis = selectedOption;
                }else {
                    variationDis = ColorUtils.getColorNameFromHex(colorName) +"-"+ selectedOption.replaceAll(" ","");
                }
                tv_installmentDis.setText("Size : " + variationDis + " Qty : " + itemCount + " Total : " + "\u20B1" + countTotal );
                tv_installmentDis.setTextColor(Color.BLACK);
            }
        });
    }
    private EditText et_LoginEmail, et_LoginPassword;
    private Button btn_signin;

    private TextView sign_up;

    private void showDialogLogin(){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        View view = getLayoutInflater().inflate(R.layout.dialog_login, null);
        et_LoginEmail = view.findViewById(R.id.et_LoginEmail);
        et_LoginPassword = view.findViewById(R.id.et_LoginPassword);
        btn_signin = view.findViewById(R.id.btn_signin);
        sign_up = view.findViewById(R.id.sign_up);
        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });
        mBuilder.setView(view);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fieldsAreEmpty())
                    Toasty.warning(context, "Complete the fields.").show();
                else {
                    if (!et_LoginEmail.getText().toString().contains("@"))
                        Toasty.warning(context, "Invalid email.").show();
                    else {
                        loginUser();
                        dialog.hide();
                    }
                }
            }
        });

    }
    private boolean fieldsAreEmpty() {
        if (et_LoginEmail.getText().toString().isEmpty() &&
                et_LoginPassword.getText().toString().isEmpty())
            return true;
        else
            return false;
    }

    private void loginUser(){
        ProgressPopup.showProgress(context);

        RequestParams params = new RequestParams();
        params.put("username", et_LoginEmail.getText().toString());
        params.put("password", et_LoginPassword.getText().toString());

        HttpProvider.post(context, "/mobile/loginUser", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ProgressPopup.hideProgress();

                String str = new String(responseBody, StandardCharsets.UTF_8);
                Debugger.logD("str " + str);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String id = jsonObject.getString("id");
                    String remember_token = jsonObject.getString("remember_token");

                    UserCustomer userCustomer = new UserCustomer();
                    userCustomer.setId(id);
                    userCustomer.setRemember_token(remember_token);

                    getUserDetails(userCustomer);

                } catch (Exception e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ProgressPopup.hideProgress();
                Toasty.error(context, "No internet connect. Please try again.").show();
            }
        });
    }

    private void getUserDetails(final UserCustomer userCustomer){
        ProgressPopup.showProgress(context);

        RequestParams params = new RequestParams();
        params.put("id", userCustomer.getId());
        params.put("remember_token", userCustomer.getRemember_token());

        HttpProvider.post(context, "/mobile/getUserDetails", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ProgressPopup.hideProgress();
                try {
                    String str = new String(responseBody, StandardCharsets.UTF_8);
                    JSONArray jsonArray = new JSONArray(str);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    Debugger.logD("FUCK " + str);

                    String id = jsonObject.getString("id");
                    String provider_id = jsonObject.getString("provider_id");
                    String user_type = jsonObject.getString("user_type");
                    String name = jsonObject.getString("name");
                    String email = jsonObject.getString("email");
                    String referred_by = jsonObject.getString("referred_by");
                    String refer_points = jsonObject.getString("refer_points");
                    String ref_id_status = jsonObject.getString("ref_id_status");
                    String email_verified_at = jsonObject.getString("email_verified_at");
                    String password = jsonObject.getString("password");
                    String remember_token = jsonObject.getString("remember_token");
                    String avatar = jsonObject.getString("avatar");
                    String avatar_original = jsonObject.getString("avatar_original");
                    String address = jsonObject.getString("address");
                    String country = jsonObject.getString("country");
                    String city = jsonObject.getString("city");
                    String barangay = jsonObject.getString("barangay");
                    String landmark = jsonObject.getString("landmark");
                    String postal_code = jsonObject.getString("postal_code");
                    String phone = jsonObject.getString("phone");
                    String bank_name = jsonObject.getString("bank_name");
                    String account_name = jsonObject.getString("account_name");
                    String account_number = jsonObject.getString("account_number");
                    String balance = jsonObject.getString("balance");
                    String created_at = jsonObject.getString("created_at");
                    String updated_at = jsonObject.getString("updated_at");

                    userCustomer.setId(id);
                    userCustomer.setProvider_id(provider_id);
                    userCustomer.setUser_type(user_type);
                    userCustomer.setName(name);
                    userCustomer.setEmail(email);
                    userCustomer.setReferred_by(referred_by);
                    userCustomer.setReferred_points(refer_points);
                    userCustomer.setRef_id_status(ref_id_status);
                    userCustomer.setEmail_verified_at(email_verified_at);
                    userCustomer.setPassword(et_LoginPassword.getText().toString());
                    userCustomer.setRemember_token(remember_token);
                    userCustomer.setAvatar(avatar);
                    userCustomer.setAvatar_original(avatar_original);
                    userCustomer.setAddress(address);
                    userCustomer.setCountry(country);
                    userCustomer.setCity(city);
                    userCustomer.setBarangay(barangay);
                    userCustomer.setLandmark(landmark);
                    userCustomer.setRemember_token(remember_token);
                    userCustomer.setPostal_code(postal_code);
                    userCustomer.setPhone(phone);
                    userCustomer.setBank_name(bank_name);
                    userCustomer.setAccount_name(account_name);
                    userCustomer.setAccount_number(account_number);
                    userCustomer.setRemember_token(remember_token);
                    userCustomer.setBalance(balance);
                    userCustomer.setCreated_at(created_at);
                    userCustomer.setUpdated_at(updated_at);

                    UserRole userRole = new UserRole();
                    userRole.setUserRole(UserRole.Customer());
                    userRole.saveRole(context);

                    userCustomer.saveUserSession(context);
                    fillUserTable();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ProgressPopup.hideProgress();
                Toasty.error(context, "No internet connect. Please try again.").show();
            }
        });
    }
    private void fillUserTable() {
        User q1 = new User(Integer.parseInt(UserCustomer.getID(context)),  UserCustomer.getName(context));

        insertUserTable(q1);
    }
    @SuppressLint("WrongConstant")
    private void insertUserTable(User user ) {
        SQLiteDatabase db;
        db = openOrCreateDatabase("MyCart.db", SQLiteDatabase.CREATE_IF_NECESSARY
                , null);
        ContentValues cv = new ContentValues();
        cv.put(CartContract.UserTable.COLUMN_ID, user.getUser_id());
        cv.put(CartContract.UserTable.COLUMN_NAME, user.getName());
        db.insert(CartContract.UserTable.TABLE_NAME, null, cv);

    }




}
