package com.example.jigijog.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jigijog.HttpProvider;
import com.example.jigijog.MainActivity;
import com.example.jigijog.R;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.ProgressPopup;
import com.example.jigijog.utils.Tools;
import com.example.jigijog.utils.UserRole;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class AccountInfoActivity extends AppCompatActivity {

    private View view;
    private Context context;

    private ImageView iv_settingsBack;
    private TextView tv_accountNames, tv_accountEmails, tv_accountMobiles,tv_accountProvinces,tv_accountCitys,tv_accountBarangays,tv_accountLandmarks;
    private ConstraintLayout constraintLayout6;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_info);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = this;

        String userID = UserCustomer.getID(context);
        String userTokens = UserCustomer.getRememberToken(context);
        Debugger.logD("AccountInfoActivity userID " + userID);
        Debugger.logD("AccountInfoActivity userTokens " + userTokens);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeUI();
    }

    private void initializeUI(){
        iv_settingsBack = findViewById(R.id.iv_settingsBack);
        iv_settingsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_accountNames = findViewById(R.id.tv_accountNames);
        tv_accountEmails = findViewById(R.id.tv_accountEmails);
        tv_accountMobiles = findViewById(R.id.tv_accountMobiles);
        tv_accountProvinces = findViewById(R.id.tv_accountProvinces);
        tv_accountCitys = findViewById(R.id.tv_accountCitys);
        tv_accountBarangays = findViewById(R.id.tv_accountBarangays);
        tv_accountLandmarks = findViewById(R.id.tv_accountLandmarks);

        tv_accountNames.setText(UserCustomer.getName(context));
        tv_accountEmails.setText(UserCustomer.getEmail(context));
        tv_accountMobiles.setText(UserCustomer.getPhone(context));
        tv_accountProvinces.setText(UserCustomer.getProvince(context));
        tv_accountCitys.setText(UserCustomer.getCity(context));
        tv_accountBarangays.setText(UserCustomer.getBarangay(context));
        tv_accountLandmarks.setText(UserCustomer.getLandmark(context));

        constraintLayout6 = findViewById(R.id.constraintLayout6);
        constraintLayout6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }
}
