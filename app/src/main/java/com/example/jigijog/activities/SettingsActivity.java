package com.example.jigijog.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jigijog.MainActivity;
import com.example.jigijog.R;
import com.example.jigijog.fragments.AccountInfoFragment;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.Tools;
import com.example.jigijog.utils.UserRole;
import com.google.android.material.snackbar.Snackbar;

import es.dmoral.toasty.Toasty;

public class SettingsActivity extends AppCompatActivity {
    private View view;
    private Context context;

    private ImageView iv_settingsBack, iv_settingsCountry;
    private TextView tv_accountInfo, tv_accountAddress, tv_accountMessages, tv_accountPolicies, tv_accountHelp,tv_accountLogout;
    private String userID;
    private ConstraintLayout constraintLayout2,constraintLayout8;
    private String role;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        role = UserRole.getRole(context);
        initializeUI();
    }

    private void initializeUI() {
        constraintLayout2 = findViewById(R.id.constraintLayout2);
        constraintLayout8 = findViewById(R.id.constraintLayout8);
        iv_settingsBack = findViewById(R.id.iv_settingsBack);
        iv_settingsCountry = findViewById(R.id.iv_settingsCountry);
        tv_accountInfo = findViewById(R.id.tv_accountInfo);
        tv_accountAddress = findViewById(R.id.tv_accountAddress);
        tv_accountPolicies = findViewById(R.id.tv_accountPolicies);
        tv_accountHelp = findViewById(R.id.tv_accountHelp);
        tv_accountLogout = findViewById(R.id.tv_accountLogout);
        iv_settingsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        constraintLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!UserCustomer.getEmailVerifiedAt(context).contains("null")) {
                    AccountInfoFragment fragment = new AccountInfoFragment();
                    fragment.show(getSupportFragmentManager(), fragment.getTag());
                }else{
                    view = findViewById(android.R.id.content);
                    Snackbar snackbar = Snackbar.make(view, "Please activate your account!", Snackbar.LENGTH_LONG)
                            .setAction("Login", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    startActivity(intent);
                                }
                            });
                    snackbar.show();
                }


            }
        });
        constraintLayout8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialogLogout();
            }
        });
        iv_settingsCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.success(context, "Country Settings").show();
            }
        });
//        tv_accountInfo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toasty.success(context, "Account Info Settings").show();
//            }
//        });
        tv_accountAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.success(context, "Account Address Settings").show();
            }
        });
        tv_accountPolicies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.success(context, "Account Policies Settings").show();
            }
        });
        tv_accountHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.success(context, "Account Help Settings").show();
            }
        });
        tv_accountLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialogLogout();
            }
        });
    }
    private void openDialogLogout() {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle("Logout")
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logoutUser();
                        finish();
                    }
                })
                .setNegativeButton("NO", null)
                .create();
        dialog.show();

    }

    private void logoutUser(){
        UserCustomer.clearSession(context);
        UserRole.clearRole(context);
        MainActivity mainActivity = new MainActivity();
        mainActivity.checkCustomerSession();
    }

}
