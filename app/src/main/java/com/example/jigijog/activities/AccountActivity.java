package com.example.jigijog.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jigijog.HttpProvider;
import com.example.jigijog.R;
import com.example.jigijog.helpers.CartDbHelper;
import com.example.jigijog.models.RecentlyView;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.models.WishList;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.ProgressPopup;
import com.example.jigijog.utils.Tools;
import com.example.jigijog.utils.UserRole;
import com.google.android.material.snackbar.Snackbar;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;

public class AccountActivity extends AppCompatActivity {
    private View view;
    private Context context;

    private ImageView iv_accountSetting,iv_accountBack,iv_accountReturns,iv_accountCancel,iv_accountImage;
    private ImageView iv_accountTopay, iv_accountToship, iv_accountToreceive, iv_accountToreview;
    private TextView tv_accountTopay, tv_accountToship, tv_accountToreceive, tv_accountToreview,iv_accountWishlist,iv_accountRecentlyViewed;
    private TextView tv_accountName, tv_myorders;
    private CardView cardView1, cardView2, cardView3, cardView4, cardView5, cardView6, cardView7, cardView9, cardView10, cardView11, cardView12, cardView13;
    private String role;
    CircleImageView circleImageView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<WishList> wishListArrayList = new ArrayList<>();
    private ArrayList<RecentlyView> recentlyViewArrayList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        Tools.setSystemBarColor(this   , R.color.deep_orange_400);
        Tools.setSystemBarLight(this);
        context = this;
        role = UserRole.getRole(context);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeUI();
        checkUserRole();
        loadUpdatedProfile();
    }

    private void initializeUI () {
        iv_accountWishlist = findViewById(R.id.iv_accountWishlist);
        iv_accountRecentlyViewed = findViewById(R.id.iv_accountRecentlyViewed);
        iv_accountSetting = findViewById(R.id.iv_accountSetting);
        iv_accountBack = findViewById(R.id.iv_accountBack);
        tv_accountName = findViewById(R.id.tv_accountName);
        tv_myorders = findViewById(R.id.tv_myorders);
        circleImageView = findViewById(R.id.iv_accountImage);
        cardView1 = findViewById(R.id.cardView1);//wishlist
        cardView2 = findViewById(R.id.cardView2);//followed seller
        cardView3 = findViewById(R.id.cardView3);//recently viewed
        cardView4 = findViewById(R.id.cardView4);//to pay
        cardView5 = findViewById(R.id.cardView5);//to ship
        cardView6 = findViewById(R.id.cardView6);//to receive
        cardView7 = findViewById(R.id.cardView7);//to review
        cardView9 = findViewById(R.id.cardView9);//my review
        cardView10 = findViewById(R.id.cardView10);//payment option
        cardView11 = findViewById(R.id.cardView11);//help
        cardView12 = findViewById(R.id.cardView12);//chat with our customer
        cardView13 = findViewById(R.id.cardView13);//sell on jigijog
        //wishlist
        cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WishListActivity.class);
                startActivity(intent);
            }
        });
        //followed seller
        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        //recently viewed
        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RecentlyViewActivity.class);
                startActivity(intent);
            }
        });
        //to pay
        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyOrdersActivity.class);
                startActivity(intent);
            }
        });
        //to ship
        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyOrdersActivity.class);
                startActivity(intent);
            }
        });
        //to receive
        cardView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyOrdersActivity.class);
                startActivity(intent);
            }
        });
        //to review
        cardView7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyOrdersActivity.class);
                startActivity(intent);
            }
        });
        //my review
        cardView9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        //payment option
        cardView10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        //help
        cardView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        //chat with our customer
        cardView12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        //sell on jigijog
        cardView13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        tv_myorders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MyOrdersActivity.class);
                startActivity(intent);
            }
        });
        iv_accountSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view = findViewById(android.R.id.content);
                if (role.equals(UserRole.Customer())){
                    Intent intent = new Intent(context, SettingsActivity.class);
                    startActivity(intent);
                }else{
                    Snackbar snackbar = Snackbar.make(view, "Please Login to view profile", Snackbar.LENGTH_LONG)
                            .setAction("Login", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(context,LoginActivity.class);
                                    startActivity(intent);
                                }
                            });
                    snackbar.show();
                }
            }
        });
        tv_accountName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
            }
        });

        tv_myorders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MyOrdersActivity.class);
                startActivity(intent);
            }
        });
        tv_accountName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
            }
        });
        iv_accountBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_accountSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SettingsActivity.class);
                startActivity(intent);
            }
        });
        iv_accountTopay = findViewById(R.id.iv_accountTopay);
        iv_accountToship = findViewById(R.id.iv_accountToship);
        iv_accountToreceive = findViewById(R.id.iv_accountToreceive);
        iv_accountToreview = findViewById(R.id.iv_accountToreview);
        tv_accountTopay = findViewById(R.id.tv_accountTopay);
        tv_accountToship = findViewById(R.id.tv_accountToship);
        tv_accountToreceive = findViewById(R.id.tv_accountToreceive);
        tv_accountToreview = findViewById(R.id.tv_accountToreview);

        swipeRefreshLayout = findViewById(R.id.swipe_Home);
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                tv_accountName.setText(UserCustomer.getName(context));
                loadUpdatedProfile();
            }
        });
    }

    private void checkUserRole(){
        if (role.equals(UserRole.Customer())) {
            checkCustomerSession();
            getWishListItem();
            getRecentlyView();
        } else {
            //checkStudentSession();
        }
    }

    public void checkCustomerSession() {
//        if (!UserRole.getRole(context).isEmpty()) {
//            loginUser();
//        }else{
//
//        }
        String userID = UserCustomer.getID(context);
        Debugger.logD("ACCOUNT checkCustomerSession" + userID);

        if(!userID.equals("")){
            Tools.displayImageRound2(getApplicationContext(), (CircleImageView) findViewById(R.id.iv_accountImage), "https://www.jigijog.com/public/" + UserCustomer.getAvatarOriginal(context));
            tv_accountName.setText(UserCustomer.getName(context));
            Debugger.logD("fuck " + UserCustomer.getName(context) + UserCustomer.getEmail(context));
        }else{
            loginUser();
            UserRole userType = new UserRole();
            userType.setUserRole(UserRole.Customer());
            userType.saveRole(context);

        }


    }

    private void loginUser() {
        ProgressPopup.showProgress(context);

        RequestParams params = new RequestParams();
        params.put("username", UserCustomer.getEmail(context));
        params.put("password", UserCustomer.getPassword(context));

        HttpProvider.post(context, "/mobile/loginUser", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ProgressPopup.hideProgress();
                try {
                    String str = new String(responseBody, StandardCharsets.UTF_8);
                    JSONArray jsonArray = new JSONArray(str);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String id = jsonObject.getString("id");
                    String provider_id = jsonObject.getString("provider_id");
                    String user_type = jsonObject.getString("user_type");
                    String name = jsonObject.getString("name");
                    String email = jsonObject.getString("email");

                    tv_accountName.setText(email);
                    Debugger.logD(name + email);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ProgressPopup.hideProgress();
                Toasty.error(context, "No internet connect. Please try again.").show();
            }
        });
    }

    private void loadUpdatedProfile(){
        swipeRefreshLayout.setRefreshing(true);


        swipeRefreshLayout.setRefreshing(false);
        if(!UserCustomer.getName(context ).isEmpty()){
            tv_accountName.setText(UserCustomer.getName(context));
            Tools.displayImageRound2(context, circleImageView , "https://www.jigijog.com/public/" + UserCustomer.getAvatarOriginal(context));
        }else{
            tv_accountName.setText("Login or Register");
        }

    }
    private void getWishListItem(){
        CartDbHelper dbHelper = CartDbHelper.getInstance(context);
        wishListArrayList = dbHelper.getAllWishList(Integer.parseInt(UserCustomer.getID(context)));
        iv_accountWishlist.setText(String.valueOf( wishListArrayList.size()));
    }
    private void getRecentlyView(){
        CartDbHelper dbHelper = CartDbHelper.getInstance(context);
        recentlyViewArrayList = dbHelper.getAllRecentlyView(Integer.parseInt(UserCustomer.getID(context)));
        iv_accountRecentlyViewed.setText(String.valueOf( recentlyViewArrayList.size()));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
