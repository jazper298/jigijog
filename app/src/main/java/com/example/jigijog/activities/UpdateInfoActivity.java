package com.example.jigijog.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jigijog.HttpProvider;
import com.example.jigijog.MainActivity;
import com.example.jigijog.R;
import com.example.jigijog.adapters.ProductsAdapter;
import com.example.jigijog.adapters.TodaysDealAdapter;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Barangay;
import com.example.jigijog.models.City;
import com.example.jigijog.models.Products;
import com.example.jigijog.models.Province;
import com.example.jigijog.models.Todaysdeal;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.ProgressPopup;
import com.example.jigijog.utils.Tools;
import com.example.jigijog.utils.UserRole;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;

public class UpdateInfoActivity extends AppCompatActivity {
    private String[] array_province ;
    private String[] array_city;
    private String[] array_brgy;
    private Context context;

    private ArrayList<Province> provinceArrayList = new ArrayList<>();
    private ArrayList<City> cityArrayList = new ArrayList<>();
    private ArrayList<Barangay> barangayArrayList = new ArrayList<>();
    private Province province = new Province();
    private City city = new City();
    private Barangay barangay = new Barangay();
    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private View bottom_sheet;

    private AppCompatEditText et_name, et_email, et_phone, et_landmark, et_password ,et_confirm_password;
    private Button btn_province, btn_city,btn_barangay;
    private String getProvCode, getCitymunCode, getBrgyCode, getBrgyDes, getCityDes, getProvDes;
    private CircleImageView image;

    private static final int REQUEST_CAMERA = 5;
    private static final int SELECT_FILE = 0;

    private static final int CAMERA_REQUEST_CODE = 200;
    private static final int STORAGE_REQUEST_CODE = 400;
    private static final int IMAGE_PICK_GALLERY_CODE = 1000;
    private static final int IMAGE_PICK_CAMERA_CODE = 1001;
    private static final int REQUEST_IMAGE_CAPTURE = 1;

    private String cameraPermission[];
    private String storagePermission[];
    private Uri image_uri;

    private List<Bitmap> mL;
    private List<Uri> mL1;
    private Uri selectImageUrl;
    private Bitmap imageBitmap;

    private String mCurrentPhotoPath;

    String picturePath;
    String path;
    String filename;
    String userID;

    private File finalFile = new File("");

    private UserCustomer userCustomer = new UserCustomer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_info);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadAllRegion();
        initToolbar();
        initComponent();

        String userID = UserCustomer.getID(context);
        String userTokens = UserCustomer.getRememberToken(context);
        Debugger.logD("SettingsActivity userID " + userID);
        Debugger.logD("SettingsActivity userTokens " + userTokens);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Manage Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.pink_400);
    }
    public void checkCustomerSession() {
        userID = UserCustomer.getID(context);
        String userTokens = UserCustomer.getRememberToken(context);
        Debugger.logD("MAIN userID " + userID);
        Debugger.logD("MAIN userTokens " + userTokens);
        if(userID.equals("88")){
                loginUser();
        }else{
            UserRole userType = new UserRole();
            userType.setUserRole(UserRole.Customer());
            userType.saveRole(context);
        }

    }
    private void initComponent() {
        bottom_sheet = findViewById(R.id.bottom_sheet);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);
        image = (CircleImageView) findViewById(R.id.image);
//        Tools.displayImageRound2(getApplicationContext(), image , "https://www.jigijog.com/public/" + UserCustomer.getAvatarOriginal(context));
        et_name = (AppCompatEditText) findViewById(R.id.et_name);
        et_name.setText(UserCustomer.getName(context));
        et_email = (AppCompatEditText) findViewById(R.id.et_email);
        et_email.setText(UserCustomer.getEmail(context));
        et_password = (AppCompatEditText) findViewById(R.id.et_password);
        et_confirm_password = (AppCompatEditText) findViewById(R.id.et_confirm_password);
        et_phone = (AppCompatEditText) findViewById(R.id.et_phone);
        //et_phone.setText(UserCustomer.getPhone(context));
        btn_province = (Button) findViewById(R.id.btn_province);
        //btn_province.setText(UserCustomer.getProvince(context));
        btn_city = (Button) findViewById(R.id.btn_city);
        //btn_city.setText(UserCustomer.getCity(context));
        btn_barangay = (Button) findViewById(R.id.btn_barangay);
        //btn_barangay.setText(UserCustomer.getBarangay(context));
        et_landmark = (AppCompatEditText) findViewById(R.id.et_landmark);
        ((Button) findViewById(R.id.btn_province)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProvinceChoiceDialog((Button) v);
            }
        });
        ((Button) findViewById(R.id.btn_city)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCityChoiceDialog((Button) v);
            }
        });
        ((Button) findViewById(R.id.btn_barangay)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBrgyChoiceDialog((Button) v);
            }
        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomSheetDialog();
            }
        });
    }

    private void showProvinceChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
        builder2.setCancelable(true);
        builder2.setSingleChoiceItems(array_province , 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(array_province[which]);
                bt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                province = provinceArrayList.get(which);
                getProvDes = province.getProvDesc();
                Debugger.logD(" getProvDes " + getProvDes);
                getProvCode = province.getProvCode();
                loadSpecificCity(province);

                updateUserProvince();
                userID = UserCustomer.getID(context);
                Debugger.logD("showProvinceChoiceDialog " + userID);
            }
        });
        builder2.show();

    }
    private void showCityChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(array_city , 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(array_city[which]);
                bt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                city = cityArrayList.get(which);
                getCityDes = city.getCitymunDesc();
                getCitymunCode = city.getCitymunCode();
                loadSpecificBrgy(city);

                updateUserCity();
                userID = UserCustomer.getID(context);
                Debugger.logD("showCityChoiceDialog " + userID);
            }
        });
        builder.show();
    }
    private void showBrgyChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(array_brgy , 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(array_brgy[which]);
                bt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                barangay = barangayArrayList.get(which);
                getBrgyDes = barangay.getBrgyDesc();
                getBrgyCode = barangay.getBrgyCode();

                //updateUserBarangay();
                userID = UserCustomer.getID(context);
                Debugger.logD("showBrgyChoiceDialog " + userID);

            }
        });
        builder.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            if (fieldsAreEmpty())
                Toasty.warning(context, "Complete the fields.").show();
            else {
                if (!et_password.getText().toString().equals(et_confirm_password.getText().toString()))
                    Toasty.warning(context, "Password didn't match.").show();
                else{
                    updateUserProfile();
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadAllRegion(){
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("https://jigijog.com/v1/mobile-api/api/mobile/getAllProvince", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String psgcCode = jsonObject.getString("psgcCode");
                        String provDesc = jsonObject.getString("provDesc");
                        String regCode = jsonObject.getString("regCode");
                        String provCode = jsonObject.getString("provCode");

                        Province province = new Province();
                        province.setId(id);
                        province.setPsgcCode(psgcCode);
                        province.setProvDesc(provDesc);
                        province.setRegCode(regCode);
                        province.setProvCode(provCode);

                        provinceArrayList.add(province);

                        array_province = new String[provinceArrayList.size()];
                        for (int x = 0; x < provinceArrayList.size(); x++) {
                            array_province[x] = provinceArrayList.get(x).getProvDesc();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadSpecificCity(final Province province) {
        RequestParams params = new RequestParams();
        params.put("provCode", province.getProvCode());

        HttpProvider.post(context, "/mobile/specificCities", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String psgcCode = jsonObject.getString("psgcCode");
                            String citymunDesc = jsonObject.getString("citymunDesc");
                            String regDesc = jsonObject.getString("regDesc");
                            String provCode = jsonObject.getString("provCode");
                            String citymunCode = jsonObject.getString("citymunCode");

                            City cityModel = new City();
                            cityModel.setId(id);
                            cityModel.setPsgcCode(psgcCode);
                            cityModel.setCitymunDesc(citymunDesc);
                            cityModel.setRegDesc(regDesc);
                            cityModel.setProvCode(provCode);
                            cityModel.setCitymunCode(citymunCode);
                            cityArrayList.add(cityModel);

                            array_city = new String[cityArrayList.size()];
                            for (int x = 0; x < cityArrayList.size(); x++) {
                                array_city[x] = cityArrayList.get(x).getCitymunDesc();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    private void loadSpecificBrgy(final City city){
        RequestParams params = new RequestParams();
        params.put("citymunCode", city.getCitymunCode());

        HttpProvider.post(context, "/mobile/specificBarangays", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String brgyCode = jsonObject.getString("brgyCode");
                            String brgyDesc = jsonObject.getString("brgyDesc");
                            String regCode = jsonObject.getString("regCode");
                            String provCode = jsonObject.getString("provCode");
                            String citymunCode = jsonObject.getString("citymunCode");

                            Barangay barangayModel = new Barangay();
                            barangayModel.setId(id);
                            barangayModel.setBrgyCode(brgyCode);
                            barangayModel.setBrgyDesc(brgyDesc);
                            barangayModel.setRegCode(regCode);
                            barangayModel.setProvCode(provCode);
                            barangayModel.setCitymunCode(citymunCode);
                            barangayArrayList.add(barangayModel);

                            array_brgy = new String[barangayArrayList.size()];
                            for (int x = 0; x < barangayArrayList.size(); x++) {
                                array_brgy[x] = barangayArrayList.get(x).getBrgyDesc();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    private void showBottomSheetDialog() {
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        cameraPermission = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        storagePermission = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

        final View view = getLayoutInflater().inflate(R.layout.profile_bottom_sheet, null);
        ((Button) view.findViewById(R.id.take_photo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkCameraPermission()){
                    requestCameraPermission();
                }else{
                    pickCamera();
                    mBottomSheetDialog.hide();
                }
            }
        });
        ((Button) view.findViewById(R.id.select_album)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkStoragePermission()){
                    requestStoragePermission();
                }else{
                    pickGallery();
                    mBottomSheetDialog.hide();
                }
            }
        });
        ((Button) view.findViewById(R.id.submit_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.hide();
            }
        });

        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        // set background transparent
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });


    }

    private void updateUserProfile(){
        checkCustomerSession();
        ProgressPopup.showProgress(context);

        RequestParams params = new RequestParams();
        params.put("user_id", UserCustomer.getID(context));
        params.put("name", et_name.getText().toString());
        params.put("logo", filename);
        params.put("phone",et_phone.getText().toString());
        params.put("password", et_password.getText().toString());
        params.put("confirm_password", et_confirm_password.getText().toString());
        params.put("province", getProvCode);
        params.put("city", getCitymunCode);
        params.put("barangay", getBrgyCode);
        params.put("landmark",et_landmark.getText().toString());

        HttpProvider.post(context, "/mobile/manageProfile", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ProgressPopup.hideProgress();

                String str = new String(responseBody, StandardCharsets.UTF_8);
                Debugger.logD(str);
                if (str.equals("{\"message\": {\"text\": \"Success\"} }")) {
                    Toasty.success(context, "Profile Updated. Please Signin again").show();
                    //updateUserSession(userID);
                } else
                    ((AppCompatEditText) findViewById(R.id.et_name)).requestFocus();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ProgressPopup.hideProgress();
                Toasty.error(context, "No internet connect. Please try again.").show();
            }
        });
    }
    private boolean fieldsAreEmpty() {
        if (Objects.requireNonNull(et_name.getText()).toString().isEmpty() ||
                Objects.requireNonNull(et_phone.getText()).toString().isEmpty() ||
                Objects.requireNonNull(et_password.getText()).toString().isEmpty() ||
                Objects.requireNonNull(et_confirm_password.getText()).toString().isEmpty() ||
                btn_province.getText().toString().isEmpty() ||
                btn_city.getText().toString().isEmpty() ||
                btn_barangay.getText().toString().isEmpty() ||
                Objects.requireNonNull(et_landmark.getText()).toString().isEmpty()) {
            return true;
        } else
            return false;
    }

    private void pickGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_GALLERY_CODE);
    }

    private void pickCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this, storagePermission, STORAGE_REQUEST_CODE);
    }
    private boolean checkStoragePermission() {
        boolean result2 = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        return  result2;
    }
    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(this, cameraPermission, CAMERA_REQUEST_CODE);
    }
    private boolean checkCameraPermission(){
        boolean result = ContextCompat.checkSelfPermission(context,Manifest.permission.CAMERA) == (PackageManager.PERMISSION_GRANTED);
        boolean result2 = ContextCompat.checkSelfPermission(context,Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        return result && result2;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case CAMERA_REQUEST_CODE:
                if(grantResults.length > 0){
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean writeStorageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if(cameraAccepted && writeStorageAccepted){
                        pickCamera();
                    }else{
                        Toasty.error(context, "permission denied ", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case STORAGE_REQUEST_CODE:
                if(grantResults.length > 0){
                    boolean writeStorageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if( writeStorageAccepted){
                        pickGallery();
                    }else{
                        Toasty.error(context, "permission denied ", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_IMAGE_CAPTURE) {
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            //imageBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
            image.setImageBitmap(imageBitmap);

            Uri tempUri = getImageUri(this, imageBitmap);

            finalFile = new File(getRealPathFromURI(tempUri));

            picturePath = finalFile.toString();
            path = picturePath;
            filename = path.substring(path.lastIndexOf("/")+1);

        }
        else if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_GALLERY_CODE)
        {
            selectImageUrl = data.getData();
            image.setImageURI(selectImageUrl);

            Uri selectedImage = data.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getApplicationContext().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            path = picturePath;
            filename = path.substring(path.lastIndexOf("/")+1);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getApplicationContext().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

//    private void updateUserSession(String userID){
//        UserCustomer userCustomer = new UserCustomer();
//        userCustomer.setId(userID);
//        userCustomer.setProvider_id(UserCustomer.getProviderID(context));
//        userCustomer.setUser_type(UserCustomer.getUserType(context));
//        userCustomer.setName(et_name.getText().toString());
//        userCustomer.setEmail(UserCustomer.getEmail(context));
//        userCustomer.setReferred_by(UserCustomer.getReferredBy(context));
//        //userCustomer.setRefer_pointsref_id_status(refer_pointsref_id_status);
//        //userCustomer.setEmail_verified_at(UserCustomer.getV(context));
//        userCustomer.setPassword(UserCustomer.getPassword(context));
//        userCustomer.setRemember_token(UserCustomer.getRememberToken(context));
//        userCustomer.setAvatar(UserCustomer.getAvatar(context));
//        userCustomer.setAvatar_original(filename);
//        userCustomer.setAddress(UserCustomer.getAddress(context));
//        userCustomer.setCountry(UserCustomer.getCountry(context));
//        userCustomer.setCity(UserCustomer.getCity(context));
//        userCustomer.setBarangay(UserCustomer.getBarangay(context));
//        userCustomer.setLandmark(et_landmark.getText().toString());
//        userCustomer.setPostal_code(UserCustomer.getPostalCode(context));
//        userCustomer.setPhone(et_phone.getText().toString());
//        userCustomer.setBank_name(UserCustomer.getBankName(context));
//        userCustomer.setAccount_name(UserCustomer.getAccountName(context));
//        //userCustomer.setAccount_number(UserCustomer.getN(context));
//        userCustomer.setBalance(UserCustomer.getBalance(context));
//        userCustomer.setLandmark(et_landmark.getText().toString());
//        userCustomer.setCreated_at(UserCustomer.getCreatedAt(context));
//        userCustomer.setUpdated_at(UserCustomer.getUpdatedAt(context));
//        userCustomer.saveUserSession(context);
//
//        Debugger.logD("getID updateUserSession "  + userID);
//
//        setUserFieldsData();
//    }

    private void setUserFieldsData(){
        et_name.setText(UserCustomer.getName(context));
        et_email.setText(UserCustomer.getEmail(context));
        et_phone.setText(UserCustomer.getPhone(context));
        et_landmark.setText(UserCustomer.getLandmark(context));
        Tools.displayImageRound2(getApplicationContext(), image , "https://www.jigijog.com/public/" + UserCustomer.getAvatarOriginal(context));

    }

    private void updateUserProvince(){
        Province province1 = new Province();
        province1.setId(Province.getID(context));
        province1.setProvCode(getProvCode);
        province1.setRegCode(Province.getRegCode(context));
        province1.setProvDesc(getProvDes);
        province1.setPsgcCode(Province.getPsgcCode(context));

        province1.saveUserSession(context);
    }
    private void updateUserCity(){
        City city1 = new City();
        city1.setId(City.getID(context));
        city1.setPsgcCode(City.getPsgcCode(context));
        city1.setCitymunCode(getCitymunCode);
        city1.setRegDesc(City.getRegCode(context));
        city1.setProvCode(City.getProvCode(context));
        city1.setCitymunDesc(getCityDes);
        city1.saveUserSession(context);
    }
    private void updateUserBarangay(){
        Barangay barangay1 = new Barangay();
        barangay1.setBrgyCode(getBrgyCode);
        barangay1.setBrgyDesc(getBrgyDes);
        barangay1.setRegCode(Barangay.getBrgyCode(context));
        barangay1.setProvCode(Barangay.getProvCode(context));
        barangay1.setCitymunCode(Barangay.getCityMunde(context));
        barangay1.saveUserSession(context);
    }

    private void loginUser(){
        ProgressPopup.showProgress(context);

        RequestParams params = new RequestParams();
        params.put("username", userCustomer.getEmail());
        params.put("password", userCustomer.getPassword());

        HttpProvider.post(context, "/mobile/loginUser", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ProgressPopup.hideProgress();

                String str = new String(responseBody, StandardCharsets.UTF_8);
                Debugger.logD("str " + str);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String id = jsonObject.getString("id");
                    String remember_token = jsonObject.getString("remember_token");

                    UserCustomer userCustomer = new UserCustomer();
                    userCustomer.setId(id);
                    userCustomer.setRemember_token(remember_token);

                    getUserDetails(userCustomer);

                } catch (Exception e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ProgressPopup.hideProgress();
                Toasty.error(context, "No internet connect. Please try again.").show();
            }
        });
    }

    private void getUserDetails(final UserCustomer userCustomer){

        ProgressPopup.showProgress(context);

        RequestParams params = new RequestParams();
        params.put("id", userCustomer.getId());
        params.put("remember_token", userCustomer.getRemember_token());
        Debugger.logD("id " + userCustomer.getId());
        Debugger.logD("remember_token " + userCustomer.getRemember_token());

        HttpProvider.post(context, "/mobile/getUserDetails", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ProgressPopup.hideProgress();
                try {
                    String str = new String(responseBody, StandardCharsets.UTF_8);
                    JSONArray jsonArray = new JSONArray(str);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    Debugger.logD("FUCK " + str);

                    String id = jsonObject.getString("id");
                    String provider_id = jsonObject.getString("provider_id");
                    String user_type = jsonObject.getString("user_type");
                    String name = jsonObject.getString("name");
                    String email = jsonObject.getString("email");
                    String referred_by = jsonObject.getString("referred_by");
                    String refer_points = jsonObject.getString("refer_points");
                    String ref_id_status = jsonObject.getString("ref_id_status");
                    String email_verified_at = jsonObject.getString("email_verified_at");
                    String password = jsonObject.getString("password");
                    String remember_token = jsonObject.getString("remember_token");
                    String avatar = jsonObject.getString("avatar");
                    String avatar_original = jsonObject.getString("avatar_original");
                    String address = jsonObject.getString("address");
                    String country = jsonObject.getString("country");
                    String city = jsonObject.getString("city");
                    String barangay = jsonObject.getString("barangay");
                    String landmark = jsonObject.getString("landmark");
                    String postal_code = jsonObject.getString("postal_code");
                    String phone = jsonObject.getString("phone");
                    String bank_name = jsonObject.getString("bank_name");
                    String account_name = jsonObject.getString("account_name");
                    String account_number = jsonObject.getString("account_number");
                    String balance = jsonObject.getString("balance");
                    String created_at = jsonObject.getString("created_at");
                    String updated_at = jsonObject.getString("updated_at");

                    userCustomer.setId(id);
                    userCustomer.setProvider_id(provider_id);
                    userCustomer.setUser_type(user_type);
                    userCustomer.setName(name);
                    userCustomer.setEmail(email);
                    userCustomer.setReferred_by(referred_by);
                    userCustomer.setReferred_points(refer_points);
                    userCustomer.setRef_id_status(ref_id_status);
                    userCustomer.setEmail_verified_at(email_verified_at);
                    userCustomer.setPassword(et_password.getText().toString());
                    userCustomer.setRemember_token(remember_token);
                    userCustomer.setAvatar(avatar);
                    userCustomer.setAvatar_original(avatar_original);
                    userCustomer.setAddress(address);
                    userCustomer.setCountry(country);
                    userCustomer.setCity(city);
                    userCustomer.setBarangay(barangay);
                    userCustomer.setLandmark(landmark);
                    userCustomer.setRemember_token(remember_token);
                    userCustomer.setPostal_code(postal_code);
                    userCustomer.setPhone(phone);
                    userCustomer.setBank_name(bank_name);
                    userCustomer.setAccount_name(account_name);
                    userCustomer.setAccount_number(account_number);
                    userCustomer.setRemember_token(remember_token);
                    userCustomer.setBalance(balance);
                    userCustomer.setCreated_at(created_at);
                    userCustomer.setUpdated_at(updated_at);

                    UserRole userRole = new UserRole();
                    userRole.setUserRole(UserRole.Customer());
                    userRole.saveRole(context);

                    Debugger.logD("FUCK");
                    userCustomer.saveUserSession(context);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ProgressPopup.hideProgress();
                Toasty.error(context, "No internet connect. Please try again.").show();
            }
        });
    }


}
