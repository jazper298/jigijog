package com.example.jigijog.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.jigijog.R;
import com.example.jigijog.adapters.ChatsAdapter;
import com.example.jigijog.adapters.NotificationsAdapter;
import com.example.jigijog.fragments.ChatFragment;
import com.example.jigijog.models.Chats;
import com.example.jigijog.models.Notifications;
import com.example.jigijog.utils.Tools;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class MyOrdersActivity extends AppCompatActivity {
    private View view;
    private Context context;
    private TabLayout tab_layout;
    private ImageView iv_mycartBack;

    private RecyclerView chatRecyclerView;
    private ArrayList<Chats> chatsArrayList;
    private ChatsAdapter chatsAdapter;
    private ArrayList<Notifications> notificationsArrayList;
    private NotificationsAdapter notificationsAdapter;

    private Button tab_all, tab_chats, tab_notification;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeUI();
    }

    private void initializeUI(){
        iv_mycartBack = findViewById(R.id.iv_mycartBack);
        iv_mycartBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
