package com.example.jigijog.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.jigijog.HttpProvider;
import com.example.jigijog.MainActivity;
import com.example.jigijog.R;
import com.example.jigijog.adapters.HotPicksAdapter;
import com.example.jigijog.adapters.JustForYouAdapter;
import com.example.jigijog.adapters.NewArrivalAdapter;
import com.example.jigijog.adapters.SubCategoriesAdapter;
import com.example.jigijog.helpers.CartDbHelper;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Cart;
import com.example.jigijog.models.CartItem;
import com.example.jigijog.models.Categories;
import com.example.jigijog.models.HotPicks;
import com.example.jigijog.models.JustForYou;
import com.example.jigijog.models.NewArrival;
import com.example.jigijog.models.SubCategories;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.Tools;
import com.example.jigijog.utils.UserRole;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class CategoriesActivity extends AppCompatActivity {
    private View view;
    private Context context;
    private  String categoryName;
    private  String category;
    private Categories categories;

    private TextView tv_categoryName;
    private ImageView iv_categoryBack;
    private ImageView iv_categorySearch;
    private ImageView iv_categoryCart;
    private ImageView iv_categoryMore;

    private TextView meanHotPicks,menFeaturedBrands,newArrivals,menJustForYou,tv_productcategoryCart;
    private TextView hotPicksSee,newArrivalSee;


    //Hot Picks
    private ImageView imageView1,imageView2,imageView3;
    private TextView textView1,textView2,textView3,textView4,textView5,textView6,textView7,textView8,textView9;

    //Featured Products
    private ImageView featuredImage1, featuredImage2, featuredImage3, featuredImage4;
    private ImageView featuredIcon1, featuredIcon2, featuredIcon3, featuredIcon4;
    private TextView featuredBrand1, featuredBrand2, featuredBrand3, featuredBrand4;

    //New Arrivals
    private RecyclerView newArrivalYouRecyclerView;
    private ArrayList<NewArrival> newArrivalArrayList;
    private NewArrivalAdapter newArrivalAdapter;
    private NewArrival selectedNewArrival;

    private RecyclerView categoryMenRecyclerview;

    private RecyclerView justForYouRecyvlerview;
    private ArrayList<JustForYou> justForYouArrayList;
    private JustForYouAdapter justForYouAdapter;
    private JustForYou selectedJustForYou;

    private RecyclerView hotpicksRecyclerview;
    private ArrayList<HotPicks> hotPicksArrayList;
    private HotPicksAdapter hotPicksAdapter;
    private HotPicks selectedHotPicks;

    private ArrayList<SubCategories> subCategoriesArrayList;
    private SubCategoriesAdapter subCategoriesAdapter;
    private FragmentManager fragmentManager;
    private SubCategories selectedSubcategories;

    int defaultValue = 0;
    int categoryID = 0;
    private String role;
    private ArrayList<CartItem> cartItemArrayList = new ArrayList<>();
    private int cartCountTotal = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        context = getApplicationContext();
//        loadCategoryMen();
        Intent intent = getIntent();
        role = intent.getStringExtra("ROLE_ID");
        categoryName = intent.getStringExtra("CATEGORYNAME");
        categoryID = Integer.parseInt(category = intent.getStringExtra("CATEGORYID"));
        Debugger.logD("CategoriesActivitys " + role);
        checkUserRole();
        initializeUI();
        //getCartItems();

        switch (categoryID){
            case 2:
                //loadCategoryWomen();
                loadAllCategories();
                meanHotPicks.setText("Women's Hot Picks");
                loadWomenHotPicks();
                loadWomenFeaturedBrand();
                loadWomenNewArrivals();
                loadWomenJustForYou();
                break;
            case 3:
                //loadCategoryMen();
                loadAllCategories();
                meanHotPicks.setText("Men's Hot Picks");
                loadMensHotPicks();
                loadMensFeaturedBrand();
                loadMensNewArrivals();
                loadMensJustForYou();
                break;
            case 4:
                //loadCategoryComputer();
                loadAllCategories();
                loadComputerHotPicks();
                loadComputerFeaturedBrand();
                loadComputerNewArrivals();
                loadComputerJustForYou();
                meanHotPicks.setText("Computer Hot Picks");
                break;
            case 5:
                //loadCategoryKids();
                loadAllCategories();
                meanHotPicks.setText("Kids Hot Picks");
                loadKidsHotPicks();
                loadKidsFeaturedBrand();
                loadKidsNewArrivals();
                loadKidsJustForYou();
                break;
            case 6:
                //loadCategorySports();
                loadAllCategories();
                meanHotPicks.setText("Sports Hot Picks");
                loadSportsHotPicks();
                loadSportsFeaturedBrand();
                loadSportsNewArrivals();
                loadSportsJustForYou();
                break;
            case 7:
                //loadCategoryJewelry();
                loadAllCategories();
                meanHotPicks.setText("Jewelries Hot Picks");
                loadJewelryHotPicks();
                loadJewelryFeaturedBrand();
                loadJewelryNewArrivals();
                loadJewelryJustForYou();
                break;
            case 8:
                //loadCategoryMobile();
                loadAllCategories();
                meanHotPicks.setText("Mobile Hot Picks");
                loadMobileHotPicks();
                loadMobileFeaturedBrand();
                loadMobileNewArrivals();
                loadMobileJustForYou();
                break;
            case 9:
                //loadCategoryHealth();
                loadAllCategories();
                meanHotPicks.setText("Health's Hot Picks");
                loadHealthHotPicks();
                loadHealthFeaturedBrand();
                loadHealthNewArrivals();
                loadHealthJustForYou();
                break;
            case 10:
                //loadCategoryHardware();
                loadAllCategories();
                meanHotPicks.setText("Hardware Hot Picks");
                loadHardwareHotPicks();
                loadHardwareFeatureBrand();
                loadHardwareNewArrivals();
                loadHardwareJustForYou();
                break;
            case 11:
                //loadCategoryHome();
                loadAllCategories();
                meanHotPicks.setText("Home Hot Picks");
                loadHomeHotPicks();
                loadHomeFeaturedBrand();
                loadHomeNewArrivals();
                loadHomeJustForYou();
                break;
            case 12:
                //loadCategoryHome();
                loadAllCategories();
                meanHotPicks.setText("Automobile Hot Picks");
                loadAutomobileHotPicks();
                loadAutomobileFeaturedBrand();
                loadAutomobileNewArrivals();
                loadAutomobileJustForYou();
                break;
            default:
        }

    }

    private void initializeUI() {
        tv_productcategoryCart = findViewById(R.id.tv_productcategoryCart);
        tv_categoryName = findViewById(R.id.tv_categoryName);
        tv_categoryName.setText(categoryName);
        iv_categoryBack = findViewById(R.id.iv_categoryBack);
        iv_categorySearch = findViewById(R.id.iv_categorySearch);
        iv_categorySearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductSearchActivity.class);
                startActivity(intent);
            }
        });
        iv_categoryCart = findViewById(R.id.iv_categoryCart);
        iv_categoryMore = findViewById(R.id.iv_categoryMore);
        meanHotPicks = findViewById(R.id.meanHotPicks);
        menFeaturedBrands = findViewById(R.id.menFeaturedBrands);
        newArrivals = findViewById(R.id.newArrivals);
        menJustForYou = findViewById(R.id.menJustForYou);
        iv_categoryCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CartActivity.class);
                startActivity(intent);
            }
        });
        iv_categoryBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        iv_categoryMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, AccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });


    }
    private void checkUserRole() {
        if (!role.isEmpty()) {
            checkCustomerSession();
            getCartItems();
        }else{

        }

    }
    public void checkCustomerSession() {
        String userID = UserCustomer.getID(context);
        Debugger.logD("CategoriesActivity " + userID);
        if(userID.equals("")){

        }else{
            UserRole userType = new UserRole();
            userType.setUserRole(UserRole.Customer());
            userType.saveRole(context);
        }
    }
    private void getCartItems(){
        CartDbHelper dbHelper = CartDbHelper.getInstance(context);
        cartItemArrayList = dbHelper.getCartItems(Integer.parseInt(UserCustomer.getID(context)));
        cartCountTotal = cartItemArrayList.size();
        if (cartCountTotal == 0){
            tv_productcategoryCart.setText("");
        }else{
            tv_productcategoryCart.setVisibility(View.VISIBLE);
            tv_productcategoryCart.setText(String.valueOf(cartCountTotal));
        }
    }
    private void loadAllCategories() {
        categoryMenRecyclerview = findViewById(R.id.categoryMenRecyclerview);
        subCategoriesArrayList = new ArrayList<>();
        RequestParams params = new RequestParams();
        params.put("category_id", categoryID);

        HttpProvider.post(context, "/mobile/subCategories", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                try{
                    JSONArray jsonArray = new JSONArray(str);
                    Debugger.logD("jsonArray " + jsonArray);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String category_id = jsonObject.getString("category_id");
                        String slug = jsonObject.getString("slug");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");

                        SubCategories subCategories = new SubCategories();
                        subCategories.setId(id);
                        subCategories.setName(name);
                        subCategories.setCategory_id(category_id);
                        subCategories.setSlug(slug);
                        subCategories.setCreated_at(created_at);
                        subCategories.setUpdated_at(updated_at);

                        subCategoriesArrayList.add(subCategories);
                    }
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                    categoryMenRecyclerview.setLayoutManager(layoutManager);
                    subCategoriesAdapter = new SubCategoriesAdapter(context, subCategoriesArrayList);
                    subCategoriesAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedSubcategories = subCategoriesArrayList.get(position);
                            Intent intent = new Intent(context, ProductCategoryActivity.class);
                            intent.putExtra("SUBCATEGORYID", selectedSubcategories.getId());
                            intent.putExtra("SUBCATEGORYNAME", selectedSubcategories.getName());
                            startActivity(intent);
                        }
                    });
                    categoryMenRecyclerview.setAdapter(subCategoriesAdapter);
                }catch (JSONException e){

                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }
    private void loadMensHotPicks() {
        hotpicksRecyclerview = findViewById(R.id.hotpicksYouRecyclerView);
        hotPicksArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getMensHotPicks", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        HotPicks hotPicksModel = new HotPicks();
                        hotPicksModel.setId(id);
                        hotPicksModel.setName(name);
                        hotPicksModel.setAdded_by(added_by);
                        hotPicksModel.setCategory_id(category_id);
                        hotPicksModel.setSubcategory_id(subcategory_id);
                        hotPicksModel.setSubsubcategory_id(subsubcategory_id);
                        hotPicksModel.setBrand_id(brand_id);
                        hotPicksModel.setPhotos(photos);
                        hotPicksModel.setThumbnail_img(thumbnail_img);
                        hotPicksModel.setFeatured_img(featured_img);
                        hotPicksModel.setFlash_deal_img(flash_deal_img);
                        hotPicksModel.setVideo_provider(video_provider);
                        hotPicksModel.setVideo_link(video_link);
                        hotPicksModel.setTags(tags);
                        hotPicksModel.setDescription(description);
                        hotPicksModel.setUnit_price(unit_price);
                        hotPicksModel.setPurchase_price(purchase_price);
                        hotPicksModel.setChoice_options(choice_options);
                        hotPicksModel.setColors(colors);
                        hotPicksModel.setVariations(variations);
                        hotPicksModel.setTodays_deal(todays_deal);
                        hotPicksModel.setPublished(published);
                        hotPicksModel.setFeatured(featured);
                        hotPicksModel.setCurrent_stock(current_stock);
                        hotPicksModel.setUnit(unit);
                        hotPicksModel.setDiscount(discount);
                        hotPicksModel.setDiscount_type(discount_type);
                        hotPicksModel.setTax(tax);
                        hotPicksModel.setTax_type(tax_type);
                        hotPicksModel.setShipping_type(shipping_type);
                        hotPicksModel.setShipping_cost(shipping_cost);
                        hotPicksModel.setWeight(weight);
                        hotPicksModel.setParcel_size(parcel_size);
                        hotPicksModel.setNum_of_sale(num_of_sale);
                        hotPicksModel.setMeta_title(meta_title);
                        hotPicksModel.setMeta_description(meta_description);
                        hotPicksModel.setMeta_img(meta_img);
                        hotPicksModel.setPdf(pdf);
                        hotPicksModel.setSlug(slug);
                        hotPicksModel.setRating(rating);
                        hotPicksModel.setCreated_at(created_at);
                        hotPicksModel.setUpdated_at(updated_at);

                        hotPicksArrayList.add(hotPicksModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    hotpicksRecyclerview.setLayoutManager(layoutManager);

                    hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
                    hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedHotPicks = hotPicksArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedHotPicks.getId());
                            intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    hotpicksRecyclerview.setAdapter(hotPicksAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadMensFeaturedBrand() {
        featuredImage1 = findViewById(R.id.featuredImage1);
        featuredImage2 = findViewById(R.id.featuredImage2);
        featuredImage3 = findViewById(R.id.featuredImage3);
        featuredImage4 = findViewById(R.id.featuredImage4);
        featuredIcon1 = findViewById(R.id.featuredIcon1);
        featuredIcon2 = findViewById(R.id.featuredIcon2);
        featuredIcon3 = findViewById(R.id.featuredIcon3);
        featuredIcon4 = findViewById(R.id.featuredIcon4);
        featuredBrand1 = findViewById(R.id.featuredBrand1);
        featuredBrand2 = findViewById(R.id.featuredBrand2);
        featuredBrand3 = findViewById(R.id.featuredBrand3);
        featuredBrand4 = findViewById(R.id.featuredBrand4);

        featuredImage1.setImageResource(R.drawable.slider7);
        featuredImage2.setImageResource(R.drawable.slider6);
        featuredImage3.setImageResource(R.drawable.slider4);
        featuredImage4.setImageResource(R.drawable.slider2);

        featuredIcon1.setImageResource(R.drawable.asus);
        featuredIcon2.setImageResource(R.drawable.lenovo);
        featuredIcon3.setImageResource(R.drawable.samsung);
        featuredIcon4.setImageResource(R.drawable.acer);

        featuredBrand1.setText("Asus");
        featuredBrand2.setText("Lennovo");
        featuredBrand3.setText("Samsung");
        featuredBrand4.setText("Acer");


    }
    private void loadMensNewArrivals() {
        newArrivalYouRecyclerView = findViewById(R.id.newArrivalYouRecyclerView);
        newArrivalArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getMenNewArrivals", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        NewArrival newArrivalModel = new NewArrival();
                        newArrivalModel.setId(id);
                        newArrivalModel.setName(name);
                        newArrivalModel.setAdded_by(added_by);
                        newArrivalModel.setCategory_id(category_id);
                        newArrivalModel.setSubcategory_id(subcategory_id);
                        newArrivalModel.setSubsubcategory_id(subsubcategory_id);
                        newArrivalModel.setBrand_id(brand_id);
                        newArrivalModel.setPhotos(photos);
                        newArrivalModel.setThumbnail_img(thumbnail_img);
                        newArrivalModel.setFeatured_img(featured_img);
                        newArrivalModel.setFlash_deal_img(flash_deal_img);
                        newArrivalModel.setVideo_provider(video_provider);
                        newArrivalModel.setVideo_link(video_link);
                        newArrivalModel.setTags(tags);
                        newArrivalModel.setDescription(description);
                        newArrivalModel.setUnit_price(unit_price);
                        newArrivalModel.setPurchase_price(purchase_price);
                        newArrivalModel.setChoice_options(choice_options);
                        newArrivalModel.setColors(colors);
                        newArrivalModel.setVariations(variations);
                        newArrivalModel.setTodays_deal(todays_deal);
                        newArrivalModel.setPublished(published);
                        newArrivalModel.setFeatured(featured);
                        newArrivalModel.setCurrent_stock(current_stock);
                        newArrivalModel.setUnit(unit);
                        newArrivalModel.setDiscount(discount);
                        newArrivalModel.setDiscount_type(discount_type);
                        newArrivalModel.setTax(tax);
                        newArrivalModel.setTax_type(tax_type);
                        newArrivalModel.setShipping_type(shipping_type);
                        newArrivalModel.setShipping_cost(shipping_cost);
                        newArrivalModel.setWeight(weight);
                        newArrivalModel.setParcel_size(parcel_size);
                        newArrivalModel.setNum_of_sale(num_of_sale);
                        newArrivalModel.setMeta_title(meta_title);
                        newArrivalModel.setMeta_description(meta_description);
                        newArrivalModel.setMeta_img(meta_img);
                        newArrivalModel.setPdf(pdf);
                        newArrivalModel.setSlug(slug);
                        newArrivalModel.setRating(rating);
                        newArrivalModel.setCreated_at(created_at);
                        newArrivalModel.setUpdated_at(updated_at);

                        newArrivalArrayList.add(newArrivalModel);
                    }

                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    newArrivalYouRecyclerView.setLayoutManager(layoutManager);

                    newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
                    newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {

                        }
                    });
                    newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }
    private void loadMensJustForYou() {
        justForYouRecyvlerview = findViewById(R.id.justForYouRecyclerView);
        justForYouArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getMenJustForYou", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        JustForYou justForYouModel = new JustForYou();
                        justForYouModel.setId(id);
                        justForYouModel.setName(name);
                        justForYouModel.setAdded_by(added_by);
                        justForYouModel.setCategory_id(category_id);
                        justForYouModel.setSubcategory_id(subcategory_id);
                        justForYouModel.setSubsubcategory_id(subsubcategory_id);
                        justForYouModel.setBrand_id(brand_id);
                        justForYouModel.setPhotos(photos);
                        justForYouModel.setThumbnail_img(thumbnail_img);
                        justForYouModel.setFeatured_img(featured_img);
                        justForYouModel.setFlash_deal_img(flash_deal_img);
                        justForYouModel.setVideo_provider(video_provider);
                        justForYouModel.setVideo_link(video_link);
                        justForYouModel.setTags(tags);
                        justForYouModel.setDescription(description);
                        justForYouModel.setUnit_price(unit_price);
                        justForYouModel.setPurchase_price(purchase_price);
                        justForYouModel.setChoice_options(choice_options);
                        justForYouModel.setColors(colors);
                        justForYouModel.setVariations(variations);
                        justForYouModel.setTodays_deal(todays_deal);
                        justForYouModel.setPublished(published);
                        justForYouModel.setFeatured(featured);
                        justForYouModel.setCurrent_stock(current_stock);
                        justForYouModel.setUnit(unit);
                        justForYouModel.setDiscount(discount);
                        justForYouModel.setDiscount_type(discount_type);
                        justForYouModel.setTax(tax);
                        justForYouModel.setTax_type(tax_type);
                        justForYouModel.setShipping_type(shipping_type);
                        justForYouModel.setShipping_cost(shipping_cost);
                        justForYouModel.setWeight(weight);
                        justForYouModel.setParcel_size(parcel_size);
                        justForYouModel.setNum_of_sale(num_of_sale);
                        justForYouModel.setMeta_title(meta_title);
                        justForYouModel.setMeta_description(meta_description);
                        justForYouModel.setMeta_img(meta_img);
                        justForYouModel.setPdf(pdf);
                        justForYouModel.setSlug(slug);
                        justForYouModel.setRating(rating);
                        justForYouModel.setCreated_at(created_at);
                        justForYouModel.setUpdated_at(updated_at);

                        justForYouArrayList.add(justForYouModel);
                    }

                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
                    justForYouRecyvlerview.setLayoutManager(layoutManager);

                    justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
                    justForYouAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {

                        }
                    });
                    justForYouRecyvlerview.setAdapter(justForYouAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void loadWomenHotPicks() {
        hotpicksRecyclerview = findViewById(R.id.hotpicksYouRecyclerView);
        hotPicksArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getWomensHotPicks", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        HotPicks hotPicksModel = new HotPicks();
                        hotPicksModel.setId(id);
                        hotPicksModel.setName(name);
                        hotPicksModel.setAdded_by(added_by);
                        hotPicksModel.setCategory_id(category_id);
                        hotPicksModel.setSubcategory_id(subcategory_id);
                        hotPicksModel.setSubsubcategory_id(subsubcategory_id);
                        hotPicksModel.setBrand_id(brand_id);
                        hotPicksModel.setPhotos(photos);
                        hotPicksModel.setThumbnail_img(thumbnail_img);
                        hotPicksModel.setFeatured_img(featured_img);
                        hotPicksModel.setFlash_deal_img(flash_deal_img);
                        hotPicksModel.setVideo_provider(video_provider);
                        hotPicksModel.setVideo_link(video_link);
                        hotPicksModel.setTags(tags);
                        hotPicksModel.setDescription(description);
                        hotPicksModel.setUnit_price(unit_price);
                        hotPicksModel.setPurchase_price(purchase_price);
                        hotPicksModel.setChoice_options(choice_options);
                        hotPicksModel.setColors(colors);
                        hotPicksModel.setVariations(variations);
                        hotPicksModel.setTodays_deal(todays_deal);
                        hotPicksModel.setPublished(published);
                        hotPicksModel.setFeatured(featured);
                        hotPicksModel.setCurrent_stock(current_stock);
                        hotPicksModel.setUnit(unit);
                        hotPicksModel.setDiscount(discount);
                        hotPicksModel.setDiscount_type(discount_type);
                        hotPicksModel.setTax(tax);
                        hotPicksModel.setTax_type(tax_type);
                        hotPicksModel.setShipping_type(shipping_type);
                        hotPicksModel.setShipping_cost(shipping_cost);
                        hotPicksModel.setWeight(weight);
                        hotPicksModel.setParcel_size(parcel_size);
                        hotPicksModel.setNum_of_sale(num_of_sale);
                        hotPicksModel.setMeta_title(meta_title);
                        hotPicksModel.setMeta_description(meta_description);
                        hotPicksModel.setMeta_img(meta_img);
                        hotPicksModel.setPdf(pdf);
                        hotPicksModel.setSlug(slug);
                        hotPicksModel.setRating(rating);
                        hotPicksModel.setCreated_at(created_at);
                        hotPicksModel.setUpdated_at(updated_at);

                        hotPicksArrayList.add(hotPicksModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    hotpicksRecyclerview.setLayoutManager(layoutManager);

                    hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
                    hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedHotPicks = hotPicksArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedHotPicks.getId());
                            intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    hotpicksRecyclerview.setAdapter(hotPicksAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }
    private void loadWomenFeaturedBrand(){
        featuredImage1 = findViewById(R.id.featuredImage1);
        featuredImage2 = findViewById(R.id.featuredImage2);
        featuredImage3 = findViewById(R.id.featuredImage3);
        featuredImage4 = findViewById(R.id.featuredImage4);
        featuredIcon1 = findViewById(R.id.featuredIcon1);
        featuredIcon2 = findViewById(R.id.featuredIcon2);
        featuredIcon3 = findViewById(R.id.featuredIcon3);
        featuredIcon4 = findViewById(R.id.featuredIcon4);
        featuredBrand1 = findViewById(R.id.featuredBrand1);
        featuredBrand2 = findViewById(R.id.featuredBrand2);
        featuredBrand3 = findViewById(R.id.featuredBrand3);
        featuredBrand4 = findViewById(R.id.featuredBrand4);

        featuredImage1.setImageResource(R.drawable.slider1);
        featuredImage2.setImageResource(R.drawable.slider2);
        featuredImage3.setImageResource(R.drawable.slider3);
        featuredImage4.setImageResource(R.drawable.slider4);

        featuredIcon1.setImageResource(R.drawable.samsung);
        featuredIcon2.setImageResource(R.drawable.acer);
        featuredIcon3.setImageResource(R.drawable.asus);
        featuredIcon4.setImageResource(R.drawable.lenovo);

        featuredBrand1.setText("Samsung");
        featuredBrand2.setText("Acer");
        featuredBrand3.setText("Asus");
        featuredBrand4.setText("Lenovo");
    }
    private void loadWomenNewArrivals() {
        newArrivalYouRecyclerView = findViewById(R.id.newArrivalYouRecyclerView);
        newArrivalArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getWomenNewArrivals", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        NewArrival newArrivalModel = new NewArrival();
                        newArrivalModel.setId(id);
                        newArrivalModel.setName(name);
                        newArrivalModel.setAdded_by(added_by);
                        newArrivalModel.setCategory_id(category_id);
                        newArrivalModel.setSubcategory_id(subcategory_id);
                        newArrivalModel.setSubsubcategory_id(subsubcategory_id);
                        newArrivalModel.setBrand_id(brand_id);
                        newArrivalModel.setPhotos(photos);
                        newArrivalModel.setThumbnail_img(thumbnail_img);
                        newArrivalModel.setFeatured_img(featured_img);
                        newArrivalModel.setFlash_deal_img(flash_deal_img);
                        newArrivalModel.setVideo_provider(video_provider);
                        newArrivalModel.setVideo_link(video_link);
                        newArrivalModel.setTags(tags);
                        newArrivalModel.setDescription(description);
                        newArrivalModel.setUnit_price(unit_price);
                        newArrivalModel.setPurchase_price(purchase_price);
                        newArrivalModel.setChoice_options(choice_options);
                        newArrivalModel.setColors(colors);
                        newArrivalModel.setVariations(variations);
                        newArrivalModel.setTodays_deal(todays_deal);
                        newArrivalModel.setPublished(published);
                        newArrivalModel.setFeatured(featured);
                        newArrivalModel.setCurrent_stock(current_stock);
                        newArrivalModel.setUnit(unit);
                        newArrivalModel.setDiscount(discount);
                        newArrivalModel.setDiscount_type(discount_type);
                        newArrivalModel.setTax(tax);
                        newArrivalModel.setTax_type(tax_type);
                        newArrivalModel.setShipping_type(shipping_type);
                        newArrivalModel.setShipping_cost(shipping_cost);
                        newArrivalModel.setWeight(weight);
                        newArrivalModel.setParcel_size(parcel_size);
                        newArrivalModel.setNum_of_sale(num_of_sale);
                        newArrivalModel.setMeta_title(meta_title);
                        newArrivalModel.setMeta_description(meta_description);
                        newArrivalModel.setMeta_img(meta_img);
                        newArrivalModel.setPdf(pdf);
                        newArrivalModel.setSlug(slug);
                        newArrivalModel.setRating(rating);
                        newArrivalModel.setCreated_at(created_at);
                        newArrivalModel.setUpdated_at(updated_at);

                        newArrivalArrayList.add(newArrivalModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    newArrivalYouRecyclerView.setLayoutManager(layoutManager);
                    newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
                    newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedNewArrival = newArrivalArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedNewArrival.getId());
                            intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadWomenJustForYou() {
        justForYouRecyvlerview = findViewById(R.id.justForYouRecyclerView);
        justForYouArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getWomenJustForYou", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        JustForYou justForYouModel = new JustForYou();
                        justForYouModel.setId(id);
                        justForYouModel.setName(name);
                        justForYouModel.setAdded_by(added_by);
                        justForYouModel.setCategory_id(category_id);
                        justForYouModel.setSubcategory_id(subcategory_id);
                        justForYouModel.setSubsubcategory_id(subsubcategory_id);
                        justForYouModel.setBrand_id(brand_id);
                        justForYouModel.setPhotos(photos);
                        justForYouModel.setThumbnail_img(thumbnail_img);
                        justForYouModel.setFeatured_img(featured_img);
                        justForYouModel.setFlash_deal_img(flash_deal_img);
                        justForYouModel.setVideo_provider(video_provider);
                        justForYouModel.setVideo_link(video_link);
                        justForYouModel.setTags(tags);
                        justForYouModel.setDescription(description);
                        justForYouModel.setUnit_price(unit_price);
                        justForYouModel.setPurchase_price(purchase_price);
                        justForYouModel.setChoice_options(choice_options);
                        justForYouModel.setColors(colors);
                        justForYouModel.setVariations(variations);
                        justForYouModel.setTodays_deal(todays_deal);
                        justForYouModel.setPublished(published);
                        justForYouModel.setFeatured(featured);
                        justForYouModel.setCurrent_stock(current_stock);
                        justForYouModel.setUnit(unit);
                        justForYouModel.setDiscount(discount);
                        justForYouModel.setDiscount_type(discount_type);
                        justForYouModel.setTax(tax);
                        justForYouModel.setTax_type(tax_type);
                        justForYouModel.setShipping_type(shipping_type);
                        justForYouModel.setShipping_cost(shipping_cost);
                        justForYouModel.setWeight(weight);
                        justForYouModel.setParcel_size(parcel_size);
                        justForYouModel.setNum_of_sale(num_of_sale);
                        justForYouModel.setMeta_title(meta_title);
                        justForYouModel.setMeta_description(meta_description);
                        justForYouModel.setMeta_img(meta_img);
                        justForYouModel.setPdf(pdf);
                        justForYouModel.setSlug(slug);
                        justForYouModel.setRating(rating);
                        justForYouModel.setCreated_at(created_at);
                        justForYouModel.setUpdated_at(updated_at);

                        justForYouArrayList.add(justForYouModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
                    justForYouRecyvlerview.setLayoutManager(layoutManager);
                    justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
                    justForYouAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedJustForYou = justForYouArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedJustForYou.getId());
                            intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    justForYouRecyvlerview.setAdapter(justForYouAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadComputerHotPicks() {
        hotpicksRecyclerview = findViewById(R.id.hotpicksYouRecyclerView);
        hotPicksArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getComputerHotPicks", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        HotPicks hotPicksModel = new HotPicks();
                        hotPicksModel.setId(id);
                        hotPicksModel.setName(name);
                        hotPicksModel.setAdded_by(added_by);
                        hotPicksModel.setCategory_id(category_id);
                        hotPicksModel.setSubcategory_id(subcategory_id);
                        hotPicksModel.setSubsubcategory_id(subsubcategory_id);
                        hotPicksModel.setBrand_id(brand_id);
                        hotPicksModel.setPhotos(photos);
                        hotPicksModel.setThumbnail_img(thumbnail_img);
                        hotPicksModel.setFeatured_img(featured_img);
                        hotPicksModel.setFlash_deal_img(flash_deal_img);
                        hotPicksModel.setVideo_provider(video_provider);
                        hotPicksModel.setVideo_link(video_link);
                        hotPicksModel.setTags(tags);
                        hotPicksModel.setDescription(description);
                        hotPicksModel.setUnit_price(unit_price);
                        hotPicksModel.setPurchase_price(purchase_price);
                        hotPicksModel.setChoice_options(choice_options);
                        hotPicksModel.setColors(colors);
                        hotPicksModel.setVariations(variations);
                        hotPicksModel.setTodays_deal(todays_deal);
                        hotPicksModel.setPublished(published);
                        hotPicksModel.setFeatured(featured);
                        hotPicksModel.setCurrent_stock(current_stock);
                        hotPicksModel.setUnit(unit);
                        hotPicksModel.setDiscount(discount);
                        hotPicksModel.setDiscount_type(discount_type);
                        hotPicksModel.setTax(tax);
                        hotPicksModel.setTax_type(tax_type);
                        hotPicksModel.setShipping_type(shipping_type);
                        hotPicksModel.setShipping_cost(shipping_cost);
                        hotPicksModel.setWeight(weight);
                        hotPicksModel.setParcel_size(parcel_size);
                        hotPicksModel.setNum_of_sale(num_of_sale);
                        hotPicksModel.setMeta_title(meta_title);
                        hotPicksModel.setMeta_description(meta_description);
                        hotPicksModel.setMeta_img(meta_img);
                        hotPicksModel.setPdf(pdf);
                        hotPicksModel.setSlug(slug);
                        hotPicksModel.setRating(rating);
                        hotPicksModel.setCreated_at(created_at);
                        hotPicksModel.setUpdated_at(updated_at);

                        hotPicksArrayList.add(hotPicksModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    hotpicksRecyclerview.setLayoutManager(layoutManager);

                    hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
                    hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedHotPicks = hotPicksArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedHotPicks.getId());
                            intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    hotpicksRecyclerview.setAdapter(hotPicksAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }
    private void loadComputerFeaturedBrand(){

    }
    private void loadComputerNewArrivals() {
        newArrivalYouRecyclerView = findViewById(R.id.newArrivalYouRecyclerView);
        newArrivalArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getComputerNewArrivals", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        NewArrival newArrivalModel = new NewArrival();
                        newArrivalModel.setId(id);
                        newArrivalModel.setName(name);
                        newArrivalModel.setAdded_by(added_by);
                        newArrivalModel.setCategory_id(category_id);
                        newArrivalModel.setSubcategory_id(subcategory_id);
                        newArrivalModel.setSubsubcategory_id(subsubcategory_id);
                        newArrivalModel.setBrand_id(brand_id);
                        newArrivalModel.setPhotos(photos);
                        newArrivalModel.setThumbnail_img(thumbnail_img);
                        newArrivalModel.setFeatured_img(featured_img);
                        newArrivalModel.setFlash_deal_img(flash_deal_img);
                        newArrivalModel.setVideo_provider(video_provider);
                        newArrivalModel.setVideo_link(video_link);
                        newArrivalModel.setTags(tags);
                        newArrivalModel.setDescription(description);
                        newArrivalModel.setUnit_price(unit_price);
                        newArrivalModel.setPurchase_price(purchase_price);
                        newArrivalModel.setChoice_options(choice_options);
                        newArrivalModel.setColors(colors);
                        newArrivalModel.setVariations(variations);
                        newArrivalModel.setTodays_deal(todays_deal);
                        newArrivalModel.setPublished(published);
                        newArrivalModel.setFeatured(featured);
                        newArrivalModel.setCurrent_stock(current_stock);
                        newArrivalModel.setUnit(unit);
                        newArrivalModel.setDiscount(discount);
                        newArrivalModel.setDiscount_type(discount_type);
                        newArrivalModel.setTax(tax);
                        newArrivalModel.setTax_type(tax_type);
                        newArrivalModel.setShipping_type(shipping_type);
                        newArrivalModel.setShipping_cost(shipping_cost);
                        newArrivalModel.setWeight(weight);
                        newArrivalModel.setParcel_size(parcel_size);
                        newArrivalModel.setNum_of_sale(num_of_sale);
                        newArrivalModel.setMeta_title(meta_title);
                        newArrivalModel.setMeta_description(meta_description);
                        newArrivalModel.setMeta_img(meta_img);
                        newArrivalModel.setPdf(pdf);
                        newArrivalModel.setSlug(slug);
                        newArrivalModel.setRating(rating);
                        newArrivalModel.setCreated_at(created_at);
                        newArrivalModel.setUpdated_at(updated_at);

                        newArrivalArrayList.add(newArrivalModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    newArrivalYouRecyclerView.setLayoutManager(layoutManager);
                    newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
                    newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedNewArrival = newArrivalArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedNewArrival.getId());
                            intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }
    private void loadComputerJustForYou() {
        justForYouRecyvlerview = findViewById(R.id.justForYouRecyclerView);
        justForYouArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getComputerJustForYou", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        JustForYou justForYouModel = new JustForYou();
                        justForYouModel.setId(id);
                        justForYouModel.setName(name);
                        justForYouModel.setAdded_by(added_by);
                        justForYouModel.setCategory_id(category_id);
                        justForYouModel.setSubcategory_id(subcategory_id);
                        justForYouModel.setSubsubcategory_id(subsubcategory_id);
                        justForYouModel.setBrand_id(brand_id);
                        justForYouModel.setPhotos(photos);
                        justForYouModel.setThumbnail_img(thumbnail_img);
                        justForYouModel.setFeatured_img(featured_img);
                        justForYouModel.setFlash_deal_img(flash_deal_img);
                        justForYouModel.setVideo_provider(video_provider);
                        justForYouModel.setVideo_link(video_link);
                        justForYouModel.setTags(tags);
                        justForYouModel.setDescription(description);
                        justForYouModel.setUnit_price(unit_price);
                        justForYouModel.setPurchase_price(purchase_price);
                        justForYouModel.setChoice_options(choice_options);
                        justForYouModel.setColors(colors);
                        justForYouModel.setVariations(variations);
                        justForYouModel.setTodays_deal(todays_deal);
                        justForYouModel.setPublished(published);
                        justForYouModel.setFeatured(featured);
                        justForYouModel.setCurrent_stock(current_stock);
                        justForYouModel.setUnit(unit);
                        justForYouModel.setDiscount(discount);
                        justForYouModel.setDiscount_type(discount_type);
                        justForYouModel.setTax(tax);
                        justForYouModel.setTax_type(tax_type);
                        justForYouModel.setShipping_type(shipping_type);
                        justForYouModel.setShipping_cost(shipping_cost);
                        justForYouModel.setWeight(weight);
                        justForYouModel.setParcel_size(parcel_size);
                        justForYouModel.setNum_of_sale(num_of_sale);
                        justForYouModel.setMeta_title(meta_title);
                        justForYouModel.setMeta_description(meta_description);
                        justForYouModel.setMeta_img(meta_img);
                        justForYouModel.setPdf(pdf);
                        justForYouModel.setSlug(slug);
                        justForYouModel.setRating(rating);
                        justForYouModel.setCreated_at(created_at);
                        justForYouModel.setUpdated_at(updated_at);

                        justForYouArrayList.add(justForYouModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
                    justForYouRecyvlerview.setLayoutManager(layoutManager);
                    justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
                    justForYouAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedJustForYou = justForYouArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedJustForYou.getId());
                            intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    justForYouRecyvlerview.setAdapter(justForYouAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadKidsHotPicks() {
        hotpicksRecyclerview = findViewById(R.id.hotpicksYouRecyclerView);
        hotPicksArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getKidsAndToysHotPicks", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        HotPicks hotPicksModel = new HotPicks();
                        hotPicksModel.setId(id);
                        hotPicksModel.setName(name);
                        hotPicksModel.setAdded_by(added_by);
                        hotPicksModel.setCategory_id(category_id);
                        hotPicksModel.setSubcategory_id(subcategory_id);
                        hotPicksModel.setSubsubcategory_id(subsubcategory_id);
                        hotPicksModel.setBrand_id(brand_id);
                        hotPicksModel.setPhotos(photos);
                        hotPicksModel.setThumbnail_img(thumbnail_img);
                        hotPicksModel.setFeatured_img(featured_img);
                        hotPicksModel.setFlash_deal_img(flash_deal_img);
                        hotPicksModel.setVideo_provider(video_provider);
                        hotPicksModel.setVideo_link(video_link);
                        hotPicksModel.setTags(tags);
                        hotPicksModel.setDescription(description);
                        hotPicksModel.setUnit_price(unit_price);
                        hotPicksModel.setPurchase_price(purchase_price);
                        hotPicksModel.setChoice_options(choice_options);
                        hotPicksModel.setColors(colors);
                        hotPicksModel.setVariations(variations);
                        hotPicksModel.setTodays_deal(todays_deal);
                        hotPicksModel.setPublished(published);
                        hotPicksModel.setFeatured(featured);
                        hotPicksModel.setCurrent_stock(current_stock);
                        hotPicksModel.setUnit(unit);
                        hotPicksModel.setDiscount(discount);
                        hotPicksModel.setDiscount_type(discount_type);
                        hotPicksModel.setTax(tax);
                        hotPicksModel.setTax_type(tax_type);
                        hotPicksModel.setShipping_type(shipping_type);
                        hotPicksModel.setShipping_cost(shipping_cost);
                        hotPicksModel.setWeight(weight);
                        hotPicksModel.setParcel_size(parcel_size);
                        hotPicksModel.setNum_of_sale(num_of_sale);
                        hotPicksModel.setMeta_title(meta_title);
                        hotPicksModel.setMeta_description(meta_description);
                        hotPicksModel.setMeta_img(meta_img);
                        hotPicksModel.setPdf(pdf);
                        hotPicksModel.setSlug(slug);
                        hotPicksModel.setRating(rating);
                        hotPicksModel.setCreated_at(created_at);
                        hotPicksModel.setUpdated_at(updated_at);

                        hotPicksArrayList.add(hotPicksModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    hotpicksRecyclerview.setLayoutManager(layoutManager);
                    hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
                    hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedHotPicks = hotPicksArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedHotPicks.getId());
                            intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    hotpicksRecyclerview.setAdapter(hotPicksAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsFeaturedBrand(){

    }
    private void loadKidsNewArrivals() {
        newArrivalYouRecyclerView = findViewById(R.id.newArrivalYouRecyclerView);
        newArrivalArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getKidsAndToysNewArrivals", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        NewArrival newArrivalModel = new NewArrival();
                        newArrivalModel.setId(id);
                        newArrivalModel.setName(name);
                        newArrivalModel.setAdded_by(added_by);
                        newArrivalModel.setCategory_id(category_id);
                        newArrivalModel.setSubcategory_id(subcategory_id);
                        newArrivalModel.setSubsubcategory_id(subsubcategory_id);
                        newArrivalModel.setBrand_id(brand_id);
                        newArrivalModel.setPhotos(photos);
                        newArrivalModel.setThumbnail_img(thumbnail_img);
                        newArrivalModel.setFeatured_img(featured_img);
                        newArrivalModel.setFlash_deal_img(flash_deal_img);
                        newArrivalModel.setVideo_provider(video_provider);
                        newArrivalModel.setVideo_link(video_link);
                        newArrivalModel.setTags(tags);
                        newArrivalModel.setDescription(description);
                        newArrivalModel.setUnit_price(unit_price);
                        newArrivalModel.setPurchase_price(purchase_price);
                        newArrivalModel.setChoice_options(choice_options);
                        newArrivalModel.setColors(colors);
                        newArrivalModel.setVariations(variations);
                        newArrivalModel.setTodays_deal(todays_deal);
                        newArrivalModel.setPublished(published);
                        newArrivalModel.setFeatured(featured);
                        newArrivalModel.setCurrent_stock(current_stock);
                        newArrivalModel.setUnit(unit);
                        newArrivalModel.setDiscount(discount);
                        newArrivalModel.setDiscount_type(discount_type);
                        newArrivalModel.setTax(tax);
                        newArrivalModel.setTax_type(tax_type);
                        newArrivalModel.setShipping_type(shipping_type);
                        newArrivalModel.setShipping_cost(shipping_cost);
                        newArrivalModel.setWeight(weight);
                        newArrivalModel.setParcel_size(parcel_size);
                        newArrivalModel.setNum_of_sale(num_of_sale);
                        newArrivalModel.setMeta_title(meta_title);
                        newArrivalModel.setMeta_description(meta_description);
                        newArrivalModel.setMeta_img(meta_img);
                        newArrivalModel.setPdf(pdf);
                        newArrivalModel.setSlug(slug);
                        newArrivalModel.setRating(rating);
                        newArrivalModel.setCreated_at(created_at);
                        newArrivalModel.setUpdated_at(updated_at);

                        newArrivalArrayList.add(newArrivalModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    newArrivalYouRecyclerView.setLayoutManager(layoutManager);
                    newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
                    newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedNewArrival = newArrivalArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedNewArrival.getId());
                            intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsJustForYou() {
        justForYouRecyvlerview = findViewById(R.id.justForYouRecyclerView);
        justForYouArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getKidsAndToysJustForYou", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        JustForYou justForYouModel = new JustForYou();
                        justForYouModel.setId(id);
                        justForYouModel.setName(name);
                        justForYouModel.setAdded_by(added_by);
                        justForYouModel.setCategory_id(category_id);
                        justForYouModel.setSubcategory_id(subcategory_id);
                        justForYouModel.setSubsubcategory_id(subsubcategory_id);
                        justForYouModel.setBrand_id(brand_id);
                        justForYouModel.setPhotos(photos);
                        justForYouModel.setThumbnail_img(thumbnail_img);
                        justForYouModel.setFeatured_img(featured_img);
                        justForYouModel.setFlash_deal_img(flash_deal_img);
                        justForYouModel.setVideo_provider(video_provider);
                        justForYouModel.setVideo_link(video_link);
                        justForYouModel.setTags(tags);
                        justForYouModel.setDescription(description);
                        justForYouModel.setUnit_price(unit_price);
                        justForYouModel.setPurchase_price(purchase_price);
                        justForYouModel.setChoice_options(choice_options);
                        justForYouModel.setColors(colors);
                        justForYouModel.setVariations(variations);
                        justForYouModel.setTodays_deal(todays_deal);
                        justForYouModel.setPublished(published);
                        justForYouModel.setFeatured(featured);
                        justForYouModel.setCurrent_stock(current_stock);
                        justForYouModel.setUnit(unit);
                        justForYouModel.setDiscount(discount);
                        justForYouModel.setDiscount_type(discount_type);
                        justForYouModel.setTax(tax);
                        justForYouModel.setTax_type(tax_type);
                        justForYouModel.setShipping_type(shipping_type);
                        justForYouModel.setShipping_cost(shipping_cost);
                        justForYouModel.setWeight(weight);
                        justForYouModel.setParcel_size(parcel_size);
                        justForYouModel.setNum_of_sale(num_of_sale);
                        justForYouModel.setMeta_title(meta_title);
                        justForYouModel.setMeta_description(meta_description);
                        justForYouModel.setMeta_img(meta_img);
                        justForYouModel.setPdf(pdf);
                        justForYouModel.setSlug(slug);
                        justForYouModel.setRating(rating);
                        justForYouModel.setCreated_at(created_at);
                        justForYouModel.setUpdated_at(updated_at);

                        justForYouArrayList.add(justForYouModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
                    justForYouRecyvlerview.setLayoutManager(layoutManager);

                    justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
                    justForYouAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedJustForYou = justForYouArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedJustForYou.getId());
                            intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    justForYouRecyvlerview.setAdapter(justForYouAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadSportsHotPicks() {
        hotpicksRecyclerview = findViewById(R.id.hotpicksYouRecyclerView);
        hotPicksArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getSportsAndOutdoorsHotPicks", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        HotPicks hotPicksModel = new HotPicks();
                        hotPicksModel.setId(id);
                        hotPicksModel.setName(name);
                        hotPicksModel.setAdded_by(added_by);
                        hotPicksModel.setCategory_id(category_id);
                        hotPicksModel.setSubcategory_id(subcategory_id);
                        hotPicksModel.setSubsubcategory_id(subsubcategory_id);
                        hotPicksModel.setBrand_id(brand_id);
                        hotPicksModel.setPhotos(photos);
                        hotPicksModel.setThumbnail_img(thumbnail_img);
                        hotPicksModel.setFeatured_img(featured_img);
                        hotPicksModel.setFlash_deal_img(flash_deal_img);
                        hotPicksModel.setVideo_provider(video_provider);
                        hotPicksModel.setVideo_link(video_link);
                        hotPicksModel.setTags(tags);
                        hotPicksModel.setDescription(description);
                        hotPicksModel.setUnit_price(unit_price);
                        hotPicksModel.setPurchase_price(purchase_price);
                        hotPicksModel.setChoice_options(choice_options);
                        hotPicksModel.setColors(colors);
                        hotPicksModel.setVariations(variations);
                        hotPicksModel.setTodays_deal(todays_deal);
                        hotPicksModel.setPublished(published);
                        hotPicksModel.setFeatured(featured);
                        hotPicksModel.setCurrent_stock(current_stock);
                        hotPicksModel.setUnit(unit);
                        hotPicksModel.setDiscount(discount);
                        hotPicksModel.setDiscount_type(discount_type);
                        hotPicksModel.setTax(tax);
                        hotPicksModel.setTax_type(tax_type);
                        hotPicksModel.setShipping_type(shipping_type);
                        hotPicksModel.setShipping_cost(shipping_cost);
                        hotPicksModel.setWeight(weight);
                        hotPicksModel.setParcel_size(parcel_size);
                        hotPicksModel.setNum_of_sale(num_of_sale);
                        hotPicksModel.setMeta_title(meta_title);
                        hotPicksModel.setMeta_description(meta_description);
                        hotPicksModel.setMeta_img(meta_img);
                        hotPicksModel.setPdf(pdf);
                        hotPicksModel.setSlug(slug);
                        hotPicksModel.setRating(rating);
                        hotPicksModel.setCreated_at(created_at);
                        hotPicksModel.setUpdated_at(updated_at);

                        hotPicksArrayList.add(hotPicksModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    hotpicksRecyclerview.setLayoutManager(layoutManager);
                    hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
                    hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedHotPicks = hotPicksArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedHotPicks.getId());
                            intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    hotpicksRecyclerview.setAdapter(hotPicksAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadSportsFeaturedBrand(){

    }
    private void loadSportsNewArrivals() {
        newArrivalYouRecyclerView = findViewById(R.id.newArrivalYouRecyclerView);
        newArrivalArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getSportsAndOutdoorsNewArrivals", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        NewArrival newArrivalModel = new NewArrival();
                        newArrivalModel.setId(id);
                        newArrivalModel.setName(name);
                        newArrivalModel.setAdded_by(added_by);
                        newArrivalModel.setCategory_id(category_id);
                        newArrivalModel.setSubcategory_id(subcategory_id);
                        newArrivalModel.setSubsubcategory_id(subsubcategory_id);
                        newArrivalModel.setBrand_id(brand_id);
                        newArrivalModel.setPhotos(photos);
                        newArrivalModel.setThumbnail_img(thumbnail_img);
                        newArrivalModel.setFeatured_img(featured_img);
                        newArrivalModel.setFlash_deal_img(flash_deal_img);
                        newArrivalModel.setVideo_provider(video_provider);
                        newArrivalModel.setVideo_link(video_link);
                        newArrivalModel.setTags(tags);
                        newArrivalModel.setDescription(description);
                        newArrivalModel.setUnit_price(unit_price);
                        newArrivalModel.setPurchase_price(purchase_price);
                        newArrivalModel.setChoice_options(choice_options);
                        newArrivalModel.setColors(colors);
                        newArrivalModel.setVariations(variations);
                        newArrivalModel.setTodays_deal(todays_deal);
                        newArrivalModel.setPublished(published);
                        newArrivalModel.setFeatured(featured);
                        newArrivalModel.setCurrent_stock(current_stock);
                        newArrivalModel.setUnit(unit);
                        newArrivalModel.setDiscount(discount);
                        newArrivalModel.setDiscount_type(discount_type);
                        newArrivalModel.setTax(tax);
                        newArrivalModel.setTax_type(tax_type);
                        newArrivalModel.setShipping_type(shipping_type);
                        newArrivalModel.setShipping_cost(shipping_cost);
                        newArrivalModel.setWeight(weight);
                        newArrivalModel.setParcel_size(parcel_size);
                        newArrivalModel.setNum_of_sale(num_of_sale);
                        newArrivalModel.setMeta_title(meta_title);
                        newArrivalModel.setMeta_description(meta_description);
                        newArrivalModel.setMeta_img(meta_img);
                        newArrivalModel.setPdf(pdf);
                        newArrivalModel.setSlug(slug);
                        newArrivalModel.setRating(rating);
                        newArrivalModel.setCreated_at(created_at);
                        newArrivalModel.setUpdated_at(updated_at);

                        newArrivalArrayList.add(newArrivalModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    newArrivalYouRecyclerView.setLayoutManager(layoutManager);
                    newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
                    newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedNewArrival = newArrivalArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedNewArrival.getId());
                            intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadSportsJustForYou() {
        justForYouRecyvlerview = findViewById(R.id.justForYouRecyclerView);
        justForYouArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getSportsAndOutdoorsJustForYou", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        JustForYou justForYouModel = new JustForYou();
                        justForYouModel.setId(id);
                        justForYouModel.setName(name);
                        justForYouModel.setAdded_by(added_by);
                        justForYouModel.setCategory_id(category_id);
                        justForYouModel.setSubcategory_id(subcategory_id);
                        justForYouModel.setSubsubcategory_id(subsubcategory_id);
                        justForYouModel.setBrand_id(brand_id);
                        justForYouModel.setPhotos(photos);
                        justForYouModel.setThumbnail_img(thumbnail_img);
                        justForYouModel.setFeatured_img(featured_img);
                        justForYouModel.setFlash_deal_img(flash_deal_img);
                        justForYouModel.setVideo_provider(video_provider);
                        justForYouModel.setVideo_link(video_link);
                        justForYouModel.setTags(tags);
                        justForYouModel.setDescription(description);
                        justForYouModel.setUnit_price(unit_price);
                        justForYouModel.setPurchase_price(purchase_price);
                        justForYouModel.setChoice_options(choice_options);
                        justForYouModel.setColors(colors);
                        justForYouModel.setVariations(variations);
                        justForYouModel.setTodays_deal(todays_deal);
                        justForYouModel.setPublished(published);
                        justForYouModel.setFeatured(featured);
                        justForYouModel.setCurrent_stock(current_stock);
                        justForYouModel.setUnit(unit);
                        justForYouModel.setDiscount(discount);
                        justForYouModel.setDiscount_type(discount_type);
                        justForYouModel.setTax(tax);
                        justForYouModel.setTax_type(tax_type);
                        justForYouModel.setShipping_type(shipping_type);
                        justForYouModel.setShipping_cost(shipping_cost);
                        justForYouModel.setWeight(weight);
                        justForYouModel.setParcel_size(parcel_size);
                        justForYouModel.setNum_of_sale(num_of_sale);
                        justForYouModel.setMeta_title(meta_title);
                        justForYouModel.setMeta_description(meta_description);
                        justForYouModel.setMeta_img(meta_img);
                        justForYouModel.setPdf(pdf);
                        justForYouModel.setSlug(slug);
                        justForYouModel.setRating(rating);
                        justForYouModel.setCreated_at(created_at);
                        justForYouModel.setUpdated_at(updated_at);

                        justForYouArrayList.add(justForYouModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
                    justForYouRecyvlerview.setLayoutManager(layoutManager);
                    justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
                    justForYouAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedJustForYou = justForYouArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedJustForYou.getId());
                            intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    justForYouRecyvlerview.setAdapter(justForYouAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadJewelryHotPicks() {
        hotpicksRecyclerview = findViewById(R.id.hotpicksYouRecyclerView);
        hotPicksArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getJewelryAndWatchesHotPicks", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        HotPicks hotPicksModel = new HotPicks();
                        hotPicksModel.setId(id);
                        hotPicksModel.setName(name);
                        hotPicksModel.setAdded_by(added_by);
                        hotPicksModel.setCategory_id(category_id);
                        hotPicksModel.setSubcategory_id(subcategory_id);
                        hotPicksModel.setSubsubcategory_id(subsubcategory_id);
                        hotPicksModel.setBrand_id(brand_id);
                        hotPicksModel.setPhotos(photos);
                        hotPicksModel.setThumbnail_img(thumbnail_img);
                        hotPicksModel.setFeatured_img(featured_img);
                        hotPicksModel.setFlash_deal_img(flash_deal_img);
                        hotPicksModel.setVideo_provider(video_provider);
                        hotPicksModel.setVideo_link(video_link);
                        hotPicksModel.setTags(tags);
                        hotPicksModel.setDescription(description);
                        hotPicksModel.setUnit_price(unit_price);
                        hotPicksModel.setPurchase_price(purchase_price);
                        hotPicksModel.setChoice_options(choice_options);
                        hotPicksModel.setColors(colors);
                        hotPicksModel.setVariations(variations);
                        hotPicksModel.setTodays_deal(todays_deal);
                        hotPicksModel.setPublished(published);
                        hotPicksModel.setFeatured(featured);
                        hotPicksModel.setCurrent_stock(current_stock);
                        hotPicksModel.setUnit(unit);
                        hotPicksModel.setDiscount(discount);
                        hotPicksModel.setDiscount_type(discount_type);
                        hotPicksModel.setTax(tax);
                        hotPicksModel.setTax_type(tax_type);
                        hotPicksModel.setShipping_type(shipping_type);
                        hotPicksModel.setShipping_cost(shipping_cost);
                        hotPicksModel.setWeight(weight);
                        hotPicksModel.setParcel_size(parcel_size);
                        hotPicksModel.setNum_of_sale(num_of_sale);
                        hotPicksModel.setMeta_title(meta_title);
                        hotPicksModel.setMeta_description(meta_description);
                        hotPicksModel.setMeta_img(meta_img);
                        hotPicksModel.setPdf(pdf);
                        hotPicksModel.setSlug(slug);
                        hotPicksModel.setRating(rating);
                        hotPicksModel.setCreated_at(created_at);
                        hotPicksModel.setUpdated_at(updated_at);

                        hotPicksArrayList.add(hotPicksModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    hotpicksRecyclerview.setLayoutManager(layoutManager);
                    hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
                    hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedHotPicks = hotPicksArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedHotPicks.getId());
                            intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    hotpicksRecyclerview.setAdapter(hotPicksAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }
    private void loadJewelryFeaturedBrand(){

    }
    private void loadJewelryNewArrivals() {
        newArrivalYouRecyclerView = findViewById(R.id.newArrivalYouRecyclerView);
        newArrivalArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getJewelryAndWatchesNewArrivals", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        NewArrival newArrivalModel = new NewArrival();
                        newArrivalModel.setId(id);
                        newArrivalModel.setName(name);
                        newArrivalModel.setAdded_by(added_by);
                        newArrivalModel.setCategory_id(category_id);
                        newArrivalModel.setSubcategory_id(subcategory_id);
                        newArrivalModel.setSubsubcategory_id(subsubcategory_id);
                        newArrivalModel.setBrand_id(brand_id);
                        newArrivalModel.setPhotos(photos);
                        newArrivalModel.setThumbnail_img(thumbnail_img);
                        newArrivalModel.setFeatured_img(featured_img);
                        newArrivalModel.setFlash_deal_img(flash_deal_img);
                        newArrivalModel.setVideo_provider(video_provider);
                        newArrivalModel.setVideo_link(video_link);
                        newArrivalModel.setTags(tags);
                        newArrivalModel.setDescription(description);
                        newArrivalModel.setUnit_price(unit_price);
                        newArrivalModel.setPurchase_price(purchase_price);
                        newArrivalModel.setChoice_options(choice_options);
                        newArrivalModel.setColors(colors);
                        newArrivalModel.setVariations(variations);
                        newArrivalModel.setTodays_deal(todays_deal);
                        newArrivalModel.setPublished(published);
                        newArrivalModel.setFeatured(featured);
                        newArrivalModel.setCurrent_stock(current_stock);
                        newArrivalModel.setUnit(unit);
                        newArrivalModel.setDiscount(discount);
                        newArrivalModel.setDiscount_type(discount_type);
                        newArrivalModel.setTax(tax);
                        newArrivalModel.setTax_type(tax_type);
                        newArrivalModel.setShipping_type(shipping_type);
                        newArrivalModel.setShipping_cost(shipping_cost);
                        newArrivalModel.setWeight(weight);
                        newArrivalModel.setParcel_size(parcel_size);
                        newArrivalModel.setNum_of_sale(num_of_sale);
                        newArrivalModel.setMeta_title(meta_title);
                        newArrivalModel.setMeta_description(meta_description);
                        newArrivalModel.setMeta_img(meta_img);
                        newArrivalModel.setPdf(pdf);
                        newArrivalModel.setSlug(slug);
                        newArrivalModel.setRating(rating);
                        newArrivalModel.setCreated_at(created_at);
                        newArrivalModel.setUpdated_at(updated_at);

                        newArrivalArrayList.add(newArrivalModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    newArrivalYouRecyclerView.setLayoutManager(layoutManager);
                    newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
                    newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedNewArrival = newArrivalArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedNewArrival.getId());
                            intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadJewelryJustForYou() {
        justForYouRecyvlerview = findViewById(R.id.justForYouRecyclerView);
        justForYouArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getJewelryAndWatchesJustForYou", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        JustForYou justForYouModel = new JustForYou();
                        justForYouModel.setId(id);
                        justForYouModel.setName(name);
                        justForYouModel.setAdded_by(added_by);
                        justForYouModel.setCategory_id(category_id);
                        justForYouModel.setSubcategory_id(subcategory_id);
                        justForYouModel.setSubsubcategory_id(subsubcategory_id);
                        justForYouModel.setBrand_id(brand_id);
                        justForYouModel.setPhotos(photos);
                        justForYouModel.setThumbnail_img(thumbnail_img);
                        justForYouModel.setFeatured_img(featured_img);
                        justForYouModel.setFlash_deal_img(flash_deal_img);
                        justForYouModel.setVideo_provider(video_provider);
                        justForYouModel.setVideo_link(video_link);
                        justForYouModel.setTags(tags);
                        justForYouModel.setDescription(description);
                        justForYouModel.setUnit_price(unit_price);
                        justForYouModel.setPurchase_price(purchase_price);
                        justForYouModel.setChoice_options(choice_options);
                        justForYouModel.setColors(colors);
                        justForYouModel.setVariations(variations);
                        justForYouModel.setTodays_deal(todays_deal);
                        justForYouModel.setPublished(published);
                        justForYouModel.setFeatured(featured);
                        justForYouModel.setCurrent_stock(current_stock);
                        justForYouModel.setUnit(unit);
                        justForYouModel.setDiscount(discount);
                        justForYouModel.setDiscount_type(discount_type);
                        justForYouModel.setTax(tax);
                        justForYouModel.setTax_type(tax_type);
                        justForYouModel.setShipping_type(shipping_type);
                        justForYouModel.setShipping_cost(shipping_cost);
                        justForYouModel.setWeight(weight);
                        justForYouModel.setParcel_size(parcel_size);
                        justForYouModel.setNum_of_sale(num_of_sale);
                        justForYouModel.setMeta_title(meta_title);
                        justForYouModel.setMeta_description(meta_description);
                        justForYouModel.setMeta_img(meta_img);
                        justForYouModel.setPdf(pdf);
                        justForYouModel.setSlug(slug);
                        justForYouModel.setRating(rating);
                        justForYouModel.setCreated_at(created_at);
                        justForYouModel.setUpdated_at(updated_at);

                        justForYouArrayList.add(justForYouModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
                    justForYouRecyvlerview.setLayoutManager(layoutManager);
                    justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
                    justForYouAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedJustForYou = justForYouArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedJustForYou.getId());
                            intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    justForYouRecyvlerview.setAdapter(justForYouAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadMobileHotPicks() {
        hotpicksRecyclerview = findViewById(R.id.hotpicksYouRecyclerView);
        hotPicksArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getMobileAndAccessoriesHotPicks", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        HotPicks hotPicksModel = new HotPicks();
                        hotPicksModel.setId(id);
                        hotPicksModel.setName(name);
                        hotPicksModel.setAdded_by(added_by);
                        hotPicksModel.setCategory_id(category_id);
                        hotPicksModel.setSubcategory_id(subcategory_id);
                        hotPicksModel.setSubsubcategory_id(subsubcategory_id);
                        hotPicksModel.setBrand_id(brand_id);
                        hotPicksModel.setPhotos(photos);
                        hotPicksModel.setThumbnail_img(thumbnail_img);
                        hotPicksModel.setFeatured_img(featured_img);
                        hotPicksModel.setFlash_deal_img(flash_deal_img);
                        hotPicksModel.setVideo_provider(video_provider);
                        hotPicksModel.setVideo_link(video_link);
                        hotPicksModel.setTags(tags);
                        hotPicksModel.setDescription(description);
                        hotPicksModel.setUnit_price(unit_price);
                        hotPicksModel.setPurchase_price(purchase_price);
                        hotPicksModel.setChoice_options(choice_options);
                        hotPicksModel.setColors(colors);
                        hotPicksModel.setVariations(variations);
                        hotPicksModel.setTodays_deal(todays_deal);
                        hotPicksModel.setPublished(published);
                        hotPicksModel.setFeatured(featured);
                        hotPicksModel.setCurrent_stock(current_stock);
                        hotPicksModel.setUnit(unit);
                        hotPicksModel.setDiscount(discount);
                        hotPicksModel.setDiscount_type(discount_type);
                        hotPicksModel.setTax(tax);
                        hotPicksModel.setTax_type(tax_type);
                        hotPicksModel.setShipping_type(shipping_type);
                        hotPicksModel.setShipping_cost(shipping_cost);
                        hotPicksModel.setWeight(weight);
                        hotPicksModel.setParcel_size(parcel_size);
                        hotPicksModel.setNum_of_sale(num_of_sale);
                        hotPicksModel.setMeta_title(meta_title);
                        hotPicksModel.setMeta_description(meta_description);
                        hotPicksModel.setMeta_img(meta_img);
                        hotPicksModel.setPdf(pdf);
                        hotPicksModel.setSlug(slug);
                        hotPicksModel.setRating(rating);
                        hotPicksModel.setCreated_at(created_at);
                        hotPicksModel.setUpdated_at(updated_at);

                        hotPicksArrayList.add(hotPicksModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    hotpicksRecyclerview.setLayoutManager(layoutManager);
                    hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
                    hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedHotPicks = hotPicksArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedHotPicks.getId());
                            intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    hotpicksRecyclerview.setAdapter(hotPicksAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }
    private void loadMobileFeaturedBrand(){

    }
    private void loadMobileNewArrivals() {
        newArrivalYouRecyclerView = findViewById(R.id.newArrivalYouRecyclerView);
        newArrivalArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getMobileAndAccessoriesNewArrivals", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        NewArrival newArrivalModel = new NewArrival();
                        newArrivalModel.setId(id);
                        newArrivalModel.setName(name);
                        newArrivalModel.setAdded_by(added_by);
                        newArrivalModel.setCategory_id(category_id);
                        newArrivalModel.setSubcategory_id(subcategory_id);
                        newArrivalModel.setSubsubcategory_id(subsubcategory_id);
                        newArrivalModel.setBrand_id(brand_id);
                        newArrivalModel.setPhotos(photos);
                        newArrivalModel.setThumbnail_img(thumbnail_img);
                        newArrivalModel.setFeatured_img(featured_img);
                        newArrivalModel.setFlash_deal_img(flash_deal_img);
                        newArrivalModel.setVideo_provider(video_provider);
                        newArrivalModel.setVideo_link(video_link);
                        newArrivalModel.setTags(tags);
                        newArrivalModel.setDescription(description);
                        newArrivalModel.setUnit_price(unit_price);
                        newArrivalModel.setPurchase_price(purchase_price);
                        newArrivalModel.setChoice_options(choice_options);
                        newArrivalModel.setColors(colors);
                        newArrivalModel.setVariations(variations);
                        newArrivalModel.setTodays_deal(todays_deal);
                        newArrivalModel.setPublished(published);
                        newArrivalModel.setFeatured(featured);
                        newArrivalModel.setCurrent_stock(current_stock);
                        newArrivalModel.setUnit(unit);
                        newArrivalModel.setDiscount(discount);
                        newArrivalModel.setDiscount_type(discount_type);
                        newArrivalModel.setTax(tax);
                        newArrivalModel.setTax_type(tax_type);
                        newArrivalModel.setShipping_type(shipping_type);
                        newArrivalModel.setShipping_cost(shipping_cost);
                        newArrivalModel.setWeight(weight);
                        newArrivalModel.setParcel_size(parcel_size);
                        newArrivalModel.setNum_of_sale(num_of_sale);
                        newArrivalModel.setMeta_title(meta_title);
                        newArrivalModel.setMeta_description(meta_description);
                        newArrivalModel.setMeta_img(meta_img);
                        newArrivalModel.setPdf(pdf);
                        newArrivalModel.setSlug(slug);
                        newArrivalModel.setRating(rating);
                        newArrivalModel.setCreated_at(created_at);
                        newArrivalModel.setUpdated_at(updated_at);

                        newArrivalArrayList.add(newArrivalModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    newArrivalYouRecyclerView.setLayoutManager(layoutManager);
                    newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
                    newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedNewArrival = newArrivalArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedNewArrival.getId());
                            intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadMobileJustForYou() {
        justForYouRecyvlerview = findViewById(R.id.justForYouRecyclerView);
        justForYouArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getMobileAndAccessoriesJustForYou", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        JustForYou justForYouModel = new JustForYou();
                        justForYouModel.setId(id);
                        justForYouModel.setName(name);
                        justForYouModel.setAdded_by(added_by);
                        justForYouModel.setCategory_id(category_id);
                        justForYouModel.setSubcategory_id(subcategory_id);
                        justForYouModel.setSubsubcategory_id(subsubcategory_id);
                        justForYouModel.setBrand_id(brand_id);
                        justForYouModel.setPhotos(photos);
                        justForYouModel.setThumbnail_img(thumbnail_img);
                        justForYouModel.setFeatured_img(featured_img);
                        justForYouModel.setFlash_deal_img(flash_deal_img);
                        justForYouModel.setVideo_provider(video_provider);
                        justForYouModel.setVideo_link(video_link);
                        justForYouModel.setTags(tags);
                        justForYouModel.setDescription(description);
                        justForYouModel.setUnit_price(unit_price);
                        justForYouModel.setPurchase_price(purchase_price);
                        justForYouModel.setChoice_options(choice_options);
                        justForYouModel.setColors(colors);
                        justForYouModel.setVariations(variations);
                        justForYouModel.setTodays_deal(todays_deal);
                        justForYouModel.setPublished(published);
                        justForYouModel.setFeatured(featured);
                        justForYouModel.setCurrent_stock(current_stock);
                        justForYouModel.setUnit(unit);
                        justForYouModel.setDiscount(discount);
                        justForYouModel.setDiscount_type(discount_type);
                        justForYouModel.setTax(tax);
                        justForYouModel.setTax_type(tax_type);
                        justForYouModel.setShipping_type(shipping_type);
                        justForYouModel.setShipping_cost(shipping_cost);
                        justForYouModel.setWeight(weight);
                        justForYouModel.setParcel_size(parcel_size);
                        justForYouModel.setNum_of_sale(num_of_sale);
                        justForYouModel.setMeta_title(meta_title);
                        justForYouModel.setMeta_description(meta_description);
                        justForYouModel.setMeta_img(meta_img);
                        justForYouModel.setPdf(pdf);
                        justForYouModel.setSlug(slug);
                        justForYouModel.setRating(rating);
                        justForYouModel.setCreated_at(created_at);
                        justForYouModel.setUpdated_at(updated_at);

                        justForYouArrayList.add(justForYouModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
                    justForYouRecyvlerview.setLayoutManager(layoutManager);
                    justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
                    justForYouAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedJustForYou = justForYouArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedJustForYou.getId());
                            intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    justForYouRecyvlerview.setAdapter(justForYouAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHealthHotPicks() {
        hotpicksRecyclerview = findViewById(R.id.hotpicksYouRecyclerView);
        hotPicksArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getHBPCHotPicks", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        HotPicks hotPicksModel = new HotPicks();
                        hotPicksModel.setId(id);
                        hotPicksModel.setName(name);
                        hotPicksModel.setAdded_by(added_by);
                        hotPicksModel.setCategory_id(category_id);
                        hotPicksModel.setSubcategory_id(subcategory_id);
                        hotPicksModel.setSubsubcategory_id(subsubcategory_id);
                        hotPicksModel.setBrand_id(brand_id);
                        hotPicksModel.setPhotos(photos);
                        hotPicksModel.setThumbnail_img(thumbnail_img);
                        hotPicksModel.setFeatured_img(featured_img);
                        hotPicksModel.setFlash_deal_img(flash_deal_img);
                        hotPicksModel.setVideo_provider(video_provider);
                        hotPicksModel.setVideo_link(video_link);
                        hotPicksModel.setTags(tags);
                        hotPicksModel.setDescription(description);
                        hotPicksModel.setUnit_price(unit_price);
                        hotPicksModel.setPurchase_price(purchase_price);
                        hotPicksModel.setChoice_options(choice_options);
                        hotPicksModel.setColors(colors);
                        hotPicksModel.setVariations(variations);
                        hotPicksModel.setTodays_deal(todays_deal);
                        hotPicksModel.setPublished(published);
                        hotPicksModel.setFeatured(featured);
                        hotPicksModel.setCurrent_stock(current_stock);
                        hotPicksModel.setUnit(unit);
                        hotPicksModel.setDiscount(discount);
                        hotPicksModel.setDiscount_type(discount_type);
                        hotPicksModel.setTax(tax);
                        hotPicksModel.setTax_type(tax_type);
                        hotPicksModel.setShipping_type(shipping_type);
                        hotPicksModel.setShipping_cost(shipping_cost);
                        hotPicksModel.setWeight(weight);
                        hotPicksModel.setParcel_size(parcel_size);
                        hotPicksModel.setNum_of_sale(num_of_sale);
                        hotPicksModel.setMeta_title(meta_title);
                        hotPicksModel.setMeta_description(meta_description);
                        hotPicksModel.setMeta_img(meta_img);
                        hotPicksModel.setPdf(pdf);
                        hotPicksModel.setSlug(slug);
                        hotPicksModel.setRating(rating);
                        hotPicksModel.setCreated_at(created_at);
                        hotPicksModel.setUpdated_at(updated_at);

                        hotPicksArrayList.add(hotPicksModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    hotpicksRecyclerview.setLayoutManager(layoutManager);
                    hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
                    hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedHotPicks = hotPicksArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedHotPicks.getId());
                            intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    hotpicksRecyclerview.setAdapter(hotPicksAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHealthFeaturedBrand(){

    }
    private void loadHealthNewArrivals() {
        newArrivalYouRecyclerView = findViewById(R.id.newArrivalYouRecyclerView);
        newArrivalArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getHBPCNewArrivals", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        NewArrival newArrivalModel = new NewArrival();
                        newArrivalModel.setId(id);
                        newArrivalModel.setName(name);
                        newArrivalModel.setAdded_by(added_by);
                        newArrivalModel.setCategory_id(category_id);
                        newArrivalModel.setSubcategory_id(subcategory_id);
                        newArrivalModel.setSubsubcategory_id(subsubcategory_id);
                        newArrivalModel.setBrand_id(brand_id);
                        newArrivalModel.setPhotos(photos);
                        newArrivalModel.setThumbnail_img(thumbnail_img);
                        newArrivalModel.setFeatured_img(featured_img);
                        newArrivalModel.setFlash_deal_img(flash_deal_img);
                        newArrivalModel.setVideo_provider(video_provider);
                        newArrivalModel.setVideo_link(video_link);
                        newArrivalModel.setTags(tags);
                        newArrivalModel.setDescription(description);
                        newArrivalModel.setUnit_price(unit_price);
                        newArrivalModel.setPurchase_price(purchase_price);
                        newArrivalModel.setChoice_options(choice_options);
                        newArrivalModel.setColors(colors);
                        newArrivalModel.setVariations(variations);
                        newArrivalModel.setTodays_deal(todays_deal);
                        newArrivalModel.setPublished(published);
                        newArrivalModel.setFeatured(featured);
                        newArrivalModel.setCurrent_stock(current_stock);
                        newArrivalModel.setUnit(unit);
                        newArrivalModel.setDiscount(discount);
                        newArrivalModel.setDiscount_type(discount_type);
                        newArrivalModel.setTax(tax);
                        newArrivalModel.setTax_type(tax_type);
                        newArrivalModel.setShipping_type(shipping_type);
                        newArrivalModel.setShipping_cost(shipping_cost);
                        newArrivalModel.setWeight(weight);
                        newArrivalModel.setParcel_size(parcel_size);
                        newArrivalModel.setNum_of_sale(num_of_sale);
                        newArrivalModel.setMeta_title(meta_title);
                        newArrivalModel.setMeta_description(meta_description);
                        newArrivalModel.setMeta_img(meta_img);
                        newArrivalModel.setPdf(pdf);
                        newArrivalModel.setSlug(slug);
                        newArrivalModel.setRating(rating);
                        newArrivalModel.setCreated_at(created_at);
                        newArrivalModel.setUpdated_at(updated_at);

                        newArrivalArrayList.add(newArrivalModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    newArrivalYouRecyclerView.setLayoutManager(layoutManager);
                    newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
                    newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedNewArrival = newArrivalArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedNewArrival.getId());
                            intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHealthJustForYou() {
        justForYouRecyvlerview = findViewById(R.id.justForYouRecyclerView);
        justForYouArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getHBPCJustForYou", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        JustForYou justForYouModel = new JustForYou();
                        justForYouModel.setId(id);
                        justForYouModel.setName(name);
                        justForYouModel.setAdded_by(added_by);
                        justForYouModel.setCategory_id(category_id);
                        justForYouModel.setSubcategory_id(subcategory_id);
                        justForYouModel.setSubsubcategory_id(subsubcategory_id);
                        justForYouModel.setBrand_id(brand_id);
                        justForYouModel.setPhotos(photos);
                        justForYouModel.setThumbnail_img(thumbnail_img);
                        justForYouModel.setFeatured_img(featured_img);
                        justForYouModel.setFlash_deal_img(flash_deal_img);
                        justForYouModel.setVideo_provider(video_provider);
                        justForYouModel.setVideo_link(video_link);
                        justForYouModel.setTags(tags);
                        justForYouModel.setDescription(description);
                        justForYouModel.setUnit_price(unit_price);
                        justForYouModel.setPurchase_price(purchase_price);
                        justForYouModel.setChoice_options(choice_options);
                        justForYouModel.setColors(colors);
                        justForYouModel.setVariations(variations);
                        justForYouModel.setTodays_deal(todays_deal);
                        justForYouModel.setPublished(published);
                        justForYouModel.setFeatured(featured);
                        justForYouModel.setCurrent_stock(current_stock);
                        justForYouModel.setUnit(unit);
                        justForYouModel.setDiscount(discount);
                        justForYouModel.setDiscount_type(discount_type);
                        justForYouModel.setTax(tax);
                        justForYouModel.setTax_type(tax_type);
                        justForYouModel.setShipping_type(shipping_type);
                        justForYouModel.setShipping_cost(shipping_cost);
                        justForYouModel.setWeight(weight);
                        justForYouModel.setParcel_size(parcel_size);
                        justForYouModel.setNum_of_sale(num_of_sale);
                        justForYouModel.setMeta_title(meta_title);
                        justForYouModel.setMeta_description(meta_description);
                        justForYouModel.setMeta_img(meta_img);
                        justForYouModel.setPdf(pdf);
                        justForYouModel.setSlug(slug);
                        justForYouModel.setRating(rating);
                        justForYouModel.setCreated_at(created_at);
                        justForYouModel.setUpdated_at(updated_at);

                        justForYouArrayList.add(justForYouModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
                    justForYouRecyvlerview.setLayoutManager(layoutManager);
                    justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
                    justForYouAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedJustForYou = justForYouArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedJustForYou.getId());
                            intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    justForYouRecyvlerview.setAdapter(justForYouAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadHardwareHotPicks(){
        hotpicksRecyclerview = findViewById(R.id.hotpicksYouRecyclerView);
        hotPicksArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getHardwaresAndToolsHotPicks", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        HotPicks hotPicksModel = new HotPicks();
                        hotPicksModel.setId(id);
                        hotPicksModel.setName(name);
                        hotPicksModel.setAdded_by(added_by);
                        hotPicksModel.setCategory_id(category_id);
                        hotPicksModel.setSubcategory_id(subcategory_id);
                        hotPicksModel.setSubsubcategory_id(subsubcategory_id);
                        hotPicksModel.setBrand_id(brand_id);
                        hotPicksModel.setPhotos(photos);
                        hotPicksModel.setThumbnail_img(thumbnail_img);
                        hotPicksModel.setFeatured_img(featured_img);
                        hotPicksModel.setFlash_deal_img(flash_deal_img);
                        hotPicksModel.setVideo_provider(video_provider);
                        hotPicksModel.setVideo_link(video_link);
                        hotPicksModel.setTags(tags);
                        hotPicksModel.setDescription(description);
                        hotPicksModel.setUnit_price(unit_price);
                        hotPicksModel.setPurchase_price(purchase_price);
                        hotPicksModel.setChoice_options(choice_options);
                        hotPicksModel.setColors(colors);
                        hotPicksModel.setVariations(variations);
                        hotPicksModel.setTodays_deal(todays_deal);
                        hotPicksModel.setPublished(published);
                        hotPicksModel.setFeatured(featured);
                        hotPicksModel.setCurrent_stock(current_stock);
                        hotPicksModel.setUnit(unit);
                        hotPicksModel.setDiscount(discount);
                        hotPicksModel.setDiscount_type(discount_type);
                        hotPicksModel.setTax(tax);
                        hotPicksModel.setTax_type(tax_type);
                        hotPicksModel.setShipping_type(shipping_type);
                        hotPicksModel.setShipping_cost(shipping_cost);
                        hotPicksModel.setWeight(weight);
                        hotPicksModel.setParcel_size(parcel_size);
                        hotPicksModel.setNum_of_sale(num_of_sale);
                        hotPicksModel.setMeta_title(meta_title);
                        hotPicksModel.setMeta_description(meta_description);
                        hotPicksModel.setMeta_img(meta_img);
                        hotPicksModel.setPdf(pdf);
                        hotPicksModel.setSlug(slug);
                        hotPicksModel.setRating(rating);
                        hotPicksModel.setCreated_at(created_at);
                        hotPicksModel.setUpdated_at(updated_at);

                        hotPicksArrayList.add(hotPicksModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    hotpicksRecyclerview.setLayoutManager(layoutManager);
                    hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
                    hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedHotPicks = hotPicksArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedHotPicks.getId());
                            intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    hotpicksRecyclerview.setAdapter(hotPicksAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHardwareFeatureBrand(){

    }
    private void loadHardwareNewArrivals(){
        newArrivalYouRecyclerView = findViewById(R.id.newArrivalYouRecyclerView);
        newArrivalArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getHardwaresAndToolsNewArrivals", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        NewArrival newArrivalModel = new NewArrival();
                        newArrivalModel.setId(id);
                        newArrivalModel.setName(name);
                        newArrivalModel.setAdded_by(added_by);
                        newArrivalModel.setCategory_id(category_id);
                        newArrivalModel.setSubcategory_id(subcategory_id);
                        newArrivalModel.setSubsubcategory_id(subsubcategory_id);
                        newArrivalModel.setBrand_id(brand_id);
                        newArrivalModel.setPhotos(photos);
                        newArrivalModel.setThumbnail_img(thumbnail_img);
                        newArrivalModel.setFeatured_img(featured_img);
                        newArrivalModel.setFlash_deal_img(flash_deal_img);
                        newArrivalModel.setVideo_provider(video_provider);
                        newArrivalModel.setVideo_link(video_link);
                        newArrivalModel.setTags(tags);
                        newArrivalModel.setDescription(description);
                        newArrivalModel.setUnit_price(unit_price);
                        newArrivalModel.setPurchase_price(purchase_price);
                        newArrivalModel.setChoice_options(choice_options);
                        newArrivalModel.setColors(colors);
                        newArrivalModel.setVariations(variations);
                        newArrivalModel.setTodays_deal(todays_deal);
                        newArrivalModel.setPublished(published);
                        newArrivalModel.setFeatured(featured);
                        newArrivalModel.setCurrent_stock(current_stock);
                        newArrivalModel.setUnit(unit);
                        newArrivalModel.setDiscount(discount);
                        newArrivalModel.setDiscount_type(discount_type);
                        newArrivalModel.setTax(tax);
                        newArrivalModel.setTax_type(tax_type);
                        newArrivalModel.setShipping_type(shipping_type);
                        newArrivalModel.setShipping_cost(shipping_cost);
                        newArrivalModel.setWeight(weight);
                        newArrivalModel.setParcel_size(parcel_size);
                        newArrivalModel.setNum_of_sale(num_of_sale);
                        newArrivalModel.setMeta_title(meta_title);
                        newArrivalModel.setMeta_description(meta_description);
                        newArrivalModel.setMeta_img(meta_img);
                        newArrivalModel.setPdf(pdf);
                        newArrivalModel.setSlug(slug);
                        newArrivalModel.setRating(rating);
                        newArrivalModel.setCreated_at(created_at);
                        newArrivalModel.setUpdated_at(updated_at);

                        newArrivalArrayList.add(newArrivalModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    newArrivalYouRecyclerView.setLayoutManager(layoutManager);
                    newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
                    newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedNewArrival = newArrivalArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedNewArrival.getId());
                            intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHardwareJustForYou(){
        justForYouRecyvlerview = findViewById(R.id.justForYouRecyclerView);
        justForYouArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getHardwaresAndToolsJustForYou", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        JustForYou justForYouModel = new JustForYou();
                        justForYouModel.setId(id);
                        justForYouModel.setName(name);
                        justForYouModel.setAdded_by(added_by);
                        justForYouModel.setCategory_id(category_id);
                        justForYouModel.setSubcategory_id(subcategory_id);
                        justForYouModel.setSubsubcategory_id(subsubcategory_id);
                        justForYouModel.setBrand_id(brand_id);
                        justForYouModel.setPhotos(photos);
                        justForYouModel.setThumbnail_img(thumbnail_img);
                        justForYouModel.setFeatured_img(featured_img);
                        justForYouModel.setFlash_deal_img(flash_deal_img);
                        justForYouModel.setVideo_provider(video_provider);
                        justForYouModel.setVideo_link(video_link);
                        justForYouModel.setTags(tags);
                        justForYouModel.setDescription(description);
                        justForYouModel.setUnit_price(unit_price);
                        justForYouModel.setPurchase_price(purchase_price);
                        justForYouModel.setChoice_options(choice_options);
                        justForYouModel.setColors(colors);
                        justForYouModel.setVariations(variations);
                        justForYouModel.setTodays_deal(todays_deal);
                        justForYouModel.setPublished(published);
                        justForYouModel.setFeatured(featured);
                        justForYouModel.setCurrent_stock(current_stock);
                        justForYouModel.setUnit(unit);
                        justForYouModel.setDiscount(discount);
                        justForYouModel.setDiscount_type(discount_type);
                        justForYouModel.setTax(tax);
                        justForYouModel.setTax_type(tax_type);
                        justForYouModel.setShipping_type(shipping_type);
                        justForYouModel.setShipping_cost(shipping_cost);
                        justForYouModel.setWeight(weight);
                        justForYouModel.setParcel_size(parcel_size);
                        justForYouModel.setNum_of_sale(num_of_sale);
                        justForYouModel.setMeta_title(meta_title);
                        justForYouModel.setMeta_description(meta_description);
                        justForYouModel.setMeta_img(meta_img);
                        justForYouModel.setPdf(pdf);
                        justForYouModel.setSlug(slug);
                        justForYouModel.setRating(rating);
                        justForYouModel.setCreated_at(created_at);
                        justForYouModel.setUpdated_at(updated_at);

                        justForYouArrayList.add(justForYouModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
                    justForYouRecyvlerview.setLayoutManager(layoutManager);
                    justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
                    justForYouAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedJustForYou = justForYouArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedJustForYou.getId());
                            intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    justForYouRecyvlerview.setAdapter(justForYouAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadHomeHotPicks(){
        hotpicksRecyclerview = findViewById(R.id.hotpicksYouRecyclerView);
        hotPicksArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getHomeAndAppliancesHotPicks", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        HotPicks hotPicksModel = new HotPicks();
                        hotPicksModel.setId(id);
                        hotPicksModel.setName(name);
                        hotPicksModel.setAdded_by(added_by);
                        hotPicksModel.setCategory_id(category_id);
                        hotPicksModel.setSubcategory_id(subcategory_id);
                        hotPicksModel.setSubsubcategory_id(subsubcategory_id);
                        hotPicksModel.setBrand_id(brand_id);
                        hotPicksModel.setPhotos(photos);
                        hotPicksModel.setThumbnail_img(thumbnail_img);
                        hotPicksModel.setFeatured_img(featured_img);
                        hotPicksModel.setFlash_deal_img(flash_deal_img);
                        hotPicksModel.setVideo_provider(video_provider);
                        hotPicksModel.setVideo_link(video_link);
                        hotPicksModel.setTags(tags);
                        hotPicksModel.setDescription(description);
                        hotPicksModel.setUnit_price(unit_price);
                        hotPicksModel.setPurchase_price(purchase_price);
                        hotPicksModel.setChoice_options(choice_options);
                        hotPicksModel.setColors(colors);
                        hotPicksModel.setVariations(variations);
                        hotPicksModel.setTodays_deal(todays_deal);
                        hotPicksModel.setPublished(published);
                        hotPicksModel.setFeatured(featured);
                        hotPicksModel.setCurrent_stock(current_stock);
                        hotPicksModel.setUnit(unit);
                        hotPicksModel.setDiscount(discount);
                        hotPicksModel.setDiscount_type(discount_type);
                        hotPicksModel.setTax(tax);
                        hotPicksModel.setTax_type(tax_type);
                        hotPicksModel.setShipping_type(shipping_type);
                        hotPicksModel.setShipping_cost(shipping_cost);
                        hotPicksModel.setWeight(weight);
                        hotPicksModel.setParcel_size(parcel_size);
                        hotPicksModel.setNum_of_sale(num_of_sale);
                        hotPicksModel.setMeta_title(meta_title);
                        hotPicksModel.setMeta_description(meta_description);
                        hotPicksModel.setMeta_img(meta_img);
                        hotPicksModel.setPdf(pdf);
                        hotPicksModel.setSlug(slug);
                        hotPicksModel.setRating(rating);
                        hotPicksModel.setCreated_at(created_at);
                        hotPicksModel.setUpdated_at(updated_at);

                        hotPicksArrayList.add(hotPicksModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    hotpicksRecyclerview.setLayoutManager(layoutManager);
                    hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
                    hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedHotPicks = hotPicksArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedHotPicks.getId());
                            intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    hotpicksRecyclerview.setAdapter(hotPicksAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHomeFeaturedBrand(){}
    private void loadHomeNewArrivals(){
        newArrivalYouRecyclerView = findViewById(R.id.newArrivalYouRecyclerView);
        newArrivalArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getHomeAndAppliancesNewArrivals", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        NewArrival newArrivalModel = new NewArrival();
                        newArrivalModel.setId(id);
                        newArrivalModel.setName(name);
                        newArrivalModel.setAdded_by(added_by);
                        newArrivalModel.setCategory_id(category_id);
                        newArrivalModel.setSubcategory_id(subcategory_id);
                        newArrivalModel.setSubsubcategory_id(subsubcategory_id);
                        newArrivalModel.setBrand_id(brand_id);
                        newArrivalModel.setPhotos(photos);
                        newArrivalModel.setThumbnail_img(thumbnail_img);
                        newArrivalModel.setFeatured_img(featured_img);
                        newArrivalModel.setFlash_deal_img(flash_deal_img);
                        newArrivalModel.setVideo_provider(video_provider);
                        newArrivalModel.setVideo_link(video_link);
                        newArrivalModel.setTags(tags);
                        newArrivalModel.setDescription(description);
                        newArrivalModel.setUnit_price(unit_price);
                        newArrivalModel.setPurchase_price(purchase_price);
                        newArrivalModel.setChoice_options(choice_options);
                        newArrivalModel.setColors(colors);
                        newArrivalModel.setVariations(variations);
                        newArrivalModel.setTodays_deal(todays_deal);
                        newArrivalModel.setPublished(published);
                        newArrivalModel.setFeatured(featured);
                        newArrivalModel.setCurrent_stock(current_stock);
                        newArrivalModel.setUnit(unit);
                        newArrivalModel.setDiscount(discount);
                        newArrivalModel.setDiscount_type(discount_type);
                        newArrivalModel.setTax(tax);
                        newArrivalModel.setTax_type(tax_type);
                        newArrivalModel.setShipping_type(shipping_type);
                        newArrivalModel.setShipping_cost(shipping_cost);
                        newArrivalModel.setWeight(weight);
                        newArrivalModel.setParcel_size(parcel_size);
                        newArrivalModel.setNum_of_sale(num_of_sale);
                        newArrivalModel.setMeta_title(meta_title);
                        newArrivalModel.setMeta_description(meta_description);
                        newArrivalModel.setMeta_img(meta_img);
                        newArrivalModel.setPdf(pdf);
                        newArrivalModel.setSlug(slug);
                        newArrivalModel.setRating(rating);
                        newArrivalModel.setCreated_at(created_at);
                        newArrivalModel.setUpdated_at(updated_at);

                        newArrivalArrayList.add(newArrivalModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    newArrivalYouRecyclerView.setLayoutManager(layoutManager);
                    newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
                    newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedNewArrival = newArrivalArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedNewArrival.getId());
                            intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadHomeJustForYou(){
        justForYouRecyvlerview = findViewById(R.id.justForYouRecyclerView);
        justForYouArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getHomeAndAppliancesJustForYou", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        JustForYou justForYouModel = new JustForYou();
                        justForYouModel.setId(id);
                        justForYouModel.setName(name);
                        justForYouModel.setAdded_by(added_by);
                        justForYouModel.setCategory_id(category_id);
                        justForYouModel.setSubcategory_id(subcategory_id);
                        justForYouModel.setSubsubcategory_id(subsubcategory_id);
                        justForYouModel.setBrand_id(brand_id);
                        justForYouModel.setPhotos(photos);
                        justForYouModel.setThumbnail_img(thumbnail_img);
                        justForYouModel.setFeatured_img(featured_img);
                        justForYouModel.setFlash_deal_img(flash_deal_img);
                        justForYouModel.setVideo_provider(video_provider);
                        justForYouModel.setVideo_link(video_link);
                        justForYouModel.setTags(tags);
                        justForYouModel.setDescription(description);
                        justForYouModel.setUnit_price(unit_price);
                        justForYouModel.setPurchase_price(purchase_price);
                        justForYouModel.setChoice_options(choice_options);
                        justForYouModel.setColors(colors);
                        justForYouModel.setVariations(variations);
                        justForYouModel.setTodays_deal(todays_deal);
                        justForYouModel.setPublished(published);
                        justForYouModel.setFeatured(featured);
                        justForYouModel.setCurrent_stock(current_stock);
                        justForYouModel.setUnit(unit);
                        justForYouModel.setDiscount(discount);
                        justForYouModel.setDiscount_type(discount_type);
                        justForYouModel.setTax(tax);
                        justForYouModel.setTax_type(tax_type);
                        justForYouModel.setShipping_type(shipping_type);
                        justForYouModel.setShipping_cost(shipping_cost);
                        justForYouModel.setWeight(weight);
                        justForYouModel.setParcel_size(parcel_size);
                        justForYouModel.setNum_of_sale(num_of_sale);
                        justForYouModel.setMeta_title(meta_title);
                        justForYouModel.setMeta_description(meta_description);
                        justForYouModel.setMeta_img(meta_img);
                        justForYouModel.setPdf(pdf);
                        justForYouModel.setSlug(slug);
                        justForYouModel.setRating(rating);
                        justForYouModel.setCreated_at(created_at);
                        justForYouModel.setUpdated_at(updated_at);

                        justForYouArrayList.add(justForYouModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
                    justForYouRecyvlerview.setLayoutManager(layoutManager);
                    justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
                    justForYouAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedJustForYou = justForYouArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedJustForYou.getId());
                            intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    justForYouRecyvlerview.setAdapter(justForYouAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadAutomobileHotPicks(){
        hotpicksRecyclerview = findViewById(R.id.hotpicksYouRecyclerView);
        hotPicksArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAutomobileAndAppliancesHotPicks", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        HotPicks hotPicksModel = new HotPicks();
                        hotPicksModel.setId(id);
                        hotPicksModel.setName(name);
                        hotPicksModel.setAdded_by(added_by);
                        hotPicksModel.setCategory_id(category_id);
                        hotPicksModel.setSubcategory_id(subcategory_id);
                        hotPicksModel.setSubsubcategory_id(subsubcategory_id);
                        hotPicksModel.setBrand_id(brand_id);
                        hotPicksModel.setPhotos(photos);
                        hotPicksModel.setThumbnail_img(thumbnail_img);
                        hotPicksModel.setFeatured_img(featured_img);
                        hotPicksModel.setFlash_deal_img(flash_deal_img);
                        hotPicksModel.setVideo_provider(video_provider);
                        hotPicksModel.setVideo_link(video_link);
                        hotPicksModel.setTags(tags);
                        hotPicksModel.setDescription(description);
                        hotPicksModel.setUnit_price(unit_price);
                        hotPicksModel.setPurchase_price(purchase_price);
                        hotPicksModel.setChoice_options(choice_options);
                        hotPicksModel.setColors(colors);
                        hotPicksModel.setVariations(variations);
                        hotPicksModel.setTodays_deal(todays_deal);
                        hotPicksModel.setPublished(published);
                        hotPicksModel.setFeatured(featured);
                        hotPicksModel.setCurrent_stock(current_stock);
                        hotPicksModel.setUnit(unit);
                        hotPicksModel.setDiscount(discount);
                        hotPicksModel.setDiscount_type(discount_type);
                        hotPicksModel.setTax(tax);
                        hotPicksModel.setTax_type(tax_type);
                        hotPicksModel.setShipping_type(shipping_type);
                        hotPicksModel.setShipping_cost(shipping_cost);
                        hotPicksModel.setWeight(weight);
                        hotPicksModel.setParcel_size(parcel_size);
                        hotPicksModel.setNum_of_sale(num_of_sale);
                        hotPicksModel.setMeta_title(meta_title);
                        hotPicksModel.setMeta_description(meta_description);
                        hotPicksModel.setMeta_img(meta_img);
                        hotPicksModel.setPdf(pdf);
                        hotPicksModel.setSlug(slug);
                        hotPicksModel.setRating(rating);
                        hotPicksModel.setCreated_at(created_at);
                        hotPicksModel.setUpdated_at(updated_at);

                        hotPicksArrayList.add(hotPicksModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    hotpicksRecyclerview.setLayoutManager(layoutManager);
                    hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
                    hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedHotPicks = hotPicksArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedHotPicks.getId());
                            intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    hotpicksRecyclerview.setAdapter(hotPicksAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadAutomobileFeaturedBrand(){}
    private void loadAutomobileNewArrivals(){
        hotpicksRecyclerview = findViewById(R.id.hotpicksYouRecyclerView);
        hotPicksArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAutomobileAndAppliancesNewArrivals", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        HotPicks hotPicksModel = new HotPicks();
                        hotPicksModel.setId(id);
                        hotPicksModel.setName(name);
                        hotPicksModel.setAdded_by(added_by);
                        hotPicksModel.setCategory_id(category_id);
                        hotPicksModel.setSubcategory_id(subcategory_id);
                        hotPicksModel.setSubsubcategory_id(subsubcategory_id);
                        hotPicksModel.setBrand_id(brand_id);
                        hotPicksModel.setPhotos(photos);
                        hotPicksModel.setThumbnail_img(thumbnail_img);
                        hotPicksModel.setFeatured_img(featured_img);
                        hotPicksModel.setFlash_deal_img(flash_deal_img);
                        hotPicksModel.setVideo_provider(video_provider);
                        hotPicksModel.setVideo_link(video_link);
                        hotPicksModel.setTags(tags);
                        hotPicksModel.setDescription(description);
                        hotPicksModel.setUnit_price(unit_price);
                        hotPicksModel.setPurchase_price(purchase_price);
                        hotPicksModel.setChoice_options(choice_options);
                        hotPicksModel.setColors(colors);
                        hotPicksModel.setVariations(variations);
                        hotPicksModel.setTodays_deal(todays_deal);
                        hotPicksModel.setPublished(published);
                        hotPicksModel.setFeatured(featured);
                        hotPicksModel.setCurrent_stock(current_stock);
                        hotPicksModel.setUnit(unit);
                        hotPicksModel.setDiscount(discount);
                        hotPicksModel.setDiscount_type(discount_type);
                        hotPicksModel.setTax(tax);
                        hotPicksModel.setTax_type(tax_type);
                        hotPicksModel.setShipping_type(shipping_type);
                        hotPicksModel.setShipping_cost(shipping_cost);
                        hotPicksModel.setWeight(weight);
                        hotPicksModel.setParcel_size(parcel_size);
                        hotPicksModel.setNum_of_sale(num_of_sale);
                        hotPicksModel.setMeta_title(meta_title);
                        hotPicksModel.setMeta_description(meta_description);
                        hotPicksModel.setMeta_img(meta_img);
                        hotPicksModel.setPdf(pdf);
                        hotPicksModel.setSlug(slug);
                        hotPicksModel.setRating(rating);
                        hotPicksModel.setCreated_at(created_at);
                        hotPicksModel.setUpdated_at(updated_at);

                        hotPicksArrayList.add(hotPicksModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                    hotpicksRecyclerview.setLayoutManager(layoutManager);
                    hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
                    hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedHotPicks = hotPicksArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedHotPicks.getId());
                            intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    hotpicksRecyclerview.setAdapter(hotPicksAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadAutomobileJustForYou(){
        hotpicksRecyclerview = findViewById(R.id.hotpicksYouRecyclerView);
        hotPicksArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAutomobileAndAppliancesJustForYou", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("FEATURED: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        JustForYou justForYouModel = new JustForYou();
                        justForYouModel.setId(id);
                        justForYouModel.setName(name);
                        justForYouModel.setAdded_by(added_by);
                        justForYouModel.setCategory_id(category_id);
                        justForYouModel.setSubcategory_id(subcategory_id);
                        justForYouModel.setSubsubcategory_id(subsubcategory_id);
                        justForYouModel.setBrand_id(brand_id);
                        justForYouModel.setPhotos(photos);
                        justForYouModel.setThumbnail_img(thumbnail_img);
                        justForYouModel.setFeatured_img(featured_img);
                        justForYouModel.setFlash_deal_img(flash_deal_img);
                        justForYouModel.setVideo_provider(video_provider);
                        justForYouModel.setVideo_link(video_link);
                        justForYouModel.setTags(tags);
                        justForYouModel.setDescription(description);
                        justForYouModel.setUnit_price(unit_price);
                        justForYouModel.setPurchase_price(purchase_price);
                        justForYouModel.setChoice_options(choice_options);
                        justForYouModel.setColors(colors);
                        justForYouModel.setVariations(variations);
                        justForYouModel.setTodays_deal(todays_deal);
                        justForYouModel.setPublished(published);
                        justForYouModel.setFeatured(featured);
                        justForYouModel.setCurrent_stock(current_stock);
                        justForYouModel.setUnit(unit);
                        justForYouModel.setDiscount(discount);
                        justForYouModel.setDiscount_type(discount_type);
                        justForYouModel.setTax(tax);
                        justForYouModel.setTax_type(tax_type);
                        justForYouModel.setShipping_type(shipping_type);
                        justForYouModel.setShipping_cost(shipping_cost);
                        justForYouModel.setWeight(weight);
                        justForYouModel.setParcel_size(parcel_size);
                        justForYouModel.setNum_of_sale(num_of_sale);
                        justForYouModel.setMeta_title(meta_title);
                        justForYouModel.setMeta_description(meta_description);
                        justForYouModel.setMeta_img(meta_img);
                        justForYouModel.setPdf(pdf);
                        justForYouModel.setSlug(slug);
                        justForYouModel.setRating(rating);
                        justForYouModel.setCreated_at(created_at);
                        justForYouModel.setUpdated_at(updated_at);

                        justForYouArrayList.add(justForYouModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
                    justForYouRecyvlerview.setLayoutManager(layoutManager);
                    justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
                    justForYouAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedJustForYou = justForYouArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedJustForYou.getId());
                            intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    justForYouRecyvlerview.setAdapter(justForYouAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        resumeAllCategories();
        switch (categoryID){
            case 2:
                meanHotPicks.setText("Women's Hot Picks");
                resumeWomenHotPicks();
                resumeWomenFeaturedProducts();
                resumeWomenNewArrivals();
                resumeWomenJustForYou();
                break;
            case 3:
                meanHotPicks.setText("Men's Hot Picks");
                resumeMensHotPicks();
                resumeMensFeaturedProducts();
                resumeMensNewArrivals();
                resumeMensJustForYou();
                break;
            case 4:
                resumeComputerHotPicks();
                resumeComputerFeaturedProducts();
                resumeComputerNewArrivals();
                resumeComputerJustForYou();
                meanHotPicks.setText("Computer Hot Picks");
                break;
            case 5:
                meanHotPicks.setText("Kids Hot Picks");
                resumeKidsHotPicks();
                resumeKidsFeaturedProducts();
                resumeKidsNewArrivals();
                resumeKidsJustForYou();
                break;
            case 6:
                meanHotPicks.setText("Sports Hot Picks");
                resumeSportsHotPicks();
                resumeSportsFeaturedProducts();
                resumeSportsNewArrivals();
                resumeSportsJustForYou();
                break;
            case 7:
                meanHotPicks.setText("Jewelries Hot Picks");
                resumeJewelryHotPicks();
                resumeJewelryFeaturedProducts();
                resumeJewelryNewArrivals();
                resumeJewelryJustForYou();
                break;
            case 8:
                meanHotPicks.setText("Mobile Hot Picks");
                resumeMobileHotPicks();
                resumeMobileFeaturedProducts();
                resumeMobileNewArrivals();
                resumeMobileJustForYou();
                break;
            case 9:
                meanHotPicks.setText("Health's Hot Picks");
                resumeHealthHotPicks();
                resumeHealthFeaturedProducts();
                resumeHealthNewArrivals();
                resumeHealthJustForYou();
                break;
            case 10:
                meanHotPicks.setText("Hardware Hot Picks");
                resumeHardwareHotPicks();
                resumeHardwareFeaturedProducts();
                resumeHardwareNewArrivals();
                resumeHardwareJustForYou();
                break;
            case 11:
                meanHotPicks.setText("Home Hot Picks");
                resumeHomeHotPicks();
                resumeHomeFeaturedProducts();
                resumeHomeNewArrivals();
                resumeHomeJustForYou();
                break;
            case 12:
                meanHotPicks.setText("Automobile Hot Picks");
                resumeAutomobileHotPicks();
                resumeAutomobileFeaturedProducts();
                resumeAutomobileNewArrivals();
                resumeAutomobileJustForYou();
                break;
            default:
        }
    }

    private void resumeAllCategories(){
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        categoryMenRecyclerview.setLayoutManager(layoutManager);

        subCategoriesAdapter = new SubCategoriesAdapter(context, subCategoriesArrayList);
        subCategoriesAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedSubcategories = subCategoriesArrayList.get(position);
                Intent intent = new Intent(context, ProductCategoryActivity.class);
                intent.putExtra("SUBCATEGORYID", selectedSubcategories.getId());
                intent.putExtra("SUBCATEGORYNAME", selectedSubcategories.getName());
                startActivity(intent);
            }
        });
        categoryMenRecyclerview.setAdapter(subCategoriesAdapter);
    }
    private void resumeMensHotPicks(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        hotpicksRecyclerview.setLayoutManager(layoutManager);

        hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
        hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedHotPicks = hotPicksArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedHotPicks.getId());
                intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        hotpicksRecyclerview.setAdapter(hotPicksAdapter);
    }
    private void resumeMensFeaturedProducts(){}
    private void resumeMensNewArrivals(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        newArrivalYouRecyclerView.setLayoutManager(layoutManager);

        newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
        newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedNewArrival = newArrivalArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedNewArrival.getId());
                intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
    }
    private void resumeMensJustForYou(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
        justForYouRecyvlerview.setLayoutManager(layoutManager);

        justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
        justForYouAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedJustForYou = justForYouArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedJustForYou.getId());
                intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        justForYouRecyvlerview.setAdapter(justForYouAdapter);
    }

    private void resumeWomenHotPicks(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        hotpicksRecyclerview.setLayoutManager(layoutManager);

        hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
        hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedHotPicks = hotPicksArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedHotPicks.getId());
                intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        hotpicksRecyclerview.setAdapter(hotPicksAdapter);
    }
    private void resumeWomenFeaturedProducts(){}
    private void resumeWomenNewArrivals(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        newArrivalYouRecyclerView.setLayoutManager(layoutManager);

        newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
        newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedNewArrival = newArrivalArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedNewArrival.getId());
                intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
    }
    private void resumeWomenJustForYou(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
        justForYouRecyvlerview.setLayoutManager(layoutManager);

        justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
        justForYouAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedJustForYou = justForYouArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedJustForYou.getId());
                intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        justForYouRecyvlerview.setAdapter(justForYouAdapter);
    }

    private void resumeComputerHotPicks(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        hotpicksRecyclerview.setLayoutManager(layoutManager);

        hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
        hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedHotPicks = hotPicksArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedHotPicks.getId());
                intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        hotpicksRecyclerview.setAdapter(hotPicksAdapter);
    }
    private void resumeComputerFeaturedProducts(){}
    private void resumeComputerNewArrivals(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        newArrivalYouRecyclerView.setLayoutManager(layoutManager);

        newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
        newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedNewArrival = newArrivalArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedNewArrival.getId());
                intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
    }
    private void resumeComputerJustForYou(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
        justForYouRecyvlerview.setLayoutManager(layoutManager);

        justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
        justForYouAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedJustForYou = justForYouArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedJustForYou.getId());
                intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        justForYouRecyvlerview.setAdapter(justForYouAdapter);
    }

    private void resumeKidsHotPicks(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        hotpicksRecyclerview.setLayoutManager(layoutManager);

        hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
        hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedHotPicks = hotPicksArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedHotPicks.getId());
                intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        hotpicksRecyclerview.setAdapter(hotPicksAdapter);
    }
    private void resumeKidsFeaturedProducts(){}
    private void resumeKidsNewArrivals(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        newArrivalYouRecyclerView.setLayoutManager(layoutManager);

        newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
        newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedNewArrival = newArrivalArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedNewArrival.getId());
                intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
    }
    private void resumeKidsJustForYou(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
        justForYouRecyvlerview.setLayoutManager(layoutManager);

        justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
        justForYouAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedJustForYou = justForYouArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedJustForYou.getId());
                intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        justForYouRecyvlerview.setAdapter(justForYouAdapter);
    }

    private void resumeSportsHotPicks(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        hotpicksRecyclerview.setLayoutManager(layoutManager);

        hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
        hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedHotPicks = hotPicksArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedHotPicks.getId());
                intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        hotpicksRecyclerview.setAdapter(hotPicksAdapter);
    }
    private void resumeSportsFeaturedProducts(){}
    private void resumeSportsNewArrivals(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        newArrivalYouRecyclerView.setLayoutManager(layoutManager);

        newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
        newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedNewArrival = newArrivalArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedNewArrival.getId());
                intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
    }
    private void resumeSportsJustForYou(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
        justForYouRecyvlerview.setLayoutManager(layoutManager);

        justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
        justForYouAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedJustForYou = justForYouArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedJustForYou.getId());
                intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        justForYouRecyvlerview.setAdapter(justForYouAdapter);
    }

    private void resumeJewelryHotPicks(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        hotpicksRecyclerview.setLayoutManager(layoutManager);

        hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
        hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedHotPicks = hotPicksArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedHotPicks.getId());
                intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        hotpicksRecyclerview.setAdapter(hotPicksAdapter);
    }
    private void resumeJewelryFeaturedProducts(){}
    private void resumeJewelryNewArrivals(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        newArrivalYouRecyclerView.setLayoutManager(layoutManager);

        newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
        newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedNewArrival = newArrivalArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedNewArrival.getId());
                intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
    }
    private void resumeJewelryJustForYou(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
        justForYouRecyvlerview.setLayoutManager(layoutManager);

        justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
        justForYouAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedJustForYou = justForYouArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedJustForYou.getId());
                intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        justForYouRecyvlerview.setAdapter(justForYouAdapter);
    }

    private void resumeMobileHotPicks(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        hotpicksRecyclerview.setLayoutManager(layoutManager);

        hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
        hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedHotPicks = hotPicksArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedHotPicks.getId());
                intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        hotpicksRecyclerview.setAdapter(hotPicksAdapter);
    }
    private void resumeMobileFeaturedProducts(){}
    private void resumeMobileNewArrivals(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        newArrivalYouRecyclerView.setLayoutManager(layoutManager);

        newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
        newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedNewArrival = newArrivalArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedNewArrival.getId());
                intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
    }
    private void resumeMobileJustForYou(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
        justForYouRecyvlerview.setLayoutManager(layoutManager);

        justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
        justForYouAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedJustForYou = justForYouArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedJustForYou.getId());
                intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        justForYouRecyvlerview.setAdapter(justForYouAdapter);
    }

    private void resumeHealthHotPicks(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        hotpicksRecyclerview.setLayoutManager(layoutManager);

        hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
        hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedHotPicks = hotPicksArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedHotPicks.getId());
                intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        hotpicksRecyclerview.setAdapter(hotPicksAdapter);
    }
    private void resumeHealthFeaturedProducts(){}
    private void resumeHealthNewArrivals(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        newArrivalYouRecyclerView.setLayoutManager(layoutManager);

        newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
        newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedNewArrival = newArrivalArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedNewArrival.getId());
                intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
    }
    private void resumeHealthJustForYou(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
        justForYouRecyvlerview.setLayoutManager(layoutManager);

        justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
        justForYouAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedJustForYou = justForYouArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedJustForYou.getId());
                intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        justForYouRecyvlerview.setAdapter(justForYouAdapter);
    }

    private void resumeHardwareHotPicks(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        hotpicksRecyclerview.setLayoutManager(layoutManager);

        hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
        hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedHotPicks = hotPicksArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedHotPicks.getId());
                intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        hotpicksRecyclerview.setAdapter(hotPicksAdapter);
    }
    private void resumeHardwareFeaturedProducts(){}
    private void resumeHardwareNewArrivals(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        newArrivalYouRecyclerView.setLayoutManager(layoutManager);

        newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
        newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedNewArrival = newArrivalArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedNewArrival.getId());
                intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
    }
    private void resumeHardwareJustForYou(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
        justForYouRecyvlerview.setLayoutManager(layoutManager);

        justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
        justForYouAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedJustForYou = justForYouArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedJustForYou.getId());
                intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        justForYouRecyvlerview.setAdapter(justForYouAdapter);
    }

    private void resumeHomeHotPicks(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        hotpicksRecyclerview.setLayoutManager(layoutManager);

        hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
        hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedHotPicks = hotPicksArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedHotPicks.getId());
                intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        hotpicksRecyclerview.setAdapter(hotPicksAdapter);
    }
    private void resumeHomeFeaturedProducts(){}
    private void resumeHomeNewArrivals(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        newArrivalYouRecyclerView.setLayoutManager(layoutManager);

        newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
        newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedNewArrival = newArrivalArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedNewArrival.getId());
                intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);

    }
    private void resumeHomeJustForYou(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
        justForYouRecyvlerview.setLayoutManager(layoutManager);

        justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
        justForYouAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedJustForYou = justForYouArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedJustForYou.getId());
                intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        justForYouRecyvlerview.setAdapter(justForYouAdapter);
    }

    private void resumeAutomobileHotPicks(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        hotpicksRecyclerview.setLayoutManager(layoutManager);

        hotPicksAdapter = new HotPicksAdapter(context, hotPicksArrayList);
        hotPicksAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedHotPicks = hotPicksArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedHotPicks.getId());
                intent.putExtra("ITEMNAME", selectedHotPicks.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        hotpicksRecyclerview.setAdapter(hotPicksAdapter);
    }
    private void resumeAutomobileFeaturedProducts(){}
    private void resumeAutomobileNewArrivals(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        newArrivalYouRecyclerView.setLayoutManager(layoutManager);

        newArrivalAdapter = new NewArrivalAdapter(context, newArrivalArrayList);
        newArrivalAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedNewArrival = newArrivalArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedNewArrival.getId());
                intent.putExtra("ITEMNAME", selectedNewArrival.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        newArrivalYouRecyclerView.setAdapter(newArrivalAdapter);
    }
    private void resumeAutomobileJustForYou(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false);
        justForYouRecyvlerview.setLayoutManager(layoutManager);

        justForYouAdapter = new JustForYouAdapter(context, justForYouArrayList);
        justForYouAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedJustForYou = justForYouArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedJustForYou.getId());
                intent.putExtra("ITEMNAME", selectedJustForYou.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        justForYouRecyvlerview.setAdapter(justForYouAdapter);
    }

}
