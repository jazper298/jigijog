package com.example.jigijog.activities;

import androidx.annotation.ColorInt;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jigijog.HttpProvider;
import com.example.jigijog.MainActivity;
import com.example.jigijog.R;
import com.example.jigijog.adapters.CheckOutCartItemAdapter;
import com.example.jigijog.helpers.CartDbHelper;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Barangay;
import com.example.jigijog.models.CartItem;
import com.example.jigijog.models.City;
import com.example.jigijog.models.OrderDetails;
import com.example.jigijog.models.Province;
import com.example.jigijog.models.Seller;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.ProgressPopup;
import com.example.jigijog.utils.Tools;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

import static java.lang.StrictMath.max;

public class CheckOutPageActivity extends AppCompatActivity {
    private View view;
    private Context context;

    private String[] array_province ;
    private String[] array_city;
    private String[] array_brgy;

    private ArrayList<Province> provinceArrayList = new ArrayList<>();
    private ArrayList<City> cityArrayList = new ArrayList<>();
    private ArrayList<Barangay> barangayArrayList = new ArrayList<>();
    private Province province = new Province();
    private City city = new City();
    private Barangay barangay = new Barangay();

    private Button btn_province, btn_city,btn_barangay;


    private CardView gcash, wallet, cod;
    private String payment;
    private TextView tv_edit;
    private ImageView iv_location;
    private TextView tv_totalAmount,tv_shippingFee ;

    private RecyclerView cartRecyclerView;
    private CheckOutCartItemAdapter checkOutCartItemAdapter;
    private ArrayList<CartItem> cartItemArrayList;

    private CartItem cartItem;

    private String userID, json;

    private ConstraintLayout emptyIndicator;
    private LinearLayout linear;
    private String getProvCode, getCitymunCode, getBrgyCode, getBrgyDes, getCityDes, getProvDes, landmark;
    double total = 0;
    double shipping = 0;
    private String courier = "JNT";
    private ArrayList<Seller> sellerArrayList;
    private ArrayList<Seller.Slider> sliderSellerArrayList;
    private ArrayList<OrderDetails> orderDetailsArrayList = new ArrayList<>();
    int quantity = 0;
    double grandTotal = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out_page);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        userID = UserCustomer.getID(context);
        initToolbar();
        initializeUI();
        loadCartItems(Integer.parseInt(userID));
        loadAllRegion();

    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.grey_60), PorterDuff.Mode.SRC_ATOP);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);

    }

    private void initializeUI() {
        emptyIndicator = findViewById(R.id.view_Empty2);
        linear = findViewById(R.id.linear);
        gcash = findViewById(R.id.gcash);
        wallet = findViewById(R.id.wallet);
        tv_edit = findViewById(R.id.tv_edit);
        cod = findViewById(R.id.cod);
        iv_location = findViewById(R.id.iv_location);
        cartRecyclerView = findViewById(R.id.cartRecyclerView);
        tv_shippingFee = findViewById(R.id.tv_shippingFee);
        tv_totalAmount = findViewById(R.id.tv_totalAmount);
        ((EditText) findViewById(R.id.et_landmark)).setText(UserCustomer.getLandmark(context));
        ((EditText) findViewById(R.id.et_address)).setText(UserCustomer.getAddress(context));
        gcash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wallet.setCardBackgroundColor(Color.WHITE);
                gcash.setCardBackgroundColor(Color.LTGRAY);
                cod.setCardBackgroundColor(Color.WHITE);
                gcash.setSelected(true);
                //payment = "gcash_e_wallet";
                payment = "cash_on_delivery";
            }
        });
        wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wallet.setCardBackgroundColor(Color.LTGRAY);
                gcash.setCardBackgroundColor(Color.WHITE);
                cod.setCardBackgroundColor(Color.WHITE);
                wallet.setSelected(true);
                //payment = "jigijog_wallet";
                payment = "cash_on_delivery";
            }
        });
        cod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wallet.setCardBackgroundColor(Color.WHITE);
                gcash.setCardBackgroundColor(Color.WHITE);
                cod.setCardBackgroundColor(Color.LTGRAY);
                cod.setSelected(true);
                payment = "cash_on_delivery";
            }
        });
        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadDialogShipping(v);
            }
        });
        linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view = findViewById(android.R.id.content);
                if(payment == null || payment.isEmpty()){
                    Snackbar snackbar = Snackbar.make(view, "Please select a payment", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }else{
                    loadNameAddress(Integer.parseInt(userID));

                    Debugger.logD("payment " + payment);
                }

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(context, item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadCartItems(int userID){
        cartItemArrayList = new ArrayList<>();
        CartDbHelper dbHelper = CartDbHelper.getInstance(context);

        orderDetailsArrayList = dbHelper.getAllOrderDetails(userID);

        cartItemArrayList = dbHelper.getCartItems(userID);
//        for (int o = 0; o < cartItemArrayList.size(); o++ ){
//            if(cartItemArrayList.get(o).getUser_ids().equals(cartItemArrayList.get(o).getUser_ids())) {
//                Debugger.logD("Duplicate: " + cartItemArrayList.get(o).getUser_ids());
//            }
//        }
        for(int i = 0; i < max(cartItemArrayList.size(), orderDetailsArrayList.size()); i++){
            String shippings = null;
            double totals =  Double.parseDouble(cartItemArrayList.get(i).getUnit_price()) * Double.parseDouble( orderDetailsArrayList.get(i).getQuantity());
            for (int o = 0; o < cartItemArrayList.size(); o++ ){
                if(cartItemArrayList.get(o).getUser_ids().equals(cartItemArrayList.get(o).getUser_ids())) {
                    Debugger.logD("Duplicate: " + cartItemArrayList.get(o).getUser_ids());
                }else{
                    shippings = String.valueOf(cartItemArrayList.get(i).getShipping_cost()); 
                }
            }
            Debugger.logD("fuck " + totals);
            Debugger.logD("shippings " + shippings);
            Debugger.logD("shippings " + cartItemArrayList.get(i).getUser_ids());
            total += totals;
            if(shippings.contains("null")){
                shipping += Double.parseDouble("0.00");
            }else{
                shipping += Double.parseDouble(shippings);
            }
        }
        tv_totalAmount.setText("\u20B1" + " " + total + "0");
        tv_shippingFee.setText("\u20B1" + " " + shipping + "0");
        grandTotal = total + shipping;

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        cartRecyclerView.setLayoutManager(layoutManager);

        checkOutCartItemAdapter = new CheckOutCartItemAdapter( context, cartItemArrayList);
        checkOutCartItemAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                cartItem = cartItemArrayList.get(position);
            }
        });
        cartRecyclerView.setAdapter(checkOutCartItemAdapter);
    }

    private void showProvinceChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
        builder2.setCancelable(true);
        builder2.setSingleChoiceItems(array_province , 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(array_province[which]);
                bt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                province = provinceArrayList.get(which);
                getProvDes = province.getProvDesc();
                Debugger.logD(" getProvDes " + getProvDes);
                getProvCode = province.getProvCode();
                loadSpecificCity(province);

                updateUserProvince();
                userID = UserCustomer.getID(context);
                Debugger.logD("showProvinceChoiceDialog " + userID);

            }
        });
        builder2.show();

    }
    private void showCityChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(array_city , 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(array_city[which]);
                bt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                city = cityArrayList.get(which);
                getCityDes = city.getCitymunDesc();
                getCitymunCode = city.getCitymunCode();
                loadSpecificBrgy(city);

                updateUserCity();
                userID = UserCustomer.getID(context);
                Debugger.logD("showCityChoiceDialog " + userID);
            }
        });
        builder.show();
    }
    private void showBrgyChoiceDialog(final Button bt) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setSingleChoiceItems(array_brgy , 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                bt.setTextColor(Color.BLACK);
                bt.setText(array_brgy[which]);
                bt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                barangay = barangayArrayList.get(which);
                getBrgyDes = barangay.getBrgyDesc();
                getBrgyCode = barangay.getBrgyCode();

                //updateUserBarangay();
                userID = UserCustomer.getID(context);
                Debugger.logD("showBrgyChoiceDialog " + userID);

            }
        });
        builder.show();
    }

    private void loadAllRegion(){
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("https://jigijog.com/v1/mobile-api/api/mobile/getAllProvince", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String psgcCode = jsonObject.getString("psgcCode");
                        String provDesc = jsonObject.getString("provDesc");
                        String regCode = jsonObject.getString("regCode");
                        String provCode = jsonObject.getString("provCode");
                        Province province = new Province();
                        province.setId(id);
                        province.setPsgcCode(psgcCode);
                        province.setProvDesc(provDesc);
                        province.setRegCode(regCode);
                        province.setProvCode(provCode);
                        provinceArrayList.add(province);
                        array_province = new String[provinceArrayList.size()];
                        for (int x = 0; x < provinceArrayList.size(); x++) {
                            array_province[x] = provinceArrayList.get(x).getProvDesc();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadSpecificCity(final Province province) {
        RequestParams params = new RequestParams();
        params.put("provCode", province.getProvCode());
        HttpProvider.post(context, "/mobile/specificCities", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String psgcCode = jsonObject.getString("psgcCode");
                            String citymunDesc = jsonObject.getString("citymunDesc");
                            String regDesc = jsonObject.getString("regDesc");
                            String provCode = jsonObject.getString("provCode");
                            String citymunCode = jsonObject.getString("citymunCode");

                            City cityModel = new City();
                            cityModel.setId(id);
                            cityModel.setPsgcCode(psgcCode);
                            cityModel.setCitymunDesc(citymunDesc);
                            cityModel.setRegDesc(regDesc);
                            cityModel.setProvCode(provCode);
                            cityModel.setCitymunCode(citymunCode);
                            cityArrayList.add(cityModel);

                            array_city = new String[cityArrayList.size()];
                            for (int x = 0; x < cityArrayList.size(); x++) {
                                array_city[x] = cityArrayList.get(x).getCitymunDesc();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    private void loadSpecificBrgy(final City city){
        RequestParams params = new RequestParams();
        params.put("citymunCode", city.getCitymunCode());
        HttpProvider.post(context, "/mobile/specificBarangays", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String brgyCode = jsonObject.getString("brgyCode");
                            String brgyDesc = jsonObject.getString("brgyDesc");
                            String regCode = jsonObject.getString("regCode");
                            String provCode = jsonObject.getString("provCode");
                            String citymunCode = jsonObject.getString("citymunCode");
                            Barangay barangayModel = new Barangay();
                            barangayModel.setId(id);
                            barangayModel.setBrgyCode(brgyCode);
                            barangayModel.setBrgyDesc(brgyDesc);
                            barangayModel.setRegCode(regCode);
                            barangayModel.setProvCode(provCode);
                            barangayModel.setCitymunCode(citymunCode);
                            barangayArrayList.add(barangayModel);
                            array_brgy = new String[barangayArrayList.size()];
                            for (int x = 0; x < barangayArrayList.size(); x++) {
                                array_brgy[x] = barangayArrayList.get(x).getBrgyDesc();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });
    }

    private void updateUserProvince(){
        Province province1 = new Province();
        province1.setId(Province.getID(context));
        province1.setProvCode(getProvCode);
        province1.setRegCode(Province.getRegCode(context));
        province1.setProvDesc(getProvDes);
        province1.setPsgcCode(Province.getPsgcCode(context));

        province1.saveUserSession(context);
    }
    private void updateUserCity(){
        City city1 = new City();
        city1.setId(City.getID(context));
        city1.setPsgcCode(City.getPsgcCode(context));
        city1.setCitymunCode(getCitymunCode);
        city1.setRegDesc(City.getRegCode(context));
        city1.setProvCode(City.getProvCode(context));
        city1.setCitymunDesc(getCityDes);
        city1.saveUserSession(context);
    }
    private void updateUserBarangay(){
        Barangay barangay1 = new Barangay();
        barangay1.setBrgyCode(getBrgyCode);
        barangay1.setBrgyDesc(getBrgyDes);
        barangay1.setRegCode(Barangay.getBrgyCode(context));
        barangay1.setProvCode(Barangay.getProvCode(context));
        barangay1.setCitymunCode(Barangay.getCityMunde(context));
        barangay1.saveUserSession(context);
    }

    private void loadDialogShipping(View v){
        final Context context=v.getContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View view = inflater.inflate(R.layout.dialog_edit_shipping, null);

        final AppCompatEditText et_landmarks;
        final Button btn_confirm, btn_barangay, btn_city,btn_province;

        btn_confirm = view.findViewById(R.id.btn_confirm);
        btn_barangay = view.findViewById(R.id.btn_barangay);
        btn_city = view.findViewById(R.id.btn_city);
        btn_province = view.findViewById(R.id.btn_province);
        et_landmarks = view.findViewById(R.id.et_landmarks);

        final Dialog mBottomSheetDialog = new Dialog (context);
        Window window = mBottomSheetDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);


        mBottomSheetDialog.setContentView (view);
        mBottomSheetDialog.setCancelable (true);
        mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow ().setGravity (Gravity.CENTER);
        mBottomSheetDialog.show ();

        btn_province.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProvinceChoiceDialog((Button) v);
            }
        });
        btn_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCityChoiceDialog((Button) v);
            }
        });
        btn_barangay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBrgyChoiceDialog((Button) v);
            }
        });
        final String landmark = et_landmarks.getText().toString();
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Debugger.logD("landmark " + et_landmarks.getText().toString());
                userID = UserCustomer.getID(context);
                Debugger.logD("btn_confirm " + userID);
                mBottomSheetDialog.hide();
            }
        });

        mBottomSheetDialog.show();
    }

    public class SelectableCardView extends CardView {
        private int _unselectedBackgroundColor;
        private int _selectedBackgroundColor;
        private boolean _isSelectedble;

        public SelectableCardView(Context context)
        {
            this(context,null);
        }
        public SelectableCardView(Context context, AttributeSet attrs)
        {
            this(context,attrs,0);
        }
        public SelectableCardView(Context context,AttributeSet attrs,int defStyleAttr) {
            super(context,attrs,defStyleAttr);
            final Resources res=context.getResources();
            _selectedBackgroundColor    =   Color.LTGRAY;
            _unselectedBackgroundColor = Color.WHITE;
        }
        public void setSelectedBackgroundColor(@ColorInt int selectedBackgroundColor) {
            _selectedBackgroundColor=selectedBackgroundColor;
        }
        public void setUnselectedBackgroundColor(@ColorInt int unselectedBackgroundColor) {
            _unselectedBackgroundColor=unselectedBackgroundColor;
        }
        @Override
        public void setSelected(final boolean selected) {
            super.setSelected(selected);
            if(_isSelectedble)
                setCardBackgroundColor(isSelected()?_selectedBackgroundColor:_unselectedBackgroundColor);
        }
        public void setSelectedble(final boolean selectedble) {
            if(!selectedble) {
                super.setSelected(false);
                setCardBackgroundColor(_unselectedBackgroundColor);
            }
            _isSelectedble=selectedble;
        }
    }
    JSONObject obj;
    JSONArray requiredDataArray;
    JSONObject data;
    private void loadNameAddress(int userID)  {
        view = findViewById(android.R.id.content);
        CartDbHelper dbHelper = CartDbHelper.getInstance(context);
        orderDetailsArrayList = dbHelper.getAllOrderDetails(userID);
        obj = new JSONObject();
        requiredDataArray = new JSONArray();
        try {
            for (int x = 0; x < orderDetailsArrayList.size(); x++) {
                data = new JSONObject();
                data.put("seller_id", orderDetailsArrayList.get(x).getSeller_id());
                data.put("product_id", orderDetailsArrayList.get(x).getProduct_id());
                data.put("variation", orderDetailsArrayList.get(x).getVariation());
                data.put("base_price", orderDetailsArrayList.get(x).getBase_price());
                data.put("selling_price", orderDetailsArrayList.get(x).getSelling_price());
                data.put("price", orderDetailsArrayList.get(x).getPrice());
                data.put("shipping_cost", orderDetailsArrayList.get(x).getShipping_cost());
                data.put("coupon_discount", orderDetailsArrayList.get(x).getCoupon_discount());
                data.put("quantity", orderDetailsArrayList.get(x).getQuantity());
                data.put("payment_status", orderDetailsArrayList.get(x).getPayment_status());
                data.put("delivery_status", orderDetailsArrayList.get(x).getDelivery_status());
                requiredDataArray.put(x, data);
                quantity = Integer.parseInt(orderDetailsArrayList.get(x).getQuantity());
                Debugger.logD("quantity 1 "+ quantity);
            }
            Debugger.logD("requiredDataArray2 "+ requiredDataArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Debugger.logD("requiredDataArray "+ requiredDataArray.length());
        JSONObject jsonResult = new JSONObject();
        try {
            jsonResult.put("name", UserCustomer.getName(context));
            jsonResult.put("email", UserCustomer.getEmail(context));
            jsonResult.put("address", UserCustomer.getAddress(context));
            jsonResult.put("country", "");
            jsonResult.put("city", UserCustomer.getCity(context));
            jsonResult.put("postal_code", "");
            jsonResult.put("phone", UserCustomer.getPhone(context));
            jsonResult.put("checkout_type", "logged");
            jsonResult.put("courier", courier);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ProgressPopup.showProgress(context);

        RequestParams params = new RequestParams();
        params.put("user_id", UserCustomer.getID(context));
        params.put("shipping_address", jsonResult);
        params.put("payment_type", payment);
        params.put("payment_status", "unpaid");
        params.put("grand_total", grandTotal + "0");
        params.put("coupon_discount", "0.00");
        params.put("products", requiredDataArray);

        Debugger.logD("DEV user_id-> "+ UserCustomer.getID(context));
        Debugger.logD("DEV shipping_address-> "+ jsonResult);
        Debugger.logD("DEV payment_type-> "+ payment);
        Debugger.logD("DEV payment_status-> "+ "unpaid");
        Debugger.logD("DEV grand_total-> "+ grandTotal + "0");
        Debugger.logD("DEV coupon_discount-> "+ "0.00");
        Debugger.logD("DEV products-> "+ requiredDataArray);

        HttpProvider.post(context, "/mobile/checkout", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ProgressPopup.hideProgress();

                String str = new String(responseBody, StandardCharsets.UTF_8);
                Debugger.logD("str " + str);
                if (str.equals("")) {
                    Snackbar snackbar = Snackbar.make(view, "Checkout Successful", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                } else{
                    Snackbar snackbar = Snackbar.make(view, "Checkout Failed", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ProgressPopup.hideProgress();
                Toasty.error(context, "No internet connect. Please try again.").show();
            }
        });
    }


}
