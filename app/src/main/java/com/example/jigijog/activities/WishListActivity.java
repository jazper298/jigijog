package com.example.jigijog.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.jigijog.MainActivity;
import com.example.jigijog.R;
import com.example.jigijog.adapters.WishListAdapter;
import com.example.jigijog.helpers.CartDbHelper;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.CartItem;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.models.WishList;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.UserRole;

import java.util.ArrayList;

public class WishListActivity extends AppCompatActivity {
    private View view;
    private Context context;
    private ImageView iv_wishListBack, iv_wishListSearch, iv_wishListCart,iv_wishListMore;
    private TextView tv_wishListCart;

    private RecyclerView wishListRecyclerView;
    private ArrayList<WishList> wishListArrayList;
    private ArrayList<CartItem> cartItemArrayList = new ArrayList<>();
    private WishListAdapter wishListAdapter;
    private WishList wishList;
    private String userID;
    private ConstraintLayout emptyIndicator;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int cartCountTotal = 0;
    private String role;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
        context = this;
        role = UserRole.getRole(context);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        userID = intent.getStringExtra("USERID");
        userID = UserCustomer.getID(context);
        initializeUI();
        if (userID == null || userID.isEmpty()){
            emptyIndicator.setVisibility(View.VISIBLE);
        }else{
            getWishListItem(Integer.parseInt(userID));
        }
        checkUserRole();
    }

    private void initializeUI(){
        emptyIndicator = findViewById(R.id.view_Empty);
        iv_wishListBack = findViewById(R.id.iv_wishListBack);
        iv_wishListBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        iv_wishListSearch = findViewById(R.id.iv_wishListSearch);
        iv_wishListSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductSearchActivity.class);
                startActivity(intent);
                finish();
            }
        });
        iv_wishListCart = findViewById(R.id.iv_wishListCart);
        iv_wishListCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CartActivity.class);
                startActivity(intent);
                finish();
            }
        });
        iv_wishListMore = findViewById(R.id.iv_wishListMore);
        tv_wishListCart = findViewById(R.id.tv_wishListCart);
        wishListRecyclerView = findViewById(R.id.wishListRecyclerView);
        swipeRefreshLayout = findViewById(R.id.productswipe_Home);
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (userID == null || userID.isEmpty()){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else{
                    getWishListItem(Integer.parseInt(userID));
                }
            }
        });
        iv_wishListMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, AccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    private void checkUserRole(){
        if (role.equals(UserRole.Customer())) {
            getCartItems();
        } else {
            //checkStudentSession();
        }
    }
    private void getWishListItem(int userID){
        swipeRefreshLayout.setRefreshing(true);


        swipeRefreshLayout.setRefreshing(false);
        CartDbHelper dbHelper = CartDbHelper.getInstance(context);
        wishListArrayList = dbHelper.getAllWishList(userID);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
        wishListRecyclerView.setLayoutManager(layoutManager);

        wishListAdapter = new WishListAdapter( context, wishListArrayList);
        wishListAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                wishList = wishListArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", wishList.getId());
                intent.putExtra("ITEMNAME", wishList.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        wishListRecyclerView.setAdapter(wishListAdapter);

    }

    private void getCartItems(){
        CartDbHelper dbHelper = CartDbHelper.getInstance(context);
        cartItemArrayList = dbHelper.getCartItems(Integer.parseInt(UserCustomer.getID(context)));
        cartCountTotal = cartItemArrayList.size();
        if (cartCountTotal == 0){
            tv_wishListCart.setText("");
        }else{
            tv_wishListCart.setVisibility(View.VISIBLE);
            tv_wishListCart.setText(String.valueOf(cartCountTotal));
        }
        Debugger.logD("asdssss" + cartCountTotal);
    }


}
