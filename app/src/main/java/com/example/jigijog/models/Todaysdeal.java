package com.example.jigijog.models;

public class Todaysdeal {
    private String id;
    private String name;
    private String photos;
    private String unit_price;
    private String discount;

    public Todaysdeal(String id, String name, String photos, String unit_price, String discount) {
        this.id = id;
        this.name = name;
        this.photos = photos;
        this.unit_price = unit_price;
        this.discount = discount;
    }

    public Todaysdeal() {
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhotos() {
        return photos;
    }

    public String getUnit_price() {
        return unit_price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public void setUnit_price(String unit_price) {
        this.unit_price = unit_price;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
}
