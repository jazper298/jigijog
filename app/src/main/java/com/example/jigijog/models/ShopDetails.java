package com.example.jigijog.models;

import java.util.ArrayList;

public class ShopDetails {
    private String id;
    private String provider_id;
    private String user_type;
    private String name;
    private String email;
    private String referred_by;
    private String refer_points;
    private String ref_id_status;
    private String email_verified_at;
    private String password;
    private String remember_token;
    private String avatar;
    private String avatar_original;
    private String address;
    private String country;
    private String province;
    private String city;
    private String barangay;
    private String landmark;
    private String postal_code;
    private String phone;
    private String bank_name;
    private String account_name;
    private String account_number;
    private String balance;
    private String created_at;
    private String updated_at;
    private String user_id;
    private String logo;
    private ArrayList<Seller.Slider> sliders;
    private String facebook;
    private String google;
    private String twitter;
    private String youtube;
    private String instagram;
    private String slug;
    public static class Slider{
        private String slider;

        public String getSlider() {
            return slider;
        }

        public void setSlider(String slider) {
            this.slider = slider;
        }
    }

    public ShopDetails(String id, String provider_id, String user_type, String name, String email, String referred_by, String refer_points, String ref_id_status, String email_verified_at, String password, String remember_token, String avatar, String avatar_original, String address, String country, String province, String city, String barangay, String landmark, String postal_code, String phone, String bank_name, String account_name, String account_number, String balance, String created_at, String updated_at, String user_id, String logo, ArrayList<Seller.Slider> sliders, String facebook, String google, String twitter, String youtube, String instagram, String slug) {
        this.id = id;
        this.provider_id = provider_id;
        this.user_type = user_type;
        this.name = name;
        this.email = email;
        this.referred_by = referred_by;
        this.refer_points = refer_points;
        this.ref_id_status = ref_id_status;
        this.email_verified_at = email_verified_at;
        this.password = password;
        this.remember_token = remember_token;
        this.avatar = avatar;
        this.avatar_original = avatar_original;
        this.address = address;
        this.country = country;
        this.province = province;
        this.city = city;
        this.barangay = barangay;
        this.landmark = landmark;
        this.postal_code = postal_code;
        this.phone = phone;
        this.bank_name = bank_name;
        this.account_name = account_name;
        this.account_number = account_number;
        this.balance = balance;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.user_id = user_id;
        this.logo = logo;
        this.sliders = sliders;
        this.facebook = facebook;
        this.google = google;
        this.twitter = twitter;
        this.youtube = youtube;
        this.instagram = instagram;
        this.slug = slug;
    }

    public ShopDetails() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(String provider_id) {
        this.provider_id = provider_id;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getReferred_by() {
        return referred_by;
    }

    public void setReferred_by(String referred_by) {
        this.referred_by = referred_by;
    }

    public String getRefer_points() {
        return refer_points;
    }

    public void setRefer_points(String refer_points) {
        this.refer_points = refer_points;
    }

    public String getRef_id_status() {
        return ref_id_status;
    }

    public void setRef_id_status(String ref_id_status) {
        this.ref_id_status = ref_id_status;
    }

    public String getEmail_verified_at() {
        return email_verified_at;
    }

    public void setEmail_verified_at(String email_verified_at) {
        this.email_verified_at = email_verified_at;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRemember_token() {
        return remember_token;
    }

    public void setRemember_token(String remember_token) {
        this.remember_token = remember_token;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar_original() {
        return avatar_original;
    }

    public void setAvatar_original(String avatar_original) {
        this.avatar_original = avatar_original;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBarangay() {
        return barangay;
    }

    public void setBarangay(String barangay) {
        this.barangay = barangay;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public ArrayList<Seller.Slider> getSliders() {
        return sliders;
    }

    public void setSliders(ArrayList<Seller.Slider> sliders) {
        this.sliders = sliders;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getGoogle() {
        return google;
    }

    public void setGoogle(String google) {
        this.google = google;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
