package com.example.jigijog.models;

public class Categories {
    private String id;
    private String name;
    private String banner;

    public Categories(String id, String name, String banner) {
        this.id = id;
        this.name = name;
        this.banner = banner;
    }

    public Categories() {
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBanner() {
        return banner;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    //    private String categoryName;
//    private int categoryImage;
//
//
//    public Categories(String categoryName, int categoryImage) {
//        this.categoryName = categoryName;
//        this.categoryImage = categoryImage;
//    }
//    public Categories() {
//
//    }
//
//    public String getCategoryName() {
//        return categoryName;
//    }
//
//    public void setCategoryName(String categoryName) {
//        this.categoryName = categoryName;
//    }
//
//    public int getCategoryImage() {
//        return categoryImage;
//    }
//
//    public void setCategoryImage(int categoryImage) {
//        this.categoryImage = categoryImage;
//    }
}
