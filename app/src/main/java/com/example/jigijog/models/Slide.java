package com.example.jigijog.models;

public class Slide {
    private String id;
    private String photos;
    private String created_at;
    private String updated_at;

    public Slide(String id, String photos, String created_at, String updated_at) {
        this.id = id;
        this.photos = photos;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public Slide() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
