package com.example.jigijog.models;

import java.util.ArrayList;

public class BestSelling {
    private String id;
    private String name;
    private String added_by;
    private String user_id;
    private String category_id;
    private String subcategory_id;
    private String subsubcategory_id;
    private String brand_id;
    private ArrayList<Products.Photos> photos;
    private String thumbnail_img;
    private String featured_img;
    private String flash_deal_img;
    private String video_provider;
    private String video_link;
    private String tags;
    private String description;
    private String unit_price;
    private String purchase_price;
    private ArrayList<Products.Choice_Options> choice_options;
    private ArrayList<Products.Colors> colors;
    private ArrayList<Products.Variations> variations;
    private String todays_deal;
    private String published;
    private String featured;
    private String current_stock;
    private String unit;
    private String discount;
    private String discount_type;
    private String tax;
    private String tax_type;
    private String shipping_type;
    private String shipping_cost;
    private String weight;
    private String parcel_size;
    private String num_of_sale;
    private String meta_title;
    private String meta_description;
    private String meta_img;
    private String pdf;
    private String slug;
    private String rating;
    private String created_at;
    private String updated_at;
    public static class Photos{
        private String id;
        private String photo;

        public Photos(String id, String photo) {
            this.id = id;
            this.photo = photo;
        }

        public Photos() {
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }
    }

    public static class Choice_Options{
        private String name;
        private String title;
        private ArrayList<Products.Choice_Options.Options> options;
        public static class Options{
            private String option;

            public String getOption() {
                return option;
            }

            public void setOption(String option) {
                this.option = option;
            }
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public ArrayList<Products.Choice_Options.Options> getOptions() {
            return options;
        }

        public void setOptions(ArrayList<Products.Choice_Options.Options> options) {
            this.options = options;
        }
    }
    public static class Colors{
        private String color;

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }
    }
    public static class Variations{

        public Variations() {
        }

        private ArrayList<Products.Variations.Variation> variation;

        public static class Variation {
            private String price;
            private String sku;
            private String qty;

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getSku() {
                return sku;
            }

            public void setSku(String sku) {
                this.sku = sku;
            }

            public String getQty() {
                return qty;
            }

            public void setQty(String qty) {
                this.qty = qty;
            }
        }

        public ArrayList<Products.Variations.Variation> getVariation() {
            return variation;
        }

        public void setVariation(ArrayList<Products.Variations.Variation> variation) {
            this.variation = variation;
        }
    }

    public BestSelling(String id, String name, String added_by, String user_id, String category_id, String subcategory_id, String subsubcategory_id, String brand_id, ArrayList<Products.Photos> photos, String thumbnail_img, String featured_img, String flash_deal_img, String video_provider, String video_link, String tags, String description, String unit_price, String purchase_price, ArrayList<Products.Choice_Options> choice_options, ArrayList<Products.Colors> colors, ArrayList<Products.Variations> variations, String todays_deal, String published, String featured, String current_stock, String unit, String discount, String discount_type, String tax, String tax_type, String shipping_type, String shipping_cost, String weight, String parcel_size, String num_of_sale, String meta_title, String meta_description, String meta_img, String pdf, String slug, String rating, String created_at, String updated_at) {
        this.id = id;
        this.name = name;
        this.added_by = added_by;
        this.user_id = user_id;
        this.category_id = category_id;
        this.subcategory_id = subcategory_id;
        this.subsubcategory_id = subsubcategory_id;
        this.brand_id = brand_id;
        this.photos = photos;
        this.thumbnail_img = thumbnail_img;
        this.featured_img = featured_img;
        this.flash_deal_img = flash_deal_img;
        this.video_provider = video_provider;
        this.video_link = video_link;
        this.tags = tags;
        this.description = description;
        this.unit_price = unit_price;
        this.purchase_price = purchase_price;
        this.choice_options = choice_options;
        this.colors = colors;
        this.variations = variations;
        this.todays_deal = todays_deal;
        this.published = published;
        this.featured = featured;
        this.current_stock = current_stock;
        this.unit = unit;
        this.discount = discount;
        this.discount_type = discount_type;
        this.tax = tax;
        this.tax_type = tax_type;
        this.shipping_type = shipping_type;
        this.shipping_cost = shipping_cost;
        this.weight = weight;
        this.parcel_size = parcel_size;
        this.num_of_sale = num_of_sale;
        this.meta_title = meta_title;
        this.meta_description = meta_description;
        this.meta_img = meta_img;
        this.pdf = pdf;
        this.slug = slug;
        this.rating = rating;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public BestSelling() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getSubcategory_id() {
        return subcategory_id;
    }

    public void setSubcategory_id(String subcategory_id) {
        this.subcategory_id = subcategory_id;
    }

    public String getSubsubcategory_id() {
        return subsubcategory_id;
    }

    public void setSubsubcategory_id(String subsubcategory_id) {
        this.subsubcategory_id = subsubcategory_id;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public ArrayList<Products.Photos> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<Products.Photos> photos) {
        this.photos = photos;
    }

    public String getThumbnail_img() {
        return thumbnail_img;
    }

    public void setThumbnail_img(String thumbnail_img) {
        this.thumbnail_img = thumbnail_img;
    }

    public String getFeatured_img() {
        return featured_img;
    }

    public void setFeatured_img(String featured_img) {
        this.featured_img = featured_img;
    }

    public String getFlash_deal_img() {
        return flash_deal_img;
    }

    public void setFlash_deal_img(String flash_deal_img) {
        this.flash_deal_img = flash_deal_img;
    }

    public String getVideo_provider() {
        return video_provider;
    }

    public void setVideo_provider(String video_provider) {
        this.video_provider = video_provider;
    }

    public String getVideo_link() {
        return video_link;
    }

    public void setVideo_link(String video_link) {
        this.video_link = video_link;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(String unit_price) {
        this.unit_price = unit_price;
    }

    public String getPurchase_price() {
        return purchase_price;
    }

    public void setPurchase_price(String purchase_price) {
        this.purchase_price = purchase_price;
    }

    public ArrayList<Products.Choice_Options> getChoice_options() {
        return choice_options;
    }

    public void setChoice_options(ArrayList<Products.Choice_Options> choice_options) {
        this.choice_options = choice_options;
    }

    public ArrayList<Products.Colors> getColors() {
        return colors;
    }

    public void setColors(ArrayList<Products.Colors> colors) {
        this.colors = colors;
    }

    public ArrayList<Products.Variations> getVariations() {
        return variations;
    }

    public void setVariations(ArrayList<Products.Variations> variations) {
        this.variations = variations;
    }

    public String getTodays_deal() {
        return todays_deal;
    }

    public void setTodays_deal(String todays_deal) {
        this.todays_deal = todays_deal;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getFeatured() {
        return featured;
    }

    public void setFeatured(String featured) {
        this.featured = featured;
    }

    public String getCurrent_stock() {
        return current_stock;
    }

    public void setCurrent_stock(String current_stock) {
        this.current_stock = current_stock;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount_type() {
        return discount_type;
    }

    public void setDiscount_type(String discount_type) {
        this.discount_type = discount_type;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getTax_type() {
        return tax_type;
    }

    public void setTax_type(String tax_type) {
        this.tax_type = tax_type;
    }

    public String getShipping_type() {
        return shipping_type;
    }

    public void setShipping_type(String shipping_type) {
        this.shipping_type = shipping_type;
    }

    public String getShipping_cost() {
        return shipping_cost;
    }

    public void setShipping_cost(String shipping_cost) {
        this.shipping_cost = shipping_cost;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getParcel_size() {
        return parcel_size;
    }

    public void setParcel_size(String parcel_size) {
        this.parcel_size = parcel_size;
    }

    public String getNum_of_sale() {
        return num_of_sale;
    }

    public void setNum_of_sale(String num_of_sale) {
        this.num_of_sale = num_of_sale;
    }

    public String getMeta_title() {
        return meta_title;
    }

    public void setMeta_title(String meta_title) {
        this.meta_title = meta_title;
    }

    public String getMeta_description() {
        return meta_description;
    }

    public void setMeta_description(String meta_description) {
        this.meta_description = meta_description;
    }

    public String getMeta_img() {
        return meta_img;
    }

    public void setMeta_img(String meta_img) {
        this.meta_img = meta_img;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
