package com.example.jigijog.models;

public class Notifications {

    private int notifIcon;
    private String notifTitle;
    private String notiftDate;
    private int notifImage;
    private String notifDescription;

    public Notifications(int notifIcon, String notifTitle, String notiftDate, int notifImage, String notifDescription) {
        this.notifIcon = notifIcon;
        this.notifTitle = notifTitle;
        this.notiftDate = notiftDate;
        this.notifImage = notifImage;
        this.notifDescription = notifDescription;
    }

    public int getNotifIcon() {
        return notifIcon;
    }

    public String getNotifTitle() {
        return notifTitle;
    }

    public String getNotiftDate() {
        return notiftDate;
    }

    public int getNotifImage() {
        return notifImage;
    }

    public String getNotifDescription() {
        return notifDescription;
    }

    public void setNotifIcon(int notifIcon) {
        this.notifIcon = notifIcon;
    }

    public void setNotifTitle(String notifTitle) {
        this.notifTitle = notifTitle;
    }

    public void setNotiftDate(String notiftDate) {
        this.notiftDate = notiftDate;
    }

    public void setNotifImage(int notifImage) {
        this.notifImage = notifImage;
    }

    public void setNotifDescription(String notifDescription) {
        this.notifDescription = notifDescription;
    }
}
