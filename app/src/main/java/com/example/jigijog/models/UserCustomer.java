package com.example.jigijog.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class UserCustomer {
    private String id;
    private String provider_id;
    private String user_type;
    private String name;
    private String email;
    private String referred_by;
    private String referred_points;
    private String ref_id_status;
    private String email_verified_at;
    private String password;
    private String remember_token;
    private String avatar;
    private String avatar_original;
    private String address;
    private String country;
    private String province;
    private String city;
    private String barangay;
    private String landmark;
    private String postal_code;
    private String phone;
    private String bank_name;
    private String account_name;
    private String account_number;
    private String balance;
    private String created_at;
    private String updated_at;

    public UserCustomer(String id, String provider_id, String user_type, String name, String email, String referred_by, String referred_points, String ref_id_status, String email_verified_at, String password, String remember_token, String avatar, String avatar_original, String address, String country, String province, String city, String barangay, String landmark, String postal_code, String phone, String bank_name, String account_name, String account_number, String balance, String created_at, String updated_at) {
        this.id = id;
        this.provider_id = provider_id;
        this.user_type = user_type;
        this.name = name;
        this.email = email;
        this.referred_by = referred_by;
        this.referred_points = referred_points;
        this.ref_id_status = ref_id_status;
        this.email_verified_at = email_verified_at;
        this.password = password;
        this.remember_token = remember_token;
        this.avatar = avatar;
        this.avatar_original = avatar_original;
        this.address = address;
        this.country = country;
        this.province = province;
        this.city = city;
        this.barangay = barangay;
        this.landmark = landmark;
        this.postal_code = postal_code;
        this.phone = phone;
        this.bank_name = bank_name;
        this.account_name = account_name;
        this.account_number = account_number;
        this.balance = balance;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public UserCustomer() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(String provider_id) {
        this.provider_id = provider_id;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getReferred_by() {
        return referred_by;
    }

    public void setReferred_by(String referred_by) {
        this.referred_by = referred_by;
    }

    public String getReferred_points() {
        return referred_points;
    }

    public void setReferred_points(String referred_points) {
        this.referred_points = referred_points;
    }

    public String getRef_id_status() {
        return ref_id_status;
    }

    public void setRef_id_status(String ref_id_status) {
        this.ref_id_status = ref_id_status;
    }

    public String getEmail_verified_at() {
        return email_verified_at;
    }

    public void setEmail_verified_at(String email_verified_at) {
        this.email_verified_at = email_verified_at;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRemember_token() {
        return remember_token;
    }

    public void setRemember_token(String remember_token) {
        this.remember_token = remember_token;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar_original() {
        return avatar_original;
    }

    public void setAvatar_original(String avatar_original) {
        this.avatar_original = avatar_original;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBarangay() {
        return barangay;
    }

    public void setBarangay(String barangay) {
        this.barangay = barangay;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public static String getID(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("ID", "");
    }

    public static String getProviderID(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("PROVIDERID", "");
    }
    public static String getUserType(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("USER_TYPE", "");
    }

    public static String getName(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("NAME", "");
    }
    public static String getEmail(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("EMAIL", "");
    }

    public static String getReferredBy(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("REFERREDBY", "");
    }
    public static String getReferPoints(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("REFERBYPOINTS", "");
    }
    public static String getRefIdStats(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("REFIDSTATUS", "");
    }
    public static String getEmailVerifiedAt(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("EMAILVERIFIEDAT", "");
    }

    public static String getPassword(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("PASSWORD", "");
    }
    public static String getRememberToken(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("REMEMBERTOKEN", "");
    }

    public static String getAvatar(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("AVATAR", "");
    }
    public static String getAvatarOriginal(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("AVATAR_ORIGINAL", "");
    }

    public static String getAddress(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("ADDRESS", "");
    }
    public static String getCountry(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("COUNTRY", "");
    }

    public static String getProvince(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("PROVINCE", "");
    }
    public static String getCity(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("CITY", "");
    }

    public static String getBarangay(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("BARANGAY", "");
    }
    public static String getLandmark(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("LANDMARK", "");
    }

    public static String getPostalCode(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("POSTAL_CODE", "");
    }
    public static String getPhone(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("PHONE", "");
    }

    public static String getBankName(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("BANK_NAME", "");
    }
    public static String getAccountName(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("ACCOUNT_NAME", "");
    }

    public static String getBalance(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("BALANCE", "");
    }
    public static String getCreatedAt(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("CREATED_AT", "");
    }

    public static String getUpdatedAt(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("UPDATED_AT", "");
    }

    public boolean saveUserSession(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("ID", id);
        editor.putString("PROVIDERID", provider_id);
        editor.putString("USER_TYPE", user_type);
        editor.putString("NAME", name);
        editor.putString("EMAIL", email);
        editor.putString("REFERREDBY", referred_by);
        editor.putString("REFERBYPOINTS",referred_points);
        editor.putString("REFIDSTATUS",ref_id_status);
        editor.putString("EMAILVERIFIEDAT",email_verified_at);
        editor.putString("PASSWORD", password);
        editor.putString("REMEMBERTOKEN", remember_token);
        editor.putString("AVATAR", avatar);
        editor.putString("AVATAR_ORIGINAL", avatar_original);
        editor.putString("ADDRESS", address);
        editor.putString("COUNTRY", country);
        editor.putString("PROVINCE", province);
        editor.putString("CITY", city);
        editor.putString("BARANGAY", barangay);
        editor.putString("LANDMARK", landmark);
        editor.putString("POSTAL_CODE", postal_code);
        editor.putString("PHONE", phone);
        editor.putString("BANK_NAME", bank_name);
        editor.putString("ACCOUNT_NAME", account_name);
        editor.putString("BALANCE", balance);
        editor.putString("CREATED_AT", created_at);
        editor.putString("UPDATED_AT", updated_at);
        return editor.commit();
    }

    public static boolean clearSession(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        return editor.commit();
    }
}
