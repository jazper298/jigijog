package com.example.jigijog.models;

import android.os.Parcel;
import android.os.Parcelable;

public class OrderDetails implements Parcelable {
    private int id;
    private String user_id;
    private String seller_id;
    private String product_id;
    private String variation;
    private String base_price;
    private String selling_price;
    private String price;
    private String tax;
    private String shipping_cost;
    private String coupon_discount;
    private String quantity;
    private String payment_status;
    private String delivery_status;
    private String pick_from;
    private String pick_to;
    private String created_at;
    private String updated_at;

    public OrderDetails(String user_id,String seller_id, String product_id, String variation, String base_price, String selling_price,String price, String tax, String shipping_cost, String coupon_discount, String quantity, String payment_status, String delivery_status, String pick_from, String pick_to, String created_at, String updated_at) {
        this.user_id = user_id;
        this.seller_id = seller_id;
        this.product_id = product_id;
        this.variation = variation;
        this.base_price = base_price;
        this.selling_price = selling_price;
        this.price = price;
        this.tax = tax;
        this.shipping_cost = shipping_cost;
        this.coupon_discount = coupon_discount;
        this.quantity = quantity;
        this.payment_status = payment_status;
        this.delivery_status = delivery_status;
        this.pick_from = pick_from;
        this.pick_to = pick_to;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public OrderDetails() {
    }

    protected OrderDetails(Parcel in) {
        id = in.readInt();
        user_id = in.readString();
        seller_id = in.readString();
        product_id = in.readString();
        variation = in.readString();
        base_price = in.readString();
        selling_price = in.readString();
        price = in.readString();
        tax = in.readString();
        shipping_cost = in.readString();
        coupon_discount = in.readString();
        quantity = in.readString();
        payment_status = in.readString();
        delivery_status = in.readString();
        pick_from = in.readString();
        pick_to = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    public static final Creator<OrderDetails> CREATOR = new Creator<OrderDetails>() {
        @Override
        public OrderDetails createFromParcel(Parcel in) {
            return new OrderDetails(in);
        }

        @Override
        public OrderDetails[] newArray(int size) {
            return new OrderDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(user_id);
        dest.writeString(seller_id);
        dest.writeString(product_id);
        dest.writeString(variation);
        dest.writeString(base_price);
        dest.writeString(selling_price);
        dest.writeString(price);
        dest.writeString(tax);
        dest.writeString(shipping_cost);
        dest.writeString(coupon_discount);
        dest.writeString(quantity);
        dest.writeString(payment_status);
        dest.writeString(delivery_status);
        dest.writeString(pick_from);
        dest.writeString(pick_to);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getVariation() {
        return variation;
    }

    public void setVariation(String variation) {
        this.variation = variation;
    }

    public String getBase_price() {
        return base_price;
    }

    public void setBase_price(String base_price) {
        this.base_price = base_price;
    }

    public String getSelling_price() {
        return selling_price;
    }

    public void setSelling_price(String selling_price) {
        this.selling_price = selling_price;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getShipping_cost() {
        return shipping_cost;
    }

    public void setShipping_cost(String shipping_cost) {
        this.shipping_cost = shipping_cost;
    }

    public String getCoupon_discount() {
        return coupon_discount;
    }

    public void setCoupon_discount(String coupon_discount) {
        this.coupon_discount = coupon_discount;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getDelivery_status() {
        return delivery_status;
    }

    public void setDelivery_status(String delivery_status) {
        this.delivery_status = delivery_status;
    }

    public String getPick_from() {
        return pick_from;
    }

    public void setPick_from(String pick_from) {
        this.pick_from = pick_from;
    }

    public String getPick_to() {
        return pick_to;
    }

    public void setPick_to(String pick_to) {
        this.pick_to = pick_to;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public static Creator<OrderDetails> getCREATOR() {
        return CREATOR;
    }
}
