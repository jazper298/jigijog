package com.example.jigijog.models;

public class Chats {

    private int chatIcon;
    private String chatTitle;
    private String chatDate;
    private int chatImage;
    private String chatDescription;

    public Chats(int chatIcon, String chatTitle, String chatDate, int chatImage, String chatDescription) {
        this.chatIcon = chatIcon;
        this.chatTitle = chatTitle;
        this.chatDate = chatDate;
        this.chatImage = chatImage;
        this.chatDescription = chatDescription;
    }

    public int getChatIcon() {
        return chatIcon;
    }

    public String getChatTitle() {
        return chatTitle;
    }

    public String getChatDate() {
        return chatDate;
    }

    public int getChatImage() {
        return chatImage;
    }

    public String getChatDescription() {
        return chatDescription;
    }

    public void setChatIcon(int chatIcon) {
        this.chatIcon = chatIcon;
    }

    public void setChatTitle(String chatTitle) {
        this.chatTitle = chatTitle;
    }

    public void setChatDate(String chatDate) {
        this.chatDate = chatDate;
    }

    public void setChatImage(int chatImage) {
        this.chatImage = chatImage;
    }

    public void setChatDescription(String chatDescription) {
        this.chatDescription = chatDescription;
    }
}
