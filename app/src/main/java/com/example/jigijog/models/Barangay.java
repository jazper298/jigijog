package com.example.jigijog.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Barangay {
    private String id;
    private String brgyCode;
    private String brgyDesc;
    private String regCode;
    private String provCode;
    private String citymunCode;

    public Barangay(String id, String brgyCode, String brgyDesc, String regCode, String provCode, String citymunCode) {
        this.id = id;
        this.brgyCode = brgyCode;
        this.brgyDesc = brgyDesc;
        this.regCode = regCode;
        this.provCode = provCode;
        this.citymunCode = citymunCode;
    }

    public Barangay() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrgyCode() {
        return brgyCode;
    }

    public void setBrgyCode(String brgyCode) {
        this.brgyCode = brgyCode;
    }

    public String getBrgyDesc() {
        return brgyDesc;
    }

    public void setBrgyDesc(String brgyDesc) {
        this.brgyDesc = brgyDesc;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getProvCode() {
        return provCode;
    }

    public void setProvCode(String provCode) {
        this.provCode = provCode;
    }

    public String getCitymunCode() {
        return citymunCode;
    }

    public void setCitymunCode(String citymunCode) {
        this.citymunCode = citymunCode;
    }

    public static String getID(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("ID", "");
    }
    public static String getBrgyCode(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("BRGYCODE", "");
    }
    public static String getBrgyDesc(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("BRGYDESC", "");
    }
    public static String getRegCode(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("REGCODE", "");
    }
    public static String getProvCode(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("PROVCODE", "");
    }
    public static String getCityMunde(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("CITYMUNCODE", "");
    }

    public boolean saveUserSession(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("ID", id);
        editor.putString("BRGYCODE", brgyCode);
        editor.putString("BRGYDESC", brgyDesc);
        editor.putString("REGCODE", regCode);
        editor.putString("PROVCODE", provCode);
        editor.putString("CITYMUNCODE", citymunCode);
        return editor.commit();
    }

    public static boolean clearSession(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        return editor.commit();
    }
}
