package com.example.jigijog.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class City {
    private String id;
    private String psgcCode;
    private String citymunDesc;
    private String regDesc;
    private String provCode;
    private String citymunCode;

    public City() {
    }

    public City(String id, String psgcCode, String citymunDesc, String regDesc, String provCode, String citymunCode) {
        this.id = id;
        this.psgcCode = psgcCode;
        this.citymunDesc = citymunDesc;
        this.regDesc = regDesc;
        this.provCode = provCode;
        this.citymunCode = citymunCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPsgcCode() {
        return psgcCode;
    }

    public void setPsgcCode(String psgcCode) {
        this.psgcCode = psgcCode;
    }

    public String getCitymunDesc() {
        return citymunDesc;
    }

    public void setCitymunDesc(String citymunDesc) {
        this.citymunDesc = citymunDesc;
    }

    public String getRegDesc() {
        return regDesc;
    }

    public void setRegDesc(String regDesc) {
        this.regDesc = regDesc;
    }

    public String getProvCode() {
        return provCode;
    }

    public void setProvCode(String provCode) {
        this.provCode = provCode;
    }

    public String getCitymunCode() {
        return citymunCode;
    }

    public void setCitymunCode(String citymunCode) {
        this.citymunCode = citymunCode;
    }

    public static String getID(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("ID", "");
    }
    public static String getPsgcCode(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("PSGCCODE", "");
    }
    public static String getCityMunDesc(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("CITYMUNDESC", "");
    }
    public static String getRegCode(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("REGCODE", "");
    }
    public static String getProvCode(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("PROVCODE", "");
    }
    public static String getCityMunCode(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("CITYMUNCODE", "");
    }
    public boolean saveUserSession(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("ID", id);
        editor.putString("PSGCCODE", psgcCode);
        editor.putString("CITYMUNDESC", citymunDesc);
        editor.putString("REGCODE", regDesc);
        editor.putString("PROVCODE", provCode);
        editor.putString("CITYMUNCODE", citymunCode);
        return editor.commit();
    }

    public static boolean clearSession(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        return editor.commit();
    }
}
