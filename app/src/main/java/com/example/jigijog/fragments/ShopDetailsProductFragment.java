package com.example.jigijog.fragments;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jigijog.HttpProvider;
import com.example.jigijog.R;
import com.example.jigijog.activities.ProductViewActivity;
import com.example.jigijog.adapters.CustomSwipeAdapter2;
import com.example.jigijog.adapters.ShopDetailsProductsAdapter;
import com.example.jigijog.adapters.TopSellingPerSellerAdapter;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Products;
import com.example.jigijog.models.TopSelling;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.utils.ColorUtils;
import com.example.jigijog.utils.Debugger;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopDetailsProductFragment extends Fragment {

    private View view;
    private Context context;
    private String userID;
    private ArrayList<Products> productsArrayList;
    private ShopDetailsProductsAdapter shopDetailsProductsAdapter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout productswipe_Home;
    private ConstraintLayout indicator_empty_records;
    private Products productsModel;
    public ShopDetailsProductFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_shop_details_product, container, false);
        context = getContext();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        userID = getArguments().getString("USERID");
        initializeUI();
        getProductBySeller();
    }
    private void initializeUI(){
        indicator_empty_records = view.findViewById(R.id.view_Empty);
        recyclerView = view.findViewById(R.id.wishListRecyclerView);
        productswipe_Home = view.findViewById(R.id.productswipe_Home);
        productswipe_Home.setRefreshing(true);
        productswipe_Home.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProductBySeller();
            }
        });

    }

    private void getProductBySeller(){
        productswipe_Home.setRefreshing(true);

        productsArrayList = new ArrayList<>();
        RequestParams params = new RequestParams();
        params.put("seller_id", userID);
        Debugger.logD("asd "+ userID);
        HttpProvider.post(context, "/mobile/getProductBySeller", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                productswipe_Home.setRefreshing(false);
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    indicator_empty_records.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String user_id = jsonObject.getString("user_id");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");

                            productsModel = new Products();

                            productsModel.setId(id);
                            productsModel.setName(name);
                            productsModel.setAdded_by(added_by);
                            productsModel.setUser_id(user_id);
                            productsModel.setCategory_id(category_id);
                            productsModel.setSubcategory_id(subcategory_id);
                            productsModel.setSubsubcategory_id(subsubcategory_id);
                            productsModel.setBrand_id(brand_id);
                            productsModel.setThumbnail_img(thumbnail_img);
                            productsModel.setFeatured_img(featured_img);
                            productsModel.setFlash_deal_img(flash_deal_img);
                            productsModel.setVideo_provider(video_provider);
                            productsModel.setVideo_link(video_link);
                            productsModel.setTags(tags);
                            productsModel.setDescription(description);
                            productsModel.setUnit_price(unit_price);
                            productsModel.setPurchase_price(purchase_price);
                            productsModel.setTodays_deal(todays_deal);
                            productsModel.setPublished(published);
                            productsModel.setFeatured(featured);
                            productsModel.setCurrent_stock(current_stock);
                            productsModel.setUnit(unit);
                            productsModel.setDiscount(discount);
                            productsModel.setDiscount_type(discount_type);
                            productsModel.setTax(tax);
                            productsModel.setTax_type(tax_type);
                            productsModel.setShipping_type(shipping_type);
                            productsModel.setShipping_cost(shipping_cost);
                            productsModel.setWeight(weight);
                            productsModel.setParcel_size(parcel_size);
                            productsModel.setNum_of_sale(num_of_sale);
                            productsModel.setMeta_title(meta_title);
                            productsModel.setMeta_description(meta_description);
                            productsModel.setMeta_img(meta_img);
                            productsModel.setPdf(pdf);
                            productsModel.setSlug(slug);
                            productsModel.setRating(rating);
                            productsModel.setCreated_at(created_at);
                            productsModel.setUpdated_at(updated_at);

                            productsArrayList.add(productsModel);

                        }
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
                        recyclerView.setLayoutManager(layoutManager);

                        shopDetailsProductsAdapter = new ShopDetailsProductsAdapter(context, productsArrayList);
                        shopDetailsProductsAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Products topSelling = productsArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMID", topSelling.getId());
                                intent.putExtra("ITEMNAME", topSelling.getName());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        recyclerView.setAdapter(shopDetailsProductsAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    private void loadProductBySeller(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        recyclerView.setLayoutManager(layoutManager);

        shopDetailsProductsAdapter = new ShopDetailsProductsAdapter(context, productsArrayList);
        shopDetailsProductsAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                Products topSelling = productsArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", topSelling.getId());
                intent.putExtra("ITEMNAME", topSelling.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(shopDetailsProductsAdapter);
    }
}
