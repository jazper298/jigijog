package com.example.jigijog.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.jigijog.R;
import com.example.jigijog.activities.AssortedProductsActivity;
import com.example.jigijog.activities.CategoriesActivity;
import com.example.jigijog.activities.FeaturedProductsActivity;
import com.example.jigijog.activities.ProductSearchActivity;
import com.example.jigijog.activities.ProductViewActivity;
import com.example.jigijog.activities.TodaysDealActivity;
import com.example.jigijog.adapters.AssortedAdapter;
import com.example.jigijog.adapters.CategoriesAdapter;
import com.example.jigijog.adapters.CustomSwipeAdapter;
import com.example.jigijog.adapters.FeaturedAdapter;
import com.example.jigijog.adapters.TodaysDealAdapter;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Assorted;
import com.example.jigijog.models.BestSelling;
import com.example.jigijog.models.Categories;
import com.example.jigijog.models.Featured;
import com.example.jigijog.models.FlashDeal;
import com.example.jigijog.models.Slide;
import com.example.jigijog.models.Todaysdeal;
import com.example.jigijog.utils.CheckNetwork;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.Tools;
import com.example.jigijog.utils.UserRole;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private View view;
    private Context context;

    private RecyclerView categoryRecyclerview;
    private RecyclerView todaysdealRecyclerView;
    private RecyclerView featuredRecyclerView;
    private RecyclerView assortedRecyclerview;
    private ArrayList<Categories> categoriesArrayList;
    private ArrayList<Todaysdeal> todaysdealArrayList;
    private ArrayList<Featured> featuredArrayList;
    private ArrayList<BestSelling> bestSellingArrayList;
    private List<Assorted> assortedArrayList;
    private CategoriesAdapter categoriesAdapter;
    private TodaysDealAdapter todaysDealAdapter;
    private FeaturedAdapter featuredAdapter;
    private AssortedAdapter assortedAdapter;

    private Categories selectedCategories = new Categories();
    private Assorted selectedAssorted = new Assorted();
    private Featured selectedFeatured = new Featured();
    private Todaysdeal selectedTodaysdeal = new Todaysdeal();

    private int item_per_display = 10;

    private TextView todaysDealSee,featureProductsSee,assortedSee;

    private SwipeRefreshLayout swipeRefreshLayout2;
    private SwipeRefreshLayout swipeRefreshLayout3;
    private SwipeRefreshLayout swipeRefreshLayout4;
    private SwipeRefreshLayout swipeRefreshLayout5;

    private ViewPager viewPager;



    private CustomSwipeAdapter customSwipeAdapter;
    private ArrayList<Slide> slideArrayList;
    private LinearLayout dotsLayout;
    private int custom_pos = 0;

    private Timer timer;
    private int current_pos = 0;

    private ArrayList<FlashDeal> flashDealArrayList;

    private ImageView iv_mensHotImage, iv_mensHotImage1,iv_mensHotImage3, iv_mensHotImage4, iv_mensHotImage5;
    private TextView tv_mensHotItemName, tv_mensHotItemName1,tv_mensHotItemName3,tv_mensHotItemName4,tv_mensHotItemName5;
    private TextView tv_mensHotPrice, tv_mensHotPrice1,tv_mensHotPrice3,tv_mensHotPrice4,tv_mensHotPrice5,tv_productSearch;
    private CardView cardView1, cardView2, cardView3, cardView4, cardView5;
    private ImageView iv_bestSellingImage1, iv_bestSellingImage2, iv_bestSellingImage3, iv_bestSellingImage4, iv_bestSellingImage5, iv_bestSellingImage6;
    private TextView tv_bestSellingItemName1, tv_bestSellingItemName2, tv_bestSellingItemName3, tv_bestSellingItemName4, tv_bestSellingItemName5, tv_bestSellingItemName6;
    private TextView tv_bestSellingPrice1, tv_bestSellingPrice2, tv_bestSellingPrice3, tv_bestSellingPrice4, tv_bestSellingPrice5, tv_bestSellingPrice6;
    private TextView tv_bestSellingSold1, tv_bestSellingSold2, tv_bestSellingSold3, tv_bestSellingSold4, tv_bestSellingSold5, tv_bestSellingSold6;
    private CardView bestSellingCardView1, bestSellingCardView2, bestSellingCardView3, bestSellingCardView4, bestSellingCardView5, bestSellingCardView6;
    private ProgressBar progress_bar;
    private LinearLayout lyt_no_connection;
    private AppCompatButton bt_retry;
    private FlashDeal flashDeal;
    private ConstraintLayout emptyIndicator;
    private String role;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment]
        if(CheckNetwork.isInternetAvailable(Objects.requireNonNull(getActivity()))){
            view = inflater.inflate(R.layout.fragment_home, container, false);
            context = getContext();
            initializeUI();
            loadSliderImages();
            loadCategories();
            loadFlashDeal();
            loadBestSelling();
            loadTodaysDeal();
            loadFeaturedProducts();
            loadAssortedProducts();
            role = UserRole.getRole(context);
            Debugger.logD("HomeFragment " + role);
        }else{
            view = inflater.inflate(R.layout.indicator_no_internet, container, false);
            context = getContext();
            initComponent();
        }
        Tools.setSystemBarColor(getActivity(), R.color.grey_5);
        Tools.setSystemBarLight(getActivity());
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }
    private void initializeUI() {
        iv_mensHotImage = view.findViewById(R.id.iv_mensHotImage);
        iv_mensHotImage1 = view.findViewById(R.id.iv_mensHotImage1);
        iv_mensHotImage3 = view.findViewById(R.id.iv_mensHotImage3);
        iv_mensHotImage4 = view.findViewById(R.id.iv_mensHotImage4);
        iv_mensHotImage5 = view.findViewById(R.id.iv_mensHotImage5);

        tv_mensHotItemName = view.findViewById(R.id.tv_mensHotItemName);
        tv_mensHotItemName1 = view.findViewById(R.id.tv_mensHotItemName1);
        tv_mensHotItemName3 = view.findViewById(R.id.tv_mensHotItemName3);
        tv_mensHotItemName4 = view.findViewById(R.id.tv_mensHotItemName4);
        tv_mensHotItemName5 = view.findViewById(R.id.tv_mensHotItemName5);

        tv_mensHotPrice = view.findViewById(R.id.tv_mensHotPrice);
        tv_mensHotPrice1 = view.findViewById(R.id.tv_mensHotPrice1);
        tv_mensHotPrice3 = view.findViewById(R.id.tv_mensHotPrice3);
        tv_mensHotPrice4 = view.findViewById(R.id.tv_mensHotPrice4);
        tv_mensHotPrice5 = view.findViewById(R.id.tv_mensHotPrice5);

        todaysDealSee = view.findViewById(R.id.todaysDealSee);
        featureProductsSee = view.findViewById(R.id.featureProductsSee);
        swipeRefreshLayout2 = view.findViewById(R.id.swipe_Home2);
        swipeRefreshLayout3 = view.findViewById(R.id.swipe_Home3);
        swipeRefreshLayout4 = view.findViewById(R.id.swipe_Home4);
        swipeRefreshLayout5 = view.findViewById(R.id.swipe_Home5);

        assortedSee = view.findViewById(R.id.assortedSee);
        tv_productSearch = view.findViewById(R.id.tv_productSearch);
        tv_productSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductSearchActivity.class);
                startActivity(intent);
            }
        });
//        swipeRefreshLayout2.setRefreshing(false);
//        swipeRefreshLayout3.setRefreshing(false);
//        swipeRefreshLayout4.setRefreshing(false);
        todaysDealSee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TodaysDealActivity.class);
                startActivity(intent);
            }
        });
        featureProductsSee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FeaturedProductsActivity.class);
                startActivity(intent);
            }
        });
        assortedSee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AssortedProductsActivity.class);
                startActivity(intent);
            }
        });

        iv_bestSellingImage1 = view.findViewById(R.id.iv_bestSellingImage1);
        iv_bestSellingImage2 = view.findViewById(R.id.iv_bestSellingImage2);
        iv_bestSellingImage3 = view.findViewById(R.id.iv_bestSellingImage3);
        iv_bestSellingImage4 = view.findViewById(R.id.iv_bestSellingImage4);
        iv_bestSellingImage5 = view.findViewById(R.id.iv_bestSellingImage5);
        iv_bestSellingImage6 = view.findViewById(R.id.iv_bestSellingImage6);

        tv_bestSellingItemName1 = view.findViewById(R.id.tv_bestSellingItemName1);
        tv_bestSellingItemName2 = view.findViewById(R.id.tv_bestSellingItemName2);
        tv_bestSellingItemName3 = view.findViewById(R.id.tv_bestSellingItemName3);
        tv_bestSellingItemName4 = view.findViewById(R.id.tv_bestSellingItemName4);
        tv_bestSellingItemName5 = view.findViewById(R.id.tv_bestSellingItemName5);
        tv_bestSellingItemName6 = view.findViewById(R.id.tv_bestSellingItemName6);

        tv_bestSellingPrice1 = view.findViewById(R.id.tv_bestSellingPrice1);
        tv_bestSellingPrice2 = view.findViewById(R.id.tv_bestSellingPrice2);
        tv_bestSellingPrice3 = view.findViewById(R.id.tv_bestSellingPrice3);
        tv_bestSellingPrice4 = view.findViewById(R.id.tv_bestSellingPrice4);
        tv_bestSellingPrice5 = view.findViewById(R.id.tv_bestSellingPrice5);
        tv_bestSellingPrice6 = view.findViewById(R.id.tv_bestSellingPrice6);

        tv_bestSellingSold1 = view.findViewById(R.id.tv_bestSellingSold1);
        tv_bestSellingSold2 = view.findViewById(R.id.tv_bestSellingSold2);
        tv_bestSellingSold3 = view.findViewById(R.id.tv_bestSellingSold3);
        tv_bestSellingSold4 = view.findViewById(R.id.tv_bestSellingSold4);
        tv_bestSellingSold5 = view.findViewById(R.id.tv_bestSellingSold5);
        tv_bestSellingSold6 = view.findViewById(R.id.tv_bestSellingSold6);

    }

    private void loadSliderImages(){
        viewPager = view.findViewById(R.id.view_pager);
        dotsLayout = view.findViewById(R.id.dotsContainer);
        slideArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getSliderImages", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String photos = jsonObject.getString("photo");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");

                        Slide slideModel = new Slide();
                        slideModel.setId(id);
                        slideModel.setPhotos(photos);
                        slideModel.setCreated_at(created_at);
                        slideModel.setUpdated_at(updated_at);

                        slideArrayList.add(slideModel);
                    }
                    customSwipeAdapter = new CustomSwipeAdapter(context, slideArrayList);
                    viewPager.setAdapter(customSwipeAdapter);
                    loadPreparedDots(custom_pos++);
                    loadCreateSlideShow();
                    viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        }
                        @Override
                        public void onPageSelected(int position) {
                            if (custom_pos>slideArrayList.size()){
                                custom_pos = 0;
                            }
                            loadPreparedDots(custom_pos++);
                        }
                        @Override
                        public void onPageScrollStateChanged(int state) {
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadCreateSlideShow(){
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (current_pos == Integer.MAX_VALUE) {
                    current_pos = 0;
                }
                viewPager.setCurrentItem(current_pos++, true);
            }
        };

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);

            }
        },250, 2500);

    }
    private void loadPreparedDots(int currentSlidePos){
        if(dotsLayout.getChildCount()>0){
            dotsLayout.removeAllViews();
        }
        ImageView dots[] = new ImageView[slideArrayList.size()];
        for (int i =0; i<slideArrayList.size();i++){
            dots[i] = new ImageView(context);
            if(i==currentSlidePos){
                dots[i].setImageDrawable(ContextCompat.getDrawable(context,R.drawable.active_dot));
            }else{
                dots[i].setImageDrawable(ContextCompat.getDrawable(context,R.drawable.inactive_dots));
            }
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            layoutParams.setMargins(4, 0, 4,0);
            dotsLayout.addView(dots[i],layoutParams);
        }
    }

    private void loadCategories() {
        categoryRecyclerview = view.findViewById(R.id.categoriesRecyclerview);
        categoriesArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllCategories", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String banner = jsonObject.getString("banner");

                        Categories categories = new Categories();
                        categories.setId(id);
                        categories.setName(name);
                        categories.setBanner(banner);

                        categoriesArrayList.add(categories);
                    }
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                    categoryRecyclerview.setLayoutManager(layoutManager);

                    categoriesAdapter = new CategoriesAdapter(context, categoriesArrayList);
                    categoriesAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedCategories = categoriesArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, CategoriesActivity.class);
                            intent.putExtra("ROLE_ID", role);
                            intent.putExtra("CATEGORYID", selectedCategories.getId());
                            intent.putExtra("CATEGORYNAME", selectedCategories.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    categoryRecyclerview.setAdapter(categoriesAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadFlashDeal() {
        flashDealArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllFlashDeal", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("loadFlashDeal: " + str);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        FlashDeal flashDeal = new FlashDeal();
                        flashDeal.setId(id);
                        flashDeal.setName(name);
                        flashDeal.setAdded_by(added_by);
                        flashDeal.setCategory_id(category_id);
                        flashDeal.setSubcategory_id(subcategory_id);
                        flashDeal.setSubsubcategory_id(subsubcategory_id);
                        flashDeal.setBrand_id(brand_id);
                        flashDeal.setPhotos(photos);
                        flashDeal.setThumbnail_img(thumbnail_img);
                        flashDeal.setFeatured_img(featured_img);
                        flashDeal.setFlash_deal_img(flash_deal_img);
                        flashDeal.setVideo_provider(video_provider);
                        flashDeal.setVideo_link(video_link);
                        flashDeal.setTags(tags);
                        flashDeal.setDescription(description);
                        flashDeal.setUnit_price(unit_price);
                        flashDeal.setPurchase_price(purchase_price);
                        flashDeal.setChoice_options(choice_options);
                        flashDeal.setColors(colors);
                        flashDeal.setVariations(variations);
                        flashDeal.setTodays_deal(todays_deal);
                        flashDeal.setPublished(published);
                        flashDeal.setFeatured(featured);
                        flashDeal.setCurrent_stock(current_stock);
                        flashDeal.setUnit(unit);
                        flashDeal.setDiscount(discount);
                        flashDeal.setDiscount_type(discount_type);
                        flashDeal.setTax(tax);
                        flashDeal.setTax_type(tax_type);
                        flashDeal.setShipping_type(shipping_type);
                        flashDeal.setShipping_cost(shipping_cost);
                        flashDeal.setWeight(weight);
                        flashDeal.setParcel_size(parcel_size);
                        flashDeal.setNum_of_sale(num_of_sale);
                        flashDeal.setMeta_title(meta_title);
                        flashDeal.setMeta_description(meta_description);
                        flashDeal.setMeta_img(meta_img);
                        flashDeal.setPdf(pdf);
                        flashDeal.setSlug(slug);
                        flashDeal.setRating(rating);
                        flashDeal.setCreated_at(created_at);
                        flashDeal.setUpdated_at(updated_at);

                        flashDealArrayList.add(flashDeal);
                    }

                    if(!flashDealArrayList.isEmpty()){
                        tv_mensHotPrice.setText("\u20B1" + " " + flashDealArrayList.get(0).getUnit_price());
                        tv_mensHotPrice1.setText("\u20B1" + " " + flashDealArrayList.get(1).getUnit_price());
                        tv_mensHotPrice3.setText("\u20B1" + " " + flashDealArrayList.get(2).getUnit_price());
                        tv_mensHotPrice4.setText("\u20B1" + " " + flashDealArrayList.get(3).getUnit_price());
                        tv_mensHotPrice5.setText("\u20B1" + " " + flashDealArrayList.get(4).getUnit_price());

                        if (flashDealArrayList.get(0).getName().length() <= 14){
                            tv_mensHotItemName.setText(flashDealArrayList.get(0).getName());
                        }else{
                            tv_mensHotItemName.setText(flashDealArrayList.get(0).getName().substring(0, 14) + ".." );
                        }
                        if (flashDealArrayList.get(1).getName().length() <= 14){
                            tv_mensHotItemName1.setText(flashDealArrayList.get(1).getName());
                        }else{
                            tv_mensHotItemName1.setText(flashDealArrayList.get(1).getName().substring(0, 14) + ".." );
                        }
                        if (flashDealArrayList.get(2).getName().length() <= 14){
                            tv_mensHotItemName3.setText(flashDealArrayList.get(2).getName());
                        }else{
                            tv_mensHotItemName3.setText(flashDealArrayList.get(2).getName().substring(0, 14) + ".." );
                        }
                        if (flashDealArrayList.get(3).getName().length() <= 12){
                            tv_mensHotItemName4.setText(flashDealArrayList.get(3).getName());
                        }else{
                            tv_mensHotItemName4.setText(flashDealArrayList.get(3).getName().substring(0, 12) + ".." );
                        }
                        if (flashDealArrayList.get(4).getName().length() <= 12){
                            tv_mensHotItemName5.setText(flashDealArrayList.get(4).getName());
                        }else{
                            tv_mensHotItemName5.setText(flashDealArrayList.get(4).getName().substring(0, 12) + ".." );
                        }

                        Glide.with(context)
                                .load( "https://www.jigijog.com/public/" + flashDealArrayList.get(0).getFeatured_img())
                                .placeholder(R.drawable.ic_account_box_black_24dp)
                                .into(iv_mensHotImage);
                        Glide.with(context)
                                .load( "https://www.jigijog.com/public/" + flashDealArrayList.get(1).getFeatured_img())
                                .placeholder(R.drawable.ic_account_box_black_24dp)
                                .into(iv_mensHotImage1);
                        Glide.with(context)
                                .load( "https://www.jigijog.com/public/" + flashDealArrayList.get(2).getFeatured_img())
                                .placeholder(R.drawable.ic_account_box_black_24dp)
                                .into(iv_mensHotImage3);
                        Glide.with(context)
                                .load( "https://www.jigijog.com/public/" + flashDealArrayList.get(3).getFeatured_img())
                                .placeholder(R.drawable.ic_account_box_black_24dp)
                                .into(iv_mensHotImage4);
                        Glide.with(context)
                                .load( "https://www.jigijog.com/public/" + flashDealArrayList.get(4).getFeatured_img())
                                .placeholder(R.drawable.ic_account_box_black_24dp)
                                .into(iv_mensHotImage5);
                        cardView1 = view.findViewById(R.id.cardView1);
                        cardView1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //flashDeal = flashDealArrayList.get(0).getId();
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMID", flashDealArrayList.get(0).getId());
                                intent.putExtra("ITEMNAME", flashDealArrayList.get(0).getName());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        cardView2 = view.findViewById(R.id.cardView2);
                        cardView2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMID", flashDealArrayList.get(1).getId());
                                intent.putExtra("ITEMNAME", flashDealArrayList.get(1).getName());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        cardView3 = view.findViewById(R.id.cardView3);
                        cardView3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMID", flashDealArrayList.get(2).getId());
                                intent.putExtra("ITEMNAME", flashDealArrayList.get(2).getName());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        cardView4 = view.findViewById(R.id.cardView4);
                        cardView4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMID", flashDealArrayList.get(3).getId());
                                intent.putExtra("ITEMNAME", flashDealArrayList.get(3).getName());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        cardView5 = view.findViewById(R.id.cardView5);
                        cardView5.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMID", flashDealArrayList.get(4).getId());
                                intent.putExtra("ITEMNAME", flashDealArrayList.get(4).getName());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadBestSelling() {
        bestSellingArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/bestSelling", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        BestSelling featuredModel = new BestSelling();
                        featuredModel.setId(id);
                        featuredModel.setName(name);
                        featuredModel.setAdded_by(added_by);
                        featuredModel.setCategory_id(category_id);
                        featuredModel.setSubcategory_id(subcategory_id);
                        featuredModel.setSubsubcategory_id(subsubcategory_id);
                        featuredModel.setBrand_id(brand_id);
                        //featuredModel.setPhotos(photos);
                        featuredModel.setThumbnail_img(thumbnail_img);
                        featuredModel.setFeatured_img(featured_img);
                        featuredModel.setFlash_deal_img(flash_deal_img);
                        featuredModel.setVideo_provider(video_provider);
                        featuredModel.setVideo_link(video_link);
                        featuredModel.setTags(tags);
                        featuredModel.setDescription(description);
                        featuredModel.setUnit_price(unit_price);
                        featuredModel.setPurchase_price(purchase_price);
                        //featuredModel.setChoice_options(choice_options);
                        //featuredModel.setColors(colors);
                        //featuredModel.setVariations(variations);
                        featuredModel.setTodays_deal(todays_deal);
                        featuredModel.setPublished(published);
                        featuredModel.setFeatured(featured);
                        featuredModel.setCurrent_stock(current_stock);
                        featuredModel.setUnit(unit);
                        featuredModel.setDiscount(discount);
                        featuredModel.setDiscount_type(discount_type);
                        featuredModel.setTax(tax);
                        featuredModel.setTax_type(tax_type);
                        featuredModel.setShipping_type(shipping_type);
                        featuredModel.setShipping_cost(shipping_cost);
                        featuredModel.setWeight(weight);
                        featuredModel.setParcel_size(parcel_size);
                        featuredModel.setNum_of_sale(num_of_sale);
                        featuredModel.setMeta_title(meta_title);
                        featuredModel.setMeta_description(meta_description);
                        featuredModel.setMeta_img(meta_img);
                        featuredModel.setPdf(pdf);
                        featuredModel.setSlug(slug);
                        featuredModel.setRating(rating);
                        featuredModel.setCreated_at(created_at);
                        featuredModel.setUpdated_at(updated_at);

                        bestSellingArrayList.add(featuredModel);
                    }
                    Debugger.logD("bestSellingArrayList " + bestSellingArrayList.size());
                    if(!bestSellingArrayList.isEmpty()) {
                        if (bestSellingArrayList.get(0).getName().length() <= 14){
                            tv_bestSellingItemName1.setText(bestSellingArrayList.get(0).getName());
                        }else{
                            tv_bestSellingItemName1.setText(bestSellingArrayList.get(0).getName().substring(0, 14) + ".." );
                        }
                        if (bestSellingArrayList.get(1).getName().length() <= 14){
                            tv_bestSellingItemName2.setText(bestSellingArrayList.get(1).getName());
                        }else{
                            tv_bestSellingItemName2.setText(bestSellingArrayList.get(1).getName().substring(0, 14) + ".." );
                        }
                        if (bestSellingArrayList.get(2).getName().length() <= 14){
                            tv_bestSellingItemName3.setText(bestSellingArrayList.get(2).getName());
                        }else{
                            tv_bestSellingItemName3.setText(bestSellingArrayList.get(2).getName().substring(0, 14) + ".." );
                        }
                        if (bestSellingArrayList.get(3).getName().length() <= 14){
                            tv_bestSellingItemName4.setText(bestSellingArrayList.get(3).getName());
                        }else{
                            tv_bestSellingItemName4.setText(bestSellingArrayList.get(3).getName().substring(0, 14) + ".." );
                        }
                        if (bestSellingArrayList.get(4).getName().length() <= 14){
                            tv_bestSellingItemName5.setText(bestSellingArrayList.get(4).getName());
                        }else{
                            tv_bestSellingItemName5.setText(bestSellingArrayList.get(4).getName().substring(0, 14) + ".." );
                        }
                        if (bestSellingArrayList.get(5).getName().length() <= 14){
                            tv_bestSellingItemName6.setText(bestSellingArrayList.get(5).getName());
                        }else{
                            tv_bestSellingItemName6.setText(bestSellingArrayList.get(5).getName().substring(0, 14) + ".." );
                        }
                        Glide.with(context)
                                .load( "https://www.jigijog.com/public/" + bestSellingArrayList.get(0).getFeatured_img())
                                .placeholder(R.drawable.ic_account_box_black_24dp)
                                .into(iv_bestSellingImage1);
                        Glide.with(context)
                                .load( "https://www.jigijog.com/public/" + bestSellingArrayList.get(1).getFeatured_img())
                                .placeholder(R.drawable.ic_account_box_black_24dp)
                                .into(iv_bestSellingImage2);
                        Glide.with(context)
                                .load( "https://www.jigijog.com/public/" + bestSellingArrayList.get(2).getFeatured_img())
                                .placeholder(R.drawable.ic_account_box_black_24dp)
                                .into(iv_bestSellingImage3);
                        Glide.with(context)
                                .load( "https://www.jigijog.com/public/" + bestSellingArrayList.get(3).getFeatured_img())
                                .placeholder(R.drawable.ic_account_box_black_24dp)
                                .into(iv_bestSellingImage4);
                        Glide.with(context)
                                .load( "https://www.jigijog.com/public/" + bestSellingArrayList.get(4).getFeatured_img())
                                .placeholder(R.drawable.ic_account_box_black_24dp)
                                .into(iv_bestSellingImage5);
                        Glide.with(context)
                                .load( "https://www.jigijog.com/public/" + bestSellingArrayList.get(5).getFeatured_img())
                                .placeholder(R.drawable.ic_account_box_black_24dp)
                                .into(iv_bestSellingImage6);

                        tv_bestSellingPrice1.setText("\u20B1" + " " + bestSellingArrayList.get(0).getUnit_price());
                        tv_bestSellingPrice2.setText("\u20B1" + " " + bestSellingArrayList.get(1).getUnit_price());
                        tv_bestSellingPrice3.setText("\u20B1" + " " + bestSellingArrayList.get(2).getUnit_price());
                        tv_bestSellingPrice4.setText("\u20B1" + " " + bestSellingArrayList.get(3).getUnit_price());
                        tv_bestSellingPrice5.setText("\u20B1" + " " + bestSellingArrayList.get(4).getUnit_price());
                        tv_bestSellingPrice6.setText("\u20B1" + " " + bestSellingArrayList.get(5).getUnit_price());

                        tv_bestSellingSold1.setText(bestSellingArrayList.get(0).getNum_of_sale() + " " + "Sold");
                        tv_bestSellingSold2.setText(bestSellingArrayList.get(1).getNum_of_sale() + " " + "Sold");
                        tv_bestSellingSold3.setText(bestSellingArrayList.get(2).getNum_of_sale() + " " + "Sold");
                        tv_bestSellingSold4.setText(bestSellingArrayList.get(3).getNum_of_sale() + " " + "Sold");
                        tv_bestSellingSold5.setText(bestSellingArrayList.get(4).getNum_of_sale() + " " + "Sold");
                        tv_bestSellingSold6.setText(bestSellingArrayList.get(5).getNum_of_sale() + " " + "Sold");

                        bestSellingCardView1 = view.findViewById(R.id.bestSellingCardView1);
                        bestSellingCardView1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMID", bestSellingArrayList.get(0).getId());
                                intent.putExtra("ITEMNAME", bestSellingArrayList.get(0).getName());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        bestSellingCardView2 = view.findViewById(R.id.bestSellingCardView2);
                        bestSellingCardView2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMID", bestSellingArrayList.get(1).getId());
                                intent.putExtra("ITEMNAME", bestSellingArrayList.get(1).getName());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        bestSellingCardView3 = view.findViewById(R.id.bestSellingCardView3);
                        bestSellingCardView3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMID", bestSellingArrayList.get(2).getId());
                                intent.putExtra("ITEMNAME", bestSellingArrayList.get(2).getName());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        bestSellingCardView4 = view.findViewById(R.id.bestSellingCardView4);
                        bestSellingCardView4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMID", bestSellingArrayList.get(3).getId());
                                intent.putExtra("ITEMNAME", bestSellingArrayList.get(3).getName());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        bestSellingCardView5 = view.findViewById(R.id.bestSellingCardView5);
                        bestSellingCardView5.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMID", bestSellingArrayList.get(4).getId());
                                intent.putExtra("ITEMNAME", bestSellingArrayList.get(4).getName());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        bestSellingCardView6 = view.findViewById(R.id.bestSellingCardView6);
                        bestSellingCardView6.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMID", bestSellingArrayList.get(5).getId());
                                intent.putExtra("ITEMNAME", bestSellingArrayList.get(5).getName());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void loadTodaysDeal() {
        todaysdealRecyclerView = view.findViewById(R.id.todaysDealRecyclerview);
        todaysdealArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getTodaysDeal", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("CATEGORIES: " + Arrays.toString(response));
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String photos = jsonObject.getString("flash_deal_img");
                        String unit_price = jsonObject.getString("unit_price");
                        String discount = jsonObject.getString("discount");

                        Todaysdeal todaysdeal = new Todaysdeal();
                        todaysdeal.setId(id);
                        todaysdeal.setName(name);
                        todaysdeal.setPhotos(photos);
                        todaysdeal.setUnit_price(unit_price);
                        todaysdeal.setDiscount(discount);

                        todaysdealArrayList.add(todaysdeal);
                    }
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                    todaysdealRecyclerView.setLayoutManager(layoutManager);

                    todaysDealAdapter = new TodaysDealAdapter(context, todaysdealArrayList);
                    todaysDealAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedTodaysdeal = todaysdealArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedTodaysdeal.getId());
                            intent.putExtra("ITEMNAME", selectedTodaysdeal.getName());
                            intent.putExtras(extras);
                            startActivity(intent);

                        }
                    });
                    todaysdealRecyclerView.setAdapter(todaysDealAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void loadFeaturedProducts() {
        featuredRecyclerView = view.findViewById(R.id.featuredRecyclerview);
        featuredArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getFeaturedProducts", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Featured featuredModel = new Featured();
                        featuredModel.setId(id);
                        featuredModel.setName(name);
                        featuredModel.setAdded_by(added_by);
                        featuredModel.setCategory_id(category_id);
                        featuredModel.setSubcategory_id(subcategory_id);
                        featuredModel.setSubsubcategory_id(subsubcategory_id);
                        featuredModel.setBrand_id(brand_id);
                        featuredModel.setPhotos(photos);
                        featuredModel.setThumbnail_img(thumbnail_img);
                        featuredModel.setFeatured_img(featured_img);
                        featuredModel.setFlash_deal_img(flash_deal_img);
                        featuredModel.setVideo_provider(video_provider);
                        featuredModel.setVideo_link(video_link);
                        featuredModel.setTags(tags);
                        featuredModel.setDescription(description);
                        featuredModel.setUnit_price(unit_price);
                        featuredModel.setPurchase_price(purchase_price);
                        featuredModel.setChoice_options(choice_options);
                        featuredModel.setColors(colors);
                        featuredModel.setVariations(variations);
                        featuredModel.setTodays_deal(todays_deal);
                        featuredModel.setPublished(published);
                        featuredModel.setFeatured(featured);
                        featuredModel.setCurrent_stock(current_stock);
                        featuredModel.setUnit(unit);
                        featuredModel.setDiscount(discount);
                        featuredModel.setDiscount_type(discount_type);
                        featuredModel.setTax(tax);
                        featuredModel.setTax_type(tax_type);
                        featuredModel.setShipping_type(shipping_type);
                        featuredModel.setShipping_cost(shipping_cost);
                        featuredModel.setWeight(weight);
                        featuredModel.setParcel_size(parcel_size);
                        featuredModel.setNum_of_sale(num_of_sale);
                        featuredModel.setMeta_title(meta_title);
                        featuredModel.setMeta_description(meta_description);
                        featuredModel.setMeta_img(meta_img);
                        featuredModel.setPdf(pdf);
                        featuredModel.setSlug(slug);
                        featuredModel.setRating(rating);
                        featuredModel.setCreated_at(created_at);
                        featuredModel.setUpdated_at(updated_at);

                        featuredArrayList.add(featuredModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context,  2, GridLayoutManager.HORIZONTAL, false);
                    featuredRecyclerView.setLayoutManager(layoutManager);

                    featuredAdapter = new FeaturedAdapter(context, featuredArrayList);
                    featuredAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedFeatured = featuredArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedFeatured.getId());
                            intent.putExtra("ITEMNAME", selectedFeatured.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    featuredRecyclerView.setAdapter(featuredAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void loadAssortedProducts() {
        assortedRecyclerview = view.findViewById(R.id.assortedRecyclerView);
        assortedArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAssortedItems", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Assorted assortedModel = new Assorted();
                        assortedModel.setId(id);
                        assortedModel.setName(name);
                        assortedModel.setAdded_by(added_by);
                        assortedModel.setCategory_id(category_id);
                        assortedModel.setSubcategory_id(subcategory_id);
                        assortedModel.setSubsubcategory_id(subsubcategory_id);
                        assortedModel.setBrand_id(brand_id);
                        assortedModel.setPhotos(photos);
                        assortedModel.setThumbnail_img(thumbnail_img);
                        assortedModel.setFeatured_img(featured_img);
                        assortedModel.setFlash_deal_img(flash_deal_img);
                        assortedModel.setVideo_provider(video_provider);
                        assortedModel.setVideo_link(video_link);
                        assortedModel.setTags(tags);
                        assortedModel.setDescription(description);
                        assortedModel.setUnit_price(unit_price);
                        assortedModel.setPurchase_price(purchase_price);
                        assortedModel.setChoice_options(choice_options);
                        assortedModel.setColors(colors);
                        assortedModel.setVariations(variations);
                        assortedModel.setTodays_deal(todays_deal);
                        assortedModel.setPublished(published);
                        assortedModel.setFeatured(featured);
                        assortedModel.setCurrent_stock(current_stock);
                        assortedModel.setUnit(unit);
                        assortedModel.setDiscount(discount);
                        assortedModel.setDiscount_type(discount_type);
                        assortedModel.setTax(tax);
                        assortedModel.setTax_type(tax_type);
                        assortedModel.setShipping_type(shipping_type);
                        assortedModel.setShipping_cost(shipping_cost);
                        assortedModel.setWeight(weight);
                        assortedModel.setParcel_size(parcel_size);
                        assortedModel.setNum_of_sale(num_of_sale);
                        assortedModel.setMeta_title(meta_title);
                        assortedModel.setMeta_description(meta_description);
                        assortedModel.setMeta_img(meta_img);
                        assortedModel.setPdf(pdf);
                        assortedModel.setSlug(slug);
                        assortedModel.setRating(rating);
                        assortedModel.setCreated_at(created_at);
                        assortedModel.setUpdated_at(updated_at);

                        assortedArrayList.add(assortedModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context,  2, GridLayoutManager.VERTICAL, false);
                    assortedRecyclerview.setLayoutManager(layoutManager);

                    assortedAdapter = new AssortedAdapter(context, assortedArrayList);
                    assortedAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedAssorted = assortedArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMID", selectedAssorted.getId());
                            intent.putExtra("ITEMNAME", selectedAssorted.getName());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    assortedRecyclerview.setAdapter(assortedAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });


    }

    private void initComponent() {
        progress_bar = (ProgressBar) view.findViewById(R.id.progress_bar);
        lyt_no_connection = (LinearLayout) view.findViewById(R.id.lyt_no_connection);
        bt_retry = (AppCompatButton) view.findViewById(R.id.bt_retry);

        progress_bar.setVisibility(View.GONE);
        lyt_no_connection.setVisibility(View.VISIBLE);

        bt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                progress_bar.setVisibility(View.VISIBLE);
                lyt_no_connection.setVisibility(View.GONE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(CheckNetwork.isInternetAvailable(Objects.requireNonNull(getActivity()))){

                        }else{
                            progress_bar.setVisibility(View.GONE);
                            lyt_no_connection.setVisibility(View.VISIBLE);
                        }

                    }
                }, 1000);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        resumeCategories();
        resumeTodaysDeal();
        resumeFeaturedProducts();
        resumeAssortedProducts();
    }

    private void resumeCategories(){
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        categoryRecyclerview.setLayoutManager(layoutManager);

        categoriesAdapter = new CategoriesAdapter(context, categoriesArrayList);
        categoriesAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedCategories = categoriesArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, CategoriesActivity.class);
                intent.putExtra("ROLE_ID", role);
                intent.putExtra("CATEGORYID", selectedCategories.getId());
                intent.putExtra("CATEGORYNAME", selectedCategories.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        categoryRecyclerview.setAdapter(categoriesAdapter);
    }
    private void resumeTodaysDeal(){
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        todaysdealRecyclerView.setLayoutManager(layoutManager);

        todaysDealAdapter = new TodaysDealAdapter(context, todaysdealArrayList);
        todaysDealAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedTodaysdeal = todaysdealArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedTodaysdeal.getId());
                intent.putExtra("ITEMNAME", selectedTodaysdeal.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        todaysdealRecyclerView.setAdapter(todaysDealAdapter);
    }
    private void resumeFeaturedProducts(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context,  2, GridLayoutManager.HORIZONTAL, false);
        featuredRecyclerView.setLayoutManager(layoutManager);

        featuredAdapter = new FeaturedAdapter(context, featuredArrayList);
        featuredAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedFeatured = featuredArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedFeatured.getId());
                intent.putExtra("ITEMNAME", selectedFeatured.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        featuredRecyclerView.setAdapter(featuredAdapter);
    }
    private void resumeAssortedProducts(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context,  2, GridLayoutManager.VERTICAL, false);
        assortedRecyclerview.setLayoutManager(layoutManager);

        assortedAdapter = new AssortedAdapter(context, assortedArrayList);
        assortedAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                selectedAssorted = assortedArrayList.get(position);
                Bundle extras = new Bundle();
                Intent intent = new Intent(context, ProductViewActivity.class);
                intent.putExtra("ITEMID", selectedAssorted.getId());
                intent.putExtra("ITEMNAME", selectedAssorted.getName());
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        assortedRecyclerview.setAdapter(assortedAdapter);
    }


}
