package com.example.jigijog.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Canvas;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.jigijog.HttpProvider;
import com.example.jigijog.R;
import com.example.jigijog.activities.CheckOutActivity;
import com.example.jigijog.activities.CheckOutPageActivity;
import com.example.jigijog.activities.LoginActivity;
import com.example.jigijog.adapters.CartAdapter;
import com.example.jigijog.adapters.CartItemAdapter;
import com.example.jigijog.helpers.CartContract;
import com.example.jigijog.helpers.CartDbHelper;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Cart;
import com.example.jigijog.models.CartItem;
import com.example.jigijog.models.OrderDetails;
import com.example.jigijog.models.Products;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.utils.CheckNetwork;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.UserRole;
import com.google.android.material.snackbar.Snackbar;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

import static java.lang.StrictMath.max;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartFragment extends Fragment {

    private View view;
    private Context context;

    private ImageView iv_mycartBack;
    private TextView tv_mycartName,tv_mycartDelete,tv_shipping,tv_total;
    private Button btn_applyVoucher,btn_checkOut;

    private String productID;
    private String userID;
    private ArrayList<Products> productsArrayList;
    private ArrayList<Products.Photos> photosArrayList;
    private RecyclerView cartRecyclerView;
    private CartAdapter cartAdapter;
    private ArrayList<Cart> cartArrayList;
    private ArrayList<Cart.Photos> cartPhotosArrayList;
    private Cart selectedCart = new Cart();

    private ConstraintLayout emptyIndicator;
    private ProgressBar progress_bar;
    private LinearLayout lyt_no_connection;
    private AppCompatButton bt_retry;

    private ArrayList<CartItem> cartItemArrayList;
    private ArrayList<OrderDetails> orderDetailsArrayList;
    private CartItemAdapter cartItemAdapter;
    private CartItem cartItem = new CartItem();
    String[] unit_price;
    double total = 0;
    double shipping = 0;
    private String role;
    public CartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(CheckNetwork.isInternetAvailable(Objects.requireNonNull(getActivity()))){
            view = inflater.inflate(R.layout.fragment_cart, container, false);
            context = getContext();
            Intent intent = getActivity().getIntent();
            //productID = intent.getStringExtra("PRODUCTID");
            userID = UserCustomer.getID(context);
            initializeUI();
            if (userID == null || userID.isEmpty()){
                emptyIndicator.setVisibility(View.VISIBLE);
            }else{
                //loadProductItemByID(Integer.parseInt(productID));
                getProductIDonCart(Integer.parseInt(userID));
            }
            role = UserRole.getRole(context);

            checkUserRole();
        }else{
            view = inflater.inflate(R.layout.indicator_no_internet, container, false);
            context = getContext();
            initComponent();
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    private void initializeUI() {
        emptyIndicator = view.findViewById(R.id.view_Empty);
        tv_mycartName = view.findViewById(R.id.tv_mycartName);
        tv_total = view.findViewById(R.id.tv_total);
        tv_shipping = view.findViewById(R.id.tv_shipping);
        btn_applyVoucher = view.findViewById(R.id.btn_applyVoucher);
        btn_applyVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.warning(context, "Button Voucher code Clicked").show();
            }
        });

        btn_checkOut = view.findViewById(R.id.btn_checkOut);
        btn_checkOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!UserCustomer.getEmailVerifiedAt(context).contains("null")){
                    Intent intent = new Intent(context, CheckOutPageActivity.class);
                    startActivity(intent);
                }else{
                    view = getActivity().findViewById(android.R.id.content);
                    Snackbar snackbar = Snackbar.make(view, "Please activate your account!", Snackbar.LENGTH_LONG)
                            .setAction("Login", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    startActivity(intent);
                                }
                            });
                    snackbar.show();
                }
            }
        });
    }
    private void checkUserRole(){
        if (!role.equals(UserRole.Customer())) {
            btn_checkOut.setEnabled(false);
        } else {
            //checkStudentSession();
        }
    }

    private void loadProductItemByID(int productID){
        cartArrayList = new ArrayList<>();
        RequestParams params = new RequestParams();
        params.put("product_id", productID);
        Debugger.logD("cart "+ productID);
        HttpProvider.post(context, "/mobile/selectedProductById", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String user_id = jsonObject.getString("user_id");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");

                            Cart productsModel = new Cart();

                            cartPhotosArrayList = new ArrayList<>();
                            JSONArray jsonArrays = new JSONArray(Arrays.asList(photos));

                            for(int z=0; z<jsonArrays.length(); z++) {
                                JSONObject jobject = jsonArray.getJSONObject(z);

                                String photos2 = jobject.getString("photos");

                                String result = photos2.substring(2, photos2.length() -2).replace("\\","");

                                Cart.Photos photos1 = new Cart.Photos();
                                photos1.setPhoto(result);

                                cartPhotosArrayList.add(photos1);

                            }

//                            choice_optionsArrayList = new ArrayList<>();
//                            JSONArray jsonArray2 = new JSONArray(Arrays.asList(choice_options));
//                            for (int k = 0; k <= jsonArray2.length() - 1; k++) {
//                                JSONObject jsonObject2 = jsonArray2.getJSONObject(k);
//                                String choice_options2 = jsonObject2.getString("choice_options");
//
//                                Products.Choice_Options choice_options1 = new Products.Choice_Options();
//                                choice_options1.setChoice_option(choice_options2);
//
//                                choice_optionsArrayList.add(choice_options1);
//                            }
//                            colorsArrayList = new ArrayList<>();
//                            JSONArray jsonArray3 = new JSONArray(Arrays.asList(colors));
//                            for (int l = 0; l <= jsonArray2.length() - 1; l++) {
//                                JSONObject jsonObject2 = jsonArray3.getJSONObject(l);
//                                String colors2 = jsonObject2.getString("colors");
//
//                                Products.Colors colors1 = new Products.Colors();
//                                colors1.setColor(colors2);
//
//                                colorsArrayList.add(colors1);
//                            }
//
//                            variationsArrayList = new ArrayList<>();
//                            JSONArray jsonArray4 = new JSONArray(Arrays.asList(variations));
//                            for (int n = 0; n <= jsonArray2.length() - 1; n++) {
//                                JSONObject jsonObject2 = jsonArray4.getJSONObject(n);
//                                String variations2 = jsonObject2.getString("variations");
//
//                                Products.Variations variations1 = new Products.Variations();
//                                variations1.setVariation(variations2);
//
//                                variationsArrayList.add(variations1);
//                            }

                            productsModel.setId(id);
                            productsModel.setName(name);
                            productsModel.setAdded_by(added_by);
                            productsModel.setUser_id(user_id);
                            productsModel.setCategory_id(category_id);
                            productsModel.setSubcategory_id(subcategory_id);
                            productsModel.setSubsubcategory_id(subsubcategory_id);
                            productsModel.setBrand_id(brand_id);
                            //productsModel.setPhotos(photos);
                            productsModel.setThumbnail_img(thumbnail_img);
                            productsModel.setFeatured_img(featured_img);
                            productsModel.setFlash_deal_img(flash_deal_img);
                            productsModel.setVideo_provider(video_provider);
                            productsModel.setVideo_link(video_link);
                            productsModel.setTags(tags);
                            productsModel.setDescription(description);
                            productsModel.setUnit_price(unit_price);
                            productsModel.setPurchase_price(purchase_price);
                            //productsModel.setChoice_options(choice_options);
                            // productsModel.setColors(colors);
                            //productsModel.setVariations(variations);
                            productsModel.setTodays_deal(todays_deal);
                            productsModel.setPublished(published);
                            productsModel.setFeatured(featured);
                            productsModel.setCurrent_stock(current_stock);
                            productsModel.setUnit(unit);
                            productsModel.setDiscount(discount);
                            productsModel.setDiscount_type(discount_type);
                            productsModel.setTax(tax);
                            productsModel.setTax_type(tax_type);
                            productsModel.setShipping_type(shipping_type);
                            productsModel.setShipping_cost(shipping_cost);
                            productsModel.setWeight(weight);
                            productsModel.setParcel_size(parcel_size);
                            productsModel.setNum_of_sale(num_of_sale);
                            productsModel.setMeta_title(meta_title);
                            productsModel.setMeta_description(meta_description);
                            productsModel.setMeta_img(meta_img);
                            productsModel.setPdf(pdf);
                            productsModel.setSlug(slug);
                            productsModel.setRating(rating);
                            productsModel.setCreated_at(created_at);
                            productsModel.setUpdated_at(updated_at);

                            cartArrayList.add(productsModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        cartRecyclerView.setLayoutManager(layoutManager);

                        cartAdapter = new CartAdapter(context, cartArrayList);
                        cartAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedCart = cartArrayList.get(position);
                            }
                        });
                        cartRecyclerView.setAdapter(cartAdapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }
    private void initComponent() {
        progress_bar = (ProgressBar) view.findViewById(R.id.progress_bar);
        lyt_no_connection = (LinearLayout) view.findViewById(R.id.lyt_no_connection);
        bt_retry = (AppCompatButton) view.findViewById(R.id.bt_retry);

        progress_bar.setVisibility(View.GONE);
        lyt_no_connection.setVisibility(View.VISIBLE);

        bt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                progress_bar.setVisibility(View.VISIBLE);
                lyt_no_connection.setVisibility(View.GONE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(CheckNetwork.isInternetAvailable(Objects.requireNonNull(getActivity()))){

                        }else{
                            progress_bar.setVisibility(View.GONE);
                            lyt_no_connection.setVisibility(View.VISIBLE);
                        }

                    }
                }, 1000);
            }
        });
    }
    private void getProductIDonCart(int userID) {
        cartItemArrayList = new ArrayList<>();
        orderDetailsArrayList = new ArrayList<>();
        cartRecyclerView = view.findViewById(R.id.cartRecyclerView);
        CartDbHelper dbHelper = CartDbHelper.getInstance(context);
        cartItemArrayList = dbHelper.getCartItems(userID);
        orderDetailsArrayList = dbHelper.getAllOrderDetails(userID);
        for(int i = 0; i < max(cartItemArrayList.size(), orderDetailsArrayList.size()); i++){
            double totals =  Double.parseDouble(cartItemArrayList.get(i).getUnit_price()) * Double.parseDouble( orderDetailsArrayList.get(i).getQuantity());
            String shippings = String.valueOf(cartItemArrayList.get(i).getShipping_cost());
            total += totals;
            if(shippings.contains("null")){
                shipping += Double.parseDouble("0.00");
            }else{
                shipping += Double.parseDouble(shippings);
            }
        }
        tv_total.setText("\u20B1" + " " + total + "0");
        tv_shipping.setText("\u20B1" + " " + shipping + "0");


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        cartRecyclerView.setLayoutManager(layoutManager);


        cartItemAdapter = new CartItemAdapter(context, cartItemArrayList);
        cartItemAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {
                //cartItem = cartItemArrayList.get(position);
            }
        });
        new ItemTouchHelper(simpleCallback).attachToRecyclerView(cartRecyclerView);
        cartRecyclerView.setAdapter(cartItemAdapter);
    }
    String deletedCart = null;
    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int direction) {
            final int position = viewHolder.getAdapterPosition();
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater = getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.dialog_delete_cart, null);
            final Button btn_cancel, btn_delete;
            dialogBuilder.setCancelable(false);
            btn_cancel = dialogView.findViewById(R.id.btn_cancel);
            btn_delete = dialogView.findViewById(R.id.btn_delete);
            dialogBuilder.setView(dialogView);
            final AlertDialog b = dialogBuilder.create();

            btn_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CartDbHelper dbHelper = CartDbHelper.getInstance(context);
                    deletedCart = cartItemArrayList.get(position).getName();
                    dbHelper.removeSingleContact(deletedCart);

                    cartItemArrayList.remove(position);
                    cartItemAdapter.notifyItemRemoved(position);
                    b.hide();
                }
            });
            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cartItemAdapter.notifyItemChanged(viewHolder.getAdapterPosition());
                    b.hide();
                }
            });
            b.show();
        }

        @Override
        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            new RecyclerViewSwipeDecorator.Builder(context, c, cartRecyclerView, viewHolder, dX,dY,actionState, isCurrentlyActive)
                    .addSwipeLeftActionIcon(R.drawable.ic_delete_black_24dp)
                    .addSwipeLeftBackgroundColor(ContextCompat.getColor(context, R.color.deep_orange_400))
                    .create()
                    .decorate();
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

        }
    };
    private void openDialogLogout() {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle("Logout")
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setNegativeButton("NO", null)
                .create();
        dialog.show();

    }


}
