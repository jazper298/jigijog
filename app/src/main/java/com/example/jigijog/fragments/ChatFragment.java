package com.example.jigijog.fragments;


import android.app.Notification;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.jigijog.R;
import com.example.jigijog.adapters.ChatsAdapter;
import com.example.jigijog.adapters.NotificationsAdapter;
import com.example.jigijog.interfaces.OnClickRecyclerView;
import com.example.jigijog.models.Chats;
import com.example.jigijog.models.Notifications;
import com.example.jigijog.utils.CheckNetwork;
import com.example.jigijog.utils.Tools;
import com.example.jigijog.utils.ViewAnimation;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends Fragment {
    private View view;
    private Context context;
    private ViewPager view_pager;
    private SectionsPagerAdapter viewPagerAdapter;
    private TabLayout tab_layout;

    private RecyclerView chatRecyclerView;
    private ArrayList<Chats> chatsArrayList;
    private ChatsAdapter chatsAdapter;
    private ArrayList<Notifications> notificationsArrayList;
    private NotificationsAdapter notificationsAdapter;

    private Button tab_all, tab_chats, tab_notification;

    private ProgressBar progress_bar;
    private LinearLayout lyt_no_connection;
    private AppCompatButton bt_retry;
    private ConstraintLayout emptyIndicator;

    public ChatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(CheckNetwork.isInternetAvailable(Objects.requireNonNull(getActivity()))){
            view = inflater.inflate(R.layout.fragment_chat, container, false);
            context = getContext();
            Tools.setSystemBarColor(getActivity(), R.color.deep_orange_400);
            Tools.setSystemBarLight(getActivity());
            initializeUI();
            initializeToolbar();
            loadAllMessages();
        }
        else{
            view = inflater.inflate(R.layout.indicator_no_internet, container, false);
            context = getContext();
            initComponent();
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void initializeToolbar() {
//        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
//        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
//        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Messages");
//        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        Tools.setSystemBarColor(getActivity());
    }

    private void initializeUI() {
        view = (View) view.findViewById(R.id.main_content);
//        view_pager = (ViewPager) view.findViewById(R.id.view_pager);
//        tab_layout = (TabLayout) view.findViewById(R.id.tab_layout);
//        setupViewPager(view_pager);
//
//        tab_layout.setupWithViewPager(view_pager);
//
//        tab_layout.getTabAt(0).setIcon(R.drawable.ic_chat_black_24dp);
//        tab_layout.getTabAt(1).setIcon(R.drawable.ic_chat_black_24dp);
//        tab_layout.getTabAt(2).setIcon(R.drawable.ic_notifications_black_24dp);
//
//        // set icon color pre-selected
//        tab_layout.getTabAt(0).getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
//        tab_layout.getTabAt(1).getIcon().setColorFilter(getResources().getColor(R.color.grey_20), PorterDuff.Mode.SRC_IN);
//        tab_layout.getTabAt(2).getIcon().setColorFilter(getResources().getColor(R.color.grey_20), PorterDuff.Mode.SRC_IN);
//
//        tab_layout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                tab.getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//                tab.getIcon().setColorFilter(getResources().getColor(R.color.grey_20), PorterDuff.Mode.SRC_IN);
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });
        tab_all = view.findViewById(R.id.tab_all);
        tab_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadAllMessages();
                //Snackbar.make(view, "All", Snackbar.LENGTH_SHORT).show();
            }
        });
        tab_chats = view.findViewById(R.id.tab_chats);
        tab_chats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadChatMessages();
                //Snackbar.make(view, "Chat", Snackbar.LENGTH_SHORT).show();
            }
        });
        tab_notification = view.findViewById(R.id.tab_notification);
        tab_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadNotificationMessages();
                //Snackbar.make(view, "Notification", Snackbar.LENGTH_SHORT).show();
            }
        });

        ViewAnimation.fadeOutIn(((NestedScrollView) view.findViewById(R.id.nested_content)));

    }


    private void setupViewPager(ViewPager viewPager) {
        viewPagerAdapter = new SectionsPagerAdapter(getFragmentManager());
        viewPagerAdapter.addFragment(ChatAllFragment.newInstance(), "All");    // index 0
        viewPagerAdapter.addFragment(ChatMessagesFragment.newInstance(), "Chats");   // index 1
        viewPagerAdapter.addFragment(ChatNotificationFragment.newInstance(), "Notifications");    // index 2
        viewPager.setAdapter(viewPagerAdapter);
    }


    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void loadAllMessages(){
        chatRecyclerView = view.findViewById(R.id.chatRecyclerView);

        chatsArrayList = new ArrayList<>();

        chatsArrayList.add(new Chats(R.drawable.ic_notifications_black_24dp, "Over 2 Million worth of prizes to be won!","09:02AM",R.drawable.slider1,"Guess the Price, Win the Price from 6-10PM today! \n Click to learn more."));
        chatsArrayList.add(new Chats(R.drawable.ic_chat_black_24dp, "Win 111,111 credits with just 1!","Yesterday",R.drawable.slider2,"Join LUCKY PISO now and use Lazada Wallet! \n Round ends on Nov 11 23:59. Draw on Nov 12."));
        chatsArrayList.add(new Chats(R.drawable.ic_notifications_black_24dp, "Over 2 Million worth of prizes to be won!","09:02AM",R.drawable.slider4,"Guess the Price, Win the Price from 6-10PM today! \nClick to learn more."));
        chatsArrayList.add(new Chats(R.drawable.ic_chat_black_24dp, "Win 111,111 credits with just 1!","Yesterday",R.drawable.slider7,"Join LUCKY PISO now and use Lazada Wallet! \n Round ends on Nov 11 23:59. Draw on Nov 12."));
        chatsArrayList.add(new Chats(R.drawable.ic_notifications_black_24dp, "Over 2 Million worth of prizes to be won!","09:02AM",R.drawable.slider1,"Guess the Price, Win the Price from 6-10PM today! \n Click to learn more."));
        chatsArrayList.add(new Chats(R.drawable.ic_chat_black_24dp, "Win 111,111 credits with just 1!","Yesterday",R.drawable.slider2,"Join LUCKY PISO now and use Lazada Wallet! \n Round ends on Nov 11 23:59. Draw on Nov 12."));
        chatsArrayList.add(new Chats(R.drawable.ic_notifications_black_24dp, "Over 2 Million worth of prizes to be won!","09:02AM",R.drawable.slider4,"Guess the Price, Win the Price from 6-10PM today! \nClick to learn more."));
        chatsArrayList.add(new Chats(R.drawable.ic_chat_black_24dp, "Win 111,111 credits with just 1!","Yesterday",R.drawable.slider7,"Join LUCKY PISO now and use Lazada Wallet! \n Round ends on Nov 11 23:59. Draw on Nov 12."));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        chatRecyclerView.setLayoutManager(layoutManager);

        chatsAdapter = new ChatsAdapter(context, chatsArrayList);
        chatsAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
        chatRecyclerView.setAdapter(chatsAdapter);
    }
    private void loadChatMessages(){
        chatRecyclerView = view.findViewById(R.id.chatRecyclerView);

        chatsArrayList = new ArrayList<>();

        chatsArrayList.add(new Chats(R.drawable.ic_chat_black_24dp, "Over 2 Million worth of prizes to be won!","09:02AM",R.drawable.slider1,"Guess the Price, Win the Price from 6-10PM today! \n Click to learn more."));
        chatsArrayList.add(new Chats(R.drawable.ic_chat_black_24dp, "Win 111,111 credits with just 1!","Yesterday",R.drawable.slider2,"Join LUCKY PISO now and use Lazada Wallet! \n Round ends on Nov 11 23:59. Draw on Nov 12."));
        chatsArrayList.add(new Chats(R.drawable.ic_chat_black_24dp, "Over 2 Million worth of prizes to be won!","09:02AM",R.drawable.slider4,"Guess the Price, Win the Price from 6-10PM today! \nClick to learn more."));
        chatsArrayList.add(new Chats(R.drawable.ic_chat_black_24dp, "Win 111,111 credits with just 1!","Yesterday",R.drawable.slider7,"Join LUCKY PISO now and use Lazada Wallet! \n Round ends on Nov 11 23:59. Draw on Nov 12."));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        chatRecyclerView.setLayoutManager(layoutManager);

        chatsAdapter = new ChatsAdapter(context, chatsArrayList);
        chatsAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
        chatRecyclerView.setAdapter(chatsAdapter);
    }
    private void loadNotificationMessages(){
        chatRecyclerView = view.findViewById(R.id.chatRecyclerView);
        notificationsArrayList = new ArrayList<>();

        notificationsArrayList.add(new Notifications(R.drawable.ic_notifications_black_24dp, "Over 2 Million worth of prizes to be won!","09:02AM",R.drawable.slider1,"Guess the Price, Win the Price from 6-10PM today! \n Click to learn more."));
        notificationsArrayList.add(new Notifications(R.drawable.ic_notifications_black_24dp, "Win 111,111 credits with just 1!","Yesterday",R.drawable.slider2,"Join LUCKY PISO now and use Lazada Wallet! \n Round ends on Nov 11 23:59. Draw on Nov 12."));
        notificationsArrayList.add(new Notifications(R.drawable.ic_notifications_black_24dp, "Over 2 Million worth of prizes to be won!","09:02AM",R.drawable.slider4,"Guess the Price, Win the Price from 6-10PM today! \nClick to learn more."));
        notificationsArrayList.add(new Notifications(R.drawable.ic_notifications_black_24dp, "Win 111,111 credits with just 1!","Yesterday",R.drawable.slider7,"Join LUCKY PISO now and use Lazada Wallet! \n Round ends on Nov 11 23:59. Draw on Nov 12."));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        chatRecyclerView.setLayoutManager(layoutManager);

        notificationsAdapter = new NotificationsAdapter(context, notificationsArrayList);
        notificationsAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
        chatRecyclerView.setAdapter(notificationsAdapter);

    }

    private void initComponent() {
        progress_bar = (ProgressBar) view.findViewById(R.id.progress_bar);
        lyt_no_connection = (LinearLayout) view.findViewById(R.id.lyt_no_connection);
        bt_retry = (AppCompatButton) view.findViewById(R.id.bt_retry);

        progress_bar.setVisibility(View.GONE);
        lyt_no_connection.setVisibility(View.VISIBLE);

        bt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                progress_bar.setVisibility(View.VISIBLE);
                lyt_no_connection.setVisibility(View.GONE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(CheckNetwork.isInternetAvailable(Objects.requireNonNull(getActivity()))){

                        }else{
                            progress_bar.setVisibility(View.GONE);
                            lyt_no_connection.setVisibility(View.VISIBLE);
                        }

                    }
                }, 1000);
            }
        });
    }
}
