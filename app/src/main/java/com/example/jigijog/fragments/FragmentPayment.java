package com.example.jigijog.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jigijog.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPayment extends Fragment {
    private View view;
    private Context cOntext;
    private CardView gcash, wallet, cod;
    private String payment;

    public FragmentPayment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_fragment_payment, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeUI();
    }

    private void initializeUI(){
        gcash = view.findViewById(R.id.gcash);
        wallet = view.findViewById(R.id.wallet);
        cod = view.findViewById(R.id.cod);
        gcash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payment = "GCASH E-WALLET";
            }
        });
        wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payment = "JIGIJOG WALLET";
            }
        });
        cod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payment = "CASH ON DELIVERY";
            }
        });

    }

}
