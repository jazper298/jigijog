package com.example.jigijog.fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jigijog.HttpProvider;
import com.example.jigijog.R;
import com.example.jigijog.activities.UpdateInfoActivity;
import com.example.jigijog.models.Barangay;
import com.example.jigijog.models.City;
import com.example.jigijog.models.Province;
import com.example.jigijog.models.User;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.ProgressPopup;
import com.example.jigijog.utils.Tools;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountInfoFragment extends BottomSheetDialogFragment {
    private View view;
    private Context context;
    private BottomSheetBehavior mBehavior;
    private AppBarLayout app_bar_layout;
    private LinearLayout lyt_profile;
    private ImageButton iv_update;
    private TextView tv_name, tv_name_toolbar, tv_mobile, tv_email, tv_province,tv_city,tv_barangay,tv_landmark;
    CircleImageView imageView;
    public AccountInfoFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        final View view = View.inflate(getContext(), R.layout.fragment_account_info, null);
        context = getContext();

        String userID = UserCustomer.getID(context);
        String userTokens = UserCustomer.getRememberToken(context);
        Debugger.logD("AccountInfoFragment userID " + userID);
        Debugger.logD("AccountInfoFragment userTokens " + userTokens);

        dialog.setContentView(view);
        mBehavior = BottomSheetBehavior.from((View) view.getParent());
        mBehavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO);

        app_bar_layout = (AppBarLayout) view.findViewById(R.id.app_bar_layout);
        lyt_profile = (LinearLayout) view.findViewById(R.id.lyt_profile);

        // set data to view
        imageView =  (CircleImageView) view.findViewById(R.id.image);

        Tools.displayImageRound2(getActivity(), (CircleImageView) view.findViewById(R.id.image), "https://www.jigijog.com/public/" + UserCustomer.getAvatarOriginal(context));
        tv_name = view.findViewById(R.id.name);
        //tv_name.setText(UserCustomer.getName(context));
        tv_name_toolbar = view.findViewById(R.id.name_toolbar);
        tv_name_toolbar.setText(UserCustomer.getName(context));
        tv_mobile = view.findViewById(R.id.tv_mobile);
        tv_mobile.setText(UserCustomer.getPhone(context));
        tv_email = view.findViewById(R.id.tv_email);
        tv_email.setText(UserCustomer.getEmail(context));
        tv_province = view.findViewById(R.id.tv_province);
        tv_province.setText(UserCustomer.getAddress(context));
//        tv_city = view.findViewById(R.id.tv_city);
//        tv_city.setText(City.getCityMunDesc(context));
//        tv_barangay = view.findViewById(R.id.tv_barangay);
//        tv_barangay.setText(Barangay.getBrgyDesc(context));
        tv_landmark =view.findViewById(R.id.tv_landmark);
        tv_landmark.setText(UserCustomer.getLandmark(context));
        loadUpdatedProfile();
        ((View) view.findViewById(R.id.lyt_spacer)).setMinimumHeight(Tools.getScreenHeight() / 2);

        hideView(app_bar_layout);
        ((ImageButton) view.findViewById(R.id.iv_update)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (context, UpdateInfoActivity.class);
                startActivity(intent);
            }
        });
        mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (BottomSheetBehavior.STATE_EXPANDED == newState) {
                    showView(app_bar_layout, getActionBarSize());
                    hideView(lyt_profile);
                }
                if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                    hideView(app_bar_layout);
                    showView(lyt_profile, getActionBarSize());
                }

                if (BottomSheetBehavior.STATE_HIDDEN == newState) {
                    dismiss();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        ((ImageButton) view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return dialog;
    }
    @Override
    public void onStart() {
        super.onStart();
        mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    private void hideView(View view) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.height = 0;
        view.setLayoutParams(params);
    }

    private void showView(View view, int size) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.height = size;
        view.setLayoutParams(params);
    }

    private int getActionBarSize() {
        final TypedArray styledAttributes = getContext().getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
        int size = (int) styledAttributes.getDimension(0, 0);
        return size;
    }


    private void loadUpdatedProfile(){

        tv_name.setText(UserCustomer.getName(context));
        tv_name_toolbar.setText(UserCustomer.getName(context));
        tv_email.setText( UserCustomer.getEmail(context));
        tv_mobile.setText(UserCustomer.getPhone(context));
        Tools.displayImageRound2(context, imageView , "https://www.jigijog.com/public/" + UserCustomer.getAvatarOriginal(context));
        tv_province.setText(UserCustomer.getAddress(context));
//        tv_province.setText(Province.getProvDesc(context));
//        tv_city.setText(City.getCityMunDesc(context));
//        tv_barangay.setText(Barangay.getBrgyDesc(context));
        tv_landmark.setText(UserCustomer.getLandmark(context));
    }

}
