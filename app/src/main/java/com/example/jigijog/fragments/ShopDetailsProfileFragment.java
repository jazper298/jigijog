package com.example.jigijog.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jigijog.HttpProvider;
import com.example.jigijog.R;
import com.example.jigijog.models.ShopDetails;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.Tools;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopDetailsProfileFragment extends Fragment {
    private View view;
    private Context context;
    private TextView tv_name, tv_products, tv_time,tv_followers,tv_website, tv_email, tv_phone, tv_location,tv_zipCode, tv_address;
    private CircularImageView image;
    private String userID;
    private ArrayList<ShopDetails> shopDetailsArrayList;
    private ArrayList<ShopDetails.Slider> sliderSellerArrayList;
    public ShopDetailsProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_shop_details_profile, container, false);
        context = getContext();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        userID = getArguments().getString("USERID");
        initializeUI();
        getShopDetails();
    }

    private void initializeUI() {
        tv_name = view.findViewById(R.id.tv_name);
        tv_products = view.findViewById(R.id.tv_products);
        tv_time = view.findViewById(R.id.tv_time);
        tv_followers = view.findViewById(R.id.tv_followers);
        tv_website = view.findViewById(R.id.tv_website);
        tv_email = view.findViewById(R.id.tv_email);
        tv_phone = view.findViewById(R.id.tv_phone);
        tv_location = view.findViewById(R.id.tv_location);
        tv_zipCode = view.findViewById(R.id.tv_zipCode);
        tv_address = view.findViewById(R.id.tv_address);
    }
    private void getShopDetails(){
        shopDetailsArrayList = new ArrayList<>();
        RequestParams params = new RequestParams();
        params.put("user_id", userID);
        Debugger.logD("user_ids "+ userID);
        HttpProvider.post(context, "/mobile/shopDetails", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String provider_id = jsonObject.getString("provider_id");
                            String user_type = jsonObject.getString("user_type");
                            String name = jsonObject.getString("name");
                            String email = jsonObject.getString("email");
                            String referred_by = jsonObject.getString("referred_by");
                            String refer_points = jsonObject.getString("refer_points");
                            String ref_id_status = jsonObject.getString("ref_id_status");
                            String email_verified_at = jsonObject.getString("email_verified_at");
                            String password = jsonObject.getString("password");
                            String remember_token = jsonObject.getString("remember_token");
                            String avatar = jsonObject.getString("avatar");
                            String avatar_original = jsonObject.getString("avatar_original");
                            String address = jsonObject.getString("address");
                            String country = jsonObject.getString("country");
                            String province = jsonObject.getString("province");
                            String city = jsonObject.getString("city");
                            String barangay = jsonObject.getString("barangay");
                            String landmark = jsonObject.getString("landmark");
                            String postal_code = jsonObject.getString("postal_code");
                            String phone = jsonObject.getString("phone");
                            String bank_name = jsonObject.getString("bank_name");
                            String account_name = jsonObject.getString("account_name");
                            String account_number = jsonObject.getString("account_number");
                            String user_id = jsonObject.getString("user_id");
                            String logo = jsonObject.getString("logo");
                            String sliders = jsonObject.getString("sliders");
                            String facebook = jsonObject.getString("facebook");
                            String google = jsonObject.getString("google");
                            String twitter = jsonObject.getString("twitter");
                            String youtube = jsonObject.getString("youtube");
                            String instagram = jsonObject.getString("instagram");
                            String slug = jsonObject.getString("slug");

                            ShopDetails sellerModel = new ShopDetails();

                            sliderSellerArrayList = new ArrayList<>();
                            JSONArray jsonArrays = new JSONArray(Arrays.asList(sliders));

                            for(int z=0; z<jsonArrays.length(); z++) {
                                JSONObject jobject = jsonArray.getJSONObject(z);
                                String result = null;
                                String sliders1 = jobject.getString("sliders");
                                if(sliders1 == null || sliders1.isEmpty() || sliders1.contains("[]")){

                                }else{
                                    result = sliders1.substring(2, sliders1.length() -2).replace("\\","");
                                }


                                ShopDetails.Slider sliders2 = new ShopDetails.Slider();
                                sliders2.setSlider(result);

                                sliderSellerArrayList.add(sliders2);

                            }
                            sellerModel.setId(id);
                            sellerModel.setProvider_id(provider_id);
                            sellerModel.setUser_type(user_type);
                            sellerModel.setName(name);
                            sellerModel.setEmail(email);
                            sellerModel.setReferred_by(referred_by);
                            sellerModel.setRefer_points(refer_points);
                            sellerModel.setRef_id_status(ref_id_status);
                            sellerModel.setEmail_verified_at(email_verified_at);
                            sellerModel.setPassword(password);
                            sellerModel.setRemember_token(remember_token);
                            sellerModel.setAvatar(avatar);
                            sellerModel.setAvatar_original(avatar_original);
                            sellerModel.setAddress(address);
                            sellerModel.setCountry(country);
                            sellerModel.setProvince(province);
                            sellerModel.setCity(city);
                            sellerModel.setBarangay(barangay);
                            sellerModel.setLandmark(landmark);
                            sellerModel.setPostal_code(postal_code);
                            sellerModel.setPhone(phone);
                            sellerModel.setBank_name(bank_name);
                            sellerModel.setAccount_name(account_name);
                            sellerModel.setAccount_number(account_number);
                            sellerModel.setUser_id(user_id);
                            sellerModel.setLogo(logo);
                            sellerModel.setFacebook(facebook);
                            sellerModel.setGoogle(google);
                            sellerModel.setTwitter(twitter);
                            sellerModel.setYoutube(youtube);
                            sellerModel.setInstagram(instagram);
                            sellerModel.setSlug(slug);
                            shopDetailsArrayList.add(sellerModel);

                            tv_name.setText(name);
                            Tools.displayImageRound2(getContext(), (CircleImageView) view.findViewById(R.id.image), "https://www.jigijog.com/public/" + logo);
                            tv_address.setText(address);
                            tv_email.setText(email);
                            tv_phone.setText(phone);
                            if (postal_code.equals(null)){
                                tv_zipCode.setText("6000");
                            }else{
                                tv_zipCode.setText("6000");
                            }
                            tv_location.setText("CEBU CITY");
                            tv_website.setText(email);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("getSellersInfo " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

}
