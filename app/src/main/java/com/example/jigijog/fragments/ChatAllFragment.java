package com.example.jigijog.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jigijog.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatAllFragment extends Fragment {

    private View view;
    private Context context;
    public ChatAllFragment() {
        // Required empty public constructor
    }


    public static ChatAllFragment newInstance() {
        ChatAllFragment fragment = new ChatAllFragment();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_chat_all, container, false);
        context = getContext();
        return view;
    }

}
