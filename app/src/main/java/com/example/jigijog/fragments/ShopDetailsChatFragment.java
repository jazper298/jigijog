package com.example.jigijog.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jigijog.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopDetailsChatFragment extends Fragment {

    private View view;
    private Context context;
    private String userID;
    public ShopDetailsChatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_shop_details_chat, container, false);
        context = getContext();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        userID = getArguments().getString("USERID");
    }
}
