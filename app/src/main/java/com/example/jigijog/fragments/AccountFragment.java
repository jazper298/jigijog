package com.example.jigijog.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jigijog.HttpProvider;
import com.example.jigijog.R;
import com.example.jigijog.activities.LoginActivity;
import com.example.jigijog.activities.MyOrdersActivity;
import com.example.jigijog.activities.RecentlyViewActivity;
import com.example.jigijog.activities.SettingsActivity;
import com.example.jigijog.activities.WishListActivity;
import com.example.jigijog.helpers.CartDbHelper;
import com.example.jigijog.models.RecentlyView;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.models.WishList;
import com.example.jigijog.utils.CheckNetwork;
import com.example.jigijog.utils.Debugger;
import com.example.jigijog.utils.ProgressPopup;
import com.example.jigijog.utils.Tools;
import com.example.jigijog.utils.UserRole;
import com.google.android.material.snackbar.Snackbar;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {

    private View view;
    private Context context;

    private ImageView iv_accountSetting,iv_accountReturns,iv_accountCancel,iv_accountImage;
    private ImageView iv_accountTopay, iv_accountToship, iv_accountToreceive, iv_accountToreview;
    private TextView tv_accountTopay, tv_accountToship, tv_accountToreceive, tv_accountToreview, iv_accountWishlist,iv_accountRecentlyViewed;
    private TextView tv_accountName,tv_myorders;
    private CardView cardView1, cardView2, cardView3, cardView4, cardView5, cardView6, cardView7, cardView9, cardView10, cardView11, cardView12, cardView13;
    CircleImageView circleImageView;
    private UserCustomer userCustomer = new UserCustomer();

    private String role;

    private ProgressBar progress_bar;
    private LinearLayout lyt_no_connection;
    private AppCompatButton bt_retry;

    private SwipeRefreshLayout swipeRefreshLayout;

    private ArrayList<WishList> wishListArrayList = new ArrayList<>();
    private ArrayList<RecentlyView> recentlyViewArrayList = new ArrayList<>();
    private int wishListCount = 0;
    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(CheckNetwork.isInternetAvailable(Objects.requireNonNull(getActivity()))){
            view = inflater.inflate(R.layout.fragment_account, container, false);
            context = getContext();
            role = UserRole.getRole(context);
            Tools.setSystemBarColor(getActivity(), R.color.deep_orange_400);
            Tools.setSystemBarLight(getActivity());
            initializeUI();
            checkUserRole();
            loadUpdatedProfile();
        }else{
            view = inflater.inflate(R.layout.indicator_no_internet, container, false);
            context = getContext();
            initComponent();
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        String userID = UserCustomer.getID(context);
        String userTokens = UserCustomer.getRememberToken(context);
        Debugger.logD("AccountFragment userID " + userID);
        Debugger.logD("AccountFragment userTokens " + userTokens);

    }

    private void initializeUI () {
        iv_accountWishlist = view.findViewById(R.id.iv_accountWishlist);
        iv_accountRecentlyViewed = view.findViewById(R.id.iv_accountRecentlyViewed);
        iv_accountSetting = view.findViewById(R.id.iv_accountSetting);
        tv_accountName = view.findViewById(R.id.tv_accountName);
        tv_myorders = view.findViewById(R.id.tv_myorders);
        circleImageView = view.findViewById(R.id.iv_accountImage);
        swipeRefreshLayout = view.findViewById(R.id.swipe_Home);
        swipeRefreshLayout.setRefreshing(true);
        cardView1 = view.findViewById(R.id.cardView1);//wishlist
        cardView2 = view.findViewById(R.id.cardView2);//followed seller
        cardView3 = view.findViewById(R.id.cardView3);//recently viewed
        cardView4 = view.findViewById(R.id.cardView4);//to pay
        cardView5 = view.findViewById(R.id.cardView5);//to ship
        cardView6 = view.findViewById(R.id.cardView6);//to receive
        cardView7 = view.findViewById(R.id.cardView7);//to review
        cardView9 = view.findViewById(R.id.cardView9);//my review
        cardView10 = view.findViewById(R.id.cardView10);//payment option
        cardView11 = view.findViewById(R.id.cardView11);//help
        cardView12 = view.findViewById(R.id.cardView12);//chat with our customer
        cardView13 = view.findViewById(R.id.cardView13);//sell on jigijog
        //wishlist
        cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WishListActivity.class);
                startActivity(intent);
            }
        });
        //followed seller
        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        //recently viewed
        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RecentlyViewActivity.class);
                startActivity(intent);
            }
        });
        //to pay
        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyOrdersActivity.class);
                startActivity(intent);
            }
        });
        //to ship
        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyOrdersActivity.class);
                startActivity(intent);
            }
        });
        //to receive
        cardView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyOrdersActivity.class);
                startActivity(intent);
            }
        });
        //to review
        cardView7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyOrdersActivity.class);
                startActivity(intent);
            }
        });
        //my review
        cardView9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        //payment option
        cardView10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        //help
        cardView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        //chat with our customer
        cardView12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        //sell on jigijog
        cardView13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        tv_myorders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MyOrdersActivity.class);
                startActivity(intent);
            }
        });
        iv_accountSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view = v.findViewById(android.R.id.content);
                if (role.equals(UserRole.Customer())){
                    Intent intent = new Intent(context, SettingsActivity.class);
                    startActivity(intent);
                }else{
                    Snackbar snackbar = Snackbar.make(v, "Please Login to view profile", Snackbar.LENGTH_LONG)
                            .setAction("Login", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(context,LoginActivity.class);
                                    startActivity(intent);
                                }
                            });
                    snackbar.show();
                }
            }
        });
        tv_accountName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
            }
        });
        iv_accountTopay = view.findViewById(R.id.iv_accountTopay);
        iv_accountToship = view.findViewById(R.id.iv_accountToship);
        iv_accountToreceive = view.findViewById(R.id.iv_accountToreceive);
        iv_accountToreview = view.findViewById(R.id.iv_accountToreview);
        tv_accountTopay = view.findViewById(R.id.tv_accountTopay);
        tv_accountToship = view.findViewById(R.id.tv_accountToship);
        tv_accountToreceive = view.findViewById(R.id.tv_accountToreceive);
        tv_accountToreview = view.findViewById(R.id.tv_accountToreview);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                tv_accountName.setText(UserCustomer.getName(context));
                loadUpdatedProfile();
            }
        });

    }

    private void checkUserRole(){
        if (role.equals(UserRole.Customer())) {
            checkCustomerSession();
            getWishListItem();
            getRecentlyView();
        } else {
            //checkStudentSession();
        }
    }

    public void checkCustomerSession() {
        String userID = UserCustomer.getID(context);
        Debugger.logD("ACCOUNT userID " + userID);

        if(!userID.equals("88")){
            loginUser();
            Tools.displayImageRound2(getActivity(), (CircleImageView) view.findViewById(R.id.iv_accountImage), "https://www.jigijog.com/public/" + UserCustomer.getAvatarOriginal(context));
            tv_accountName.setText(UserCustomer.getName(context));
            tv_accountName.setEnabled(false);
            Debugger.logD("fuck " + UserCustomer.getName(context) + UserCustomer.getEmail(context) +  " " + UserCustomer.getAvatarOriginal(context));
        }else{
            UserRole userType = new UserRole();
            userType.setUserRole(UserRole.Customer());
            userType.saveRole(context);

        }


    }

    private void loginUser(){

        RequestParams params = new RequestParams();
        params.put("username", UserCustomer.getEmail(context));
        params.put("password", UserCustomer.getPassword(context));

        HttpProvider.post(context, "/mobile/loginUser", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String str = new String(responseBody, StandardCharsets.UTF_8);
                Debugger.logD("str " + str);
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String id = jsonObject.getString("id");
                    String remember_token = jsonObject.getString("remember_token");

                    UserCustomer userCustomer = new UserCustomer();
                    userCustomer.setId(id);
                    userCustomer.setRemember_token(remember_token);

                    getUserDetails(userCustomer);

                } catch (Exception e) {
                    e.printStackTrace();
                    Debugger.logD(e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ProgressPopup.hideProgress();
                Toasty.error(context, "No internet connect. Please try again.").show();
            }
        });
    }

    private void getUserDetails(final UserCustomer userCustomer){

        ProgressPopup.showProgress(context);

        RequestParams params = new RequestParams();
        params.put("id", userCustomer.getId());
        params.put("remember_token", userCustomer.getRemember_token());
        Debugger.logD("id " + userCustomer.getId());
        Debugger.logD("remember_token " + userCustomer.getRemember_token());

        HttpProvider.post(context, "/mobile/getUserDetails", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ProgressPopup.hideProgress();
                try {
                    String str = new String(responseBody, StandardCharsets.UTF_8);
                    JSONArray jsonArray = new JSONArray(str);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    Debugger.logD("FUCK " + str);

                    String id = jsonObject.getString("id");
                    String provider_id = jsonObject.getString("provider_id");
                    String user_type = jsonObject.getString("user_type");
                    String name = jsonObject.getString("name");
                    String email = jsonObject.getString("email");
                    String referred_by = jsonObject.getString("referred_by");
                    String refer_points = jsonObject.getString("refer_points");
                    String ref_id_status = jsonObject.getString("ref_id_status");
                    String email_verified_at = jsonObject.getString("email_verified_at");
                    String password = jsonObject.getString("password");
                    String remember_token = jsonObject.getString("remember_token");
                    String avatar = jsonObject.getString("avatar");
                    String avatar_original = jsonObject.getString("avatar_original");
                    String address = jsonObject.getString("address");
                    String country = jsonObject.getString("country");
                    String city = jsonObject.getString("city");
                    String barangay = jsonObject.getString("barangay");
                    String landmark = jsonObject.getString("landmark");
                    String postal_code = jsonObject.getString("postal_code");
                    String phone = jsonObject.getString("phone");
                    String bank_name = jsonObject.getString("bank_name");
                    String account_name = jsonObject.getString("account_name");
                    String account_number = jsonObject.getString("account_number");
                    String balance = jsonObject.getString("balance");
                    String created_at = jsonObject.getString("created_at");
                    String updated_at = jsonObject.getString("updated_at");

                    userCustomer.setId(id);
                    userCustomer.setProvider_id(provider_id);
                    userCustomer.setUser_type(user_type);
                    userCustomer.setName(name);
                    userCustomer.setEmail(email);
                    userCustomer.setReferred_by(referred_by);
                    userCustomer.setReferred_points(refer_points);
                    userCustomer.setRef_id_status(ref_id_status);
                    userCustomer.setEmail_verified_at(email_verified_at);
                    userCustomer.setPassword(UserCustomer.getPassword(context));
                    userCustomer.setRemember_token(remember_token);
                    userCustomer.setAvatar(avatar);
                    userCustomer.setAvatar_original(avatar_original);
                    userCustomer.setAddress(address);
                    userCustomer.setCountry(country);
                    userCustomer.setCity(city);
                    userCustomer.setBarangay(barangay);
                    userCustomer.setLandmark(landmark);
                    userCustomer.setRemember_token(remember_token);
                    userCustomer.setPostal_code(postal_code);
                    userCustomer.setPhone(phone);
                    userCustomer.setBank_name(bank_name);
                    userCustomer.setAccount_name(account_name);
                    userCustomer.setAccount_number(account_number);
                    userCustomer.setRemember_token(remember_token);
                    userCustomer.setBalance(balance);
                    userCustomer.setCreated_at(created_at);
                    userCustomer.setUpdated_at(updated_at);

                    UserRole userRole = new UserRole();
                    userRole.setUserRole(UserRole.Customer());
                    userRole.saveRole(context);

                    userCustomer.saveUserSession(context);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ProgressPopup.hideProgress();
                Toasty.error(context, "No internet connect. Please try again.").show();
            }
        });
    }

    private void initComponent() {
        progress_bar = (ProgressBar) view.findViewById(R.id.progress_bar);
        lyt_no_connection = (LinearLayout) view.findViewById(R.id.lyt_no_connection);
        bt_retry = (AppCompatButton) view.findViewById(R.id.bt_retry);

        progress_bar.setVisibility(View.GONE);
        lyt_no_connection.setVisibility(View.VISIBLE);

        bt_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                progress_bar.setVisibility(View.VISIBLE);
                lyt_no_connection.setVisibility(View.GONE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(CheckNetwork.isInternetAvailable(Objects.requireNonNull(getActivity()))){

                        }else{
                            progress_bar.setVisibility(View.GONE);
                            lyt_no_connection.setVisibility(View.VISIBLE);
                        }

                    }
                }, 1000);
            }
        });
    }

    private void loadUpdatedProfile(){
        swipeRefreshLayout.setRefreshing(true);


        swipeRefreshLayout.setRefreshing(false);
        if(!UserCustomer.getName(context ).isEmpty()){
            tv_accountName.setText(UserCustomer.getName(context));
            Tools.displayImageRound2(context, circleImageView , "https://www.jigijog.com/public/" + UserCustomer.getAvatarOriginal(context));
        }else{
            tv_accountName.setText("Login or Register");
        }

    }

    private void getWishListItem(){
        CartDbHelper dbHelper = CartDbHelper.getInstance(context);
        wishListArrayList = dbHelper.getAllWishList(Integer.parseInt(UserCustomer.getID(context)));
        iv_accountWishlist.setText(String.valueOf( wishListArrayList.size()));
    }
    private void getRecentlyView(){
        CartDbHelper dbHelper = CartDbHelper.getInstance(context);
        recentlyViewArrayList = dbHelper.getAllRecentlyView(Integer.parseInt(UserCustomer.getID(context)));
        iv_accountRecentlyViewed.setText(String.valueOf( recentlyViewArrayList.size()));
    }


}
