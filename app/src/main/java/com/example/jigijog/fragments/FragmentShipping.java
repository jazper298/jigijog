package com.example.jigijog.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.jigijog.R;
import com.example.jigijog.models.UserCustomer;
import com.example.jigijog.utils.Debugger;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentShipping extends Fragment {

    private View view;
    private Context context;
    public FragmentShipping() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_fragment_shipping, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        context = getContext();
        initializeUI();
    }

    private void initializeUI(){
        ((EditText) view.findViewById(R.id.et_name)).setText(UserCustomer.getName(context));
        ((EditText) view.findViewById(R.id.et_email)).setText(UserCustomer.getEmail(context));
        ((EditText) view.findViewById(R.id.et_phone)).setText(UserCustomer.getPhone(context));
        ((EditText) view.findViewById(R.id.et_landmark)).setText(UserCustomer.getLandmark(context));
        ((EditText) view.findViewById(R.id.et_address)).setText(UserCustomer.getAddress(context));
    }
}
